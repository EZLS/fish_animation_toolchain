#include "fish/bone_modifier/dorsal_fin_modifier.h"

#include "ros/joy_indices.h"

using namespace irr;
using namespace scene;

// Maximum rotation values for dorsal fin up and down
#define DORSAL_FIN_DOWN 25.0f
#define DORSAL_FIN_UP 78.0f

namespace fishsim
{


DorsalFinModifier::DorsalFinModifier(IAnimatedMeshSceneNode* fishNode,
                           std::string name,
                           float speed,
                           bool isActive)
    : BoneModifier(fishNode, name, speed, isActive)
{
    // Default: move all bones in the same way
    m_connected = true;
    m_dorsalFin1Inverted = false;
    m_dorsalFin2Inverted = false;
}

DorsalFinModifier::~DorsalFinModifier(){

    if(m_dorsalFin1)
        m_dorsalFin1->drop();

    if(m_dorsalFin2)
        m_dorsalFin2->drop();

    if(m_dorsalFin3)
        m_dorsalFin3->drop();
}

bool DorsalFinModifier::init(){

    if(!m_fishNode)
        return false;

    bool allOk = true;
    allOk = allOk && getAndGrabBone(m_dorsalFin1,"dorsal_fin1");
    allOk = allOk && getAndGrabBone(m_dorsalFin2,"dorsal_fin2");
    allOk = allOk && getAndGrabBone(m_dorsalFin3,"dorsal_fin3");

    if(allOk)
    {
        return true;
    }
    else
    {
        std::cerr << "Was not able to init dorsal fin modifier." << std::endl;
        return false;
    }
}

/// Extract value of rear left 1 button and pass it to update function
void DorsalFinModifier::update(const sensor_msgs::Joy& joystickState){

    if(!m_isActive)
        return;

    float val = joystickState.buttons.at(PS3_AXIS_BUTTON_REAR_LEFT_1);
    float val2 = joystickState.buttons.at(PS3_AXIS_BUTTON_REAR_LEFT_2);

    update(val, val2);
}

/// If button is pressed, rise dorsal fin
void DorsalFinModifier::update(float inputVal1, float inputVal2, float inputVal3){

    if(!m_isActive)
        return;

    int dorsalFin1Dir=-1;
    int dorsalFin2Dir=-1;

    // All bones should rotate in the same way
    if(m_connected)
    {
        if(inputVal1 > 0)
        {
            dorsalFin1Dir = 1;
            dorsalFin2Dir = 1;
        }
    }
    // Dorsal fin 1 can move independently
    else
    {
        if(inputVal1 > 0)
            dorsalFin1Dir = 1;
        if(inputVal2 > 0)
            dorsalFin2Dir = 1;
    }

    if(m_dorsalFin1Inverted)
        dorsalFin1Dir *= -1;

    if(m_dorsalFin2Inverted)
        dorsalFin2Dir *= -1;


    changeDorsalFin(m_dorsalFin1, dorsalFin1Dir);
    changeDorsalFin(m_dorsalFin2, dorsalFin2Dir);
    changeDorsalFin(m_dorsalFin3, dorsalFin2Dir);
}



void DorsalFinModifier::changeDorsalFin(irr::scene::IBoneSceneNode *bone, int dir){

    core::vector3df dorsalFinRot = bone->getRotation();

    float currRot = dorsalFinRot.X;
    currRot += m_speed * dir;

    /* Workaround until old fish models have been fixed:
     * The poecilias have different default rotation for
     * each dorsal fin. Need to fix this in blender. For now,
     * assume that onyl the poecilia is using the connected
     * mode
     */
    if(m_connected && bone == m_dorsalFin1)
    {
        currRot = currRot - 62.602;
    }
    /*
     * Workaround end.
     */


    // Check bounds
    if(currRot > DORSAL_FIN_DOWN && currRot < DORSAL_FIN_UP)
    {
        addRotationToBone(bone, m_speed*dir, 0.0, 0.0);
    }
}

void DorsalFinModifier::modify(FishSnapshot* startState,
                               FishSnapshot* endState,
                               const sensor_msgs::Joy& joystickState ){


    float inputVal1 = joystickState.buttons.at(PS3_AXIS_BUTTON_REAR_LEFT_1);
    float inputVal2 = joystickState.buttons.at(PS3_AXIS_BUTTON_REAR_LEFT_2);

    int dorsalFin1Dir=-1;
    int dorsalFin2Dir=-1;

    // All bones should rotate in the same way
    if(m_connected)
    {
        if(inputVal1 > 0)
        {
            dorsalFin1Dir = 1;
            dorsalFin2Dir = 1;
        }
    }
    // Dorsal fin 1 can move independently
    else
    {
        if(inputVal1 > 0)
            dorsalFin1Dir = 1;
        if(inputVal2 > 0)
            dorsalFin2Dir = 1;
    }

    if(m_dorsalFin1Inverted)
        dorsalFin1Dir *= -1;
    if(m_dorsalFin2Inverted)
        dorsalFin2Dir *= -1;


    core::vector3df dorsalFin1Rot = startState->getSingleBoneRot(m_dorsalFin1->getName());
    core::vector3df dorsalFin2Rot = startState->getSingleBoneRot(m_dorsalFin2->getName());
    core::vector3df dorsalFin3Rot = startState->getSingleBoneRot(m_dorsalFin3->getName());

    float currRot1 = dorsalFin1Rot.X;
    currRot1 += m_speed * dorsalFin1Dir;

    /* Workaround until old fish models have been fixed:
     * The poecilias have different default rotation for
     * each dorsal fin. Need to fix this in blender. For now,
     * assume that onyl the poecilia is using the connected
     * mode
     */
    if(m_connected)
    {
        currRot1 = currRot1 - 62.602;
    }
    /*
     * Workaround end.
     */
    float currRot2 = dorsalFin2Rot.X;
    currRot2 += m_speed * dorsalFin2Dir;

    // Check bounds
    if(currRot1 > DORSAL_FIN_DOWN && currRot1 < DORSAL_FIN_UP)
    {
        core::vector3df vecToAdd = core::vector3df(m_speed * dorsalFin1Dir, 0.0, 0.0);
        dorsalFin1Rot += vecToAdd;
    }

    if(currRot2 > DORSAL_FIN_DOWN && currRot2 < DORSAL_FIN_UP)
    {
        core::vector3df vecToAdd = core::vector3df(m_speed * dorsalFin2Dir, 0.0, 0.0);
        dorsalFin2Rot += vecToAdd;
        dorsalFin3Rot += vecToAdd;
    }
    endState->setSingleBoneRot(m_dorsalFin1->getName(), dorsalFin1Rot);
    endState->setSingleBoneRot(m_dorsalFin2->getName(), dorsalFin2Rot);
    endState->setSingleBoneRot(m_dorsalFin3->getName(), dorsalFin3Rot);

}
} // end namespace fishsim

