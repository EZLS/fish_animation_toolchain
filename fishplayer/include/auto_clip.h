/*! Copyright (C) 2017  	Department of Electrical Engineering & Computer Science, Institute of
 *			Real-Time Learning Systems, University of Siegen
 *
 *			Research Group of Ecology and Behavioral Biology, Institute of Biology,
 *			University of Siegen
 *
 * Contact: virtual.fish.project@gmail.com
 *
 * License: GNU General Public License  See LICENSE.txt for the full license.
 *
 * This file is part of the "FishSim Animation Toolchain".
 * "FishSim Animation Toolchain" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "FishSim Animation Toolchain" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "FishSim Animation Toolchain".  If not, see <http: *www.gnu.org/licenses/>.
 *
 * The "FishSim Animation Toolchain" was developed within the scope of the interdisciplinary,
 * DFG (Deutsche Forschungsgemeinschaft)-funded project “virtual fish” (KU
 * 689/11-1 and Wi 1531/12-1) of the Institute of Real-Time Learning Systems
 * (EZLS) and the Research Group of Ecology and Behavioral Biology at the
 * University of Siegen.
*/
#ifndef H_AUTO_CLIP_H
#define H_AUTO_CLIP_H


#include "include/clip.h"

#include <QPainter>
#include <QPaintEvent>
#include <QSize>
#include <QLineEdit>
#include <QString>

namespace fishplayer
{
class AutoClip : public Clip
{
    Q_OBJECT

public:
    
    AutoClip(QWidget *parent, float playTime);

    AutoClip(QWidget *parent, const YAML::Node &node);

    ~AutoClip();

    //
    // Clip overrides
    //

    void play(RosInterface* roshandler);

    bool verify();

    float getDuration();

    void save(YAML::Emitter& emitter);

    bool load( const YAML::Node& node);

    //
    // QT overrides
    //

//    void paintEvent(QPaintEvent *event);

    QSize sizeHint();

//    void paintEvent(QPaintEvent *event);

public slots:
    void mimicChanged(bool enabled);
    void dorsalFinChanged(bool enabled);
    void gonopodiumChanged(bool enabled);
    void playtimeChanged(QString playtimeString);

private:

    float m_playTime;
    bool m_mimicEnabled;
    bool m_dorsalFinEnabled;
    bool m_gonopodiumEnabled;

    void initUI();

};
}
#endif
