#include <qt5/QtWidgets/QApplication>
#include "mainwindow.h"


int main(int argc, char *argv[])
{
    ros::init( argc, argv, "fishplayer" );

    QApplication a(argc, argv);
    fishplayer::MainWindow w;

    w.show();

    
    return a.exec();
}
