#include "include/pause_clip.h"

#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QLabel>
#include <QLineEdit>

#include "include/utils.h"

namespace fishplayer
{
PauseClip::PauseClip(QWidget *parent, const YAML::Node& node) : Clip(parent, E_CLIP_TYPE::PAUSE)
{
    load(node);
    verify();
    initUI();
}

PauseClip::PauseClip(QWidget *parent, float pauseTime) : Clip(parent, E_CLIP_TYPE::PAUSE)
{
    if(m_pauseTime < 0.0)
        m_pauseTime = 0.0;

    m_pauseTime = pauseTime;
    verify();
    initUI();
}


PauseClip::~PauseClip()
{

}


void PauseClip::initUI()
{
    QHBoxLayout *mainLayout = new QHBoxLayout;

    // Type
    QLabel *labelType = new QLabel("PAUSE");
    labelType->setStyleSheet("font-weight: bold;");
    labelType->setMaximumWidth(LABEL_TYPE_WIDTH);
    mainLayout->addWidget(labelType);

    // Options
    QLabel *labelPlaceholder = new QLabel(" ");
    mainLayout->addWidget(labelPlaceholder);

    // Time
    QLineEdit *timeEdit = new QLineEdit(generateTimeString(m_pauseTime));
    timeEdit->setInputMask("00:00");
    timeEdit->setAlignment(Qt::AlignHCenter);
    timeEdit->setMaximumWidth(LABEL_TIME_WIDTH);
    mainLayout->addWidget(timeEdit);

    setLayout(mainLayout);


    connect(timeEdit, SIGNAL(textChanged(QString)), this, SLOT(pausetimeChanged(QString)));
}

bool PauseClip::verify()
{
    // Pause is always valid
    m_isValid = true;
    return m_isValid;
}

float PauseClip::getDuration()
{
    return m_pauseTime;
}

void PauseClip::play(RosInterface* roshandler)
{
    m_isPlaying = true;

    ros::Time startTime = ros::Time::now();
    ros::Duration playDur = ros::Duration(m_pauseTime);
    ros::Duration sleepDur = ros::Duration(0.5);

    while( (( ros::Time::now() - startTime) <= playDur) && m_isPlaying)
    {
        sleepDur.sleep();
    }
}


void PauseClip::pausetimeChanged(QString pauseString)
{
    float pausetime = getTimeFromTimeString(pauseString);
    m_pauseTime = pausetime;

    // Tell playlist UI to update
    emit(clipHasChanged());
}

void PauseClip::save(YAML::Emitter& emitter)
{
    emitter << YAML::BeginMap;

    emitter << YAML::Key << "type";
    emitter << YAML::Value << m_type;
    emitter << YAML::Key << "pauseTime";
    emitter << YAML::Value << m_pauseTime;

    emitter << YAML::EndMap;
}

bool PauseClip::load(const YAML::Node& node)
{
    if(! node.IsMap())
        return false;

    // Make sure all data is there
    YAML::Node pauseTime = node["pauseTime"];
    if(!pauseTime)
        return false;

    // Fill object
    m_pauseTime = pauseTime.as<float>();
    if(m_pauseTime < 0.0)
        m_pauseTime = 0.0;

    return true;
}
}
