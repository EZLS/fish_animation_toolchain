#include "sim/irrhandler.h"

#include <X11/Xlib.h>
#include "X11/Xutil.h"

#include "sim/fishsim_globals.h"
#include "fish/fish.h"

#include "utils/fishsim_logger.h"

using namespace irr;
using namespace core;
using namespace scene;
using namespace video;
using namespace io;
using namespace gui;

namespace fishsim
{

IrrHandler::IrrHandler()
{
    m_device = 0;
    m_driver = 0;
    m_smgr = 0;
    m_guienv = 0;
    m_eventReceiver = 0;
    m_gui = 0;
    m_fishSimSmgr = 0;
    m_shadowEffects = 0;
    m_fishSimPath = "";

}

IrrHandler::~IrrHandler()
{
    if(m_gui)
        delete m_gui;

    if(m_fishSimSmgr)
        delete m_fishSimSmgr;

    if(m_shadowEffects)
        delete m_shadowEffects;

    if(m_eventReceiver)
        delete m_eventReceiver;

    if(m_device)
        m_device->drop();

}

bool IrrHandler::init(uint width, uint height, bool fullscreen, irr::IrrlichtDevice* device)
{

    logInfo("Irrhandler is about to be initialized");
    if(device == NULL)
    {
        m_runningInLib = false;
        m_device = createDevice( video::EDT_OPENGL, core::dimension2d<u32>(width, height),
                                 32,fullscreen,true,true);

        if(!m_device)
        {
            logFatal("Error creating irrlicht device. Will exit");
            return false;
        }

        m_device->setResizable(true);
    }
    else
    {
        m_device = device;
    }

    logInfo("Create device success.");

    m_driver = m_device->getVideoDriver();
    m_smgr = m_device->getSceneManager();
    m_guienv = m_device->getGUIEnvironment();

    m_driver->setTextureCreationFlag(video::ETCF_ALWAYS_32_BIT, true);
    m_device->setWindowCaption(L"FishSim");

    m_fishSimPath = ros::package::getPath("fishsim");
    logInfo("FishSim working directory: " + m_fishSimPath);
    m_device->getFileSystem()->changeWorkingDirectoryTo(m_fishSimPath.c_str());

    // Create shadow manager
    irr::core::dimension2du screenBufferRes = irr::core::dimension2du(width, height );
    m_shadowEffects =  new EffectHandler(m_device, screenBufferRes, false,false,true );
    logInfo("Create shadow manager success");

    // Create fishsim scene manager
    m_fishSimSmgr = new FishSimSceneManager(m_smgr, m_fishSimPath, m_shadowEffects);
    logInfo("Create fishsim scene manager success");

    // Create fishsim gui
    m_gui = new FishSimGUI(m_device, m_fishSimSmgr, m_fishSimPath);
    logInfo("Create fishsim GUI success");

    // Create custom event receiver
    m_eventReceiver = new FishSimEventReceiver(m_device, m_fishSimSmgr, m_gui);
    m_device->setEventReceiver(m_eventReceiver);

    // Set window class hint
    setWindowClass();

    logInfo("All ok. Now load default scene.");

    // Load the default scene
    return load();

}


bool IrrHandler::load()
{
    std::string defaultScenePath = m_fishSimPath + DEFAULT_SCENE_PATH;
    bool sceneLoaded = m_fishSimSmgr->loadScene(defaultScenePath, false);

    if(sceneLoaded)
    {
        m_fishSimSmgr->enableMayaCamera(false);
        m_fishSimSmgr->saveTempScene();
        logInfo("Default scene loaded.");
    }
    else
    {
        logError("Default scene could not be loaded.");
    }

    return sceneLoaded;
}

void IrrHandler::moveWindow(int x, int y)
{

    const SExposedVideoData& data = m_driver->getExposedVideoData();

    // Just assume this is on OpenGL and Linux, everything else does not make sense anyway.
    Window win = (Window)data.OpenGLLinux.X11Window;
    Display* disp = (Display*)data.OpenGLLinux.X11Display;

    XMoveWindow(disp, win, x, y);
}

void IrrHandler::resizeWindow(int w, int h)
{

    const SExposedVideoData& data = m_driver->getExposedVideoData();

    // Just assume this is on OpenGL and Linux, everything else does not make sense anyway.
    Window win = (Window)data.OpenGLLinux.X11Window;
    Display* disp = (Display*)data.OpenGLLinux.X11Display;

    XResizeWindow(disp, win, w, h);
}

void IrrHandler::setWindowClass()
{

    const SExposedVideoData& data = m_driver->getExposedVideoData();

    // Just assume this is on OpenGL and Linux, everything else does not make sense anyway.
    Window win = (Window)data.OpenGLLinux.X11Window;
    Display* disp = (Display*)data.OpenGLLinux.X11Display;

    XClassHint cHint;
    cHint.res_name = "FishSim";
    cHint.res_class = "FishSim";

    XSetClassHint(disp,win, &cHint);
}

void IrrHandler::setTitle(const std::string& newTitle)
{
    std::wstringstream ws;
    ws << newTitle.c_str();

    m_device->setWindowCaption(ws.str().c_str());
}

bool IrrHandler::run()
{
    return m_device->run();
}

void IrrHandler::draw3DAxes(){

    video::SMaterial axesMat;
    axesMat.Lighting = false;
    axesMat.Thickness = 5.0;

    axesMat.DiffuseColor = SColor(1,1,0,0);
    m_driver->setMaterial(axesMat);

    const float lineLength = 4.0;
    m_driver->setTransform(video::ETS_WORLD, core::IdentityMatrix);

    m_driver->draw3DLine(vector3df(0.0, 0.0, 0.0), vector3df(lineLength, 0.0, 0.0),SColor(255,255,0,0));
    m_driver->draw3DLine(vector3df(0.0, 0.0, 0.0), vector3df(0.0, lineLength, 0.0),SColor(255,0,255,0));
    m_driver->draw3DLine(vector3df(0.0, 0.0, 0.0), vector3df(0.0, 0.0, lineLength),SColor(255,0,0,255));

    /// VISUALIZE REAL FISH

//    aabbox3df box;
//    box.MinEdge = m_realFishPos - vector3df(-0.5, -0.5, -0.5);
//    box.MaxEdge = m_realFishPos - vector3df(0.5, 0.5, 0.5);

//    if(m_realFishPos.Z < 30.5)
//        m_driver->draw3DBox(box, SColor(255,255,0,0));
//    else
//        m_driver->draw3DBox(box, SColor(255,0,0,0));


}


void IrrHandler::exec()
{

    m_driver->beginScene(true, true, DEFAULT_BACKGROUND_COLOR);
    //std::wstring fps_text = L"FPS: ";
    //fps_text.append(boost::lexical_cast<std::wstring>(m_driver->getFPS()));
    //m_guienv->getBuiltInFont()->draw(fps_text.c_str(), rect<s32>(5, 5, 300, 50), SColor(255, 255, 255, 255));

    m_fishSimSmgr->drawAll();

    if(m_fishSimSmgr->isInEditMode())
        draw3DAxes();

    m_gui->draw();
    m_driver->endScene();

}

void IrrHandler::drop()
{
    // Close device and run it one last time
    // to process the quit event.
    m_device->closeDevice();
    m_device->run();

}

void IrrHandler::addSnapshot(FishSnapshot& snapshot, const std::string& modelName){

    // Add the new snapshot if model exist in current scene
    Fish* fish = m_fishSimSmgr->getFishByName(modelName);

    if(fish != nullptr){
        fish->addSnapshot(snapshot);

    }
}

bool IrrHandler::loadScene(const std::string& scenePath, bool hideFishModels)
{
    bool sceneLoaded =  m_fishSimSmgr->loadScene(scenePath, hideFishModels);

    if(sceneLoaded){
        m_fishSimSmgr->enableMayaCamera(false);
    }

    return sceneLoaded;
}

void IrrHandler::setRealFishPosition(irr::core::vector3df fishPos){

    m_realFishPos = fishPos;
}


std::vector<std::string> IrrHandler::getFishModels()
{
    return m_fishSimSmgr->getCurrentFishModels();
}

//std::vector<std::string> IrrHandler::getModelBones(const std::string& modelName)
//{
//    return m_fishSimSmgr->getFishByName(modelName)->getBones();
//}

//bool IrrHandler::loadFishModel(const std::string& path)
//{
//    return m_fishSimSmgr->loadXFishModel(path);
//}

//bool IrrHandler::unloadFishModel(const std::string& name)
//{
//    return m_fishSimSmgr->removeFishModel(name);
//}

} // end namespace fishsim
