#!/bin/bash
#
# Updates fishsim animation toolchain
#
GIT_HOME=https://bitbucket.org/EZLS/fish_animation_toolchain

# Locate fishsim by checking env variables
if [[ -z "${FISHSIM_HOME}" ]]; then
  echo "Unable to locate the fishsim animation toolchain. FISHSIM_HOME is not set."
  exit -1 
fi

if [[ -z "${FISHSIM_CATKIN_WS}" ]]; then
  echo "Unable to locate the fishsim animation toolchain. FISHSIM_CATKIN_WS is not set."
  exit -1 
fi

source ${FISHSIM_CATKIN_WS}/devel/setup.bash

cd ${FISHSIM_CATKIN_WS}/src/fish_animation_toolchain
git pull
cd ${FISHSIM_CATKIN_WS}
catkin_make
catkin_make install
