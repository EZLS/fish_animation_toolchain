#include "utils/joystick_mapping.h"
#include "ros/joy_indices.h"

JoystickMapping::JoystickMapping()
{
    initFromROSParams();
}


void JoystickMapping::initFromROSParams()
{
    // Default values are for the sony playstation 3 controller.

    ros::NodeHandle nh;

    nh.param<int>("/fishsim/joystick_mapping/AXIS_STICK_LEFT_LEFTWARDS",
                AXIS_STICK_LEFT_LEFTWARDS_IDX, PS3_AXIS_STICK_LEFT_LEFTWARDS);

    nh.param<int>("/fishsim/joystick_mapping/AXIS_STICK_LEFT_UPWARDS",
                AXIS_STICK_LEFT_UPWARDS_IDX, PS3_AXIS_STICK_LEFT_UPWARDS);

    nh.param<int>("/fishsim/joystick_mapping/AXIS_STICK_RIGHT_LEFTWARDS",
                AXIS_STICK_RIGHT_LEFTWARDS_IDX, PS3_AXIS_STICK_RIGHT_LEFTWARDS);

    nh.param<int>("/fishsim/joystick_mapping/AXIS_STICK_RIGHT_UPWARDS",
                AXIS_STICK_RIGHT_UPWARDS_IDX, PS3_AXIS_STICK_RIGHT_UPWARDS);

    nh.param<int>("/fishsim/joystick_mapping/BUTTON_L1",
                BUTTON_L1_IDX, PS3_BUTTON_REAR_LEFT_1);

    nh.param<int>("/fishsim/joystick_mapping/BUTTON_L2",
                BUTTON_L2_IDX, PS3_BUTTON_REAR_LEFT_2);

    nh.param<int>("/fishsim/joystick_mapping/BUTTON_R2",
                BUTTON_R1_IDX, PS3_BUTTON_REAR_RIGHT_1);

    nh.param<int>("/fishsim/joystick_mapping/BUTTON_R1",
                BUTTON_R2_IDX, PS3_BUTTON_REAR_RIGHT_2);

    nh.param<int>("/fishsim/joystick_mapping/BUTTON_CROSS_UP",
                BUTTON_CROSS_UP_IDX, PS3_BUTTON_CROSS_UP);

    nh.param<int>("/fishsim/joystick_mapping/BUTTON_CROSS_RIGHT",
                BUTTON_CROSS_RIGHT_IDX, PS3_BUTTON_CROSS_RIGHT);

    nh.param<int>("/fishsim/joystick_mapping/BUTTON_CROSS_DOWN",
                BUTTON_CROSS_DOWN_IDX, PS3_BUTTON_CROSS_DOWN);

    nh.param<int>("/fishsim/joystick_mapping/BUTTON_CROSS_LEFT",
                BUTTON_CROSS_LEFT_IDX, PS3_BUTTON_CROSS_LEFT);

}


sensor_msgs::Joy JoystickMapping::remap(sensor_msgs::Joy source)
{
    sensor_msgs::Joy result;

    std::vector<float> axes(PS3_NUM_AXES);
    std::vector<int> buttons(PS3_NUM_BUTTONS);

    // Axes
    axes.at(PS3_AXIS_STICK_LEFT_LEFTWARDS) = source.axes.at(AXIS_STICK_LEFT_LEFTWARDS_IDX);
    axes.at(PS3_AXIS_STICK_LEFT_UPWARDS) = source.axes.at(AXIS_STICK_LEFT_UPWARDS_IDX);
    axes.at(PS3_AXIS_STICK_RIGHT_LEFTWARDS) = source.axes.at(AXIS_STICK_RIGHT_LEFTWARDS_IDX);
    axes.at(PS3_AXIS_STICK_RIGHT_UPWARDS) = source.axes.at(AXIS_STICK_RIGHT_UPWARDS_IDX);

    // Rear buttons
    buttons.at(PS3_BUTTON_REAR_LEFT_1) = source.buttons.at(BUTTON_L1_IDX);
    buttons.at(PS3_BUTTON_REAR_LEFT_2) = source.buttons.at(BUTTON_L2_IDX);
    buttons.at(PS3_BUTTON_REAR_RIGHT_1) = source.buttons.at(BUTTON_R1_IDX);
    buttons.at(PS3_BUTTON_REAR_RIGHT_2) = source.buttons.at(BUTTON_R2_IDX);

    // Cross buttons
    buttons.at(PS3_BUTTON_CROSS_UP) = source.buttons.at(BUTTON_CROSS_UP_IDX);
    buttons.at(PS3_BUTTON_CROSS_RIGHT) = source.buttons.at(BUTTON_CROSS_RIGHT_IDX);
    buttons.at(PS3_BUTTON_CROSS_DOWN) = source.buttons.at(BUTTON_CROSS_DOWN_IDX);
    buttons.at(PS3_BUTTON_CROSS_LEFT) = source.buttons.at(BUTTON_CROSS_LEFT_IDX);

    result.axes = axes;
    result.buttons = buttons;
    result.header = source.header;

    return result;
}
