#include "mainwindow.h"
#include "yaml-cpp/yaml.h"
#include <ros/package.h>

namespace fishplayer
{
MainWindow::MainWindow(QWidget *parent) : QMainWindow(parent), m_ui(new Ui::MainWindow)
{
    m_ui->setupUi(this);

    initPlaylists();
    initUI();
}

MainWindow::~MainWindow()
{
    delete m_ui;
}


void MainWindow::initUI()
{

    // Add playlist to UI
    for(auto playlist: m_playlists)
        m_ui->horizontalLayout->addWidget(playlist);

    m_buttonPlay = new QPushButton("Play", this);
    m_buttonPlay->setStyleSheet("font-weight: bold; color: green");
    m_buttonPlay->setMaximumWidth(400);
    m_buttonPlay->setMinimumWidth(200);

    m_buttonStop = new QPushButton("Stop", this);
    m_buttonStop->setStyleSheet("font-weight: bold; color: red");
    m_buttonStop->setMaximumWidth(400);
    m_buttonStop->setMinimumWidth(200);

    m_ui->verticalLayout->addWidget(m_buttonPlay);
    m_ui->verticalLayout->setAlignment(m_buttonPlay, Qt::AlignHCenter);
    m_ui->verticalLayout->addWidget(m_buttonStop);
    m_ui->verticalLayout->setAlignment(m_buttonStop, Qt::AlignHCenter);
    connect(m_buttonPlay, SIGNAL(clicked()),this, SLOT(play()));
    connect(m_buttonStop, SIGNAL(clicked()),this, SLOT(stop()));

    for(auto playlist: m_playlists)
        connect(playlist, SIGNAL(donePlaying()), this, SLOT(playbackDone()));
}

void MainWindow::initPlaylists()
{

    std::string configFile = ros::package::getPath("fishplayer");
    configFile = configFile + "/config/monitor_config.yaml";
    YAML::Node playlistConfig = YAML::LoadFile(configFile);

    if(playlistConfig.size() == 0)
    {
        makeDefaultPlaylists();
    }
//    std::cout << playlistConfig["left"] << std::endl;


    for (std::size_t i = 0 ; i < playlistConfig.size() ; i++) {

        YAML::Node node = playlistConfig[i];


        // Node has to be a map
        if(!node || !node.IsMap())
        {
            makeDefaultPlaylists();
            break;
        }

        std::string ns = node["namespace"].as<std::string>();
        bool isActive = node["active"].as<bool>();

        if(isActive && ns != "player")
        {
            PlaylistWidget* p = new PlaylistWidget(this, ns);
            m_playlists.push_back(p);
        }

    }
}
void MainWindow::makeDefaultPlaylists()
{

    for (auto playlist : m_playlists)
    {
        delete playlist;
    }
    m_playlists.clear();

    PlaylistWidget* left = new PlaylistWidget(this, "left");
    PlaylistWidget* right = new PlaylistWidget(this, "right");

    m_playlists.push_back(left);
    m_playlists.push_back(right);

}

void MainWindow::stop()
{

    // Stop playlists
    for(auto playlist: m_playlists)
        playlist->stop();

    m_buttonPlay->setEnabled(true);
    m_buttonPlay->setStyleSheet("font-weight: bold; color: green");

}
void MainWindow::play()
{
    // No playlist is playing
    if(allPlaylistsDone())
    {

        // Make sure each playlist is valid
        bool allOk = true;
        for(auto playlist: m_playlists)
            allOk = allOk && playlist->checkPlaylist();

        if(allOk)
        {
            for(auto playlist: m_playlists)
                playlist->play();

            m_buttonPlay->setStyleSheet("font-weight: bold; color: gray");
            m_buttonPlay->setEnabled(false);
        }
        else
        {
            QMessageBox msgBox;
            msgBox.setText("You have errors in your playlist. Please fix them before playing.");
            msgBox.exec();
        }
    }


}


void MainWindow::playbackDone()
{
    if(allPlaylistsDone())
    {
        m_buttonPlay->setText("Play");
        m_buttonPlay->setStyleSheet("font-weight: bold; color: green");
        m_buttonPlay->setEnabled(true);
    }
}

bool MainWindow::allPlaylistsDone()
{

    bool playingDone = true;
    for(auto playlist: m_playlists)
        playingDone = playingDone && !playlist->isPlaying();

    return playingDone;
}

}
