#include "fish/poecilia/poecilia.h"

#include "sim/fishsim_globals.h"

#include "ros/joy_indices.h"

using namespace irr;
using namespace scene;
using namespace core;

// Gets multiplied with foreward input
#define FOREWARD_MULTIPLIER 0.8

// Gets multiplied with yaw input
#define YAW_MULTIPLIER 6.0

// Gets multiplied with pitch input
#define PITCH_MULTIPLIER 2.0

namespace fishsim
{
Poecilia::Poecilia(IAnimatedMeshSceneNode *node, bool isMale) : Fish(node)
{

    m_dorsalFinModifier = 0;
    m_gonopodiumModifier = 0;
    m_forewardSwimModifier = 0;
    m_turnSwimModifier = 0;
    m_upSwimModifier = 0;
    m_isMale = isMale;

    m_forewardMultiplier = FOREWARD_MULTIPLIER;
    m_yawMultiplier = YAW_MULTIPLIER;
    m_pitchMultiplier = PITCH_MULTIPLIER;
}

Poecilia::~Poecilia()
{
    unload();
}

bool Poecilia::unload()
{

    if(m_dorsalFinModifier)
    {
        delete m_dorsalFinModifier;
        m_dorsalFinModifier = 0;
    }

    if(m_gonopodiumModifier)
    {
        delete m_gonopodiumModifier;
        m_gonopodiumModifier = 0;
    }

    if(m_forewardSwimModifier)
    {
        delete m_forewardSwimModifier;
        m_forewardSwimModifier = 0;
    }

    if(m_turnSwimModifier)
    {
        delete m_turnSwimModifier;
        m_turnSwimModifier = 0;
    }

    if(m_upSwimModifier)
    {
        delete m_upSwimModifier;
        m_upSwimModifier = 0;
    }

    m_boneModifiers.clear();

    Fish::unload();

    return true;
}

bool Poecilia::load()
{

    /// 1. Mesh scene node settings like material flags, debug stuff etc..
    Fish::load();

    /// 2. Bone renaming. Remove bone prefix.
    Fish::removeBonePrefix("head");

    /// 3. Init editable body parts of the fish.
    initEditableMaterials();

    /// 4. init bone modifier
    initBoneModifier();

    return true;
}


void Poecilia::initEditableMaterials(){

    // Body should not be transparent
    //m_irrNode->getMaterial(0).MaterialType = irr::video::EMT_SOLID;

    makeMaterialEditable(0, L"body", true);
    makeMaterialEditable(1, L"dorsal", true);
    makeMaterialEditable(2, L"caudal", true);
    makeMaterialEditable(3, L"pectoral", true);

    // Females have more fins
    if(!m_isMale)
    {
        makeMaterialEditable(5, L"anal", true);
        makeMaterialEditable(6, L"pelvic", true);
    }

}

void Poecilia::initBoneModifier(){

    /// Init modifiers that are only used in direct mode:
    /// They only get used in move function. Not visible in
    /// fishsteering.

    // Foreward swimming animation
    m_forewardSwimModifier = new ForewardSwimModifier(m_irrNode, "foreward swim animator");
    m_forewardSwimModifier->init();

    // Idle animation
    m_idleModifier = new IdleModifier(m_irrNode, "idle animator", 0.2);
    m_idleModifier->init();

    // Turn animation
    m_turnSwimModifier = new TurnSwimModifier(m_irrNode, "turn animator",  0.1);
    m_turnSwimModifier->init();

    // up/down animation
    m_upSwimModifier = new UpSwimModifier(m_irrNode, "up/down animator",  0.1);
    m_upSwimModifier->init();


    /// Init bone modifiers that can be used in indirect mode too.
    /// They are called from move and are also able to modify fishsnapshots
    /// through their modify function.
    /// These modifier will show up in fishsteering.

    // Only male
    if(m_isMale)
    {
        // Dorsal fin
        m_dorsalFinModifier = new DorsalFinModifier(m_irrNode, "Dorsal fin (L1)", 3.5);
        if(m_dorsalFinModifier->init())
        {
            // Use dorsal fin 1 independantly
            m_dorsalFinModifier->useConnectedMode(true);
            addBoneModifier(m_dorsalFinModifier);
        }

        // Gonopodium
        m_gonopodiumModifier = new GonopodiumModifier(m_irrNode, "Gonopodium (R1, R2)", 3.5);
        if(m_gonopodiumModifier->init())
            addBoneModifier(m_gonopodiumModifier);
    }

}

void Poecilia::move(const sensor_msgs::Joy& joyState){

    float forewardAmount = joyState.axes.at(PS3_AXIS_STICK_LEFT_UPWARDS) * m_forewardMultiplier;
    float yawAmount = joyState.axes.at(PS3_AXIS_STICK_RIGHT_LEFTWARDS) * m_yawMultiplier;
    float pitchAmount = joyState.axes.at(PS3_AXIS_STICK_RIGHT_UPWARDS) * m_pitchMultiplier;

    move(forewardAmount,0.0, 0.0 ,yawAmount,pitchAmount,0.0);

    if(m_gonopodiumModifier)
        m_gonopodiumModifier->update(joyState);

    if(m_dorsalFinModifier)
        m_dorsalFinModifier->update(joyState);
}

void Poecilia::move(float forewardAmount, float sidewardAmount, float upwardAmount, float yawAmount, float pitchAmount, float rollAmount)
{

    forewardAmount = Fish::getForewardSmoothed(forewardAmount);
    Fish::move(forewardAmount, sidewardAmount,  upwardAmount,  yawAmount,  pitchAmount,  rollAmount);

    // Update foreward swim modifier. This modifier uses absolute rotation
    // values. It should be called first, since it resets all rotation values
    // from the last move call
    m_forewardSwimModifier->update(forewardAmount);

    // Do turn and up animation. These modifier limit the rotation values to
    // a maximum.
    m_turnSwimModifier->update(yawAmount);
    m_upSwimModifier->update(pitchAmount);

    // Idle modifier kicks in, if foreward amount is small or near zero.
    // Will play idle animation
    m_idleModifier->update(forewardAmount);
}


} // end namespace fishsim
