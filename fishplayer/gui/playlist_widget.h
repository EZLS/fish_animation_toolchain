/*! Copyright (C) 2017  	Department of Electrical Engineering & Computer Science, Institute of
 *			Real-Time Learning Systems, University of Siegen
 *
 *			Research Group of Ecology and Behavioral Biology, Institute of Biology,
 *			University of Siegen
 *
 * Contact: virtual.fish.project@gmail.com
 *
 * License: GNU General Public License  See LICENSE.txt for the full license.
 *
 * This file is part of the "FishSim Animation Toolchain".
 * "FishSim Animation Toolchain" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "FishSim Animation Toolchain" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "FishSim Animation Toolchain".  If not, see <http: *www.gnu.org/licenses/>.
 *
 * The "FishSim Animation Toolchain" was developed within the scope of the interdisciplinary,
 * DFG (Deutsche Forschungsgemeinschaft)-funded project “virtual fish” (KU
 * 689/11-1 and Wi 1531/12-1) of the Institute of Real-Time Learning Systems
 * (EZLS) and the Research Group of Ecology and Behavioral Biology at the
 * University of Siegen.
*/
#ifndef H_PLAYLIST_WIDGET_H
#define H_PLAYLIST_WIDGET_H

#include <QWidget>
#include <QFileDialog>
#include <QMessageBox>
#include <QListWidget>
#include <QTimer>

#include "include/clip.h"
#include "include/playlist.h"
#include "include/ros_interface.h"
#include "ui_playlist_widget.h"

namespace Ui {
    class PlaylistWidget;
}

namespace fishplayer
{
class PlaylistWidget : public QWidget
{
    Q_OBJECT
    
public:
    explicit PlaylistWidget(QWidget *parent, std::string rosNamespace);

    ~PlaylistWidget();


    bool checkPlaylist();

    void play();
    void stop();

    bool isPlaying()
    {
        return m_playlist->isPlaying();
    }

signals:
    void donePlaying();

public slots:

    void changeCurrentScene();
    void addLoadScene();
    void addPause();
    void addPlayBag();
    void addPlayAuto();
    void save();
    void load();
    void updatePlaytime();
    void deleteEntry();
    void updateUI();
    void playbackFinished();
    void startNewClip(int);

    
private:

    Ui::PlaylistWidget *m_ui;

    // The model that stores the data
    Playlist* m_playlist;

    // Interface to ROS
    RosInterface* m_roshandler;

    // The list widget where all clips are showed
    QListWidget* m_listWidget;

    // Total duration of playlist
    float m_playlistDuration;

    // Current playtime
    float m_playTime;

    QTimer *m_playTimer;

    void connectSignalsAndSlots();

    std::vector<Clip*> getClips();

    void addClipToList(Clip *widget);

    void showErrorBox(std::string message);

    void updateTimeLabel();

};

} // end namespace fishplayer
#endif //H_PLAYLIST_WIDGET_H
