/*! Copyright (C) 2017  	Department of Electrical Engineering & Computer Science, Institute of
 *			Real-Time Learning Systems, University of Siegen
 *
 *			Research Group of Ecology and Behavioral Biology, Institute of Biology,
 *			University of Siegen
 *
 * Contact: virtual.fish.project@gmail.com
 *
 * License: GNU General Public License  See LICENSE.txt for the full license.
 *
 * This file is part of the "FishSim Animation Toolchain".
 * "FishSim Animation Toolchain" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "FishSim Animation Toolchain" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "FishSim Animation Toolchain".  If not, see <http: *www.gnu.org/licenses/>.
 *
 * The "FishSim Animation Toolchain" was developed within the scope of the interdisciplinary,
 * DFG (Deutsche Forschungsgemeinschaft)-funded project “virtual fish” (KU
 * 689/11-1 and Wi 1531/12-1) of the Institute of Real-Time Learning Systems
 * (EZLS) and the Research Group of Ecology and Behavioral Biology at the
 * University of Siegen.
 *
 *  \file camera_editor.h
 *  \brief A toolbox window that allows changing camera settings.
 *
*/


#ifndef H_CAMERA_EDITOR_H
#define H_CAMERA_EDITOR_H

#include <irrlicht.h>

#include "IGUIEnvironment.h"
#include "IGUIElement.h"
#include "IGUIWindow.h"

#include "gui/fishsim_toolbox_tab.h"


namespace fishsim
{
class CameraEditor
{

public:

        /*!
          \brief Constructor.
          \param device The irrlicht device.
          \param mayaCam Pointer to maya camera used in edit mode.
          \param staticCam Pointer to static camera used play mode.
        */
        CameraEditor(irr::IrrlichtDevice* device,
                     irr::scene::ICameraSceneNode* mayaCam,
                     irr::scene::ICameraSceneNode* staticCam);

        /*!
          \brief Destructor
        */
        ~CameraEditor();

        /*!
          \brief Apply all changes to the settings made in the
          camera tab.
        */
        void applyCameraSettings();


private:

    /// Irrlicht device
    irr::IrrlichtDevice* m_device;

    /// Irrlicht gui environment
    irr::gui::IGUIEnvironment* m_guiEnv;

    /// Pointer to maya camera
    irr::scene::ICameraSceneNode* m_mayaCam;

    /// Pointer to static camera
    irr::scene::ICameraSceneNode* m_staticCam;

    /// Camera settings window
    irr::gui::IGUIWindow* m_winCameraSettings;

    /// Camera tab elements
    irr::gui::IGUIEditBox *m_edtBoxFOV;
    irr::gui::IGUIEditBox *m_edtBoxAspectRatio;

    /// Camera tab
    FishSimToolboxTab* m_cameraTab;


    /*!
          \brief Update the informations that gets
           displayed in position tab.
     */
    void updateCameraTab();


    /*!
          \brief Create the settings window
     */
    void createSettingsWindow();
};


} // end namespace fishsim

#endif // H_CAMERA_EDITOR_H
