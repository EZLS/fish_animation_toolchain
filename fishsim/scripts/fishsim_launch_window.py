#!/usr/bin/env python
# Copyright (C) 2017  	Department of Electrical Engineering & Computer Science, Institute of
#			Real-Time Learning Systems, University of Siegen
#
#			Research Group of Ecology and Behavioral Biology, Institute of Biology,
#			University of Siegen
#
# Contact: virtual.fish.project@gmail.com
#
# License: GNU General Public License  See LICENSE.txt for the full license.
#
# This file is part of the "FishSim Animation Toolchain".
# "FishSim Animation Toolchain" is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# "FishSim Animation Toolchain" is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with "FishSim Animation Toolchain".  If not, see <http: *www.gnu.org/licenses/>.
#
# The "FishSim Animation Toolchain" was developed within the scope of the interdisciplinary,
# DFG (Deutsche Forschungsgemeinschaft)-funded project "virtual fish" (KU
# 689/11-1 and Wi 1531/12-1) of the Institute of Real-Time Learning Systems
# (EZLS) and the Research Group of Ecology and Behavioral Biology at the
# University of Siegen.
#
#

import rospy
import rospkg
import rosparam
import subprocess
import time
import yaml
import roslaunch
from PyQt5.QtWidgets import (QWidget, QMainWindow, QDesktopWidget, QLabel,
  QApplication, QPushButton, QHBoxLayout, QVBoxLayout, QMessageBox, QComboBox)
from PyQt5.QtCore import Qt

class FishSimLaunchWindow(QWidget):

    __DEFAULT_CONFIG = {"resolution_x": 1920, "resolution_y": 1080, "quality": "good"}
    
    def __init__(self):
        super(FishSimLaunchWindow, self).__init__()

        # Path where config file is located
        rospack = rospkg.RosPack()
        self.__config_filepath = rospack.get_path('fishsim') + "/config/fishsim_launch_config.yaml"

        self.__config = self._load_config()

        # Start roscore and upload parameter to parameter server
        #self.__roscore_process =  subprocess.Popen('roscore')

        # Roslaunch instance
        self.__roslaunch = roslaunch.scriptapi.ROSLaunch()

        # Create user interface
        self._init_UI()

    def __del__(self):
        print("Terminate")
        #self.__roscore_process.terminate()



    def _validate_config(self, config):
         
        # Make sure  fishsim config is still valid 
        if(len(config) == 0):
            return self.__DEFAULT_CONFIG
        try:
            if 'resolution_x' in config and 'resolution_y' in config: #and 'quality' in config:  
                return config
            else:
                return self.__DEFAULT_CONFIG
        except ValueError:
            print("Config corrupted. Will load default config")
            return self.__DEFAULT_CONFIG


    def _load_config(self):

        # Get monitor configuration from yaml config file
        try:

            with open(self.__config_filepath, 'r') as infile:
                config = yaml.load(infile)
        
        except IOError, yaml.scanner.ScannerError:
            print("Config file corrupted or not found. Will load default config")        
            config = self.__DEFAULT_CONFIG
        
        return self._validate_config(config)


    def _save_config(self):

        # Update configuration file from GUI
        self.__config = dict()

        res = self.__comboBoxRes.currentText()
        res_x = int( res.split('x')[0]) 
        res_y = int( res.split('x')[1])
        self.__config['resolution_x'] = res_x
        self.__config['resolution_y'] = res_y
        #self.__config['quality'] = self.__comboBoxQuality.currentText()
        try:           
           self._write_yaml_data(self.__config, self.__config_filepath)
        except IOError:
            print("Could not write configration to file")

    def _write_yaml_data(self, dictionary, filepath):
        
        # Save config to yaml file
        with open(filepath, 'w') as outfile:
            outfile.write(yaml.safe_dump(dictionary, default_flow_style=False))
        

 


    def _init_UI(self):

        # Horizontal box layout for whole window
        vbox = QVBoxLayout()
        vbox.setSpacing(0)
        self.setLayout(vbox)

        # Description
        label_descr = QLabel("Configure Screen Resolution:") 
        label_descr.setWordWrap(True)
        vbox.addWidget(label_descr)


        # Resolution
        label_res = QLabel("Resolution: ")
        self.__comboBoxRes = QComboBox(self)
        self.__comboBoxRes.addItem("800 x 600")
        self.__comboBoxRes.addItem("1024 x 768")
        self.__comboBoxRes.addItem("1280 x 800")
        self.__comboBoxRes.addItem("1280 x 1024")
        self.__comboBoxRes.addItem("1366 x 768")
        self.__comboBoxRes.addItem("1920 x 1080")

        # Get index of the resolution that matches the config
        res = str(self.__config['resolution_x']) + " x " +  str(self.__config['resolution_y'])
        index = self.__comboBoxRes.findData(res, 0)
        if index != -1:
            self.__comboBoxRes.setCurrentIndex(index)

        hboxRes = QHBoxLayout()
        hboxRes.addWidget(label_res)
        hboxRes.addWidget(self.__comboBoxRes)
        vbox.addLayout(hboxRes)
        
        # Quality
        #label_quality = QLabel("Quality: ")
        #self.__comboBoxQuality = QComboBox(self)
        #self.__comboBoxQuality.addItem("Simple")
        #self.__comboBoxQuality.addItem("Good")

        # Get index of the quality that matches the config
        #index = self.__comboBoxQuality.findData(self.__config['quality'], 0)
        #if index != -1:
        #    self.__comboBoxQuality.setCurrentIndex(index)
        #hboxQuality = QHBoxLayout()
        #hboxQuality.addWidget(label_quality)
        #hboxQuality.addWidget(self.__comboBoxQuality)
        #vbox.addLayout(hboxQuality)
        
        # Launch button
        self.__launch_button = QPushButton('Launch', self)
        self.__launch_button.clicked.connect(self.launch) 
        vbox.addWidget(self.__launch_button)
           
        self.setGeometry(300, 300, 300, 220)
        self.setWindowTitle('FishSim launcher')    
        self.show()



        

    def launch(self):

        # Save monitor configuration
        self._save_config()
        
        self.__roslaunch.start()
        
        res = str(self.__config['resolution_x']) + " " + str(self.__config['resolution_y'])
        fishsim_node = roslaunch.core.Node('fishsim', 'FishSim', 'fishsim', '/', None, res)
        fishsim_process = self.__roslaunch.launch(fishsim_node)

                
        #self.hide()

        """
        # Check if all process are still running
        all_nodes_running = True
        while all_nodes_running:
            time.sleep(1)
            
            for fishsim in self.__fishsim_instances:
                all_nodes_running = all_nodes_running and fishsim.is_running()

            all_nodes_running = all_nodes_running and fishplayer_process.is_alive()
            #all_nodes_running = all_nodes_running and (joy_process.is_alive())

        fishsim_process.stop()
           
        self.show()
        """


