/*! Copyright (C) 2017  	Department of Electrical Engineering & Computer Science, Institute of
 *			Real-Time Learning Systems, University of Siegen
 *
 *			Research Group of Ecology and Behavioral Biology, Institute of Biology,
 *			University of Siegen
 *
 * Contact: virtual.fish.project@gmail.com
 *
 * License: GNU General Public License  See LICENSE.txt for the full license.
 *
 * This file is part of the "FishSim Animation Toolchain".
 * "FishSim Animation Toolchain" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "FishSim Animation Toolchain" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "FishSim Animation Toolchain".  If not, see <http: *www.gnu.org/licenses/>.
 *
 * The "FishSim Animation Toolchain" was developed within the scope of the interdisciplinary,
 * DFG (Deutsche Forschungsgemeinschaft)-funded project “virtual fish” (KU
 * 689/11-1 and Wi 1531/12-1) of the Institute of Real-Time Learning Systems
 * (EZLS) and the Research Group of Ecology and Behavioral Biology at the
 * University of Siegen.
 *
 *  \file jostick_mapping.h
 *  \brief Provides access to the current joystick mapping through ROS parameters.
 *
*/


#ifndef JOYSTICK_MAPPING_H
#define JOYSTICK_MAPPING_H

#include "ros/ros.h"
#include "sensor_msgs/Joy.h"

class JoystickMapping
{


public:

    /*!
        Default Constructor.
    */
    JoystickMapping();

    /*!
        Get joystick mappings from parameter server.
     */
    void initFromROSParams();


    /*!
        Remaps from a source message to a target message.

     */
    sensor_msgs::Joy remap(sensor_msgs::Joy source);

private:

    // The source message is the one from which the target
    // message will be created by remapping.

    int AXIS_STICK_LEFT_UPWARDS_IDX;
    int AXIS_STICK_LEFT_LEFTWARDS_IDX;
    int AXIS_STICK_RIGHT_UPWARDS_IDX;
    int AXIS_STICK_RIGHT_LEFTWARDS_IDX;
    int BUTTON_L1_IDX;
    int BUTTON_L2_IDX;
    int BUTTON_R1_IDX;
    int BUTTON_R2_IDX;
    int BUTTON_CROSS_UP_IDX;
    int BUTTON_CROSS_DOWN_IDX;
    int BUTTON_CROSS_LEFT_IDX;
    int BUTTON_CROSS_RIGHT_IDX;
    

};


#endif // JOYSTICK_MAPPING_H
