#include "include/steering_roshandler.h"

using namespace fishsim;

namespace fishsteering
{
SteeringROSHandler::SteeringROSHandler(){

    m_nodeHandle = new ros::NodeHandle();
    std::string snapshotTopic = m_nodeHandle->getNamespace() + "fishsim/fishSnapshot";

    m_joySubscriber =
            m_nodeHandle->subscribe<sensor_msgs::Joy>("/joy", 1, &SteeringROSHandler::joyCallback, this);
    m_asyncSpinner = new ros::AsyncSpinner(4);


    m_snapshotPublisher =
            m_nodeHandle->advertise<fishsim::FishSnapshotStamped>(snapshotTopic, 10);

    m_asyncSpinner->start();
}

SteeringROSHandler::~SteeringROSHandler(){

    m_asyncSpinner->stop();

    delete m_asyncSpinner;
    delete m_nodeHandle;

}

void SteeringROSHandler::joyCallback(const sensor_msgs::Joy::ConstPtr& joy)
{

    sensor_msgs::Joy tempJoy = *joy;

    m_joyMutex.lock();
    m_joyState = tempJoy;
    m_joyMutex.unlock();
}

bool SteeringROSHandler::isJoystickDataAvailable(){

    return !m_joyState.axes.empty();
}

sensor_msgs::Joy SteeringROSHandler::getJoystickState()
{
    sensor_msgs::Joy toReturn;
    m_joyMutex.lock();
    toReturn = m_joyState;
    m_joyMutex.unlock();

    return toReturn;
}

void SteeringROSHandler::publishSnapshot(const FishSnapshot* snapshot)
{
    // Stamp message.
    fishsim::FishSnapshotStamped rosSnapshot;
    rosSnapshot = snapshot->toRosSnapshot();
    rosSnapshot.timestamp = ros::Time::now();

    // Publish topic.
    m_snapshotPublisher.publish(rosSnapshot);
}

}
