#include "utils/FIR_filter.h"


FIRFilter::FIRFilter()
{
    m_updateFilter = true;
}

FIRFilter::FIRFilter(std::vector<float> coefficients)
{

    setCoefficients(coefficients);
}
 #include <iostream>
FIRFilter::~FIRFilter()
{
    std::cout << "byby" << std::endl;
    m_valueHistory.clear();
}


float FIRFilter::getFilterValue(){

    if(!m_updateFilter){
        return m_result;
    }

    if(m_coefficients.size() > 0){

        float res = 0.0;
        for (unsigned int i = 0; i < m_valueHistory.size(); i++){

           // std::cout << i << " " << gaussianBlurKernel[i] << "  *  " <<  valueHistory[i] << std::endl;
            res += m_coefficients[i] * m_valueHistory[i];
        }

        m_result = res;
        m_updateFilter = false;

        return res;
    }

    return 0.0;


}


void FIRFilter::addNewInputVal(float inputVal){

    // Most recent value goes to the front
    m_valueHistory.push_front(inputVal);

    // remove oldest value if queue gets larger than coefficient vector
    if(m_valueHistory.size() > m_coefficients.size()){
        m_valueHistory.pop_back();
    }

    m_updateFilter = true;

}


void FIRFilter::setCoefficients(std::vector<float> coefficients){

    if(coefficients.size() > 0){
        m_coefficients = coefficients;
        m_updateFilter = true;
    }


}

void FIRFilter::clear(){

    m_valueHistory.clear();

    m_updateFilter = true;
}
