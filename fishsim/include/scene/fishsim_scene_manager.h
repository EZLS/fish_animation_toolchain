/*! Copyright (C) 2017  	Department of Electrical Engineering & Computer Science, Institute of
 *			Real-Time Learning Systems, University of Siegen
 *
 *			Research Group of Ecology and Behavioral Biology, Institute of Biology,
 *			University of Siegen
 *
 * Contact: virtual.fish.project@gmail.com
 *
 * License: GNU General Public License  See LICENSE.txt for the full license.
 *
 * This file is part of the "FishSim Animation Toolchain".
 * "FishSim Animation Toolchain" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "FishSim Animation Toolchain" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "FishSim Animation Toolchain".  If not, see <http: *www.gnu.org/licenses/>.
 *
 * The "FishSim Animation Toolchain" was developed within the scope of the interdisciplinary,
 * DFG (Deutsche Forschungsgemeinschaft)-funded project “virtual fish” (KU
 * 689/11-1 and Wi 1531/12-1) of the Institute of Real-Time Learning Systems
 * (EZLS) and the Research Group of Ecology and Behavioral Biology at the
 * University of Siegen.
 *
 *  \file fishsim_scene_manager.h
 *  \brief FishSimScene manager manages all models in scene (fish and environment),
 *  the various cameras, lights and shadows. The class provides several helper functions,
 *  that allow load fish and environment models from disk. Also complete scenes can be
 *  loaded and saved to/from file.
 *
 * The FishSimSceneManager keeps track of all fish and environment models in the scene. These
 * models can be loaded from file with the respective loader functions. The loader functions
 * load a model from file, add them to the irrlicht scene manager and set up neccessary stuff for
 * fishsim, like collision and shadows.
 *
 * The manager also handles the camera and light setup. User can switch between a static cam and
 * a maya cam. The lights and shadows are drawn using XEffect. Only one light is supported right
 * now.
 *
 * The FishSimSceneManager is also able to load and save whole scenes from/to .scene files.
 * These are xml based files that describe a complete simulation scene (for more informations
 * see documentation about .irr files and the corresponding SceneLoaders on irrlicht website).
 *
*/

#ifndef H_FISHSIMSCENEMANAGER_H
#define H_FISHSIMSCENEMANAGER_H

#include <iostream>
#include <string>
#include <vector>

#include <irrlicht.h>
#include <irrExt/xEffects/XEffects.h>

#include "fish/fish.h"


namespace fishsim
{
class FishSimSceneManager
{

public:

    ///***********************************************************
    /// Initialization and drawing
    ///***********************************************************


    /*!
       \brief Default Constructor.

       \param sceneManager pointer to the irrlicht scene manager.
       \param simTimer pointer to simulation timer.
       \param pathToFishSim path where fishsim is located.
       \param shadowManager pointer to XEffect shadow manager that will be used to draw soft
                  shadows. In fishsteering and fish_automover not needed.
     */
    FishSimSceneManager(irr::scene::ISceneManager *sceneManager,
                        const std::string pathToFishSim, EffectHandler *shadowManager = 0);


    /*!
       \brief Destructor.
     */
    ~FishSimSceneManager();


    /*!
       \brief If a XEffect ShadowManager was passed on contructor, this function
       will set up the ShadowManager for fishsim.
     */
    void initXEffect();


    /*!
       \brief Update all fish states depending on their snapshots and the current
       simulation time. Then draws the scene.
     */
    void drawAll();


    ///***********************************************************
    /// Scene loading and unloading functions
    ///***********************************************************


    /*!
       \brief Clears the whole scene.
     */
    void clearScene();


    /*!
       \brief Save scene to temp scene. This scene will be loaded from
       fishsteering.
     */
    void saveTempScene();


    /*!
       \brief Load temp scene. Is used in fishsteering.

       \return true, if load was successfull, false otherwise.
     */
    bool loadTempScene();


    /*!
       \brief Save scene to given filepath.

       \param filepath filepath where scene will be saved.
     */
    void saveScene(const std::string& filepath);


    /*!
       \brief Load scene from given filepath.

       \param filepath filepath from which scene will be loaded.
       \param hideFishModels if true, fish models wont be visible on load.

       \return true, if load was successfull, false otherwise.
     */
    bool loadScene(const std::string& filepath, bool hideFishModels);


    ///***********************************************************
    /// Environment loading and unloading functions
    ///***********************************************************


    /*!
       \brief Extract model name (filename without path) and pass it to
       loadEnvModel(const std::string& path,const std::string& name).

       \param filePath filePath of the env model.

       \return true, if load was successfull, false otherwise.
     */
    bool loadEnvModel(const std::string &filePath);


    /*!
       \brief Load model from given filepath as env model to irrlicht scene
       manager.

       \param path filepath of the env model.
       \param name name the env model should get. If name already exist in scene,
                  a prefix will be added (like "_0" or "_1").

       \return true, if load was successfull, false otherwise.
     */
    bool loadEnvModel(const std::string& path,const std::string& name);


    /*!
       \brief Remove a env node by name.

       \param name name of the env model to remove

       \return true, if env model was found and removed, false otherwise.
     */
    bool removeEnvNodeByName(const std::string &name);


    /*!
        \brief Get an env node by name.

       \param name name of the env model

       \return Pointer to the IMeshSceneNode, if model could be found in scene,
               0 otherwise.
     */
    irr::scene::IMeshSceneNode *getEnvNodeByName(const std::string name);


    ///***********************************************************
    /// Fish loading and unloading functions
    ///***********************************************************


    /*!
       \brief Load fish model from file.

       \param filePath filepath of the fish model to load.
                  File extension must be ".x" or ".fish".

       \return true, if load was successfull, false otherwise.
     */
    bool loadFishModel(const irr::io::path& filePath);


    /*!
       \brief Load fish model from directX file.

       \param filePath filepath of the model.
       \param name name of the model. If name already exist in scene,
                  a prefix will be added (like "_0" or "_1").

       \return true, if load was successfull, false otherwise.
     */
    bool loadXFishModel(const std::string& filePath, const std::string& name);


    /*!
       \brief Load fish model from .fish file.

       \param path filepath of the model.

       \return true, if load was successfull, false otherwise.
     */
    bool loadIrrFishModel(const irr::io::path &path);


    /*!
        \brief Remove a fish by name.

       \param name name of the fish model to remove.

       \return true, if fish model was found and removed, false otherwise.
     */
    bool removeFishByName(const std::string& name);


    /*!
       \brief Get a fish by name.

       \param name name of fish.

       \return Pointer to fish, if it can be found in scene,
               0 otherwise.
     */
    Fish* getFishByName(const std::string &name);


    ///***********************************************************
    /// Simulation and helper functions
    ///***********************************************************


    /*!
       \brief Camera will focus the selected object.
              Does nothing if no object is selected.
     */
    void focusSelected();


    /*!
       \brief Copy all camera parameters (position, rotation, fov, ...) from maya camera
       to static camera.
     */
    void setStaticCamToCurrentView();


    /*!
       \brief Delete the selected node.

       \return true, if a node is selected and could be deleted, false otherwise.
     */
    bool deleteSelectedNode();


    /*!
       \brief Update the world triangles of the fish collision animators.

       After adding or removing an env node, this fuction should be called, to tell
       the animators that the "world" has changed. Will pass the m_triangleSelector
       to the animators.
     */
    void updateCollisionAnimators();


    /*!
       \brief Switch between edit camera and static camera.

       \param enableMayaCam if true, edit camera is set as main camera,
                  if false, static cam will be the main camera.
     */
    void enableMayaCamera(bool enableMayaCam);


    /*!
       \brief Update the m_triangleSelector which holds the "world" triangles.

       Should be called after adding or removing an env node.
     */
    void updateTriangleWorld();


    /*!
        \brief Deselect node that is currently selected.
     */
    void deselect();


    /*!
        \brief Try to select a node at given screen coordinates. If a node could be found
        under the given screen coordinates, you can get this node through getSelectedNode().
        If no node could be found, getSelectedNode() will return 0.

       \param screenX X position.
       \param screenY Y position.
     */
    void selectNodeAtScreenCoordinates(int screenX, int screenY);


    /*!
     * \brief Set the shadow level either to soft or no shadows.
     * \param level shadow level.
     */
    void setShadowLevel(int level);


    /*!
     * \brief Returns pointer to currently selected fish.
     * \return pointer to fish that is currently selected.
     */
    Fish* getSelected()
    {

        if(!m_selectedNode)
            return 0;

        return getFishByName(m_selectedNode->getName());
    }

    /// Returns true, if fishsim is in edit mode.
    bool isInEditMode()
    {
        return m_editMode;
    }

    /// Returns bounding box.
    irr::core::aabbox3df getBoundingBox()
    {
        return m_boundingBox;
    }

    /// Returns shadow light.
    SShadowLight* getShadowLight()
    {
        return m_shadowLight;
    }

    /// Returns name of selected node, if one is selected,
    /// returns empty string otherwise.
    std::string getNameOfSelectedNode()
    {

        if(m_selectedNode != 0)
            return m_selectedNode->getName();
        else
            return "";
    }

    /// Returns shadow manager
    EffectHandler* getShadowManager(){
        return m_shadowMgr;
    }


    /// Returns pointer to maya camera
    irr::scene::ICameraSceneNode* getMayaCam(){
        return m_mayaCam;
    }

    /// Returns pointer to static camera
    irr::scene::ICameraSceneNode* getStaticCam(){
        return m_staticCam;
    }

    /// Return current fish names in a vector
    std::vector<std::string>  getCurrentFishModels();

    /// Print all nodes in scene.
    void printNodes();

private:

    ///Pointer to scene manager.
    irr::scene::ISceneManager *m_smgr;

    /// Pointer to shadow manager, used to draw soft shadows.
    EffectHandler* m_shadowMgr;

    /// Pointer to triangle selector. These are all the "world"
    /// triangles. Normally all the triangles of the env nodes.
    irr::scene::IMetaTriangleSelector* m_selector;

    /// All environment models that are currently in the scene.
    std::vector<irr::scene::IMeshSceneNode*> m_envModels;

    /// All fish models that are currently in the scene.
    std::vector<Fish*> m_fishModels;

    /// Pointer to node that is currently selected by user.
    /// If no node is selected, this pointer is 0.
    irr::scene::ISceneNode *m_selectedNode;

    /// Static camera scene node.
    irr::scene::ICameraSceneNode* m_staticCam;

    /// Camera with controls similar to Autodesk Maya cams.
    irr::scene::ICameraSceneNode* m_mayaCam;

    /// Shadow light for drawing soft shadows
    SShadowLight* m_shadowLight;

    /// Is true, if fishsim is in edit mode.
    bool m_editMode;

    /// Path of fishsim directory.
    std::string m_fishSimPath;

    /// Bounding Box of all environment models together.
    irr::core::aabbox3df m_boundingBox;

    /// Current shadow level (none or soft).
    int m_shadowLevel;

    /// Workaround until we got custom scene loader:
    /// Store some global scene settings in this node.
    irr::scene::ISceneNode* m_sceneSettingsNode;

//    ISceneNode* m_parentSaveNode;


    /*!
        \brief Add a IMeshSceneNode to scene as env model.

       \param node pointer to the node that should be added.

       \return true, if node could be added successfully, false otherwise.
     */
    bool addEnvNode(irr::scene::IMeshSceneNode* node);


    /*!
        \brief Remove a IMeshSceneNode from env models of the scene.

       \param node pointer to the node that should be deleted.

       \return true, if node could be found and remove. False otherwise.
     */
    bool removeEnvNode(irr::scene::IMeshSceneNode* node);


    /*!
        \brief Add a IAnimatedMeshSceneNode to scene as env model.

       \param node pointer to the node that should be added.
       \param fishType specify what kind of fish the node is. Right now, only
                  sailfin molly exist.

       \return true, if node could be added successfully, false otherwise.
     */
    bool addFishNode(irr::scene::IAnimatedMeshSceneNode* node, int fishType);


    /*!
        \brief Create a unique name that is not in scene.

       \param name name to be checked. If name already exist in scene, a prefix will be
                  added to the name to make it unique.

       \return an unique name that is not in the scene.
     */
    std::string makeUniqueName(const std::string &name);

    /*!
        \brief Remove all fish nodes from scene.
     */
    void removeAllFishModels();


    /*!
        \brief Remove all env nodes from scene.
     */
    void removeAllEnvModels();

    /*!
        \brief Recalculate bounding box of scene.
     */
    void recalculateBoundingBox();


    /*!
        \brief Disable soft shadows
     */
    void disableSoftShadows();


    /*!
        \brief Enable soft shadows.
     */
    void enableSoftShadows();


    /*!
        \brief Add shadow to given node
     */
    void addShadowToNode(irr::scene::ISceneNode* node, E_SHADOW_MODE mode);


    /*!
        \brief Remove shadow from given node
     */
    void removeShadowFromNode(irr::scene::ISceneNode* node);


    /*!
        \brief Enable/Disable lighting on all nodes. To draw soft shadows in XEffect,
        lighing must be disabled.
     */
    void setLightingOnNodes(bool doLighting);


    /// Returns SCENE_ELEMENT_SAILFIN_MOLLY_W, when last character of
    /// string is f, returns SCENE_ELEMENT_SAILFIN_MOLLY_M otherwise.
    int getTypeOfModel(const std::string& modelName);



    //    /*!
    //       \brief Add a point light to current scene. If you pass a direction as parameter,
    //       the point light gets then converted into a spot light. This light is not casting shadows.

    //       \param[in] lightName the name of the light
    //       \param[in] position global position of the light
    //       \param[in] color color of the light
    //       \param[in] radius radius of the light
    //       \param[in] parentNode the name of the parent node
    //       \param[in] direction direction where the light should point at

    //       \return true, if light scene node was created successfully. False otherwise
    //    */
    //    bool addLight(const std::string& lightName,
    //                  const irr::core::vector3df& position,
    //                  const irr::video::SColorf& color = irr::video::SColorf(1, 1, 1),
    //                  float radius = 100,
    //                  const std::string& parentNode = "",
    //                  const irr::core::vector3df& direction = irr::core::vector3df(0, 0, 0));


};


} // end namespace fishsim

#endif // H_FISHSIMSCENEMANAGER_H
