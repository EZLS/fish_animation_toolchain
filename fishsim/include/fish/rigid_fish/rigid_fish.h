/*! Copyright (C) 2017  	Department of Electrical Engineering & Computer Science, Institute of
 *			Real-Time Learning Systems, University of Siegen
 *
 *			Research Group of Ecology and Behavioral Biology, Institute of Biology,
 *			University of Siegen
 *
 * Contact: virtual.fish.project@gmail.com
 *
 * License: GNU General Public License  See LICENSE.txt for the full license.
 *
 * This file is part of the "FishSim Animation Toolchain".
 * "FishSim Animation Toolchain" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "FishSim Animation Toolchain" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "FishSim Animation Toolchain".  If not, see <http: *www.gnu.org/licenses/>.
 *
 * The "FishSim Animation Toolchain" was developed within the scope of the interdisciplinary,
 * DFG (Deutsche Forschungsgemeinschaft)-funded project “virtual fish” (KU
 * 689/11-1 and Wi 1531/12-1) of the Institute of Real-Time Learning Systems
 * (EZLS) and the Research Group of Ecology and Behavioral Biology at the
 * University of Siegen.
 *
 *  \file sailfin_molly.h
 *  \brief A implementation of a rigid fish that does move but does not use
 *   bone rotations
 *
*/

#ifndef H_RIGID_FISH_H
#define H_RIGID_FISH_H

#include <irrlicht.h>

#include <string>

#include "fish/fish.h"

namespace fishsim
{

class RigidFish : public Fish
{

public:


    /*!
      \brief Constructor

      \param[in] fishNode pointer to the IAnimatedMeshSceneNode that should be controlled
      as a fish model.
    */
    RigidFish(irr::scene::IAnimatedMeshSceneNode *node);



    /*!
      \brief Destructor
    */
    ~RigidFish();


    /*!
       \brief Is called, whenever a fish is added to the scene.

       Initialize the fish.

       \return True, if fish was loaded successful and all bones where found.
       False otherwise.
     */
    bool load();


    /*!
       \brief Is called, whenever a fish is removed from scene.

       \return True, if fish was removed successful.
       False otherwise.
     */
    bool unload();


    /*!
       \brief Is called, whenever the fish should move.

       Move the fish with joystick message.

       \param[in]  joystickState
     */
    void move(const sensor_msgs::Joy& joyState);


    /*!
       \brief Is called, whenever the fish should move.

        \param[in]  forewardAmount amount of foreward/backward translation, in irrlicht units
        \param[in]  sidewardAmount amount of left/right translation, in irrlicht units
        \param[in]  upwardAmount amount of up/down translation, in irrlicht units
        \param[in]  yawAmount amount of yaw rotation, in degrees
        \param[in]  pitchAmount amount of pitch rotation, in degrees
        \param[in]  rollAmount amount of roll rotation, in degrees
     */
    void move(float forewardAmount, float sidewardAmount, float upwardAmount,
              float yawAmount, float pitchAmount, float rollAmount);

private:

    /*!
       \brief Set up all editable textures.
    */
    void initEditableMaterials();

};


} // end namespace fishsim

#endif // H_RIGID_FISH_H
