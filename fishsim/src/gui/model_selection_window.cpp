#include "gui/model_selection_window.h"

using namespace irr;
using namespace core;
using namespace video;
using namespace gui;

ModelSelectionWindow::ModelSelectionWindow(irr::IrrlichtDevice *device,
                                           const std::string &fishSimPath)
{
    m_device = device;
    m_device->grab();

    m_guiEnv = m_device->getGUIEnvironment();
    m_guiEnv->grab();

    m_fishSimPath = fishSimPath;

    createModelSelectionWindow();
}

ModelSelectionWindow::~ModelSelectionWindow(){

    if(m_device)
        m_device->drop();

    if(m_guiEnv)
        m_guiEnv->drop();
}


void ModelSelectionWindow::createModelSelectionWindow(){


    ///
    /// Create window
    ///

    const int width = MODEL_SELECTION_WINDOW_WIDTH;
    const int height = MODEL_SELECTION_WINDOW_HEIGHT;
    const core::vector2di windowSartPos = core::vector2di(TOOLBOX_START_X,
                                                          TOOLBOX_START_Y);

    rect<s32> windowRect(0,0, width, height);
    windowRect.UpperLeftCorner +=  windowSartPos;
    windowRect.LowerRightCorner += windowSartPos;

    rect<s32> tabCtrlRect(2, 20,width -2,height -2);

    // Create toolbox
    m_modelSelectionWindow = m_guiEnv->addWindow(windowRect, false,
                                                 L"Select new model",
                                                 0, GUI_ID_WIN_SELECT_NEW_MODEL);

    // create tab control
    IGUITabControl* tabCtrl = m_guiEnv->addTabControl( tabCtrlRect, m_modelSelectionWindow,
                                                       true, true);


    ///
    /// Create sailfin molly tab
    ///
    IGUITab* tabSailfinMollies = tabCtrl->addTab(L"Sailfin mollies");
    m_sailfinMolliesTab = new FishSimToolboxTab(m_guiEnv, tabSailfinMollies, width, height);


    // Poecilia latipinna male normal
    std::string previewTexturePath = m_fishSimPath + "/media/PL_male_normal_prev.png";
    ITexture* previewTexture = m_device->getVideoDriver()->getTexture(previewTexturePath.c_str());
    IGUIButton* button = m_sailfinMolliesTab->appendFishTextureButton(L"",
                                                                      previewTexture,
                                                                      L"Poecilia latipinna male",
                                                                      GUI_ID_BTN_SELECT_NEW_FISH);
    button->setName(core::stringc(0));



    // Poecilia latipinna female normal
    previewTexturePath = m_fishSimPath + "/media/PL_female_normal_prev.png";
    previewTexture = m_device->getVideoDriver()->getTexture(previewTexturePath.c_str());
    button = m_sailfinMolliesTab->appendFishTextureButton(L"Poecilia latipinna female",
                                                          previewTexture,
                                                          L"Poecilia latipinna female",
                                                          GUI_ID_BTN_SELECT_NEW_FISH);
    button->setName(core::stringc(1));


    // Poecilia mexicana male normal
    previewTexturePath = m_fishSimPath + "/media/PM_male_normal_prev.png";
    previewTexture = m_device->getVideoDriver()->getTexture(previewTexturePath.c_str());
    button = m_sailfinMolliesTab->appendFishTextureButton(L"Poecilia mexicana male",
                                                          previewTexture,
                                                          L"Poecilia mexicana male",
                                                          GUI_ID_BTN_SELECT_NEW_FISH);
    button->setName(core::stringc(2));


    // Poecilia mexicana female normal
    previewTexturePath = m_fishSimPath + "/media/PM_female_normal_prev.png";
    previewTexture = m_device->getVideoDriver()->getTexture(previewTexturePath.c_str());
    button = m_sailfinMolliesTab->appendFishTextureButton(L"Poecilia mexicana female",
                                                          previewTexture,
                                                          L"Poecilia mexicana female",
                                                          GUI_ID_BTN_SELECT_NEW_FISH);
    button->setName(core::stringc(3));
}
