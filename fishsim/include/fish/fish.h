/*! Copyright (C) 2017  	Department of Electrical Engineering & Computer Science, Institute of
 *			Real-Time Learning Systems, University of Siegen
 *
 *			Research Group of Ecology and Behavioral Biology, Institute of Biology,
 *			University of Siegen
 *
 * Contact: virtual.fish.project@gmail.com
 *
 * License: GNU General Public License  See LICENSE.txt for the full license.
 *
 * This file is part of the "FishSim Animation Toolchain".
 * "FishSim Animation Toolchain" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "FishSim Animation Toolchain" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "FishSim Animation Toolchain".  If not, see <http: *www.gnu.org/licenses/>.
 *
 * The "FishSim Animation Toolchain" was developed within the scope of the interdisciplinary,
 * DFG (Deutsche Forschungsgemeinschaft)-funded project “virtual fish” (KU
 * 689/11-1 and Wi 1531/12-1) of the Institute of Real-Time Learning Systems
 * (EZLS) and the Research Group of Ecology and Behavioral Biology at the
 * University of Siegen.
 *
 *   \file   Fish.h
 *   \brief  Fish is the base class for all fish models in FishSim. All new fish types
 *           should inherit from this class.
 *
 *   Fish is a wrapper around an irrlicht IAnimatedMeshSceneNode that provides additional
 *   functionality with regards to the use as a fish model in fishsim. Fish provides access to
 *   the main attributes of the IAnimatedMeshSceneNode (like setter/getter for position,
 *   rotation, scale, name ...) and also some complex functionalities like setting up collision,
 *   handeling bone modifiers and editable materials.
 *
 *   There are two main functions that should be overriten, when implementing a new fish class.
 *   The function load() is called, whenever a fish is added to the scene. One has the opportunity
 *   to set up the scene node properly (like setting animation mode or init bone modifiers).
 *
 *   The function move(...) should be overridden, if you want to implement your own movement
 *   behaivior for your fish. See SailfinMolly::move(...) as a reference.
 *
 *   Also implemented in Fish is the snapshot interpolation (also referred as entity interpolation).
 *   FishStateHandler controls the movement of a fish scene node. It uses several recent
 *   snapshots (current states of position, rotation, scale and bone rotation) of the fish and
 *   interpolates between them. This will result in a smooth movement of the fish.
*/

#ifndef H_FISH_H
#define H_FISH_H

#include <irrlicht.h>

#include <string>
#include <vector>
#include <deque>
#include <ros/time.h>

#include <sensor_msgs/Joy.h>

#include "fish/fishsnapshot.h"
#include "fish/bone_modifier.h"

#include "utils/FIR_filter.h"

#include "irrExt/CSceneNodeAnimatorCollisionResponse.h"

namespace fishsim
{

/// Structure material with name
struct SNamedMaterial
{

    // Name of the material
    std::wstring name;

    // The irrlicht material
    irr::video::SMaterial* material;

    // Does material has a diffuse texture.
    // If set to true, the fish editor will allow
    // changing the diffuse texture (texture index 0).
    //
    // If set to false, user should change color of
    // material instead. (NOT IMPLEMENTED RIGHT NOW)
    bool hasDiffuseTexture;

    //    std::string texturePrefix;

};



class Fish
{

public:


    /*!
      \brief Constructor

      \param[in] fishNode pointer to the IAnimatedMeshSceneNode that should be controlled
      as a fish model.
    */
    Fish(irr::scene::IAnimatedMeshSceneNode *fishNode);


    /*!
      \brief Destructor
    */
    ~Fish();


    /*!
       \brief Is called, when a fish is added to the scene.

       Override this, if you need to do something on load, like initialize editable
       materials or bone modifiers.
     */
    virtual bool load();


    /*!
       \brief Is called, whenever a fish is removed from scene

       Override this, if you need to do something on unload, like delete pointers to
       modifiers or to drop bones that have been grabbed on init.
     */
    virtual bool unload();


    /*!
       \brief Is called, whenever the fish should move. Move the fish with joystick message.

       Override this, if you want to implement your own movement behaivior for your
       fish. Is used in fishsteering to move the fish via joystick.

       \param[in]  joystickState joy ros message of fishsteering.
     */
    virtual void move(const sensor_msgs::Joy& joystickState);


    /*!
       \brief Is called, whenever the fish should move.

       Override this, if you want to implement your own movement behaivior for your
       fish. All movement parameters (like forewardAmount) are relative to the fish
       current rotation (relative to fish reference frame).

        \param[in]  forewardAmount amount of foreward/backward translation, in irrlicht units
        \param[in]  sidewardAmount amount of left/right translation, in irrlicht units
        \param[in]  upwardAmount amount of up/down translation, in irrlicht units
        \param[in]  yawAmount amount of yaw rotation, in degrees
        \param[in]  pitchAmount amount of pitch rotation, in degrees
        \param[in]  rollAmount amount of roll rotation, in degrees
     */
    virtual void move(float forewardAmount, float sidewardAmount, float upwardAmount,
              float yawAmount, float pitchAmount, float rollAmount);


    /*!
       \brief Add a snapshot to the interpolation buffer. Snapshot will only be added, if
       snapshot time is greater than all snapshots in the buffer. Otherwise snapshot
       will be ignored, since it is most likely too late.

       \param[in]  snapshot The snapshot to add.
     */
    void addSnapshot(FishSnapshot &snapshot);


    /*!
       \brief Update the state of the fish to match a position and rotation of
       the fish at a given time. Uses interpolation between two snapshots
       wich will result in a smooth movement.

       \param[in]  time The current simulation time.
     */
    void updateState(ros::Time time);


    /*!
       \brief "Teleport" fish directly to snapshot state. Does not use interpolation

       \param[in] to The new state the fish gets teleported to.
     */
    void setFishStateFromSnapshot(const FishSnapshot &to);


    /*!
       \brief Create a pointer of a new FishSnapshot. A fish snapshot fully describes
       a state of the fish in time.

       \return a pointer to a new FishSnapshot, that is created from current
               state of the fish.
     */
    FishSnapshot *createNewSnapshotFromCurrentState();


    /*!
       \brief Create a triangle selector for the mesh of the fish. Is used
       for collision detection.

       \param[in] sceneMngr the current irrlicht scene manager.
                  Is used to create the selector.
     */
    void createTriangleSelector(irr::scene::ISceneManager* sceneMngr);

    /*!
       \brief Set a new triangle selector as the collision world

       \param[in] newWorld a triangle selector that contains all the "world"
                  triangles. In case of fishsim, these are all the triangles from
                  the environment nodes.
     */
    void setNewCollisionWorld(irr::scene::ITriangleSelector* newWorld){
        m_collisionAnimator->setWorld(newWorld);
    }


    /*!
       \brief Update the bounding box that is used in the collision animator
       for collision detection. Should be called after move function has
       transformed the fish.
     */
    void updateCollisionBoundingBox();


    /*!
       \brief Create the collision animator for the fish node.

       \param[in] smgr the current irrlicht scene manager.
                  The scene manager exectute the collision check.

       \param[in] worldTriangles a triangle selector that contains all the "world"
                  triangles. In case of fishsim, these are all the triangles from
                  the environment nodes.
     */
    void createCollisionResponseAnimator(irr::scene::ISceneManager* smgr,
                                         irr::scene::ITriangleSelector* worldTriangles);


    /*!
       \brief Perform collision check.

       The bunding box of CollisionAnimator is updated to the current one of the
       fish and the animateNode() function of the CollisionAnimator is then called.
     */
    void doCollisionCheck();


    /*!
       \brief Update all bone modifier of the fish depending on user input.

       \param[in] joyState current joystick state from fishsteering.
     */
    void updateBoneModifiers(const sensor_msgs::Joy &joyState);


    /*!
       \brief Make the material with given index an editable material, where user
       can change texture or color in fishsim.

       \param[in] materialIndex the material index that should be editable.
       \param[in] materialName the name of the material.
       \param[in] hasTexture if material should have a diffuse texture, set it to true.

       \return Pointer to a struct holding all information about the new editable texture.
     */
    SNamedMaterial* makeMaterialEditable(int materialIndex, const wchar_t *materialName,
                                         bool hasDiffuseTexture);

    /// Get all bone names of model.
    std::vector<std::string> getBones();

    /// Returns pointer to irrlicht IAnimatedMeshSceneNode.
    irr::scene::IAnimatedMeshSceneNode* getAnimatedMeshSceneNodePtr()
    {
        return m_irrNode;
    }

    /// Returns position of the fish node
    irr::core::vector3df getPosition()
    {
        return m_irrNode->getPosition();
    }

    /// Returns rotation of the fish node
    irr::core::vector3df getRotation()
    {
        return m_irrNode->getRotation();
    }

    /// Returns name of the fish node
    std::string getName()
    {
        return m_irrNode->getName();
    }

    /// Returns scale of fish node
    irr::core::vector3df getScale()
    {
        return m_irrNode->getScale();
    }


    /// Sets position of the fish node
    void setPosition(irr::core::vector3df pos)
    {
        m_irrNode->setPosition(pos);
    }


    /// Sets rotation of the fish node
    void setRotation(irr::core::vector3df rot)
    {
        m_irrNode->setRotation(rot);
    }

    /// Set name of fish node
    void setName(std::string name)
    {
        m_irrNode->setName(name.c_str());
    }

    /// Set scale of fish node
    void setScale(irr::core::vector3df scale){
        m_irrNode->setScale(scale);
    }

    /// Returns current triangle selector
    irr::scene::ITriangleSelector* getTriangleSelector()
    {
        return m_irrNode->getTriangleSelector();
    }


    /// Returns all editable materials
    std::vector<SNamedMaterial*> getNamedMaterials()
    {
        return m_editableMaterials;
    }

    /// Returns all bone modifier
    std::vector<BoneModifier*> getBoneModifiers()
    {
        return m_boneModifiers;
    }

    /// Add a new modifier to fish
    void addBoneModifier(BoneModifier* modifier){
        m_boneModifiers.push_back(modifier);
    }

    /// Set debug flag
    void setDebugDataVisible(irr::u32 state)
    {
        m_irrNode->setDebugDataVisible(state);
    }

    /// Get debug flag
    irr::u32 isDebugDataVisible(){
        return m_irrNode->isDebugDataVisible();
    }

    /// Set foreward speed
    float setForewardSpeed(float forewardSpeed)
    {
        m_forewardMultiplier = forewardSpeed / 10.0;
    }

    /// Set yaw speed
    float setYawSpeed(float yawSpeed)
    {
        m_yawMultiplier = yawSpeed;
    }

    /// Set pitch speed
    float setPitchSpeed(float pitchSpeed)
    {
        m_pitchMultiplier = pitchSpeed / 2.0;
    }

protected:

    /// Pointer to irrlicht scene node.
    irr::scene::IAnimatedMeshSceneNode *m_irrNode;

    /// All materials that can be edited ( where texture or color can be changed in fishsim).
    std::vector<SNamedMaterial*> m_editableMaterials;

    /// All bone modifier. These will affect one or more bones depending on joystick input
    std::vector<BoneModifier*> m_boneModifiers;

    /// Collision animator.
    irr::scene::CSceneNodeAnimatorCollisionResponse* m_collisionAnimator;

    /// Gets multiplied with joy input. Affects foreward speed
    float m_forewardMultiplier;

    /// Gets multiplied with joy input. Affects yaw speed
    float m_yawMultiplier;

    /// Gets multiplied with joy input. Affects pitch speed
    float m_pitchMultiplier;

    /// Current rotation around yaw axis in degree
    float m_currentYaw;

    /*!
       \brief Pitch fish node. Rotate it around its local z axis.

       \param[in] degs amount of rotation, in degrees
     */
    virtual void  pitch(float degs);


    /*!
       \brief Yaw fish node. Rotate it around its local y axis.

       \param[in] degs amount of rotation, in degrees
     */
    virtual void  yaw(float degs);


    /*!
       \brief Translate the fish node.

       \param[in] foreward amount of foreward translation, in irrlicht units
       \param[in] sideward amount of sideward translation, in irrlicht units
       \param[in] upward amount of upward translation, in irrlicht units
     */
    virtual void  translate(float foreward, float sideward, float upward);

    /*!
       \brief Slides the fish node.

       \param[in] slideFwdAmount amount of foreward slide, in irrlicht units
       \param[in] slideSideAmount amount of sideward slide, in irrlicht units
     */
    void slide(float slideFwdAmount, float slideSideAmount);


    /*!
       \brief Rename bones. Remove bone prefix.

       \param[in] nameOfHeadBone name of a the head bone after the renaming.
        Is us used to identify the bone prefix.
    */
    void removeBonePrefix(std::string nameOfHeadBone);


    /*!
       \brief Apply the box filter to smooth foreward amount

       \param[in] inputVal the input value that gets passed to the filter.
       \return filtered value.
    */
    float getForewardSmoothed(float inputVal);


private:

    /// The snapshot buffer that stores all recent snapshots.
    std::deque<FishSnapshot> m_snapshotBuffer;

    /// Maximum size of the snapshot buffer.
    unsigned int m_snapshotBufferSize;

    /// Time difference between clock of fishsim (Client) and
    /// fishsteering, fish_automover or fishplayer (Server).
    /// Is used as a simple clock sync mechanism.
    int64_t m_timeDiff;

    /// FIR Box filter that smoothes foreward movement.
    FIRFilter* m_forewardSmoothFilter;

    /*!

       \brief Use interpolation to define a new state of the fish between two given snapshots.

       \param[in] from Start state of the interpolation.

       \param[in] to End state of the interpolation.

       \param[in] frac Interpolation value between 0.0f (all state from) and 1.0f (all state to)
     */
    void transformFish(const FishSnapshot &from, const FishSnapshot &to, float frac);

};


} // end namespace fishsim

#endif // H_FISH_H
