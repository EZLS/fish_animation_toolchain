/*! Copyright (C) 2017  	Department of Electrical Engineering & Computer Science, Institute of
 *			Real-Time Learning Systems, University of Siegen
 *
 *			Research Group of Ecology and Behavioral Biology, Institute of Biology,
 *			University of Siegen
 *
 * Contact: virtual.fish.project@gmail.com
 *
 * License: GNU General Public License  See LICENSE.txt for the full license.
 *
 * This file is part of the "FishSim Animation Toolchain".
 * "FishSim Animation Toolchain" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "FishSim Animation Toolchain" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "FishSim Animation Toolchain".  If not, see <http: *www.gnu.org/licenses/>.
 *
 * The "FishSim Animation Toolchain" was developed within the scope of the interdisciplinary,
 * DFG (Deutsche Forschungsgemeinschaft)-funded project “virtual fish” (KU
 * 689/11-1 and Wi 1531/12-1) of the Institute of Real-Time Learning Systems
 * (EZLS) and the Research Group of Ecology and Behavioral Biology at the
 * University of Siegen.
 *
 *  \file dorsal_fin_modifier.h
 *  \brief Bone modifer for the pelvic spine of sticklebacks. Can be used in
 *   recording mode and in editing mode.
*/



#ifndef H_PELVIC_SPINE_MODIFIER_H
#define H_PELVIC_SPINE_MODIFIER_H

#include <irrlicht.h>
#include <string>
#include <sensor_msgs/Joy.h>

#include "fish/bone_modifier.h"

namespace fishsim
{

class PelvicSpineModifier : public BoneModifier
{

public:

    /// Constructor
    PelvicSpineModifier(irr::scene::IAnimatedMeshSceneNode* fishNode,
                      std::string name, float speed = 1.0, bool isActive = true);

    /// Destructor
    ~PelvicSpineModifier();

    /// Initialize the modifier
    bool init();    

    /// Update with joy message
    void update(const sensor_msgs::Joy& joystickState);

    /// Update with single message
    void update(float inputVal1, float inputVal2 = 0, float inputVal3 = 0);

    /// Modify based on existing snapshots
    void modify(FishSnapshot* startState,
                FishSnapshot* endState,
                const sensor_msgs::Joy& joystickState );


private:

    /// Bones of the pelvic spine (left and right)
    irr::scene::IBoneSceneNode *m_pelvicSpineLeft;
    irr::scene::IBoneSceneNode *m_pelvicSpineRight;

    /// Current rotation that is added on x axis
    float m_currentXRotation;

    /// Rotate dorsal fin either in or out
    void changePelvicSpines(int dir);
};

} // end namespace fishsim

#endif // H_PELVIC_SPINE_MODIFIER_H
