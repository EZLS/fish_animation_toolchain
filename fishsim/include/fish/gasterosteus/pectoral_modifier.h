/*! Copyright (C) 2017  	Department of Electrical Engineering & Computer Science, Institute of
 *			Real-Time Learning Systems, University of Siegen
 *
 *			Research Group of Ecology and Behavioral Biology, Institute of Biology,
 *			University of Siegen
 *
 * Contact: virtual.fish.project@gmail.com
 *
 * License: GNU General Public License  See LICENSE.txt for the full license.
 *
 * This file is part of the "FishSim Animation Toolchain".
 * "FishSim Animation Toolchain" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "FishSim Animation Toolchain" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "FishSim Animation Toolchain".  If not, see <http: *www.gnu.org/licenses/>.
 *
 * The "FishSim Animation Toolchain" was developed within the scope of the interdisciplinary,
 * DFG (Deutsche Forschungsgemeinschaft)-funded project “virtual fish” (KU
 * 689/11-1 and Wi 1531/12-1) of the Institute of Real-Time Learning Systems
 * (EZLS) and the Research Group of Ecology and Behavioral Biology at the
 * University of Siegen.
 *
 *  \file pectoral_modifier.h
 *  \brief Bone modifer that plays pectoral animation when fish is moving. Only for sticklebacks.
 *
 * Is only be used directly in move function, thus cannot be used in edit mode in fishsteering.
*/

#ifndef H_PECTORAL_MODIFIER_H
#define H_PECTORAL_MODIFIER_H

#include <irrlicht.h>
#include <string>
#include <sensor_msgs/Joy.h>

#include "fish/bone_modifier.h"

#include "sim/fishsim_helper_routines.h"


namespace fishsim
{
class PectoralModifier : public BoneModifier
{

public:

    /// Constructor
    PectoralModifier(irr::scene::IAnimatedMeshSceneNode* fishNode,
                 std::string name, float speed = 1.0, bool isActive = true);

    /// Destructor
    ~PectoralModifier();

     /// Init modifier.
    bool init();    

    /// Update with joy message.
    void update(const sensor_msgs::Joy& joystickState);

    /// Update with single message.
    void update(float inputVal1, float inputVal2 = 0, float inputVal3 = 0);


    /// Modify based on existing snapshots
    void modify(FishSnapshot* startState,
                FishSnapshot* endState,
                const sensor_msgs::Joy& joystickState );


private:

    /// To do the idle animation, two sinus function are used
    /// to rotate bones back and forth. The second sinus
    /// has a phase shift of 90 deg to rotate in another
    /// direction than the first one.

    /// Current input to the sinus functions of the tail.
    /// Gets increased every frame, but is reset to 0.0,
    /// if fish starts moving.
    float m_timerTail;

    /// Previous output value of sinus 1 (no phase shift).
    /// used for tail fin.
    float m_prevTailSinus1;

    /// Previous output value of sinus 2 (90 deg phase shift).
    /// used for tail fin.
    float m_prevTailSinus2;

    /// Current input to the sinus functions of the side fins.
    /// Gets increased every frame. Gets not reset! Side fins
    /// play animation all the time
    float m_timerSide;

    /// Previous output value of sinus 1 (no phase shift).
    /// used for side fins.
    float m_prevSideSinus1;

    /// Previous output value of sinus 2 (90 deg phase shift).
    /// used for side fins.
    float m_prevSideSinus2;

    /// Vertical fins right bones
    irr::scene::IBoneSceneNode *m_vertical_fin_up_right1;
    irr::scene::IBoneSceneNode *m_vertical_fin_middle_right1;
    irr::scene::IBoneSceneNode *m_vertical_fin_down_right1;

    /// Vertical fins left bones
    irr::scene::IBoneSceneNode *m_vertical_fin_up_left1;
    irr::scene::IBoneSceneNode *m_vertical_fin_middle_left1;
    irr::scene::IBoneSceneNode *m_vertical_fin_down_left1;



};


} // end namespace fishsim

#endif // H_PECTORAL_MODIFIER_H
