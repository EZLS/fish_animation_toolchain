/*! Copyright (C) 2017  	Department of Electrical Engineering & Computer Science, Institute of
 *			Real-Time Learning Systems, University of Siegen
 *
 *			Research Group of Ecology and Behavioral Biology, Institute of Biology,
 *			University of Siegen
 *
 * Contact: virtual.fish.project@gmail.com
 *
 * License: GNU General Public License  See LICENSE.txt for the full license.
 *
 * This file is part of the "FishSim Animation Toolchain".
 * "FishSim Animation Toolchain" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "FishSim Animation Toolchain" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "FishSim Animation Toolchain".  If not, see <http: *www.gnu.org/licenses/>.
 *
 * The "FishSim Animation Toolchain" was developed within the scope of the interdisciplinary,
 * DFG (Deutsche Forschungsgemeinschaft)-funded project “virtual fish” (KU
 * 689/11-1 and Wi 1531/12-1) of the Institute of Real-Time Learning Systems
 * (EZLS) and the Research Group of Ecology and Behavioral Biology at the
 * University of Siegen.
*/
#ifndef H_UTILS_H
#define H_UTILS_H

#include <QString>


inline QString generateTimeString(float durationInSec)
{
    int min = static_cast<int>(durationInSec / 60.0);
    int sec = static_cast<int>(durationInSec) % 60;

    // Generate time string.
    QString timeString =  QString("%2").arg(min,2, 10, QChar('0'))
            + ":" +  QString("%2").arg(sec,2, 10, QChar('0'));

    return timeString;
}


inline float getTimeFromTimeString(QString timeString)
{
    QStringList l = timeString.split(":");

    if(l.length() ==2 )
    {
        int mins = l.at(0).toInt();
        int secs = l.at(1).toInt();
        float durInSec = mins * 60 + secs;

        return durInSec;
    }
    else
    {
        return 0.0;
    }
}

inline std::string getBaseFilename(std::string path)
{
    return path.substr(path.find_last_of("/\\") + 1);
}

#endif
