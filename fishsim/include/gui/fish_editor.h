/*! Copyright (C) 2017  	Department of Electrical Engineering & Computer Science, Institute of
 *			Real-Time Learning Systems, University of Siegen
 *
 *			Research Group of Ecology and Behavioral Biology, Institute of Biology,
 *			University of Siegen
 *
 * Contact: virtual.fish.project@gmail.com
 *
 * License: GNU General Public License  See LICENSE.txt for the full license.
 *
 * This file is part of the "FishSim Animation Toolchain".
 * "FishSim Animation Toolchain" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "FishSim Animation Toolchain" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "FishSim Animation Toolchain".  If not, see <http: *www.gnu.org/licenses/>.
 *
 * The "FishSim Animation Toolchain" was developed within the scope of the interdisciplinary,
 * DFG (Deutsche Forschungsgemeinschaft)-funded project “virtual fish” (KU
 * 689/11-1 and Wi 1531/12-1) of the Institute of Real-Time Learning Systems
 * (EZLS) and the Research Group of Ecology and Behavioral Biology at the
 * University of Siegen.
 *
 *  \file fish_editor.h
 *  \brief A toolbox window that allows changing various aspect of a selected fish, like name, size and texture.
 *
*/

#ifndef H_FISH_EDITOR_H
#define H_FISH_EDITOR_H

#include <string>
#include <vector>

#include <irrlicht.h>

#include "IGUIButton.h"
#include "IGUIEnvironment.h"
#include "IGUIElement.h"
#include "IGUIWindow.h"

#include "fish/fish.h"

#include "irrExt/CGUITextureOpenDialog.h"

#include "gui/fishsim_toolbox_tab.h"


namespace fishsim
{
class FishEditor
{

public:

    /*!
     * \brief Custom constructor.
     * \param device Pointer to irrlicht device.
     * \param fish Pointer to the fish that will be edited.
     * \param fishSimPath path where fishSim is located. Used to load fish textures.
     */
    FishEditor(irr::IrrlichtDevice* device, Fish* fish, const std::string& fishSimPath);

    /*!
      \brief Destructor
      */
    ~FishEditor();


    /*!
     * \brief Open the texture dialog window.
     * \param index Material index of the that should be edited.
     */
    void openTextureDialog(const irr::core::stringc &index);

    /*!
     * \brief Save the fish to a file.
     * \param filePath file to save the fish.
     */
    void saveFish(std::string filePath);


    /*!
     * \brief Apply all changes that have been made in the window.
     */
    void applyConfigTabChanges();

    /*!
     * \brief Change texture of material to the texture that has been selected in
     *  the texture window.
     */
    void changeTextureToSelected();


    void shapeMe(int pos);


private:

    /// Pointer to irrlicht device
    irr::IrrlichtDevice* m_device;

    /// Pointer to irrlicht gui environment
    irr::gui::IGUIEnvironment* m_guiEnv;

    /// Fish that is currently edited
    Fish* m_fish;

    /// The toolbox window
    irr::gui::IGUIWindow* m_winToolbox;

    /// The texture dialog window
    irr::gui::CGUITextureOpenDialog *m_textureDialog;

    /// Position edit boxes
    irr::gui::IGUIEditBox* m_edtBoxPosX;
    irr::gui::IGUIEditBox* m_edtBoxPosY;
    irr::gui::IGUIEditBox* m_edtBoxPosZ;

    /// Scale edit boxes
    irr::gui::IGUIEditBox *m_edtBoxScaleX;
    irr::gui::IGUIEditBox *m_edtBoxScaleY;
    irr::gui::IGUIEditBox *m_edtBoxScaleZ;

    /// Name edit box
    irr::gui::IGUIEditBox *m_edtBoxName;

    /// Toggle show mesh box
    irr::gui::IGUICheckBox *m_checkBoxShowMesh;

    ///Shadow settings
    irr::gui::IGUICheckBox *m_checkBoxCastShadows;

    /// When user opens texture chooser,
    /// the index of the material that is currently
    /// edited is stored here.
    int m_selectedMaterialIndex;

    /// Path where fishsim is located
    std::string m_fishSimPath;

    /// Texture that will be shown on fish, if the
    /// material should have a texture but was not found.
    irr::video::ITexture* m_notFoundTexture;

    /// Config tab
    FishSimToolboxTab* m_configTab;

    /// Texture tab
    FishSimToolboxTab* m_textureTab;

    /// Pointer to texture buttons to change its texture in
    /// callback.
    std::vector<irr::gui::IGUIButton*> m_textureButtons;

    /*!
     * \brief Updates the config tab of the toolbox.
     */
    void updateConfigTab();

    /*!
     * \brief Creates the toolbox.
     */
    void createEditToolbox();

};


} // end namespace fishsim

#endif // H_FISH_EDITOR_H

