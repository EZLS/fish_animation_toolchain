#ifndef IRRQWIDGET_H
#define IRRQWIDGET_H
//-------------includes-----------------//
#include <QtGui>
#include <irrlicht1.8/irrlicht.h>
#include "QIrrlichtWidget.h"

#ifndef Q_MOC_RUN
#include <ros/ros.h>
#endif


//----------------namespaces------------//
using namespace irr;
using namespace irr::video;
using namespace irr::core;
using namespace irr::scene;
using namespace irr::gui;
//---------------classe------------------//
class QIrrlichtWidget : public QWidget
{
    Q_OBJECT
public:

    QIrrlichtWidget(QWidget *parent = 0);
//--------------------------------funcoes-----------------------------------------//
    void createIrrlichtDevice(QWidget *window = 0, int winWidth = 640, int winHeight = 360);
    void buildIrrlichtScene();
    void drawIrrlichtScene();
    void sendMouseEventToIrrlicht( QMouseEvent* event);
    void mousePressEvent( QMouseEvent* event );
    void mouseMoveEvent(QMouseEvent* event );
    void mouseReleaseEvent( QMouseEvent* event );

    IrrlichtDevice *getIrrlichtDevice(){ return device;  }
    ISceneManager *getIrrlichtScene(){ return cena;  }
    IVideoDriver *getIrrlichtDrive(){ return driver;  }

    virtual void paintEvent( QPaintEvent *event );
    virtual void resizeEvent( QResizeEvent *event );
    virtual QPaintEngine * paintEngine() const;

    int MANIPULADOR;
    bool maniATIVA;
//signals:
   //void SinalMouseEvent(double a);

//public slots:


protected:
    E_DRIVER_TYPE driverType;
private:
    IrrlichtDevice *device;
    ISceneManager *cena;
    IVideoDriver *driver;
    IGUIEnvironment *env;
    scene::ISceneNode* no_cubo;
    scene::ICameraSceneNode* camera;
    double xI, yI, zI;
    double mouseXi,mouseYi,dx,dy;
};
#endif // IRRQWIDGET_H
