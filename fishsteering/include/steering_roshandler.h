/*! Copyright (C) 2017  	Department of Electrical Engineering & Computer Science, Institute of
 *			Real-Time Learning Systems, University of Siegen
 *
 *			Research Group of Ecology and Behavioral Biology, Institute of Biology,
 *			University of Siegen
 *
 * Contact: virtual.fish.project@gmail.com
 *
 * License: GNU General Public License  See LICENSE.txt for the full license.
 *
 * This file is part of the "FishSim Animation Toolchain".
 * "FishSim Animation Toolchain" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "FishSim Animation Toolchain" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "FishSim Animation Toolchain".  If not, see <http: *www.gnu.org/licenses/>.
 *
 * The "FishSim Animation Toolchain" was developed within the scope of the interdisciplinary,
 * DFG (Deutsche Forschungsgemeinschaft)-funded project “virtual fish” (KU
 * 689/11-1 and Wi 1531/12-1) of the Institute of Real-Time Learning Systems
 * (EZLS) and the Research Group of Ecology and Behavioral Biology at the
 * University of Siegen.
 *
*/
#ifndef H_STEERING_ROSHANDLER_H
#define H_STEERING_ROSHANDLER_H

#include <ros/ros.h>

#include <boost/filesystem.hpp>
#include <sensor_msgs/Joy.h>
#include <ros/timer.h>

#include <fishsim/FishSnapshotStamped.h>
#include <boost/thread.hpp>
#include <boost/filesystem.hpp>
#include <boost/lexical_cast.hpp>

#include "fish/fish_animation.h"
#include "fish/fishsnapshot.h"

namespace fishsteering
{
class SteeringROSHandler
{
public:
    SteeringROSHandler();

    ~SteeringROSHandler();

    void joyCallback(const sensor_msgs::Joy::ConstPtr& joy);

    sensor_msgs::Joy getJoystickState();


    bool isJoystickDataAvailable();

    void publishSnapshot(const fishsim::FishSnapshot* snapshot);

    bool writeAnimationsToBag(const std::string& filename, std::vector<fishsim::FishAnimation*> animations);

private:
    ros::NodeHandle* m_nodeHandle;

    ros::Publisher m_snapshotPublisher;
    ros::Subscriber m_joySubscriber;
    ros::AsyncSpinner* m_asyncSpinner;

    sensor_msgs::Joy m_joyState;

    boost::mutex m_joyMutex;


};
}
#endif // H_STEERING_ROSHANDLER_H
