/*! Copyright (C) 2017  	Department of Electrical Engineering & Computer Science, Institute of
 *			Real-Time Learning Systems, University of Siegen
 *
 *			Research Group of Ecology and Behavioral Biology, Institute of Biology,
 *			University of Siegen
 *
 * Contact: virtual.fish.project@gmail.com
 *
 * License: GNU General Public License  See LICENSE.txt for the full license.
 *
 * This file is part of the "FishSim Animation Toolchain".
 * "FishSim Animation Toolchain" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "FishSim Animation Toolchain" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "FishSim Animation Toolchain".  If not, see <http: *www.gnu.org/licenses/>.
 *
 * The "FishSim Animation Toolchain" was developed within the scope of the interdisciplinary,
 * DFG (Deutsche Forschungsgemeinschaft)-funded project “virtual fish” (KU
 * 689/11-1 and Wi 1531/12-1) of the Institute of Real-Time Learning Systems
 * (EZLS) and the Research Group of Ecology and Behavioral Biology at the
 * University of Siegen.
 *
 *  \file fishsim_globals
 *  \brief Some global constants that are used in fishsim. Mainly GUI IDs and filepaths.
 *
*/
#ifndef H_FISHSIM_GLOBALS_H
#define H_FISHSIM_GLOBALS_H

#include <irrlicht.h>

#include <string>
#include <ros/time.h>
#include <geometry_msgs/Vector3.h>


namespace fishsim
{
/// Lighting
const irr::video::SColor DEFAULT_BACKGROUND_COLOR =
        irr::video::SColor(0,170,170,170);

const irr::video::SColor DEFAULT_LIGHT_COLOR =
        irr::video::SColor(255, 255, 255, 255);

const irr::video::SColor DEFAULT_AMBIENT_COLOR =
        irr::video::SColor(255, 220, 220, 220);

/// Camera
const float DEFAULT_CAM_FOV = irr::core::DEGTORAD * 22;
const float DEFAULT_CAM_ASPECT_RATIO =  16.0 / 9.0;


/// Filepaths, relative to fish sim path
const std::string DEFAULT_SCENE_PATH = "/scenes/default_scene.scene";
const std::string TEMP_SCENE_PATH = "/scenes/.temp_scene.scene";
const std::string MODELS_PATH = "/models";
const std::string GLOBAL_FONT_PATH = "/media/fishSimFont.xml";
const std::string MEDIA_PATH = "/media";

/// Default toolbox width
const int TOOLBOX_WITDH = 200;

/// Default toolbox height
const int TOOLBOX_HEIGHT = 750;

/// Default start position of toolbox
const int TOOLBOX_START_X = 200;
const int TOOLBOX_START_Y = 200;

/// Offset from top, left and right border
const int TOOLBOX_BORDER_OFFSET = 10;

/// Heigth of one standart element
const int TOOLBOX_STANDART_ELEMENT_HEIGHT =  30;

// Help window
const int HELP_WINDOW_POS_X = 500;
const int HELP_WINDOW_POS_Y = 250;

const int HELP_WINDOW_WIDTH = 360;
const int HELP_WINDOW_HEIGHT = 770;

const std::wstring VERSION = L"0.1.0";

/// Enum to identify scene elements
enum E_SCENE_ELEMENT {
    ROOT_SCENE_NODE = 0,
    ENVIRONMENT,
    STATIC_CAM,
    MAYA_CAM,
    SHADOW_LIGHT,
    POECILIA_LATIPINNA_M,
    POECILIA_LATIPINNA_F,
    STICKLEBACK_M,
    STICKLEBACK_W,
    ENVIRONMENT_LIGHT,
    RIGID_FISH,
    SCENE_SETTINGS_NODE,
    POECILIA_MEXICANA_M,
    POECILIA_MEXICANA_F,
    POECILIA_RETICULATA_M,
    POECILIA_RETICULATA_F,
    GASTEROSTEUS_ACULEATUS_M,
    CYNOSCION_NEBULOSUS_M,
    GASTEROSTEUS_ACULEATUS_F,
    MARINE_STICKLEBACK,
    PUNGITIUS_PUNGITIUS_M,
};

/// Shadow settings
enum
{
    SHADOW_SETTING_NONE = 101,
    SHADOW_SETTING_HARD,
    SHADOW_SETTING_SOFT
};

/// Values that are used to identify individual GUI controls.
enum E_FISHSIM_GUI_ID
{
    // Menu and toolbar icons
    GUI_ID_MENU_QUIT = 201,
    GUI_ID_MENU_LOAD_FISH,
    GUI_ID_MENU_ADD_STATIC_OBJ,
    GUI_ID_MENU_DELETE_SELECTION,
    GUI_ID_MENU_SAVE_SCENE,
    GUI_ID_MENU_LOAD_SCENE,
    GUI_ID_MENU_HELP,
    GUI_ID_MENU_DISABLE_LIGHTS,
    GUI_ID_MENU_ENABLE_LIGHTS,
    GUI_ID_MENU_EDIT_FISH,
    GUI_ID_MENU_OPEN_CAMERA_SETTINGS,
    GUI_ID_MENU_OPEN_LIGHT_SETTINGS,
    GUI_ID_MENU_DELETE_NODE,

    // Windows
    GUI_ID_WIN_FISH_SETTINGS,
    GUI_ID_WIN_LIGHT_SETTINGS,
    GUI_ID_WIN_CAMERA_SETTINGS,
    GUI_ID_WIN_HELP,

    /// Buttons. Does not include toolbar buttons.
    /// Toolbar buttons can be found under GUI_ID_MENU

    // Fish editor
    GUI_ID_BTN_SET_SCALE,
    GUI_ID_BTN_CHANGE_TEXTURE,
    GUI_ID_BTN_SAVE_FISH,
    GUI_ID_CBOX_SHOW_MESH,

    // Light editor
    GUI_ID_BTN_SET_LIGHT_POSITION,
    GUI_ID_BTN_SET_LIGHT_QUALITY,
    GUI_ID_BTN_SET_LIGHT_COLOR,

    // Camera editor
    GUI_ID_BTN_SET_CAMERA_SETTINGS,
    GUI_ID_BTN_SET_STATIC_CAM,

    // Fish texture listbox
    GUI_ID_LISTBOX_FISH_TEXTURES,

    GUI_ID_PELVIC_SPINE_SCROLLBAR,

};

/// Values that are used to identify individual file dialogs
enum E_FISHSIM_FILE_DIALOG_ID
{
    FILE_DIALOG_ADD_STATIC_OBJ = 301,
    FILE_DIALOG_LOAD_FISH,
    FILE_DIALOG_SAVE_FISH,
    FILE_DIALOG_SAVE_SCENE,
    FILE_DIALOG_LOAD_SCENE,
    FILE_DIALOG_CHOOSE_TEXTURE

};

} // End namespace fishsim



#endif //H_FISHSIM_GLOBALS_H
