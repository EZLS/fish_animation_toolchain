// Copyright (C) 2002-2012 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

#include "irrExt/CGUITextureListBox.h"
#ifdef _IRR_COMPILE_WITH_GUI_
#include "IrrlichtDevice.h"


namespace irr
{
namespace gui
{

//! constructor
CGUITextureListBox::CGUITextureListBox(IGUIEnvironment* environment, IGUIElement* parent,  ITimer *timer,
			s32 id, core::rect<s32> rectangle, bool clip,
			bool drawBack, bool moveOverSelect)
: IGUIElement(EGUIET_LIST_BOX,environment, parent, id, rectangle),
    ItemHeight(256),
    TotalItemHeight(0), ItemsTextureWidth(256), Font(0),
    ScrollBar(0), selectTime(0), LastKeyTime(0), Selecting(false), DrawBack(drawBack),
	MoveOverSelect(moveOverSelect), AutoScroll(true), HighlightWhenNotFocused(true)
{
	#ifdef _DEBUG
	setDebugName("CGUIListBox");
	#endif

	IGUISkin* skin = Environment->getSkin();
	const s32 s = skin->getSize(EGDS_SCROLLBAR_SIZE);

    ScrollBar = environment->addScrollBar(false,
          core::rect<s32>(RelativeRect.getWidth() - s, 0, RelativeRect.getWidth(), RelativeRect.getHeight()) , this);
    ScrollBar->setNotClipped(!clip);

    ScrollBar->setSubElement(true);
    ScrollBar->setTabStop(false);
    ScrollBar->setAlignment(EGUIA_LOWERRIGHT, EGUIA_LOWERRIGHT, EGUIA_UPPERLEFT, EGUIA_LOWERRIGHT);
    ScrollBar->setVisible(false);
    ScrollBar->setPos(0);

	setNotClipped(!clip);

	// this element can be tabbed to
	setTabStop(true);
	setTabOrder(-1);

    Timer = timer;

	updateAbsolutePosition();
}


//! destructor
CGUITextureListBox::~CGUITextureListBox()
{
    if (ScrollBar)
        ScrollBar->drop();

	if (Font)
		Font->drop();

}


//! returns amount of list items
u32 CGUITextureListBox::getItemCount() const
{
	return Items.size();
}


//! Add a texture and return id of item
u32 CGUITextureListBox::addItem(const wchar_t *text, video::ITexture *texture){

    ListItem i;
    i.text = text;
    i.texture = texture;

    Items.push_back(i);

    recalculateItemHeight();

    return Items.size() - 1;

}



s32 CGUITextureListBox::getItemAt(s32 xpos, s32 ypos) const
{
	if ( 	xpos < AbsoluteRect.UpperLeftCorner.X || xpos >= AbsoluteRect.LowerRightCorner.X
		||	ypos < AbsoluteRect.UpperLeftCorner.Y || ypos >= AbsoluteRect.LowerRightCorner.Y
		)
		return -1;

	if ( ItemHeight == 0 )
		return -1;

    s32 item = ((ypos - AbsoluteRect.UpperLeftCorner.Y - 1) + ScrollBar->getPos()) / ItemHeight;
	if ( item < 0 || item >= (s32)Items.size())
		return -1;

	return item;
}

//! clears the list
void CGUITextureListBox::clear()
{
    Items.clear();
	Selected = -1;

    if (ScrollBar)
        ScrollBar->setPos(0);

	recalculateItemHeight();
}


void CGUITextureListBox::recalculateItemHeight()
{
	IGUISkin* skin = Environment->getSkin();

	if (Font != skin->getFont())
	{
		if (Font)
			Font->drop();

		Font = skin->getFont();

		if (Font)
		{
			Font->grab();
		}
	}

	TotalItemHeight = ItemHeight * Items.size();
    ScrollBar->setMax( core::max_(0, TotalItemHeight - AbsoluteRect.getHeight()) );
	s32 minItemHeight = ItemHeight > 0 ? ItemHeight : 1;
    ScrollBar->setSmallStep ( minItemHeight );
    ScrollBar->setLargeStep ( 2*minItemHeight );

	if ( TotalItemHeight <= AbsoluteRect.getHeight() )
        ScrollBar->setVisible(false);
	else
        ScrollBar->setVisible(true);
}


//! returns id of selected item. returns -1 if no item is selected.
s32 CGUITextureListBox::getSelected() const
{
	return Selected;
}

//! sets the selected item. Set this to -1 if no item should be selected
void CGUITextureListBox::setSelected(s32 id)
{
    if ((u32)id>=Items.size())
        Selected = -1;
    else
        Selected = id;

    selectTime = Timer->getTime();

    recalculateScrollPos();
}


//! called if an event happened.
bool CGUITextureListBox::OnEvent(const SEvent& event)
{
	if (isEnabled())
	{
		switch(event.EventType)
		{
		case EET_KEY_INPUT_EVENT:
			if (event.KeyInput.PressedDown &&
				(event.KeyInput.Key == KEY_DOWN ||
				event.KeyInput.Key == KEY_UP   ||
				event.KeyInput.Key == KEY_HOME ||
				event.KeyInput.Key == KEY_END  ||
				event.KeyInput.Key == KEY_NEXT ||
				event.KeyInput.Key == KEY_PRIOR ) )
			{
				s32 oldSelected = Selected;
				switch (event.KeyInput.Key)
				{
					case KEY_DOWN:
						Selected += 1;
						break;
					case KEY_UP:
						Selected -= 1;
						break;
					case KEY_HOME:
						Selected = 0;
						break;
					case KEY_END:
						Selected = (s32)Items.size()-1;
						break;
					case KEY_NEXT:
						Selected += AbsoluteRect.getHeight() / ItemHeight;
						break;
					case KEY_PRIOR:
						Selected -= AbsoluteRect.getHeight() / ItemHeight;
						break;
					default:
						break;
				}
				if (Selected<0)
					Selected = 0;
				if (Selected >= (s32)Items.size())
					Selected = Items.size() - 1;	// will set Selected to -1 for empty listboxes which is correct
				

				recalculateScrollPos();

				// post the news

				if (oldSelected != Selected && Parent && !Selecting && !MoveOverSelect)
				{
					SEvent e;
					e.EventType = EET_GUI_EVENT;
					e.GUIEvent.Caller = this;
					e.GUIEvent.Element = 0;
					e.GUIEvent.EventType = EGET_LISTBOX_CHANGED;
					Parent->OnEvent(e);
				}

				return true;
			}
			else
			if (!event.KeyInput.PressedDown && ( event.KeyInput.Key == KEY_RETURN || event.KeyInput.Key == KEY_SPACE ) )
			{
				if (Parent)
				{
					SEvent e;
					e.EventType = EET_GUI_EVENT;
					e.GUIEvent.Caller = this;
					e.GUIEvent.Element = 0;
					e.GUIEvent.EventType = EGET_LISTBOX_SELECTED_AGAIN;
					Parent->OnEvent(e);
				}
				return true;
			}
			else if (event.KeyInput.PressedDown && event.KeyInput.Char)
			{
				// change selection based on text as it is typed.
                u32 now = Timer->getTime();

				if (now - LastKeyTime < 500)
				{
					// add to key buffer if it isn't a key repeat
					if (!(KeyBuffer.size() == 1 && KeyBuffer[0] == event.KeyInput.Char))
					{
						KeyBuffer += L" ";
						KeyBuffer[KeyBuffer.size()-1] = event.KeyInput.Char;
					}
				}
				else
				{
					KeyBuffer = L" ";
					KeyBuffer[0] = event.KeyInput.Char;
				}
				LastKeyTime = now;

				// find the selected item, starting at the current selection
				s32 start = Selected;
				// dont change selection if the key buffer matches the current item
				if (Selected > -1 && KeyBuffer.size() > 1)
				{
					if (Items[Selected].text.size() >= KeyBuffer.size() &&
						KeyBuffer.equals_ignore_case(Items[Selected].text.subString(0,KeyBuffer.size())))
						return true;
				}

				s32 current;
				for (current = start+1; current < (s32)Items.size(); ++current)
				{
					if (Items[current].text.size() >= KeyBuffer.size())
					{
						if (KeyBuffer.equals_ignore_case(Items[current].text.subString(0,KeyBuffer.size())))
						{
							if (Parent && Selected != current && !Selecting && !MoveOverSelect)
							{
								SEvent e;
								e.EventType = EET_GUI_EVENT;
								e.GUIEvent.Caller = this;
								e.GUIEvent.Element = 0;
								e.GUIEvent.EventType = EGET_LISTBOX_CHANGED;
								Parent->OnEvent(e);
							}
							setSelected(current);
							return true;
						}
					}
				}
				for (current = 0; current <= start; ++current)
				{
					if (Items[current].text.size() >= KeyBuffer.size())
					{
						if (KeyBuffer.equals_ignore_case(Items[current].text.subString(0,KeyBuffer.size())))
						{
							if (Parent && Selected != current && !Selecting && !MoveOverSelect)
							{
								Selected = current;
								SEvent e;
								e.EventType = EET_GUI_EVENT;
								e.GUIEvent.Caller = this;
								e.GUIEvent.Element = 0;
								e.GUIEvent.EventType = EGET_LISTBOX_CHANGED;
								Parent->OnEvent(e);
							}
							setSelected(current);
							return true;
						}
					}
				}

				return true;
			}
			break;

		case EET_GUI_EVENT:
			switch(event.GUIEvent.EventType)
			{
			case gui::EGET_SCROLL_BAR_CHANGED:
                if (event.GUIEvent.Caller == ScrollBar)
					return true;
				break;
			case gui::EGET_ELEMENT_FOCUS_LOST:
				{
					if (event.GUIEvent.Caller == this)
						Selecting = false;
				}
			default:
			break;
			}
			break;

		case EET_MOUSE_INPUT_EVENT:
            {
				core::position2d<s32> p(event.MouseInput.X, event.MouseInput.Y);

				switch(event.MouseInput.Event)
				{
				case EMIE_MOUSE_WHEEL:
                    ScrollBar->setPos(ScrollBar->getPos() + (event.MouseInput.Wheel < 0 ? -1 : 1)*-ItemHeight/2);
					return true;

				case EMIE_LMOUSE_PRESSED_DOWN:
				{
					Selecting = true;
					return true;
				}

				case EMIE_LMOUSE_LEFT_UP:
				{
                    Selecting = false;
					if (isPointInside(p))
						selectNew(event.MouseInput.Y);

					return true;
				}

				case EMIE_MOUSE_MOVED:
					if (Selecting || MoveOverSelect)
					{
						if (isPointInside(p))
						{
							selectNew(event.MouseInput.Y, true);
							return true;
						}
					}
				default:
				break;
				}
			}
			break;
		case EET_LOG_TEXT_EVENT:
		case EET_USER_EVENT:
		case EET_JOYSTICK_INPUT_EVENT:
		case EGUIET_FORCE_32_BIT:
			break;
		}
	}

	return IGUIElement::OnEvent(event);
}


void CGUITextureListBox::selectNew(s32 ypos, bool onlyHover)
{
    u32 now = Timer->getTime();
    s32 oldSelected = Selected;

	Selected = getItemAt(AbsoluteRect.UpperLeftCorner.X, ypos);
	if (Selected<0 && !Items.empty())
		Selected = 0;

	recalculateScrollPos();

	gui::EGUI_EVENT_TYPE eventType = (Selected == oldSelected && now < selectTime + 500) ? EGET_LISTBOX_SELECTED_AGAIN : EGET_LISTBOX_CHANGED;
	selectTime = now;
	// post the news
	if (Parent && !onlyHover)
	{
		SEvent event;
		event.EventType = EET_GUI_EVENT;
		event.GUIEvent.Caller = this;
		event.GUIEvent.Element = 0;
		event.GUIEvent.EventType = eventType;
		Parent->OnEvent(event);
	}
}


//! Update the position and size of the listbox, and update the scrollbar
void CGUITextureListBox::updateAbsolutePosition()
{
	IGUIElement::updateAbsolutePosition();

	recalculateItemHeight();
}


//! draws the element and its children
void CGUITextureListBox::draw()
{
	if (!IsVisible)
		return;

	recalculateItemHeight(); // if the font changed

	IGUISkin* skin = Environment->getSkin();

	core::rect<s32>* clipRect = 0;

	// draw background
	core::rect<s32> frameRect(AbsoluteRect);

	// draw items

	core::rect<s32> clientClip(AbsoluteRect);
	clientClip.UpperLeftCorner.Y += 1;
	clientClip.UpperLeftCorner.X += 1;
    if (ScrollBar->isVisible())
		clientClip.LowerRightCorner.X = AbsoluteRect.LowerRightCorner.X - skin->getSize(EGDS_SCROLLBAR_SIZE);
	clientClip.LowerRightCorner.Y -= 1;
	clientClip.clipAgainst(AbsoluteClippingRect);

    skin->draw3DSunkenPane(this, skin->getColor(EGDC_3D_HIGH_LIGHT), true,
        DrawBack, frameRect, &AbsoluteClippingRect);

	if (clipRect)
		clientClip.clipAgainst(*clipRect);

	frameRect = AbsoluteRect;
	frameRect.UpperLeftCorner.X += 1;
    if (ScrollBar->isVisible())
		frameRect.LowerRightCorner.X = AbsoluteRect.LowerRightCorner.X - skin->getSize(EGDS_SCROLLBAR_SIZE);

	frameRect.LowerRightCorner.Y = AbsoluteRect.UpperLeftCorner.Y + ItemHeight;

    frameRect.UpperLeftCorner.Y -= ScrollBar->getPos();
    frameRect.LowerRightCorner.Y -= ScrollBar->getPos();



    bool hl = (HighlightWhenNotFocused || Environment->hasFocus(this) || Environment->hasFocus(ScrollBar));

	for (s32 i=0; i<(s32)Items.size(); ++i)
	{
		if (frameRect.LowerRightCorner.Y >= AbsoluteRect.UpperLeftCorner.Y &&
			frameRect.UpperLeftCorner.Y <= AbsoluteRect.LowerRightCorner.Y)
		{
            if (i == Selected && hl){
                skin->draw2DRectangle(this, skin->getColor(EGDC_HIGH_LIGHT), frameRect, &clientClip);
            }

			core::rect<s32> textRect = frameRect;
            textRect.UpperLeftCorner.X += 3;

            core::rect<s32> textureRect = frameRect;
            textureRect.UpperLeftCorner.X += 9;
            textureRect.UpperLeftCorner.Y += 9;
            textureRect.LowerRightCorner.Y = textureRect.UpperLeftCorner.Y + ItemHeight -18;
            textureRect.LowerRightCorner.X = textRect.UpperLeftCorner.X + ItemsTextureWidth;

			if (Font)
			{
                if (Items[i].texture != 0)
                {

                        Environment->getVideoDriver()->draw2DImage(Items[i].texture,
                                                                   textureRect,
                                                                   core::rect<s32>(core::vector2di(0,0),Items[i].texture->getSize())
                                                                   , &clientClip
                                                                   );

                }

                textRect.UpperLeftCorner.X += ItemsTextureWidth+3;

                if ( i==Selected && hl )
                {
                    Font->draw(Items[i].text.c_str(), textRect, skin->getColor(EGDC_HIGH_LIGHT),
                        false, true, &clientClip);
                }
                else
                {
                    Font->draw(Items[i].text.c_str(), textRect, skin->getColor(EGDC_BUTTON_TEXT),
						false, true, &clientClip);
                }

                textRect.UpperLeftCorner.X -= ItemsTextureWidth+3;
			}
		}

		frameRect.UpperLeftCorner.Y += ItemHeight;
		frameRect.LowerRightCorner.Y += ItemHeight;
    }
	IGUIElement::draw();
}


void CGUITextureListBox::recalculateScrollPos()
{
	if (!AutoScroll)
		return;

    const s32 selPos = (Selected == -1 ? TotalItemHeight : Selected * ItemHeight) - ScrollBar->getPos();

	if (selPos < 0)
	{
        ScrollBar->setPos(ScrollBar->getPos() + selPos);
	}
	else
	if (selPos > AbsoluteRect.getHeight() - ItemHeight)
	{
        ScrollBar->setPos(ScrollBar->getPos() + selPos - AbsoluteRect.getHeight() + ItemHeight);
	}
}





//! set global itemHeight
void CGUITextureListBox::setItemHeight( s32 height )
{
	ItemHeight = height;
    ItemsTextureWidth = height;

}


//! Sets whether to draw the background
void CGUITextureListBox::setDrawBackground(bool draw)
{
    DrawBack = draw;
}


} // end namespace gui
} // end namespace irr

#endif // _IRR_COMPILE_WITH_GUI_

