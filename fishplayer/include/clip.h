/*! Copyright (C) 2017  	Department of Electrical Engineering & Computer Science, Institute of
 *			Real-Time Learning Systems, University of Siegen
 *
 *			Research Group of Ecology and Behavioral Biology, Institute of Biology,
 *			University of Siegen
 *
 * Contact: virtual.fish.project@gmail.com
 *
 * License: GNU General Public License  See LICENSE.txt for the full license.
 *
 * This file is part of the "FishSim Animation Toolchain".
 * "FishSim Animation Toolchain" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "FishSim Animation Toolchain" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "FishSim Animation Toolchain".  If not, see <http: *www.gnu.org/licenses/>.
 *
 * The "FishSim Animation Toolchain" was developed within the scope of the interdisciplinary,
 * DFG (Deutsche Forschungsgemeinschaft)-funded project “virtual fish” (KU
 * 689/11-1 and Wi 1531/12-1) of the Institute of Real-Time Learning Systems
 * (EZLS) and the Research Group of Ecology and Behavioral Biology at the
 * University of Siegen.
*/
#ifndef H_CLIP_H
#define H_CLIP_H

#include <string>
#include <yaml-cpp/yaml.h>
#include <QWidget>
#include <QPainter>
#include <QPaintEvent>

#include "ros/ros.h"
#include "include/ros_interface.h"


#define LABEL_TYPE_WIDTH 60
#define LABEL_TIME_WIDTH 55

namespace fishplayer
{
enum E_CLIP_TYPE
{
    BAG = 1,
    LOAD,
    PAUSE,
    AUTO

};

class Clip : public QWidget
{

    Q_OBJECT

public:

    Clip(QWidget *parent, E_CLIP_TYPE type) : QWidget(parent)
    {
        m_type = type;
        m_isPlaying = false;
        m_isValid = false;
    }

    ~Clip()
    {

    }

    E_CLIP_TYPE getType()
    {
        return m_type;
    }

    virtual float getDuration() = 0;

    virtual void play(RosInterface* roshandler) = 0;

    virtual bool verify() = 0;

    void stop()
    {
        m_isPlaying = false;
    }

    virtual void save(YAML::Emitter& emitter) = 0;

    virtual bool load(const YAML::Node& node) = 0;

    void paintEvent(QPaintEvent * event)
    {

        // Only draw error rectangle if clip is invalid
        if(m_isValid)
            return;

        QRect rect = event->rect();
        // Outer border
        QPainter painter(this);
        painter.setRenderHint(QPainter::Antialiasing);
        QPen pen(Qt::red, 2);
        painter.setPen(pen);
        painter.drawRect(rect);
    }


signals:
    void clipHasChanged();
    void playbackTimeChanged(float);

protected:

    E_CLIP_TYPE m_type;
    bool m_isPlaying;
    bool m_isValid;


};
}

#endif
