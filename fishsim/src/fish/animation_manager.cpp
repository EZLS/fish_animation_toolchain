#include "fish/animation_manager.h"

#include <iostream>


#include <rosbag/bag.h>
#include <rosbag/query.h>
#include <rosbag/view.h>
#include <rosbag/time_translator.h>
#include <rosbag/player.h>

#include <boost/filesystem.hpp>
#include <boost/foreach.hpp>


namespace fishsim
{
AnimationManager::AnimationManager()
{

}

AnimationManager::~AnimationManager()
{
    clear();
}

FishAnimation* AnimationManager::createNewAnimation(const std::string &modelName)
{

    // Animation for model already exist?
    FishAnimation* anim = getAnimationForModel(modelName);
    if(anim)
    {
        return anim;
    }
    else
    {
        FishAnimation* newAnimation = new FishAnimation();
        m_animations[modelName] = newAnimation;
        return newAnimation;
    }

}

FishAnimation* AnimationManager::getAnimationForModel(const std::string &modelName){

    std::map<std::string,FishAnimation*>::iterator it;
    it = m_animations.find(modelName);

    if(it != m_animations.end())
    {        return it->second;
    }
    else
    {
        return 0;
    }

}

bool AnimationManager::removeAnimation(const std::string& modelName){

    for(auto it = m_animations.begin(); it != m_animations.end(); it++)
    {
        if((it->first) == modelName)
        {
            m_animations.erase(modelName);
            delete it->second;
            return true;
        }
    }
    return false;
}

void AnimationManager::clear(){

    for(auto it : m_animations)
    {
        delete it.second;
    }
    m_animations.clear();
}

std::vector<FishSnapshot*> AnimationManager::getAllKeyframesForCounter(int animationCounter){

    std::vector<FishSnapshot*> toReturn;

    for(auto it: m_animations)
    {
        FishAnimation* anim = it.second;
        FishSnapshot* snapshot = anim->getKeyframe(animationCounter);

        if(snapshot)
            toReturn.push_back(snapshot);
    }

    return toReturn;
}


std::vector<FishAnimation*> AnimationManager::getAllAnimations(){

    std::vector<FishAnimation*> toReturn;

    for(auto it: m_animations)
    {
        FishAnimation* anim = it.second;
        toReturn.push_back(anim);
    }

    return toReturn;
}

bool AnimationManager::saveAsBag(const std::string &filename)
{
    bool allOk = true;

    try
    {
        rosbag::Bag* bag = new rosbag::Bag(filename, rosbag::bagmode::Write);

        std::cout << "Save bag: " << filename << std::endl;

        for(auto it : m_animations)
        {
            std::cout << "--Saving: " << it.first << std::endl;
            allOk = it.second->writeToBag(bag);
            if(!allOk) break;
            std::cout << "--Done" << std::endl;
        }

        bag->close();

        if(allOk)
            return true;
        else
            return false;
    }
    catch(rosbag::BagException e)
    {

        std::cerr<< "Error: " << e.what() << std::endl;
        return false;
    }

}

bool AnimationManager::loadFromBag(const std::string &filename)
{

    clear();

    try
    {
        rosbag::Bag bag;
        bag.open(filename, rosbag::bagmode::Read);

        std::vector<std::string> topics;
        topics.push_back(std::string("/fishsim/fishSnapshot"));
        rosbag::View view(bag, rosbag::TopicQuery(topics));

        BOOST_FOREACH(rosbag::MessageInstance const m, view)
        {
            fishsim::FishSnapshotStampedConstPtr i =
                    m.instantiate<fishsim::FishSnapshotStamped>();

            FishSnapshot* keyframe = new FishSnapshot();
            keyframe->fromRosSnapshot(i);
            std::string modelName = keyframe->getName();

            FishAnimation *animation = getAnimationForModel(modelName);

            if(!animation)
                animation = createNewAnimation(modelName);

            animation->addKeyframe(keyframe);


        }
        bag.close();

        return true;
    }
    catch(rosbag::BagException e)
    {
        std::cerr << e.what() <<std::endl;
        return false;
    }


}


} // end namespace fishsim
