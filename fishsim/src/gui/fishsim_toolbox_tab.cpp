#include "gui/fishsim_toolbox_tab.h"
#include "sim/fishsim_globals.h"

using namespace irr;
using namespace gui;

namespace fishsim
{

FishSimToolboxTab::FishSimToolboxTab(irr::gui::IGUIEnvironment *guiEnvironment, irr::gui::IGUITab *tab, u32 toolboxWidth, u32 toolboxHeight){

    m_guiEnv = guiEnvironment;
    m_tab = tab;
    m_tab->grab();

    m_toolboxWidth = toolboxWidth;
    m_toolboxHeight = toolboxHeight;
    m_totalHeight = 0;
}

FishSimToolboxTab::~FishSimToolboxTab()
{
    // Drop elements
    for(int i = 0; i < m_elements.size(); i++)
    {
        if(m_elements.at(i) != 0)
            m_elements.at(i)->drop();
    }
    m_elements.clear();

    // Drop tab
    m_tab->drop();

}

IGUIStaticText* FishSimToolboxTab::appendStaticText(const wchar_t *text, irr::s32 id)
{
//    if(!m_tab)
//        return 0;


//    core::rect<s32> textRec = makeRectangle(TOOLBOX_STANDART_ELEMENT_HEIGHT);

//    IGUIStaticText*  newElement = m_guiEnv->addStaticText(text, textRec, false, false, m_tab, id);
//    newElement->grab();
//    m_elements.push_back(newElement);
//    m_totalHeight += 30;
//    return newElement;


    if(!m_tab)
        return 0;

    /*  Workaround: Let irrlicht figure out the height the rectangle should have
        to display all the text.
        Irrlicht does not let you change the rectangle of the text
        after it has been created. So we create a temp text,
        get its height and then create the real text element.
    */
    core::rect<s32> textRec = makeRectangle(TOOLBOX_STANDART_ELEMENT_HEIGHT);
    IGUIStaticText*  tempElement = m_guiEnv->addStaticText(text, textRec, false, true, 0, 0);
    int recHeight = tempElement->getTextHeight();
    tempElement->remove();

    // Now create the real text
    textRec = makeRectangle(recHeight + 10);
    IGUIStaticText*  newElement = m_guiEnv->addStaticText(text, textRec, false, true, m_tab, 0);
    newElement->grab();
    m_elements.push_back(newElement);

    m_totalHeight += recHeight + 10;
    return newElement;

}

IGUIEditBox* FishSimToolboxTab::appendEditBox(const wchar_t* edtBoxTex, irr::s32 id){

    if(!m_tab)
        return 0;

    core::rect<s32> fullRect = makeRectangle(TOOLBOX_STANDART_ELEMENT_HEIGHT);

    core::rect<s32> edtBoxRect(fullRect.UpperLeftCorner.X + 10,
                               fullRect.UpperLeftCorner.Y,
                               fullRect.LowerRightCorner.X - 10,
                               fullRect.LowerRightCorner.Y);

    // Add edit box
    IGUIEditBox* edtBox = m_guiEnv->addEditBox(edtBoxTex,edtBoxRect, true, m_tab, id);
    edtBox->grab();
    m_elements.push_back(edtBox);

    m_totalHeight += 30;
    return edtBox;
}

IGUIEditBox* FishSimToolboxTab::appendEditBoxWithDescription(const wchar_t *description,
                                                             const wchar_t *edtBoxTex, s32 id){

    if(!m_tab)
        return 0;

    // The full rectangle we can fill with elements
    core::rect<s32> fullRect = makeRectangle(TOOLBOX_STANDART_ELEMENT_HEIGHT);

    // Rectangle of text and edtBox. Their sizes depend on the size
    // of full rectangle
    core::rect<s32> textRect(fullRect.UpperLeftCorner.X + 10,
                             fullRect.UpperLeftCorner.Y,
                             fullRect.UpperLeftCorner.X + 20,
                             fullRect.LowerRightCorner.Y);

    core::rect<s32> edtBoxRect(fullRect.UpperLeftCorner.X + 25,
                               fullRect.UpperLeftCorner.Y,
                               fullRect.LowerRightCorner.X - 25,
                               fullRect.LowerRightCorner.Y);

    // Would be better to create a new gui class that combines the desciption and
    // edit box and push this one into the vector.

    // Add description text
    IGUIStaticText *text = m_guiEnv->addStaticText(description, textRect, false, false, m_tab);
    text->grab();
    m_elements.push_back(text);

    // Add edit box
    IGUIEditBox* edtBox = m_guiEnv->addEditBox(edtBoxTex,edtBoxRect, true, m_tab, id);
    edtBox->grab();
    m_elements.push_back(edtBox);

    m_totalHeight += 30;
    return edtBox;
}

IGUIButton* FishSimToolboxTab::appendButton(const wchar_t *text, s32 id, const wchar_t *tooltiptext){

    if(!m_tab)
        return 0;

    core::rect<s32> fullRect = makeRectangle(TOOLBOX_STANDART_ELEMENT_HEIGHT);
    core::rect<s32> buttonRect(fullRect.UpperLeftCorner.X + 10,
                               fullRect.UpperLeftCorner.Y,
                               fullRect.LowerRightCorner.X - 25,
                               fullRect.LowerRightCorner.Y);

    IGUIButton* newButton = m_guiEnv->addButton(buttonRect, m_tab, id, text, tooltiptext);
    newButton->grab();
    m_elements.push_back(newButton);


    m_totalHeight += 30;

    return newButton;

}

irr::gui::IGUICheckBox* FishSimToolboxTab::appendCheckBox(bool checked, const wchar_t* text,
                                                          irr::s32 id)
{
    if(!m_tab)
        return 0;

    core::rect<s32> fullRect = makeRectangle(TOOLBOX_STANDART_ELEMENT_HEIGHT);
    core::rect<s32> checkBoxRect(fullRect.UpperLeftCorner.X + 10,
                                 fullRect.UpperLeftCorner.Y,
                                 fullRect.LowerRightCorner.X - 25,
                                 fullRect.LowerRightCorner.Y);

    IGUICheckBox* checkBox = m_guiEnv->addCheckBox(checked, checkBoxRect, m_tab, id, text);
    checkBox->grab();
    m_elements.push_back(checkBox);

    m_totalHeight += 30;
    return checkBox;
}

void FishSimToolboxTab::appendSeparator(){

    if(!m_tab)
        return;

    m_totalHeight += 30;
}


irr::gui::IGUIButton* FishSimToolboxTab::appendTextureButton(const wchar_t* buttonText,
                                                                 irr::video::ITexture *buttonImage,
                                                                 const wchar_t* descriptionText,
                                                                 irr::s32 id)
{

    if(!m_tab)
        return 0;

    core::rect<s32> fullRect = makeRectangle(100);

    core::rect<s32> buttonRect(fullRect.UpperLeftCorner.X,
                               fullRect.UpperLeftCorner.Y + 5 ,
                               fullRect.UpperLeftCorner.X + 100,
                               fullRect.LowerRightCorner.Y - 5);

    core::rect<s32> descriptionRect(buttonRect.LowerRightCorner.X + 10,
                                    buttonRect.UpperLeftCorner.Y + 40,
                                    buttonRect.LowerRightCorner.X + 100,
                                    buttonRect.UpperLeftCorner.Y + 60);

    // Show path of texture as tooltip
    std::wstring tooltip = core::stringw(buttonImage->getName()).c_str();

    // Button
    IGUIButton* newButton = m_guiEnv->addButton(buttonRect, m_tab, id, buttonText, tooltip.c_str());
    newButton->setImage(buttonImage);
    newButton->setScaleImage(true);
    newButton->grab();
    m_elements.push_back(newButton);

    //Text
    IGUIStaticText *text = m_guiEnv->addStaticText(descriptionText, descriptionRect, false, false, m_tab);
    text->grab();
    m_elements.push_back(text);

    m_totalHeight += 100;

    return newButton;

}


irr::gui::IGUIScrollBar* FishSimToolboxTab::appendHorizontalScrollbar(irr::s32 id){


    if(!m_tab)
        return 0;

    core::rect<s32> fullRect = makeRectangle(25);

    IGUIScrollBar* newScrollbar = m_guiEnv->addScrollBar(true, fullRect, m_tab, id);
    newScrollbar->grab();
    m_elements.push_back(newScrollbar);

    m_totalHeight += 25;

    return newScrollbar;

}

IGUIStaticText *FishSimToolboxTab::appendDescriptionText(const wchar_t *text, s32 id)
{
}


core::rect<s32> FishSimToolboxTab::makeRectangle(int rectangleHeight)
{
    // Set up rectangle parameters
    int xBegin = TOOLBOX_BORDER_OFFSET;
    int yBegin = TOOLBOX_BORDER_OFFSET;
    int xEnd = m_toolboxWidth - TOOLBOX_BORDER_OFFSET;
    int yEnd = rectangleHeight;

    // Adjust yBegin and yEnd depending on number of elements
    int yOffset = m_totalHeight;
    yBegin = yBegin + yOffset;
    yEnd = yEnd + yOffset;

    core::rect<s32> rectangle(xBegin, yBegin, xEnd, yEnd);

    return rectangle;

}


} // end namespace fishsim
