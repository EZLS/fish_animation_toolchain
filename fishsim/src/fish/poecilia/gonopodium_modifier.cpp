#include "fish/poecilia/gonopodium_modifier.h"

#include "ros/joy_indices.h"

using namespace irr;
using namespace scene;


//Maximum rotation values for gonopodium UP/DOWN
#define GONOPODIUM_DOWN 12.0f
#define GONOPODIUM_UP 62.0f

//Maximum rotation values for gonopodium FOREWARD/BACKWARD
#define GONOPODIUM_FOREWARD 170.0f
#define GONOPODIUM_BACKWARD 0.0f

namespace fishsim
{
GonopodiumModifier::GonopodiumModifier(IAnimatedMeshSceneNode* fishNode,
                           std::string name, float speed, bool isActive)
    : BoneModifier(fishNode, name, speed, isActive)
{

}

GonopodiumModifier::~GonopodiumModifier(){

    if(m_gonopodium)
        m_gonopodium->drop();
}

bool GonopodiumModifier::init(){

    if(!m_fishNode)
        return false;

    return getAndGrabBone(m_gonopodium, "gonopodium");

}

/// Extract value of rear right 1 button and pass it to update function
void GonopodiumModifier::update(const sensor_msgs::Joy& joystickState){

    if(!m_isActive)
        return;

    float upDown = joystickState.buttons.at(PS3_AXIS_BUTTON_REAR_RIGHT_1);
    float leftRight = joystickState.buttons.at(PS3_AXIS_BUTTON_REAR_RIGHT_2);

    update(upDown, leftRight);

}

/// If button is pressed, rise dorsal fin
void GonopodiumModifier::update(float inputVal1, float inputVal2, float inputVal3){

    if(!m_isActive)
        return;

    /// Rotate gonopodium up or down (x axis)
    if(inputVal1 > 0)
        changeGonopodiumUp(-1);
    else
        changeGonopodiumUp(1);


    /// Rotate gonopodium foreward or backward (z axis)
    if(inputVal2 > 0)
        changeGonopodiumForeward(1);
    else
        changeGonopodiumForeward(-1);

}

void GonopodiumModifier::changeGonopodiumUp(int dir){

    core::vector3df gonopodiumRot = m_gonopodium->getRotation();
    float currRot = gonopodiumRot.X;
    currRot += m_speed * dir;

    // Check bounds
    if(currRot > GONOPODIUM_DOWN && currRot < GONOPODIUM_UP)
        addRotationToBone(m_gonopodium, m_speed * dir, 0.0, 0.0);

}

void GonopodiumModifier::changeGonopodiumForeward(int dir){

    core::vector3df gonopodiumRot = m_gonopodium->getRotation();
    float currRot = gonopodiumRot.Y;
    currRot += m_speed * dir;

    // Check bounds
    if(currRot > GONOPODIUM_BACKWARD && currRot < GONOPODIUM_FOREWARD)
        addRotationToBone(m_gonopodium, 0.0, m_speed * dir, 0.0);

}

void GonopodiumModifier::modify(FishSnapshot* startState,
                    FishSnapshot* endState,
                    const sensor_msgs::Joy& joystickState ){


    // rotation of bone in start state
    core::vector3df gonopodiumRot = startState->getSingleBoneRot(m_gonopodium->getName());

    ///
    /// X rotation
    ///
    float valX = joystickState.buttons.at(PS3_AXIS_BUTTON_REAR_RIGHT_1);
    int dirX = 1;
    if(valX > 0) dirX = -1;


    float currRotX = gonopodiumRot.X;
    currRotX += m_speed * dirX;


    // Check bounds
    if(currRotX > GONOPODIUM_DOWN && currRotX < GONOPODIUM_UP)
    {
        core::vector3df vecToAdd = core::vector3df(m_speed * dirX, 0.0, 0.0);
        gonopodiumRot += vecToAdd;
    }


    ///
    /// Y rotation
    ///
    float valY = joystickState.buttons.at(PS3_AXIS_BUTTON_REAR_RIGHT_2);
    int dirY = -1;
    if(valY > 0) dirY = 1;

    float currRotY = gonopodiumRot.Y;
    currRotY += m_speed * dirY;

    // Check bounds
    if(currRotY > GONOPODIUM_BACKWARD && currRotY < GONOPODIUM_FOREWARD)
    {
        core::vector3df vecToAdd = core::vector3df(0.0, m_speed * dirY, 0.0);
        gonopodiumRot += vecToAdd;

    }

    // set rotation
    endState->setSingleBoneRot(m_gonopodium->getName(), gonopodiumRot);


}


} // end namespace fishsim
