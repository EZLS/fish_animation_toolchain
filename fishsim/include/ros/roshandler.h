/*! Copyright (C) 2017  	Department of Electrical Engineering & Computer Science, Institute of
 *			Real-Time Learning Systems, University of Siegen
 *
 *			Research Group of Ecology and Behavioral Biology, Institute of Biology,
 *			University of Siegen
 *
 * Contact: virtual.fish.project@gmail.com
 *
 * License: GNU General Public License  See LICENSE.txt for the full license.
 *
 * This file is part of the "FishSim Animation Toolchain".
 * "FishSim Animation Toolchain" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "FishSim Animation Toolchain" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "FishSim Animation Toolchain".  If not, see <http: *www.gnu.org/licenses/>.
 *
 * The "FishSim Animation Toolchain" was developed within the scope of the interdisciplinary,
 * DFG (Deutsche Forschungsgemeinschaft)-funded project “virtual fish” (KU
 * 689/11-1 and Wi 1531/12-1) of the Institute of Real-Time Learning Systems
 * (EZLS) and the Research Group of Ecology and Behavioral Biology at the
 * University of Siegen.
 *
 *  \file roshandler.h
 *  \brief Interface class to ROS.
 *
 * Irrhandler initializes device, scenemanager, GUI and event receiver in the init() function. Once
 * initialization has been done, everything can be drawn to screen by calling exec() repeatedly.
 *
 * Irrhandler also provides interface functions to roshandler, like load and save scene as well as adding new
 * fish snapshots.
 *
*/

#ifndef H_ROSHANDLER_H
#define H_ROSHANDLER_H

#include <ros/ros.h>
#include <dynamic_reconfigure/server.h>

#include <std_msgs/Empty.h>
#include <geometry_msgs/Twist.h>

#include "sim/irrhandler.h"

#include "fishsim/getModels.h"
#include "fishsim/FishSnapshotStamped.h"
#include "fishsim/FishSimConfig.h"
#include "fishsim/loadScene.h"


namespace fishsim
{
class RosHandler
{
public:

    /*!
     * \brief Default constructor.
     * \param irrhandler pointer to irrhandler.
     */
    RosHandler(IrrHandler* irrhandler);

    /*!
     * \brief Destructor.
     */
    ~RosHandler();

    /*!
     * \brief spinOnce let ROS do it's work. Calls ros::spinOnce().
     */
    void spinOnce();

    /// Returns true if still running
    bool isRunning();

    /// Tell roshandler to stop running
    void die();

    /// Callback for stop message
    void die(const std_msgs::EmptyConstPtr& empty);

    /// Callback for snapshots
    void parseSnapshot(const fishsim::FishSnapshotStampedConstPtr &rosSnapshot);

    /// Dynamic reconfigure initialization
    void initDynReconfigure();


private:

    /// Get models service
    bool getModelsSrv(fishsim::getModelsRequest& req, fishsim::getModelsResponse& resp);

    /// Load scene service
    bool loadSceneSrv(fishsim::loadSceneRequest& req, fishsim::loadSceneResponse& resp);

    /// Dynamic reconfigure
    void dyncfgCallback(fishsim::FishSimConfig& config, uint32_t level);

    /// Used to visualize the position of the real fish
    void fishPositionCallback(const geometry_msgs::TwistConstPtr& fishPos);



private:

    /// Pointer to ros node handle
    ros::NodeHandle* m_nh;

    /// Pointer to irrhandler
    IrrHandler* m_irrhandler;

    /// Stop subscriber
    ros::Subscriber m_dieSub;

    /// Fish snapshot subscriber
    ros::Subscriber m_fishSnapshotSub;

    /// Get models service
    ros::ServiceServer m_modelSrv;

    /// Load scene service
    ros::ServiceServer m_loadSceneSrv;

    /// Dynamic reconfigure
    dynamic_reconfigure::Server<fishsim::FishSimConfig>* m_dyn_server;

    /// Is running or not
    bool m_keepRunning;

    /// Real fish position subscriber
    ros::Subscriber realFishPosSub;

};


} // end namespace fishsim

#endif // H_ROSHANDLER_H
