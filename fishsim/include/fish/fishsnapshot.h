/*! Copyright (C) 2017  	Department of Electrical Engineering & Computer Science, Institute of
 *			Real-Time Learning Systems, University of Siegen
 *
 *			Research Group of Ecology and Behavioral Biology, Institute of Biology,
 *			University of Siegen
 *
 * Contact: virtual.fish.project@gmail.com
 *
 * License: GNU General Public License  See LICENSE.txt for the full license.
 *
 * This file is part of the "FishSim Animation Toolchain".
 * "FishSim Animation Toolchain" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "FishSim Animation Toolchain" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "FishSim Animation Toolchain".  If not, see <http: *www.gnu.org/licenses/>.
 *
 * The "FishSim Animation Toolchain" was developed within the scope of the interdisciplinary,
 * DFG (Deutsche Forschungsgemeinschaft)-funded project “virtual fish” (KU
 * 689/11-1 and Wi 1531/12-1) of the Institute of Real-Time Learning Systems
 * (EZLS) and the Research Group of Ecology and Behavioral Biology at the
 * University of Siegen.
 *
 *  \file fishsnapshot.h
 *  \brief A class that represents a full state of a fish in simulation at a certain time.
 *
 *  A fish snapshot stores a full state (position, rotation and bone rotations)
 *  of a fish in the simulation at a specific time. Two snapshots can be used to
 *  interpolate between them, which will result in a smoother movement of the fish.
 *
*/

#ifndef H_FISHSNAPSHOT_H
#define H_FISHSNAPSHOT_H

#include <irrlicht.h>
#include <string>
#include <ros/time.h>
#include <map>

#include <fishsim/FishSnapshotStamped.h>


namespace fishsim
{

class FishSnapshot
{

public:


    /*!
      \brief Constuctor
    */
    FishSnapshot();


    /*!
      \brief Destructor
    */
    ~FishSnapshot();


    /*!
      \brief Set the time for the snapshot.
      \param t The time of the snapshot.
    */
    void setTime(ros::Time t);

    /*!
      \brief Set the name of the fish.
      \param name The name of the fish.
    */
    void setName(const std::string& name);

    /*!
      \brief Set the position of the fish at snapshot time.
      \param pos the position of the fish.
    */
    void setPos(const irr::core::vector3df &pos);

    /*!
      \brief Set the rotation of the fish at snapshot time.
      \param rot the rotation of the fish.
    */
    void setRot(const irr::core::vector3df &rot);

    /*!
      \brief Set the scale of the fish at snapshot time.
      \param scale The scale of the fish.
    */
    void setScale(const irr::core::vector3df &scale);

    /*!
      \brief Set the rotation of a single bone at snapshot time.
      \param boneName The name of the bone.
      \param boneRot The rotation of the bone.
    */
    void setSingleBoneRot(const std::string &boneName, const irr::core::vector3df &boneRot);


    /*!
      \brief Get the time of the keyframe.
      \return the time of the keyframe.
    */
    ros::Time getTime();


    /*!
      \brief Get name of the fish.
      \return the name of the fish.
    */
    const std::string& getName() const;

    /*!
      \brief Get position of the fish at snapshot time.
      \return the position of the fish.
    */
    const irr::core::vector3df& getPos() const;

    /*!
      \brief Get rotation of the fish at snapshot time.
      \return the rotation of the fish.
    */
    const irr::core::vector3df& getRot() const;

    /*!
      \brief Get scale of the fish at snapshot time.
      \return the scale of the fish.
    */
    const irr::core::vector3df& getScale() const;

    /*!
      \brief Get the rotation of each bone at snapshot time in a map.
      \return map that contains all bone rotations at snapshot time.
    */
    const std::map<std::string, irr::core::vector3df> getBoneRotationMap() const;

    /*!
      \brief Get a rotation of a single bone.
      \return boneName name of the bone.
    */
    const irr::core::vector3df& getSingleBoneRot(const std::string &boneName) const;


    /*!
      \brief Create a ROS message from the properties of this object.
      \return a ROS message that contains all properties of this object.
    */
    fishsim::FishSnapshotStamped toRosSnapshot() const;


    /*!
      \brief Initialize this snapshot from a ROS message.
      \param rosSnapshot The ROS message from which this object will be initialized.
    */
    void fromRosSnapshot(const fishsim::FishSnapshotStampedConstPtr &rosSnapshot);


    /*!
      \brief Does this snapshot has a rotation value for given bone.
      \param boneName name of the bone.
      \return true, if bone rotation exist. False otherwise.
    */
    const bool hasBoneRotationValue(const std::string boneName) const;

private:

    /// Time when the snapshot was captured at the server.
    ros::Time m_time;

    /// Name of the model from which this snapshot was generated.
    std::string m_modelName;

    /// Position of the fish at snapshot time.
    irr::core::vector3df m_pos;

    /// Rotation of the fish at snapshot time.
    irr::core::vector3df m_rot;

    /// Scale of the fish at snapshot time.
    irr::core::vector3df m_scale;

    /// All bone rotations at snapshot time.
    std::map<std::string, irr::core::vector3df> m_boneRot;

    /// All bone sizes at snapshot time.
    std::map<std::string, irr::core::vector3df> m_boneScale;

};


} // end namespace fishsim

#endif // H_FISHSNAPSHOT_H
