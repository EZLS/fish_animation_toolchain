/*! Copyright (C) 2017  	Department of Electrical Engineering & Computer Science, Institute of
 *			Real-Time Learning Systems, University of Siegen
 *
 *			Research Group of Ecology and Behavioral Biology, Institute of Biology,
 *			University of Siegen
 *
 * Contact: virtual.fish.project@gmail.com
 *
 * License: GNU General Public License  See LICENSE.txt for the full license.
 *
 * This file is part of the "FishSim Animation Toolchain".
 * "FishSim Animation Toolchain" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "FishSim Animation Toolchain" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "FishSim Animation Toolchain".  If not, see <http: *www.gnu.org/licenses/>.
 *
 * The "FishSim Animation Toolchain" was developed within the scope of the interdisciplinary,
 * DFG (Deutsche Forschungsgemeinschaft)-funded project “virtual fish” (KU
 * 689/11-1 and Wi 1531/12-1) of the Institute of Real-Time Learning Systems
 * (EZLS) and the Research Group of Ecology and Behavioral Biology at the
 * University of Siegen.
 *
 * \file fish_animation.h
 * \brief FishAnimation allows to combine several FishSnapshots to an animation.
 *
 * User can add keyframes (in form of FishSnapshots) to the animation, as well as override
 * keyframes (used in fishsteering while doing edit). The animation can also be saved and
 * loaded to/from a rosbag.
 *
*/

#ifndef H_FISH_ANIMATION_H
#define H_FISH_ANIMATION_H

#include <string>
#include <vector>

#include <rosbag/bag.h>

#include "fish/fishsnapshot.h"



namespace fishsim
{

class FishAnimation
{

public:

    /*!
      \brief Constructor
    */
    FishAnimation();

    /*!
      \brief Destructor
    */
    ~FishAnimation();

    /*!
      \brief Get a keyframe at given index. Will return 0,
      if index is greater than animation time.

      \param index The index of the keyframe.

      \return Pointer to a FishSnapshot at given index, or 0, if
      animation length is smaller than index.

    */
    FishSnapshot* getKeyframe(int index);
    

    /*!
      \brief Append the keyfame to the animation.

      Take special care of the timestamp of the FishSnapshot you want to add.
      Timestamp of the new snapshot should be greater than last keyframe/snapshot.

      \param snapshot The snapshot/keyframe to add

      \return true, if snapshot could be added. False, if timestamp
      of the new snapshot is smaller than timestamp of the last snapshot that was
      added.

    */
    bool addKeyframe(FishSnapshot* snapshot);


    /*!
      \brief Overridea keyframe at given index.

      \param snapshot The snapshot/keyframe to add
      \param index The index where snapshot should go to in
      the animation.

      \return true, index is smaller than animation size. False,
      if index i greater then animation size.

    */
    bool overrideKeyframe(int index, FishSnapshot* snapshot);


    /*!
      \brief Load animation from rosbag.

      \param filename The filename of the bag

      \return true, if loading was successful. False otherwise.

    */
    bool loadFromBag(const std::string& filename);


    /*!
      \brief Write animation to rosbag.

      \param filename The filename of the bag

      \return true, if animation was saved successfull. False otherwise.

    */
    bool writeToBag(const std::string& filename);


    /*!
      \brief Write animation to rosbag.

      \param bag Pointer to bag where animation keyframes are added.

      \return true, if animation was saved successfull. False otherwise.

    */
    bool writeToBag(rosbag::Bag* bag);


    /*!
      \brief Get number of keyframes of animation.
      \return Number of keyframes of animation.

    */
    int getKeyframeCount(){
        return m_keyframes.size();
    }


    /*!
      \brief Clear the animation. Delete all keyframes.

    */
    void clear();

    /*!
      \brief Get duration of whole animation.
      \return Duration of whole animation.

    */
    ros::Duration getAnimationDuration();


    /*!
      \brief Get duration of time between to keyframes.
      \param indexFrom Index of keyframe where duration messurement starts.
      \param indexTo Index of keyframe where duration messurement ends.
      \return Duration of time between keyframes between the two given
      indices.
    */
    ros::Duration getDurationBetweenKeyframes(int indexFrom, int indexTo);

private:

    /// The animation, stored in a vector
    std::vector<FishSnapshot*> m_keyframes;

    /// Name of the model the animation was created for
    std::string m_modelName;

};


} // end namespace fishsim

#endif // H_FISH_ANIMATION_H
