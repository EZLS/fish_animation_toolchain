#include "gui/mainwindow.h"
#include "include/section.h"

namespace fishsteering
{

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),  ui(new Ui::MainWindow)
{
    // Init FishSim scene manager
    m_device = irr::createDevice(irr::video::EDT_NULL);
    std::string fishSimPath = ros::package::getPath("fishsim");
    m_fishSimSmgr = new fishsim::FishSimSceneManager(m_device->getSceneManager(),
                                                     fishSimPath, 0);
    // Init SteeringRecorder
    steeringRec = new SteeringRecorder(m_fishSimSmgr);

    // Init UI
    ui->setupUi(this);
    fillModelListBox();
    createSettingsSection();
    connectSignalsAndSlots();
    updateGUI();

    if(m_fishSimSmgr->getCurrentFishModels().size() == 0)
    {
        showCriticalMessageBox("Could not find models in current scene. Please add some "
                               "models to FishSim and restart FishSteering.");
        ui->label_userInfo->setText("No models in current scene. Please add models and restart FishSteering");
    }
    else
    {
        setModelName(0);
    }
}

MainWindow::~MainWindow()
{

    if(m_fishSimSmgr)
    {
        delete m_fishSimSmgr;
        m_fishSimSmgr = 0;
    }

    if(m_device)
    {
        m_device->drop();
        m_device = 0;
    }

    if(ui)
    {
        delete ui;
    }
}

void MainWindow::createSettingsSection()
{
    Section* section = new Section("Setting", 300);
    auto* sectionMainLayout = new QVBoxLayout();

    m_forewardSpeedEdit = new QLineEdit("0.0");
    m_forewardSpeedEdit->setInputMask("0.0");
    m_yawSpeedEdit = new QLineEdit("0.0");
    m_yawSpeedEdit->setInputMask("0.0");
    m_pitchSpeedEdit = new QLineEdit("0.0");
    m_pitchSpeedEdit->setInputMask("0.0");

    QHBoxLayout* forewardLayout = new QHBoxLayout();
    forewardLayout->addWidget(new QLabel("Foreward speed:", section));
    forewardLayout->addWidget(m_forewardSpeedEdit);
    sectionMainLayout->addLayout(forewardLayout);

    QHBoxLayout* yawLayout = new QHBoxLayout();
    yawLayout->addWidget(new QLabel("Yaw speed:", section));
    yawLayout->addWidget(m_yawSpeedEdit);
    sectionMainLayout->addLayout(yawLayout);

    QHBoxLayout* pitchLayout = new QHBoxLayout();
    pitchLayout->addWidget(new QLabel("Pitch speed:", section));
    pitchLayout->addWidget(m_pitchSpeedEdit);
    sectionMainLayout->addLayout(pitchLayout);

    m_pushButtonSaveSettings = new QPushButton("Save settings");

//    sectionMainLayout->addWidget(new QLabel("Some Text in Section", section));
    sectionMainLayout->addWidget(m_pushButtonSaveSettings);

    section->setContentLayout(*sectionMainLayout);
    ui->sectionLayout->addWidget(section);

    // Default config
    float fwd =  5.0;
    float yaw =  5.0;
    float pitch =  5.0;

    // Load from config
    std::string filePath = ros::package::getPath("fishsteering") +"/config/fishsteering.config";
    try
    {
        YAML::Node config = YAML::LoadFile(filePath);
        if(config.size() != 0)
        {
            fwd =  config["forewardSpeed"].as<float>();
            yaw =  config["yawSpeed"].as<float>();
            pitch =  config["pitchSpeed"].as<float>();
        }
    }
    catch (YAML::BadFile exception)
    {
    }

    m_forewardSpeedEdit->setText(QString::fromStdString(boost::lexical_cast<std::string>(fwd)));
    m_yawSpeedEdit->setText(QString::fromStdString(boost::lexical_cast<std::string>(yaw)));
    m_pitchSpeedEdit->setText(QString::fromStdString(boost::lexical_cast<std::string>(pitch)));

    steeringRec->changeSettings(fwd, yaw, pitch);

}

void MainWindow::connectSignalsAndSlots()
{
    connect(ui->pushButton_load, SIGNAL(clicked()), this, SLOT(loadBagFile()));
    connect(ui->pushButton_save, SIGNAL(clicked()), this, SLOT(saveBagFile()));
    connect(ui->pushButton_play, SIGNAL(clicked()), this, SLOT(playButtonPressed()));
    connect(ui->pushButton_startRecording, SIGNAL(clicked()), this, SLOT(recordButtonPressed()));
    connect(ui->pushButton_startPlacing, SIGNAL(clicked()), this,SLOT(placingButtonPressed()) );
    connect(m_pushButtonSaveSettings, SIGNAL(clicked()), this, SLOT(saveSettingsPressed()));

    connect(ui->comboBox_modelNames,  SIGNAL(currentIndexChanged(int)) , this, SLOT(setModelName(int)));
    connect(ui->horizontalSlider_bag, SIGNAL(actionTriggered(int)), this, SLOT(timeSliderActionTriggered(int)));

    connect(steeringRec, SIGNAL(bagCounterChanged(int)), ui->horizontalSlider_bag, SLOT(setValue(int)) );
    connect(steeringRec, SIGNAL(bagCounterChanged(int)), this, SLOT(setTimeLabelBag(int)));
    connect(steeringRec, SIGNAL(bagFileReady()), this, SLOT(bagReady()));
    connect(steeringRec, SIGNAL(stateChanged()), this, SLOT(updateGUI()));
    connect(steeringRec, SIGNAL(waitingForJoystick()), this, SLOT(noJoystickData()));
}

void MainWindow::recordButtonPressed(){

    std::string modelName = ui->comboBox_modelNames->currentText().toStdString();
    E_RECORDER_STATE state = steeringRec->getState();

    if(state == ERS_IDLE)
    {
        if(steeringRec->modelAnimationExist(modelName))
        {
            startEditing();
        }
        else
        {
            startRecording();
        }

        return;
    }

    if(state == ERS_RECORDING_RUNNING)
    {
        stopRecording();
        ui->horizontalSlider_bag->setValue(0);
        return;
    }

    if(state == ERS_EDITING_RUNNING)
    {
        stopEditing();
        return;
    }
}

void MainWindow::startRecording()
{
    std::cout << "Start recording..." <<std::endl;

    if(ui->comboBox_modelNames->currentText().toStdString() != "")
    {
        std::string modelName = ui->comboBox_modelNames->currentText().toStdString();
        steeringRec->startRecording(modelName);
    }
}

void MainWindow::stopRecording()
{
    steeringRec->stopRecording();
}

void MainWindow::fillEditingListBox()
{

    ui->comboBox_edititngOptions->clear();
    std::vector<std::string> editingOptions = steeringRec->getCurentEditingOptions();
    std::vector<std::string>::const_iterator it = editingOptions.begin();

    for(it; it != editingOptions.end(); it++)
    {
        QString edtOption = QString::fromStdString(*it);
        ui->comboBox_edititngOptions->addItem(edtOption);
    }
}


void MainWindow::fillModelListBox()
{
    // Add models to model dropdown
    std::vector<std::string> fishModels = m_fishSimSmgr->getCurrentFishModels();
    for(std::vector<std::string>::const_iterator it = fishModels.begin(); it != fishModels.end(); it++)
    {
        QString modelName = QString::fromStdString(*it);
        ui->comboBox_modelNames->addItem(modelName);
    }
}

void MainWindow::placingButtonPressed()
{
    E_RECORDER_STATE state = steeringRec->getState();

    if(state == ERS_IDLE)
    {
        startPlacing();
    }
    else if(state == ERS_PLACING_RUNNING)
    {
        stopPlacing();
    }
}

void MainWindow::startPlacing()
{
    std::string modelName = ui->comboBox_modelNames->currentText().toStdString();

    if(modelName != "")
        steeringRec->startPlacingModel(modelName);
}


void MainWindow::stopPlacing()
{
    steeringRec->stopPlacingModel();
}


void MainWindow::startEditing()
{
    std::string modelName = ui->comboBox_modelNames->currentText().toStdString();
    int boneModifierIndex = ui->comboBox_edititngOptions->currentIndex();
    steeringRec->startEditing(modelName, boneModifierIndex);
}

void MainWindow::stopEditing()
{
   steeringRec->stopEditing();
}

void MainWindow::setModelName(int)
{
    QString selection = ui->comboBox_modelNames->currentText();
    std::string modelName = selection.toStdString();
    steeringRec->changeCurrentModel(modelName);
}

void MainWindow::loadBagFile()
{
    QString filename = QFileDialog::getOpenFileName(0, "Choose bag-File:", "", "*.bag", 0, 0);
    if (filename != "")
    {
        bool result = steeringRec->loadBag(filename.toStdString());

        if(!result)
            showCriticalMessageBox("Could not load bag.");
    }
}


void MainWindow::saveBagFile()
{
  QString filename = QFileDialog::getSaveFileName(0, "Choose file to save","","*.bag", 0, 0);

    if(filename != "")
    {
        bool result = steeringRec->saveBag(filename.toStdString());
        if(!result)
           showCriticalMessageBox("Could not save bag. Please try again using a different file location");
    }
}


void MainWindow::setTimeLabelBag(int msec)
{
    int mins = msec/600000;
    int secs = (msec - (mins * 600000))/10000;

    int totalBagLen = steeringRec->getAnimationLength();
    int minsTotal = totalBagLen/600000;
    int secsTotal = (totalBagLen - (minsTotal * 600000))/10000;

    //generate time string
    QString timeString =  QString("%2").arg(mins,2, 10, QChar('0')) + ":" +  QString("%2").arg(secs,2, 10, QChar('0')) + " / "
            +  QString("%2").arg(minsTotal,2, 10, QChar('0')) + ":" + QString("%2").arg(secsTotal,2, 10, QChar('0'));

    ui->label_Time->setText(timeString);
}

void MainWindow::bagReady()
{
    setTimeLabelBag(0);
    ui->horizontalSlider_bag->setValue(0);
    updateGUI();
}

void MainWindow::timeSliderActionTriggered(int action){

    if(action == ui->horizontalSlider_bag->SliderPageStepAdd ||
       action == ui->horizontalSlider_bag->SliderPageStepSub ||
       action == ui->horizontalSlider_bag->SliderMove)
    {

        int timeSliderVal = ui->horizontalSlider_bag->value();

        setTimeLabelBag(timeSliderVal);
        steeringRec->setbagCounter(timeSliderVal);
    }
}


void MainWindow::saveSettingsPressed()
{
    float forewardSpeed = m_forewardSpeedEdit->text().toFloat();
    float yawSpeed = m_yawSpeedEdit->text().toFloat();
    float pitchSpeed = m_pitchSpeedEdit->text().toFloat();

    steeringRec->changeSettings(forewardSpeed,yawSpeed, pitchSpeed);

    YAML::Emitter out;
    out << YAML::BeginMap;

    out << YAML::Key << "forewardSpeed";
    out << YAML::Value << forewardSpeed;
    out << YAML::Key << "yawSpeed";
    out << YAML::Value << yawSpeed;
    out << YAML::Key << "pitchSpeed";
    out << YAML::Value << pitchSpeed;
    out << YAML::EndMap;

    std::string filePath = ros::package::getPath("fishsteering") +"/config/fishsteering.config";
    std::ofstream fout(filePath);
    fout << out.c_str();

}

void MainWindow::playButtonPressed(){

    E_RECORDER_STATE state = steeringRec->getState();

    if(state == ERS_IDLE)
    {
        startPlaying();
    }
    else if (state == ERS_PLAYING_RUNNING)
    {
        stopPlaying();
    }
}

void MainWindow::startPlaying()
{
    steeringRec->startPlaying();
}

void MainWindow::stopPlaying()
{
    steeringRec->stopPlaying();
}

void MainWindow::updateGUI(){

    std::string modelName = ui->comboBox_modelNames->currentText().toStdString();
    E_RECORDER_STATE state = steeringRec->getState();

    // Nothing is running
    if(state == ERS_IDLE)
    {
        ui->label_userInfo->setText("");
        // Animations are available.
        if(steeringRec->modelAnimationExist(modelName))
        {
            int bagLength = steeringRec->getAnimationLength();
            ui->pushButton_startRecording->setText("Start editing");
            ui->comboBox_edititngOptions->setEnabled(true);
            ui->pushButton_play->setEnabled(true);
            ui->pushButton_startPlacing->setEnabled(false);
            ui->pushButton_save->setEnabled(true);
            fillEditingListBox();

            //slider
            ui->horizontalSlider_bag->setEnabled(true);
            ui->horizontalSlider_bag->setMinimum(0);
            ui->horizontalSlider_bag->setMaximum(bagLength);
            ui->horizontalSlider_bag->setPageStep(bagLength / 10);
        }
        else // No animations there. Need to record.
        {
            ui->pushButton_startRecording->setText("Start recording");
            ui->comboBox_edititngOptions->setEnabled(false);
            ui->horizontalSlider_bag->setEnabled(false);
            ui->pushButton_play->setEnabled(false);
            ui->pushButton_startPlacing->setEnabled(true);
            ui->pushButton_save->setEnabled(false);

        }

        ui->comboBox_modelNames->setEnabled(true);
        ui->pushButton_startRecording->setEnabled(true);
        ui->pushButton_load->setEnabled(true);
        ui->pushButton_startPlacing->setText("Start placing");
        ui->pushButton_play->setText("Play");
    }
    // Something is running
    else
    {

        ui->comboBox_edititngOptions->setEnabled(false);
        ui->horizontalSlider_bag->setEnabled(false);
        ui->comboBox_modelNames->setEnabled(false);
        ui->pushButton_save->setEnabled(false);
        ui->pushButton_load->setEnabled(false);

        // Record is running
        if(state == ERS_RECORDING_RUNNING)
        {
            ui->pushButton_startRecording->setText("Stop recording");
            ui->pushButton_startRecording->setEnabled(true);
            ui->pushButton_startPlacing->setEnabled(false);
            ui->pushButton_play->setEnabled(false);
            ui->label_userInfo->setText("Recording...");
        }

        // Editing running
        if(state == ERS_EDITING_RUNNING)
        {
            ui->pushButton_startRecording->setText("Stop editing");
            ui->pushButton_startRecording->setEnabled(true);
            ui->pushButton_startPlacing->setEnabled(false);
            ui->pushButton_play->setEnabled(false);
            ui->label_userInfo->setText("Editing...");
        }

        // Placing running
        if(state == ERS_PLACING_RUNNING)
        {
            ui->pushButton_startPlacing->setText("Stop placing");
            ui->pushButton_startRecording->setEnabled(false);
            ui->pushButton_startPlacing->setEnabled(true);
            ui->pushButton_play->setEnabled(false);
            ui->label_userInfo->setText("Placing...");

        }

        // Playing is running
        if(state == ERS_PLAYING_RUNNING)
        {
            ui->pushButton_play->setText("Stop");
            ui->pushButton_load->setEnabled(false);
            ui->pushButton_save->setEnabled(false);
            ui->pushButton_startRecording->setEnabled(false);
            ui->label_userInfo->setText("Play...");

        }
    }
}

void MainWindow::showCriticalMessageBox(QString message)
{
    QMessageBox::critical(this, "Error:", message,QMessageBox::Ok);
}

void MainWindow::noJoystickData()
{
    ui->label_userInfo->setText("Waiting for joystick data. Please make sure joystick is connected and press a button.");
}
}
