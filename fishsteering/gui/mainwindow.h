/*! Copyright (C) 2017  	Department of Electrical Engineering & Computer Science, Institute of
 *			Real-Time Learning Systems, University of Siegen
 *
 *			Research Group of Ecology and Behavioral Biology, Institute of Biology,
 *			University of Siegen
 *
 * Contact: virtual.fish.project@gmail.com
 *
 * License: GNU General Public License  See LICENSE.txt for the full license.
 *
 * This file is part of the "FishSim Animation Toolchain".
 * "FishSim Animation Toolchain" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "FishSim Animation Toolchain" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "FishSim Animation Toolchain".  If not, see <http: *www.gnu.org/licenses/>.
 *
 * The "FishSim Animation Toolchain" was developed within the scope of the interdisciplinary,
 * DFG (Deutsche Forschungsgemeinschaft)-funded project “virtual fish” (KU
 * 689/11-1 and Wi 1531/12-1) of the Institute of Real-Time Learning Systems
 * (EZLS) and the Research Group of Ecology and Behavioral Biology at the
 * University of Siegen.
 *
*/
#ifndef H_MAINWINDOW_H
#define H_MAINWINDOW_H

#include <qt5/QtWidgets/QMainWindow>
#include <QDateTime>
#include <QLineEdit>
#include <iostream>
#include <QTimer>
#include <QWidget>
#include <QFileDialog>
#include <QMessageBox>

#include "ros/message_traits.h"
#include <include/steeringrecorder.h>
#include <yaml-cpp/yaml.h>
#include <iostream>
#include <fstream>

// Fishsim includes
#include <scene/fishsim_scene_manager.h>

#include "ui_mainwindow.h"

namespace Ui {
    class MainWindow;
}

namespace fishsteering
{

class MainWindow : public QMainWindow
{
    Q_OBJECT


public:
    explicit MainWindow(QWidget *parent = 0);

    ~MainWindow();

public slots:

    void placingButtonPressed();
    void recordButtonPressed();
    void playButtonPressed();
    void saveSettingsPressed();

    void startRecording();
    void stopRecording();
    void startEditing();
    void stopEditing();
    void loadBagFile();
    void saveBagFile();
    void setTimeLabelBag(int msec);
    void bagReady();
    void startPlaying();
    void stopPlaying();
    void timeSliderActionTriggered(int action);
    void setModelName(int);
    void noJoystickData();
    void updateGUI();
    

private:

    void connectSignalsAndSlots();
    
    Ui::MainWindow *ui;

    SteeringRecorder* steeringRec;


    irr::IrrlichtDevice *m_device;
    fishsim::FishSimSceneManager *m_fishSimSmgr;

    QLineEdit* m_forewardSpeedEdit;
    QLineEdit* m_yawSpeedEdit;
    QLineEdit* m_pitchSpeedEdit;

    QPushButton* m_pushButtonSaveSettings;

    void startPlacing();

    void stopPlacing();


    void fillModelListBox();

    void fillEditingListBox();

    void showCriticalMessageBox(QString message);

    void createSettingsSection();


};

}

#endif // H_MAINWINDOW_H
