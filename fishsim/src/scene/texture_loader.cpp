#include "scene/texture_loader.h"

#include "sim/fishsim_globals.h"
#include "utils/fishsim_logger.h"
using namespace irr;

namespace fishsim
{

TextureLoader::TextureLoader(video::IVideoDriver *driver, io::IFileSystem *filesystem){

    m_driver = driver;
    m_filesystem = filesystem;

    m_filesystem->grab();
    m_driver->grab();

    m_isLoadRunning = false;

    // Get all files in models folder
    const io::path restoreDir = m_filesystem->getWorkingDirectory();
    m_filesystem->changeWorkingDirectoryTo(restoreDir + MODELS_PATH.c_str());
    m_files = m_filesystem->createFileList();
    m_filesystem->changeWorkingDirectoryTo(restoreDir);

    // Start loading
    loadTextures();


}

TextureLoader::~TextureLoader()
{
    // Stop loading of still in progress
    if(m_isLoadRunning)
        m_isLoadRunning = false;

    if(m_driver)
        m_driver->drop();

    if(m_filesystem)
        m_filesystem->drop();

    clear();
}

std::vector<video::ITexture*> TextureLoader::getTextures(const std::string &prefix){


    std::vector<video::ITexture*> textures;

    io::path fileName;
    std::string filePrefix;
    for(int i = 0; i < m_textures.size(); i++)
    {
        fileName = m_textures.at(i)->getName().getPath();
        fileName = m_filesystem->getFileBasename(fileName, false);

        filePrefix = fileName.subString(0, prefix.length(),false).c_str();

        if(filePrefix == prefix)
        {
            textures.push_back(m_textures.at(i));
        }

    }

    return textures;
}

void TextureLoader::clear(){

}

void TextureLoader::loadTextures()
{

    logInfo("Texture loader: start loading all textures from " + std::string(m_textureDir));
    m_isLoadRunning = true;

    std::string fileName;
    io::path extension;

    if (m_files)
    {
        for (u32 i = 0; i < m_files->getFileCount(); ++i)
        {
            fileName = m_files->getFileName(i).c_str();

            core::getFileNameExtension(extension, m_files->getFullFileName(i));
            extension.make_lower();

            if(extension == ".png" )
            {
                video::ITexture *tex = m_driver->getTexture(m_files->getFullFileName(i));

                if(tex != 0)
                {
                    m_textures.push_back(tex);
                }
            }
        }
    }

    m_isLoadRunning = false;


}

} // end namespace fishsim
