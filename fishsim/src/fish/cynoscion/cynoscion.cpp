#include "fish/cynoscion/cynoscion.h"
#include "sim/fishsim_globals.h"
#include "ros/joy_indices.h"

using namespace irr;
using namespace scene;
using namespace core;

// Gets multiplied with foreward input
#define FOREWARD_MULTIPLIER 0.8

// Gets multiplied with yaw input
#define YAW_MULTIPLIER 6.0

// Gets multiplied with pitch input
#define PITCH_MULTIPLIER 2.0

namespace fishsim
{

Cynoscion::Cynoscion(IAnimatedMeshSceneNode *node) : Fish(node)
{

    m_dorsalFinModifier = 0;
    m_forewardSwimModifier = 0;
    m_turnSwimModifier = 0;
    m_upSwimModifier = 0;

    m_forewardMultiplier = FOREWARD_MULTIPLIER;
    m_yawMultiplier = YAW_MULTIPLIER;
    m_pitchMultiplier = PITCH_MULTIPLIER;
}

Cynoscion::~Cynoscion()
{
    unload();
}

bool Cynoscion::unload()
{

    if(m_dorsalFinModifier)
    {
        delete m_dorsalFinModifier;
        m_dorsalFinModifier = 0;
    }

    if(m_forewardSwimModifier)
    {
        delete m_forewardSwimModifier;
        m_forewardSwimModifier = 0;
    }

    if(m_turnSwimModifier)
    {
        delete m_turnSwimModifier;
        m_turnSwimModifier = 0;
    }

    if(m_upSwimModifier)
    {
        delete m_upSwimModifier;
        m_upSwimModifier = 0;
    }

    m_boneModifiers.clear();

    Fish::unload();

    return true;
}

bool Cynoscion::load()
{

    /// 1. Mesh scene node settings like material flags, debug stuff etc..
    Fish::load();

    /// 2. Bone renaming. Remove bone prefix.
    Fish::removeBonePrefix("head");

    /// 3. Init editable body parts of the fish.
    initEditableMaterials();

    /// 4. init bone modifier
    initBoneModifier();

    return true;
}


void Cynoscion::initEditableMaterials(){

    // Body should not be transparent
    //m_irrNode->getMaterial(0).MaterialType = irr::video::EMT_SOLID;

    makeMaterialEditable(0, L"body", true);
    makeMaterialEditable(1, L"dorsal", true);
    makeMaterialEditable(2, L"caudal", true);
    makeMaterialEditable(3, L"pectoral", true);
    makeMaterialEditable(4, L"anal", true);
    makeMaterialEditable(5, L"pelvic", true);
    makeMaterialEditable(6, L"adipose", true);


}

void Cynoscion::initBoneModifier(){

    /// Init modifiers that are only used in direct mode:
    /// They only get used in move function. Not visible in
    /// fishsteering.

    // Foreward swimming animation
    m_forewardSwimModifier = new ForewardSwimModifier(m_irrNode, "foreward swim animator");
    m_forewardSwimModifier->init();
    m_forewardSwimModifier->setAmplitude(0.3);
    m_forewardSwimModifier->setSpeed(0.5);

    // Idle animation
    m_idleModifier = new IdleModifier(m_irrNode, "idle animator", 0.2);
    m_idleModifier->init();

    // Turn animation
    m_turnSwimModifier = new TurnSwimModifier(m_irrNode, "turn animator",  0.1);
    m_turnSwimModifier->init();

    // up/down animation
    m_upSwimModifier = new UpSwimModifier(m_irrNode, "up/down animator",  0.05);
    m_upSwimModifier->init();


    /// Init bone modifiers that can be used in indirect mode too.
    /// They are called from move and are also able to modify fishsnapshots
    /// through their modify function.
    /// These modifier will show up in fishsteering.

    // Dorsal fin
    m_dorsalFinModifier = new DorsalFinModifier(m_irrNode, "Dorsal fin (L1/L2)", 3.5);
    if(m_dorsalFinModifier->init())
    {
        // Use dorsal fin 1 independantly
        m_dorsalFinModifier->useConnectedMode(false);
        m_dorsalFinModifier->setDorsalFin1Inverted(true);
        m_dorsalFinModifier->setDorsalFin2Inverted(true);
        addBoneModifier(m_dorsalFinModifier);
    }
}

void Cynoscion::move(const sensor_msgs::Joy& joyState){

    float forewardAmount = joyState.axes.at(PS3_AXIS_STICK_LEFT_UPWARDS) * m_forewardMultiplier;
    float yawAmount = joyState.axes.at(PS3_AXIS_STICK_RIGHT_LEFTWARDS) * m_yawMultiplier;
    float pitchAmount = joyState.axes.at(PS3_AXIS_STICK_RIGHT_UPWARDS) * m_pitchMultiplier;

    move(forewardAmount,0.0, 0.0 ,yawAmount,pitchAmount,0.0);

    m_dorsalFinModifier->update(joyState);

}

void Cynoscion::move(float forewardAmount, float sidewardAmount, float upwardAmount, float yawAmount, float pitchAmount, float rollAmount)
{

    forewardAmount = Fish::getForewardSmoothed(forewardAmount);
    Fish::move(forewardAmount, sidewardAmount,  upwardAmount,  yawAmount,  pitchAmount,  rollAmount);

    // Update foreward swim modifier. This modifier uses absolute rotation
    // values. It should be called first, since it resets all rotation values
    // from the last move call
    m_forewardSwimModifier->update(forewardAmount);

    // Do turn and up animation. These modifier limit the rotation values to
    // a maximum.
    m_turnSwimModifier->update(yawAmount);
    m_upSwimModifier->update(pitchAmount);

    // Idle modifier kicks in, if foreward amount is small or near zero.
    // Will play idle animation
    m_idleModifier->update(forewardAmount);
}


} // end namespace fishsim
