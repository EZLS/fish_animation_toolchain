/*! Copyright (C) 2017  	Department of Electrical Engineering & Computer Science, Institute of
 *			Real-Time Learning Systems, University of Siegen
 *
 *			Research Group of Ecology and Behavioral Biology, Institute of Biology,
 *			University of Siegen
 *
 * Contact: virtual.fish.project@gmail.com
 *
 * License: GNU General Public License  See LICENSE.txt for the full license.
 *
 * This file is part of the "FishSim Animation Toolchain".
 * "FishSim Animation Toolchain" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "FishSim Animation Toolchain" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "FishSim Animation Toolchain".  If not, see <http: *www.gnu.org/licenses/>.
 *
 * The "FishSim Animation Toolchain" was developed within the scope of the interdisciplinary,
 * DFG (Deutsche Forschungsgemeinschaft)-funded project “virtual fish” (KU
 * 689/11-1 and Wi 1531/12-1) of the Institute of Real-Time Learning Systems
 * (EZLS) and the Research Group of Ecology and Behavioral Biology at the
 * University of Siegen.
 *
 *  \file irrhandler.h
 *  \brief Entry point of simulation. Will initialize all simulation components and draws everything to screen.
 *
 * Irrhandler initializes device, scenemanager, GUI and event receiver in the init() function. Once
 * initialization has been done, everything can be drawn to screen by calling exec() repeatedly.
 *
 * Irrhandler also provides interface functions to roshandler, like load and save scene as well as adding new
 * fish snapshots.
 *
*/

#ifndef H_IRRHANDLER_H
#define H_IRRHANDLER_H

#include <irrlicht.h>
#include <string>
#include <vector>

#include <ros/ros.h>
#include <ros/package.h>

#include "irrExt/xEffects/XEffects.h"
#include "scene/fishsim_scene_manager.h"
#include "fishsim_event_receiver.h"
#include "gui/fishsim_GUI.h"


namespace fishsim
{


class IrrHandler
{
    public:

        /*!
           \brief Default Constructor
         */
		IrrHandler();


        /*!
          \brief Destructor
         */
        ~IrrHandler();


        /*!
         * \brief Init irrlicht engine. Device, driver, shadowManager and sceneManager are created here.

           \param width Width size of window.
           \param height Height size of window.
           \param fullscreen if true, simulation will run in fullscreen mode.
           \param device pointer to an existing device. If provided, irrhandler will use this device
            instead of creating a new one.
           \return true, if all components could be initialized successfully. False if something went wrong.
         */
        bool init(uint width = 960, uint height = 540, bool fullscreen = false,
                  irr::IrrlichtDevice* device = NULL);


        /*!
           \brief Run the irrlicht device.
           \return Returns false if device wants to be deleted.
         */
		bool run();


        /*!
           \brief Render the current scene.
         */
        void exec();


        /*!
           \brief Drops the irrlicht device.
         */
        void drop();


        /*!
           \brief Get name of all models in scene.

           \return vector with names of all models in scene
        */
        std::vector<std::string> getFishModels();


        /*!
           \brief Add a new snapshot for given fish model. Snapshots are used for movement interpolation.

           \param snapshot the snapshot
           \param modelName name of the fish model
        */
        void addSnapshot(FishSnapshot& snapshot, const std::string& modelName);


        /*!
           \brief Load default scene

           \return true, if scene was loaded successfully. False otherwise.
         */
        bool load();


        /*!
           \brief Load a scene from given filepath.

           \param scenePath the filepath of the scene that should be loaded
           \return true, if scene was loaded successfully. False otherwise.
        */
        bool loadScene(const std::string& scenePath, bool hideFishModels);


        /*!
         * \brief Move simulation window.
         * \param x new X position.
         * \param y new Y position.
         */
        void moveWindow(int x, int y);

        /*!
         * \brief Resize simulation window.
         * \param w new width of window.
         * \param h new height of window.
         */
        void resizeWindow(int w, int h);

        /*!
           \brief Set the X window class hint. Used to
           let unity know which programs it should group in the
           launcher.
         */
        void setWindowClass();

        /*!
         * \brief Set the title of the window
         * \param newTitle the new title.
         */
        void setTitle(const std::string& newTitle);

        void setRealFishPosition(irr::core::vector3df fishPos);

        /// TODO: for further usage, if we need to load/unload through ros commands
//        /*!
//           \brief Load a single model (fish model) from given path to scene.
//           Won't work for any other models than fish models, because the model
//           must have the appropriate bones.

//           \param [in] path the filepath of the model
//           \param [in] name the name that the model will get in simulation. Used to
//                       identify the model later on.

//           \return true, if model was loaded properly, false otherwise.
//         */
//        bool loadFishModel(const std::string& path);


//        /*!
//           \brief Unload a model with given name from scene.

//           \param [in] name the name that the model will be unloaded.

//           \return true, if model was found in scene and unloaded properly. False otherwise
//         */
//        bool unloadFishModel(const std::string& name);


//        /*!
//           \brief Unload all current fish nodes from scene and load new ones that are
//           stored at given path.

//           \param [in] folderPath the folderpath with the new models to load.

//           \return true, if model was found in scene and unloaded properly. False otherwise
//         */
//        bool exchangeFishModels(const std::string& folderPath);


//        /*!
//           \brief Get the name of all bones that belong to the given models.

//           \return vector with names of all bones of the given model
//        */
//        std::vector<std::string> getModelBones(const std::string& modelName);

        /// TODO END

    private:

        //! Irrlicht device
        irr::IrrlichtDevice* m_device;

        /// Irrlicht driver
        irr::video::IVideoDriver* m_driver;

        /// Irrlicht scene manager
        irr::scene::ISceneManager* m_smgr;

        /// Irrlicht GUI environment
        irr::gui::IGUIEnvironment* m_guienv;

        /// FishSimSceneManager that handles loading/unloading of environment and fish models
        /// as well as managing these throughout simulation.
        FishSimSceneManager* m_fishSimSmgr;

        /// XEffect handler. Is used to draw soft shadows in Irrlicht.
        EffectHandler* m_shadowEffects;

        /// Only true if irrlicht simulation is running in local FishSteering widget.
        bool m_runningInLib;

        /// Custom event receiver for handeling button clicks,
        /// menue events, mouse/keyboard events etc..
        FishSimEventReceiver *m_eventReceiver;

        /// Initialize and draws GUI and is responsible for creating
        /// setting windows.
        FishSimGUI *m_gui;

        /// Path where fishsim is located
        std::string m_fishSimPath;

        irr::core::vector3df m_realFishPos;

        /*!
         * \brief When in edit mode: draw 3d axis
         */
        void draw3DAxes();

};

} // end namespace fishsim

#endif // H_IRRHANDLER_H
