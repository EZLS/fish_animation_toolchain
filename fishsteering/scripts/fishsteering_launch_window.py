#!/usr/bin/env python
# Copyright (C) 2017  	Department of Electrical Engineering & Computer Science, Institute of
#			Real-Time Learning Systems, University of Siegen
#
#			Research Group of Ecology and Behavioral Biology, Institute of Biology,
#			University of Siegen
#
# Contact: virtual.fish.project@gmail.com
#
# License: GNU General Public License  See LICENSE.txt for the full license.
#
# This file is part of the "FishSim Animation Toolchain".
# "FishSim Animation Toolchain" is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# "FishSim Animation Toolchain" is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with "FishSim Animation Toolchain".  If not, see <http: *www.gnu.org/licenses/>.
#
# The "FishSim Animation Toolchain" was developed within the scope of the interdisciplinary,
# DFG (Deutsche Forschungsgemeinschaft)-funded project "virtual fish" (KU
# 689/11-1 and Wi 1531/12-1) of the Institute of Real-Time Learning Systems
# (EZLS) and the Research Group of Ecology and Behavioral Biology at the
# University of Siegen.
#
#

import os
import rospy
import rospkg
import rosparam
import subprocess
import time
import yaml
import roslaunch
from PyQt5.QtWidgets import (QWidget, QMainWindow, QDesktopWidget, QLabel,
  QApplication, QPushButton, QHBoxLayout, QVBoxLayout, QMessageBox, QComboBox)
from PyQt5.QtCore import Qt

class FishSteeringLaunchWindow(QWidget):

    __DEFAULT_CONFIG = {"input_device": "/dev/input/js0", "joystick_mapping": "fishsim_joystick_mapping_ps3.yaml"}

    _DEVICE_PATH = "/dev/input/"
    
    def __init__(self):
        super(FishSteeringLaunchWindow, self).__init__()

        # Path where config file is located
        rospack = rospkg.RosPack()
        self.__config_filepath = rospack.get_path('fishsteering') + "/config/fishteering_launch_config.yaml"

        self.__config = self._load_config()

        # Roslaunch instance
        self.__roslaunch = roslaunch.scriptapi.ROSLaunch()

        # Create user interface
        self._init_UI()

    


    def __del__(self):
        print("Terminate")

    def _validate_config(self, config):
         
        # Make sure  fishsim config is still valid 
        if(len(config) == 0):
            return self.__DEFAULT_CONFIG
        try:
            if 'input_device' in config and 'joystick_mapping' in config:
                return config
            else:
                return self.__DEFAULT_CONFIG
        except ValueError:
            print("Config corrupted. Will load default config")
            return self.__DEFAULT_CONFIG


    def _load_config(self):

        # Get fishsteering configuration from yaml config file
        try:
            with open(self.__config_filepath, 'r') as infile:
                config = yaml.load(infile)
        
        except IOError, yaml.scanner.ScannerError:
            print("Config file corrupted or not found. Will load default config")        
            config = self.__DEFAULT_CONFIG
        
        return self._validate_config(config)

    # Get current joystick input devices in self._DEVICE_PATH (default: /dev/input) 
    def _get_input_devices(self):

        path = self._DEVICE_PATH
        devices = []
        for i in os.listdir(path):
            if  'js' in i:
                devices.append(i)
        return devices

    # Get available mapping files in fishsteering/config
    def _get_mappings(self):
        
        rospack = rospkg.RosPack()
        path = rospack.get_path('fishsteering') + "/config/"
        mappings = []
        for i in os.listdir(path):
            if os.path.isfile(os.path.join(path,i)) and 'joystick_mapping' in i:
                mappings.append(i)
        return mappings
    

    
    def _save_config(self):

        # Update configuration file from GUI
        self.__config = dict()

        dev = self.__comboBoxInput.currentText()
        self.__config['input_device'] = dev

        mapping = self.__comboBoxMapping.currentText()
        self.__config['joystick_mapping'] = mapping
        try:           
           self._write_yaml_data(self.__config, self.__config_filepath)
        except IOError:
            print("Could not write configration to file")
    

    def _write_yaml_data(self, dictionary, filepath):
        
        # Save config to yaml file
        with open(filepath, 'w') as outfile:
            outfile.write(yaml.safe_dump(dictionary, default_flow_style=False))
        
    def _init_UI(self):

        # Horizontal box layout for whole window
        vbox = QVBoxLayout()
        vbox.setSpacing(0)
        self.setLayout(vbox)

        # Description
        label_descr = QLabel("Select Input device and joystick mapping file.") 
        label_descr.setWordWrap(True)
        vbox.addWidget(label_descr)

        # Input device
        label_input = QLabel("Input device: ")
        self.__comboBoxInput = QComboBox(self)

        for dev in self._get_input_devices():
            self.__comboBoxInput.addItem(self._DEVICE_PATH + dev)

        # Get index of the resolution that matches the config
        dev = str(self.__config['input_device']) 
        index = self.__comboBoxInput.findData(dev, 0)
        if index != -1:
            self.__comboBoxInput.setCurrentIndex(index)

        hboxRes = QHBoxLayout()
        hboxRes.addWidget(label_input)
        hboxRes.addWidget(self.__comboBoxInput)
        vbox.addLayout(hboxRes)
        
        # Mapping
        label_mapping = QLabel("Mapping: ")
        self.__comboBoxMapping = QComboBox(self)
        for m in self._get_mappings():
            self.__comboBoxMapping.addItem(m)

        # Get index of the mapping that matches the config
        mapping = str(self.__config['joystick_mapping']) 
        index = self.__comboBoxMapping.findData(mapping, 0)
        if index != -1:
            self.__comboBoxMapping.setCurrentIndex(index)

        hboxQuality = QHBoxLayout()
        hboxQuality.addWidget(label_mapping)
        hboxQuality.addWidget(self.__comboBoxMapping)
        vbox.addLayout(hboxQuality)
        

        # Launch button

        if len(self._get_input_devices()) is not 0:
            self.__launch_button = QPushButton('Launch', self)
            self.__launch_button.clicked.connect(self.launch) 
            vbox.addWidget(self.__launch_button)

        else:
            label_error = QLabel("No input device found. Please connect controller and restart fishsteering.") 
            vbox.addWidget(label_error)
       
        self.setGeometry(300, 300, 300, 220)
        self.setWindowTitle('FishSteering launcher')    
        self.show()



        

    def launch(self):

        # Save monitor configuration
        self._save_config()
        
        self.__roslaunch.start()

        # Upload mapping params
        mapping_file = rospkg.RosPack().get_path('fishsteering') + "/config/" + self.__config["joystick_mapping"]
        mapping_params = rosparam.load_file(mapping_file)
        rosparam.upload_params("", mapping_params[0][0])

        # Upload device params
        dev_params = {'/joy_node/dev': self.__config["input_device"]}
        rosparam.upload_params("", dev_params)
        
        fishsteering_node = roslaunch.core.Node('fishsteering', 'fishsteering', 'fishsteering', '/', None)
        fishsteering_process = self.__roslaunch.launch(fishsteering_node)

        joy_node = roslaunch.core.Node('joy', 'joy_node', 'joy_node', '/', None)
        joy_process = self.__roslaunch.launch(joy_node)


                
        self.hide()

        
        # Check if all process are still running
        all_nodes_running = True
        while all_nodes_running:
            time.sleep(1)
            all_nodes_running = joy_process.is_alive() and fishsteering_process.is_alive()
            
        joy_process.stop()
        fishsteering_process.stop()
           
        self.show()
        


