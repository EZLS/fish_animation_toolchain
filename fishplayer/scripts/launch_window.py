#!/usr/bin/env python
# Copyright (C) 2017  	Department of Electrical Engineering & Computer Science, Institute of
#			Real-Time Learning Systems, University of Siegen
#
#			Research Group of Ecology and Behavioral Biology, Institute of Biology,
#			University of Siegen
#
# Contact: virtual.fish.project@gmail.com
#
# License: GNU General Public License  See LICENSE.txt for the full license.
#
# This file is part of the "FishSim Animation Toolchain".
# "FishSim Animation Toolchain" is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# "FishSim Animation Toolchain" is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with "FishSim Animation Toolchain".  If not, see <http: *www.gnu.org/licenses/>.
#
# The "FishSim Animation Toolchain" was developed within the scope of the interdisciplinary,
# DFG (Deutsche Forschungsgemeinschaft)-funded project "virtual fish" (KU
# 689/11-1 and Wi 1531/12-1) of the Institute of Real-Time Learning Systems
# (EZLS) and the Research Group of Ecology and Behavioral Biology at the
# University of Siegen.
#


import rospy
import rospkg
import rosparam
import subprocess
import time
import yaml
import roslaunch
from PyQt5.QtWidgets import (QWidget, QMainWindow, QDesktopWidget, QLabel,
  QApplication, QPushButton, QHBoxLayout, QVBoxLayout, QMessageBox)
from PyQt5.QtCore import Qt

from fishsim_instance import FishSimInstance
from monitor_identification_window import MonitorIdentifier


# TODO: QMainWindow vs QWidget
class LaunchWindow(QWidget):

    _DEFAULT_CONFIG = {"left": {"active": True, "monitor_num": 1},
                       "right":{"active": True, "monitor_num": 1},
                       "player":{"active": True, "monitor_num": 1}}
    
    def __init__(self):
        super(LaunchWindow, self).__init__()

        # Path where config file is located
        rospack = rospkg.RosPack()
        self._config_filepath = rospack.get_path('fishplayer') + "/config/monitor_config.yaml"
        self._params_filepath = rospack.get_path('fishplayer') + "/config/fishplayer_params.yaml"

        # List containing monitor geometries
        self._monitor_list = self._get_monitor_list()

        # Configuration of fishsim instances and the monitor 
        # number they will show on
        self._config = self._load_config()

        # Keep track of all identification windows that have
        # been created.
        self._monitor_identification_windows = []

        # All fishsim instances that have been configured to launch
        self._fishsim_instances = []

        # Start roscore and upload parameter to parameter server
        #self._roscore_process =  subprocess.Popen('roscore')

        # Roslaunch instance
        self._roslaunch = roslaunch.scriptapi.ROSLaunch()

        # Create user interface
        self._init_UI()

    def __del__(self):
        pass
        #self._roscore_process.terminate()



    def _validate_config(self, config):
         
        # Make sure monitor configuration is still valid for current monitor 
        # setup

        if(config == None):
            print("config not found. Use default config")
            return self._DEFAULT_CONFIG

        if(len(config) == 0):
            print("config not found. Use default config")
            return self._DEFAULT_CONFIG

        try:
            for entry in config:
                if int(entry["monitor_num"]) > len(self._monitor_list) or int(entry["monitor_num"]) <= 0:
                    entry["monitor_num"] = 1
            return config
        except ValueError:
            print("Config corrupted. Will load default config")
            return self._DEFAULT_CONFIG


    def _load_config(self):


        # Get monitor configuration from yaml config file
        try:

            with open(self._config_filepath, 'r') as infile:
                config = yaml.load(infile)
    
        except IOError, yaml.scanner.ScannerError:
            print("Config file corrupted or not found. Will load default config")        
            config = self._DEFAULT_CONFIG
        
        return self._validate_config(config)


    def _save_config(self):

        # Update configuration file from GUI
        self._config = []       
        for fishsim in self._fishsim_instances:
            instance = dict()
            instance["namespace"] = fishsim.get_namespace()
            instance["active"] = fishsim.is_active()
            instance["monitor_num"] =fishsim.get_monitor()
            #self._config[fishsim.get_namespace()] = instance
            self._config.append(instance)
        try:           
           self._write_yaml_data(self._config, self._config_filepath)
        except IOError:
            print("Could not write configration to file")

    def _write_yaml_data(self, dictionary, filepath):
        
        # Save config to yaml file
        with open(filepath, 'w') as outfile:
            outfile.write(yaml.safe_dump(dictionary, default_flow_style=False))
        

    def _get_monitor_list(self):

        # Get the number of monitors and their geometry
        desktop = QApplication.desktop()
        num_screens = desktop.screenCount()

        monitor_list = []
        for i in range(0, num_screens):
            monitor_list.append(desktop.screenGeometry(i))
        
        print(monitor_list)
        return monitor_list   


    def _show_dialog(self, text, title):
        msg = QMessageBox()
        msg.setIcon(QMessageBox.Information)

        msg.setText(str(text))
        #msg.setInformativeText("This is additional information")
        msg.setWindowTitle(title)
        #msg.setDetailedText("The details are as follows:")
        msg.setStandardButtons(QMessageBox.Ok)
        #msg.buttonClicked.connect(msgbtn)
	
        msg.exec_()


    def _init_UI(self):
        
        # Vertical box layout
        vbox = QVBoxLayout()
        vbox.setSpacing(0)
        self.setLayout(vbox)


        #label_descr = QLabel("desc") 
        #label_descr.setWordWrap(True);
        #vbox.addWidget(label_descr)

        label_active = QLabel("Active: ")
        label_active.setAlignment(Qt.AlignCenter |Qt.AlignLeft)

        label_ns = QLabel("Namespace: ")
        label_ns.setAlignment(Qt.AlignCenter |Qt.AlignLeft)

        label_m = QLabel("Monitor: ")
        label_m.setAlignment(Qt.AlignCenter | Qt.AlignRight)

        headlines = QHBoxLayout()
        headlines.addWidget(label_active)
        headlines.addWidget(label_ns)
        headlines.addWidget(label_m)
        
        vbox.addLayout(headlines)
            
        # Create a fishsim widget for every entry in the config
        for entry in self._config:
            #entry_properties = self._config[entry]
            instance = FishSimInstance(entry["namespace"], entry["monitor_num"], entry["active"], len(self._monitor_list), self._roslaunch)         
            self._fishsim_instances.append(instance)
            vbox.addWidget(instance)
            
        # Identify monitors Button
        self._identify_monitors = QPushButton('Identify monitors', self)
        self._identify_monitors.clicked.connect(self.identify_monitors)        
        vbox.addWidget(self._identify_monitors)

        # Launch button
        self._launch_button = QPushButton('Launch', self)
        self._launch_button.clicked.connect(self.launch) 
        vbox.addWidget(self._launch_button)
           
        self.setGeometry(320, 320, 240, 240)
        self.setWindowTitle('Fishplayer launcher')    
        self.show()

    def _create_params(self):
        namespaces = dict()
        for fishsim in self._fishsim_instances:
        
            monitor_num = fishsim.get_monitor() -1
            monitor_rect = self._monitor_list[monitor_num]
            
            params = dict()
            params['win_x_pos'] = monitor_rect.left()
            params['win_y_pos'] = monitor_rect.top()
            params['win_height'] = monitor_rect.height()
            params['win_width'] = monitor_rect.width()
            params['win_title'] = "FishSim " + str(fishsim.get_namespace())
            params['active'] = fishsim.is_active()
        
            namespaces[str(fishsim.get_namespace())] = dict()
            namespaces[str(fishsim.get_namespace())]['fishsim'] = params             
        
        return namespaces

        

    def launch(self):

        # Save monitor configuration
        self._save_config()

        # Create parameters for the current configuration and
        # save them to file
        params = self._create_params()
        self._write_yaml_data(params, self._params_filepath)
             
        self._roslaunch.start()

        rosparam.upload_params("", params)

        # Start fishsim instances
        for fishsim in self._fishsim_instances:
            fishsim.launch()
            time.sleep(0.5)

        fishplayer_node = roslaunch.core.Node('fishplayer', 'fishplayer', 'fishplayer')
        fishplayer_process = self._roslaunch.launch(fishplayer_node)
                
        self.hide()

        # Check if all process are still running
        all_nodes_running = True
        while all_nodes_running:
            time.sleep(1)
            
            for fishsim in self._fishsim_instances:
                if(fishsim.is_active()):
                    all_nodes_running = all_nodes_running and fishsim.is_running()

            all_nodes_running = all_nodes_running and fishplayer_process.is_alive()
            print(all_nodes_running)




        # Kill all remaining processes
        for fishsim in self._fishsim_instances:
            fishsim.stop_process()

        fishplayer_process.stop()
        self.show()

                
        

    def identify_monitors(self):

        # remove all old windows
        for window in self._monitor_identification_windows:
            window.close()
        self._monitor_identification_windows = []

        # Create a window on every screen showing monitor number
        i = 1
        for monitor in self._monitor_list:
            
            w = MonitorIdentifier(self)
            w.move(monitor.center())
            w.setWindowTitle("Monitor " + str(i))
            w.show()
            self._monitor_identification_windows.append(w)
            i = i + 1

