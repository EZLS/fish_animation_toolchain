/*! Copyright (C) 2017  	Department of Electrical Engineering & Computer Science, Institute of
 *			Real-Time Learning Systems, University of Siegen
 *
 *			Research Group of Ecology and Behavioral Biology, Institute of Biology,
 *			University of Siegen
 *
 * Contact: virtual.fish.project@gmail.com
 *
 * License: GNU General Public License  See LICENSE.txt for the full license.
 *
 * This file is part of the "FishSim Animation Toolchain".
 * "FishSim Animation Toolchain" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "FishSim Animation Toolchain" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "FishSim Animation Toolchain".  If not, see <http: *www.gnu.org/licenses/>.
 *
 * The "FishSim Animation Toolchain" was developed within the scope of the interdisciplinary,
 * DFG (Deutsche Forschungsgemeinschaft)-funded project “virtual fish” (KU
 * 689/11-1 and Wi 1531/12-1) of the Institute of Real-Time Learning Systems
 * (EZLS) and the Research Group of Ecology and Behavioral Biology at the
 * University of Siegen.
 *
 *  \file fishsim_helper_routines.h
 *  \brief Some functions that are commonly used over the entire programm.
 *
*/


#ifndef H_FISHSIM_HELPER_ROUTINES_H
#define H_FISHSIM_HELPER_ROUTINES_H

#include <irrlicht.h>

#include <string>
#include <vector>
#include <geometry_msgs/Vector3.h>



namespace fishsim
{

/*!
 * \brief  Convert a ROS vector3 to irrlicht vector3.
 * \param vec a vector to be converted as ROS vector3.
 * \return same vector as irrlicht vector3.
 */
inline irr::core::vector3df ROSVec3ToIrrVec3(const geometry_msgs::Vector3 &vec){

    return irr::core::vector3df(vec.x, vec.y, vec.z);
}
///
/*!
 * \brief Convert a irrlicht vector3 to ROS vector3.
 * \param vec a vector as irrlicht vector3.
 * \return same vector as ROS vector3.
 */
inline geometry_msgs::Vector3 IrrVec3ToROSVec3(const irr::core::vector3df &vec){

    geometry_msgs::Vector3 rosVec;
    rosVec.x = vec.X;
    rosVec.y = vec.Y;
    rosVec.z = vec.Z;
    return rosVec;
}

/// Get the percentage value of x between min and max.
/// Will return 1.0, if x >= max
/// Will return 0.0, if x <= min
/// Will return percentage otherwise
inline float percentage(float x, float max, float min = 0){

    if(x > max)
        return 1.0;
    else if(x < min)
        return 0.0;
    else
        return (x - min) / (max - min);
}


//inline unsigned int generateComposedID(unsigned int id, unsigned int metaID){

//    //   metaId        ID
//    //0x  0000        0000

//    unsigned int result = 0x00000000;

//    result = result | id;
//    result = result | (metaID << 16);
//    std::cout << "result " << std::endl;std::cout  << result << std::endl;
//}

//inline unsigned int getIDFromComposed(unsigned int composedID)
//{
////    return (composedID & 0x0000FFFF);
////     n & ((1 << lo) - 1)
//             return composedID & ((1 << 16) -1);
//}

//inline unsigned int getMetaIDFromComposed(unsigned int composedID)
//{
//    return (composedID >> 16);
//}

/*!
 * \brief generate a vector3 from strings.
 * \param X value of X-component as a string.
 * \param Y value of Y-component as a string.
 * \param Z value of Z-component as a string.
 * \return vector specified through the input values.
 */
inline irr::core::vector3df makeVec3df(const irr::core::stringc& X,
                                       const irr::core::stringc& Y,
                                       const irr::core::stringc& Z){
    irr::core::vector3df result;
    result.X = (irr::f32)atof(X.c_str());
    result.Y = (irr::f32)atof(Y.c_str());
    result.Z = (irr::f32)atof(Z.c_str());

    return result;
}

} // end namespace fishsim


#endif // H_FISHSIM_HELPER_ROUTINES_H
