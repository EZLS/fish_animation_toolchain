#include "include/bag_clip.h"

#include "include/utils.h"

#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QLabel>

namespace fishplayer
{
BagClip::BagClip(QWidget *parent,  const YAML::Node& node) : Clip(parent, E_CLIP_TYPE::BAG)
{

    load(node);

    m_animManager = new fishsim::AnimationManager();
    verify();
    initUI();
}

BagClip::BagClip(QWidget *parent, std::string path) : Clip(parent, E_CLIP_TYPE::BAG)
{
    m_path = path;
    m_animManager = new fishsim::AnimationManager();

    verify();
    initUI();
}

BagClip::~BagClip()
{
    m_animManager->clear();
    delete m_animManager;
}

void BagClip::initUI()
{
    QHBoxLayout *mainLayout = new QHBoxLayout;

    QLabel *labelType = new QLabel("BAG");
    labelType->setStyleSheet("font-weight: bold;");
    labelType->setMaximumWidth(LABEL_TYPE_WIDTH);
    mainLayout->addWidget(labelType);

    std::string base = getBaseFilename(m_path);
    QLabel* labelPath = new QLabel(QString::fromStdString(base));
    labelPath->setWordWrap(true);
    labelPath->setMaximumWidth(400);
    mainLayout->addWidget(labelPath);

    QLabel* labelPlaytime = new QLabel("  " + generateTimeString(getDuration()));
    labelPlaytime->setMaximumWidth(LABEL_TIME_WIDTH);
    mainLayout->addWidget(labelPlaytime);

    setLayout(mainLayout);
}

void BagClip::play(RosInterface* roshandler)
{
    std::cout << "playing bag clip" << std::endl;
    m_isPlaying = true;

    // For now: just assume that all animation have the
    // same length.
    fishsim::FishAnimation* refAnimation = m_animManager->getAllAnimations().at(0);
    int keyframeCount = refAnimation->getKeyframeCount();

    for(int counter = 1; counter < keyframeCount && m_isPlaying; counter++)
    {

        ros::Time start = ros::Time::now();

        // Publish snapshot to FishSim
        roshandler->publishAll(m_animManager->getAllKeyframesForCounter(counter));

        ros::Duration runDuration = ros::Time::now() - start;
        ros::Duration deltaTime =
                refAnimation->getDurationBetweenKeyframes(counter -1, counter);
        deltaTime = deltaTime - runDuration;

        if(deltaTime.toSec() > 0.0)
            deltaTime.sleep();

    }

}

bool BagClip::verify()
{
    m_animManager->loadFromBag(m_path);
    m_isValid = (m_animManager->getAllAnimations().size() > 0);
    return m_isValid;
}

float BagClip::getDuration()
{
    if(m_animManager->getAllAnimations().size() > 0)
    {
        fishsim::FishAnimation* ref = m_animManager->getAllAnimations().at(0);
        return ref->getAnimationDuration().toSec();
    }
    else
        return 0.0;
}


void BagClip::save(YAML::Emitter& emitter)
{
    emitter << YAML::BeginMap;
    emitter << YAML::Key << "type";
    emitter << YAML::Value << m_type;
    emitter << YAML::Key << "path";
    emitter << YAML::Value << m_path;
    emitter << YAML::EndMap;
}

bool BagClip::load(const YAML::Node& node)
{
    if(! node.IsMap())
        return false;

    // Make sure all data is there
    YAML::Node path = node["path"];
    if(!path)
        return false;

    // Fill object
    m_path = path.as<std::string>();

    return true;
}


}
