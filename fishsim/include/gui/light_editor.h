/*! Copyright (C) 2017  	Department of Electrical Engineering & Computer Science, Institute of
 *			Real-Time Learning Systems, University of Siegen
 *
 *			Research Group of Ecology and Behavioral Biology, Institute of Biology,
 *			University of Siegen
 *
 * Contact: virtual.fish.project@gmail.com
 *
 * License: GNU General Public License  See LICENSE.txt for the full license.
 *
 * This file is part of the "FishSim Animation Toolchain".
 * "FishSim Animation Toolchain" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "FishSim Animation Toolchain" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "FishSim Animation Toolchain".  If not, see <http: *www.gnu.org/licenses/>.
 *
 * The "FishSim Animation Toolchain" was developed within the scope of the interdisciplinary,
 * DFG (Deutsche Forschungsgemeinschaft)-funded project “virtual fish” (KU
 * 689/11-1 and Wi 1531/12-1) of the Institute of Real-Time Learning Systems
 * (EZLS) and the Research Group of Ecology and Behavioral Biology at the
 * University of Siegen.
 *
 *  \file light_editor.h
 *  \brief A toolbox window that allows changing light settings.
 *
*/

#ifndef H_LIGHT_EDITOR_H
#define H_LIGHT_EDITOR_H

#include <irrlicht.h>
#include <string>
#include "irrExt/xEffects/XEffects.h"

#include "IGUIButton.h"
#include "IGUIEnvironment.h"
#include "IGUIElement.h"
#include "IGUIWindow.h"

#include "gui/fishsim_toolbox_tab.h"


namespace fishsim
{
//// Structure that holds lighting information about fishsim.
struct SFishSimLightEnv{

    // Normal irrlicht light
    irr::scene::ILightSceneNode* irrLight;

    // XEffect shadow light
    SShadowLight* shadowLight;

    // XEffect shadow manager
    EffectHandler* shadowManager;
};


class LightEditor
{

public:

    /*!
          \brief Constructor.
          \param device the irrlicht device.
          \param lightEnv Struct that holds some lighting informations.
     */
    LightEditor(irr::IrrlichtDevice* device, SFishSimLightEnv* lightEnv);


    /*!
          \brief Destructor
     */
    ~LightEditor();


    /*!
          \brief Apply all changes to the settings made in the
          quality tab.
     */
    void applyQualitySettings();


    /*!
          \brief Apply all changes to the settings made in the
          position tab.
     */
    void applyPositionSettings();


    /*!
          \brief Apply all changes to the settings made in the
          color tab.
     */
    void applyColorSettings();

private:

    /// Irrlicht device.
    irr::IrrlichtDevice* m_device;

    /// Irrlicht GUI environment.
    irr::gui::IGUIEnvironment* m_guiEnv;

    /// Structur with lighting information that can be changed.
    SFishSimLightEnv* m_lightEnv;

    /// Toolbox window.
    irr::gui::IGUIWindow* m_winLightSettings;

    /// Elements of the position tab.
    irr::gui::IGUIEditBox *m_edtBoxLightPosX;
    irr::gui::IGUIEditBox *m_edtBoxLightPosY;
    irr::gui::IGUIEditBox *m_edtBoxLightPosZ;

    irr::gui::IGUIEditBox *m_edtBoxTargetPosX;
    irr::gui::IGUIEditBox *m_edtBoxTargetPosY;
    irr::gui::IGUIEditBox *m_edtBoxTargetPosZ;

    irr::gui::IGUIEditBox *m_edtBoxFOV;


    /// Elements of the color tab.
    irr::gui::IGUIEditBox *m_edtBoxLightColR;
    irr::gui::IGUIEditBox *m_edtBoxLightColG;
    irr::gui::IGUIEditBox *m_edtBoxLightColB;

    irr::gui::IGUIEditBox *m_edtBoxBackColR;
    irr::gui::IGUIEditBox *m_edtBoxBackColG;
    irr::gui::IGUIEditBox *m_edtBoxBackColB;

    irr::gui::IGUIEditBox *m_etdBoxAmbientIntensity;

    /// Elements of the quality tab.
    irr::gui::IGUIEditBox *m_edtBoxShadowRes;
    irr::gui::IGUIEditBox *m_edtBoxNearVal;
    irr::gui::IGUIEditBox *m_edtBoxFarVal;

    /// Pointer to position tab.
    FishSimToolboxTab* m_positionTab;

    /// Pointer to color tab.
    FishSimToolboxTab* m_colorTab;

    /// Pointer to quality tab.
    FishSimToolboxTab* m_qualityTab;

    /*!
          \brief Update the informations that gets
           displayed in position tab.
     */
    void updatePositionTab();


    /*!
          \brief Update the informations that gets
           displayed in quality tab.
     */
    void updateQualityTab();

    /*!
          \brief Update the informations that gets
           displayed in color tab.
     */
    void updateColorTab();


    /*!
          \brief Create the toolbox window.
     */
    void createSettingsWindow();

};


} // end namespace fishsim

#endif // H_LIGHT_EDITOR_H
