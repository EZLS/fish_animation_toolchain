/*! Copyright (C) 2017  	Department of Electrical Engineering & Computer Science, Institute of
 *			Real-Time Learning Systems, University of Siegen
 *
 *			Research Group of Ecology and Behavioral Biology, Institute of Biology,
 *			University of Siegen
 *
 * Contact: virtual.fish.project@gmail.com
 *
 * License: GNU General Public License  See LICENSE.txt for the full license.
 *
 * This file is part of the "FishSim Animation Toolchain".
 * "FishSim Animation Toolchain" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "FishSim Animation Toolchain" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "FishSim Animation Toolchain".  If not, see <http: *www.gnu.org/licenses/>.
 *
 * The "FishSim Animation Toolchain" was developed within the scope of the interdisciplinary,
 * DFG (Deutsche Forschungsgemeinschaft)-funded project “virtual fish” (KU
 * 689/11-1 and Wi 1531/12-1) of the Institute of Real-Time Learning Systems
 * (EZLS) and the Research Group of Ecology and Behavioral Biology at the
 * University of Siegen.
 *
 *  \file fishsim_gui.h
 *  \brief Main GUI class that sets up all GUI elements, like menu bar and toolbar.
 *
*/

#ifndef H_FISHSIM_GUI_H
#define H_FISHSIM_GUI_H

#include <irrlicht.h>
#include <string>

#include "scene/fishsim_scene_manager.h"

#include "irrExt/CGUIFileSelector.h"

#include "gui/fish_editor.h"
#include "gui/light_editor.h"
#include "gui/camera_editor.h"
#include "gui/help_window.h"


namespace fishsim
{
class FishSimGUI
{
public:

    /// Constructor
    FishSimGUI(irr::IrrlichtDevice* device, FishSimSceneManager* fishsimSmgr,
               const std::string& pathToFishSim);


    /// Destructor
    ~FishSimGUI();


    /// Open camera editor
    void openCameraSettings();


    /// Do some cleanup if camera settings get closed
    void onCloseCameraSettings();


    /// Open edit toolbox, if fish is selected.
    void openFishEditToolbox();


    /// If edit toolbox was closed, do some cleanup
    void onCloseFishEditToolbox();


    /// open light settings toolbox
    void openLightSettings();


    /// If light settings toolbox was closed, do some cleanup
    void onCloseLightSettings();

    /// Open help window
    void openHelpWindow();

    /// Do some cleanup on closing help window
    void onCloseHelpWindow();

    /// Draw GUI to screen if visible
    void draw();

    /// Close all toolbox windows.
    bool closeAllWindows();

    /// Show loading texture info.
    void showLoadingInfo();

    /// Hide loading texture info.
    void hideLoadingInfo();


    /*!
     * \brief Add a file selector as modal screen to GUI environment.
     * \param title the title of the new window.
     * \param id the id of the new window.
     * \param type can be either a load screen or a save screen. See E_FILESELECTOR_TYPE for more info.
     * \return pointer to the new CGUIFileSelector.
     */
    irr::gui::CGUIFileSelector* addFileSelector(const wchar_t* title,
                                                irr::s32 id,
                                                irr::gui::E_FILESELECTOR_TYPE type);


    /// Change visibility of GUI
    void toggleVisiblility()
    {
        m_isVisible = !m_isVisible;
    }


    /// Returns true, if GUI is currently visible to user
    bool isVisible()
    {
        return m_isVisible;
    }


    /// Returns the current light editor, if one is availabe, 0 otherwise
    LightEditor* getLightEditor(){
        return m_lightEditor;
    }


    /// Returns the current fish editor, if one is availabe, 0 otherwise
    FishEditor* getFishEditor(){
        return m_fishEditor;
    }

    /// Returns pointer to camera editor, if camera editor is open,
    /// 0 otherwise.
    CameraEditor* getCameraEditor()
    {
        return m_cameraEditor;
    }


    /// Returns true, if one of the toolboxes is currently visible
    bool hasToolboxOpen(){

        // Is in edit mode if one of the pointers is not 0
        return m_lightEditor || m_fishEditor || m_cameraEditor;
    }


    //    /// Open model selection window
    //    void openModelSelectionWindow();

    //    /// called if an event happened.
    //    bool OnEvent(const SEvent& event);


    //    ///If model selection was closed, dosome cleanup
    //    void onCloseModelSelectionWindow();


private:

    /// Pointer to irrlicht device
    irr::IrrlichtDevice* m_device;

    /// Pointer to video driver
    irr::video::IVideoDriver* m_driver;

    /// Pointer to irrlicht gui environmet
    irr::gui::IGUIEnvironment* m_guiEnv;

    /// Pointer to fishsim scene manager.
    FishSimSceneManager* m_fishsimSmgr;

    /// Only true, if gui is visible
    bool m_isVisible;

    /// Pointer to fish editor
    FishEditor* m_fishEditor;

    /// Pointer to light editor
    LightEditor* m_lightEditor;

    /// Pointer to camera editor
    CameraEditor* m_cameraEditor;

    /// Pointer to help window
    HelpWindow* m_helpWindow;

    /// Screen height
    int m_screenHeight;

    /// Screen width
    int m_screenWidth;

    /// Path to fishsim
    std::string m_fishSimPath;

    /// Loading info. Only visible if fishsim is loading
    /// textures
    irr::gui::IGUIStaticText* m_loadingInfo;

    /// Create the static GUI (menu bar, toolbar, etc)
    void createStaticGUI();

};


} // end namespace fishsim

#endif // H_FISHSIM_GUI_H
