/*! Copyright (C) 2017  	Department of Electrical Engineering & Computer Science, Institute of
 *			Real-Time Learning Systems, University of Siegen
 *
 *			Research Group of Ecology and Behavioral Biology, Institute of Biology,
 *			University of Siegen
 *
 * Contact: virtual.fish.project@gmail.com
 *
 * License: GNU General Public License  See LICENSE.txt for the full license.
 *
 * This file is part of the "FishSim Animation Toolchain".
 * "FishSim Animation Toolchain" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "FishSim Animation Toolchain" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "FishSim Animation Toolchain".  If not, see <http: *www.gnu.org/licenses/>.
 *
 * The "FishSim Animation Toolchain" was developed within the scope of the interdisciplinary,
 * DFG (Deutsche Forschungsgemeinschaft)-funded project “virtual fish” (KU
 * 689/11-1 and Wi 1531/12-1) of the Institute of Real-Time Learning Systems
 * (EZLS) and the Research Group of Ecology and Behavioral Biology at the
 * University of Siegen.
 *
 *  \file help_window.h
 *  \brief The help window.
 *
*/


#ifndef H_HELP_WINDOW_H
#define H_HELP_WINDOW_H

#include <irrlicht.h>

#include "IGUIEnvironment.h"
#include "IGUIElement.h"
#include "IGUIWindow.h"



namespace fishsim
{
class HelpWindow
{

public:

        /*!
          \brief Constructor.
          \param device The irrlicht device.
        */
        HelpWindow(irr::IrrlichtDevice* device);

        /*!
          \brief Destructor
        */
        ~HelpWindow();

private:

    /// Irrlicht device
    irr::IrrlichtDevice* m_device;

    /// Irrlicht gui environment
    irr::gui::IGUIEnvironment* m_guiEnv;

    /// Help window
    irr::gui::IGUIWindow* m_helpWindow;

    /*!
          \brief Create the help window
     */
    void createHelpWindow();
};


} // end namespace fishsim

#endif // H_HELP_WINDOW_H
