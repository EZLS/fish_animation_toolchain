#include "fish/bone_modifier/up_swim_modifier.h"

using namespace irr;
using namespace scene;

// Maximum rotation value of bones
#define MAX_UP_ANGLE 15.0

namespace fishsim
{

UpSwimModifier::UpSwimModifier(IAnimatedMeshSceneNode *fishNode,
                               std::string name, float speed, bool isActive)
    : BoneModifier(fishNode, name, speed, isActive)
{
    m_currentUpAngle = 0.0;
}

UpSwimModifier::~UpSwimModifier()
{

}

bool UpSwimModifier::init()
{
    bool allBonesFound = true;
    allBonesFound = allBonesFound && getAndGrabBone(m_backbone3, "backbone_3");
    allBonesFound = allBonesFound && getAndGrabBone(m_backbone4, "backbone_4");

    if(allBonesFound)
        return true;

    // error handling: just disable modifier for now
    std::cerr << "Up swim modifier failed on init." << std::endl;
    m_isActive = false;
    return false;
}

void UpSwimModifier::update(float inputVal1, float inputVal2, float inputVal3){

    if(!m_isActive)
        return;

    upAnimation(inputVal1);
}

void UpSwimModifier::modify(FishSnapshot *startState,
                            FishSnapshot *endState,
                            const sensor_msgs::Joy &joystickState)
{
    // Wont show up in fishsteering. Return
    return;
}

void UpSwimModifier::update(const sensor_msgs::Joy &joystickState)
{
    // Wont show up in fishsteering. Return
    return;

}

void UpSwimModifier::upAnimation(float pitchAmount){

    // Simple P controller for turn animation
    float goalAngle = 5 * -pitchAmount;
    if(fabs(goalAngle) > MAX_UP_ANGLE ) goalAngle = copysign(MAX_UP_ANGLE, goalAngle);
    float error = goalAngle - m_currentUpAngle;
    m_currentUpAngle = m_currentUpAngle +  m_speed * error;

    addRotationToBone(m_backbone3, m_currentUpAngle, 0.0, 0.0);
    addRotationToBone(m_backbone4, m_currentUpAngle, 0.0, 0.0);
}


} // end namespace fishsim
