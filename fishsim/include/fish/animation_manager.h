/*! Copyright (C) 2017  	Department of Electrical Engineering & Computer Science, Institute of
 *			Real-Time Learning Systems, University of Siegen
 *
 *			Research Group of Ecology and Behavioral Biology, Institute of Biology,
 *			University of Siegen
 *
 * Contact: virtual.fish.project@gmail.com
 *
 * License: GNU General Public License  See LICENSE.txt for the full license.
 *
 * This file is part of the "FishSim Animation Toolchain".
 * "FishSim Animation Toolchain" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "FishSim Animation Toolchain" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "FishSim Animation Toolchain".  If not, see <http: *www.gnu.org/licenses/>.
 *
 * The "FishSim Animation Toolchain" was developed within the scope of the interdisciplinary,
 * DFG (Deutsche Forschungsgemeinschaft)-funded project “virtual fish” (KU
 * 689/11-1 and Wi 1531/12-1) of the Institute of Real-Time Learning Systems
 * (EZLS) and the Research Group of Ecology and Behavioral Biology at the
 * University of Siegen.
 *
 * \file animation_manager.h
 * \brief AnimationManager is a helper class to combine multiple animations of different models.
 *
 * User can create animations with the manager for a given model name. Helps to prevent having
 * more than one animation for a model, which would create an inconsistent state of fishsteering.
 * Mainly used to save and load animation from and to rosbags.
*/

#ifndef H_ANIMATION_MANAGER_H
#define H_ANIMATION_MANAGER_H

#include <string>
#include <vector>

#include "fish/fishsnapshot.h"
#include "fish/fish_animation.h"


namespace fishsim
{
class AnimationManager
{

public:

    /*!
      \brief Constructor
    */
    AnimationManager();

    /*!
      \brief Destructor
    */
    ~AnimationManager();


    /*!
      \brief Create a new animation for given model.

      Will create a new animation, if an animation for given model not
      already exist. If an animation exists, this animation will be returned.

      \param modelName Name of the fish model that should be animated.

      \return A pointer to a new animation, if animation for model not
      already exist, A pointer to an existing animation, if animation was already
      created before.

    */
    FishAnimation* createNewAnimation(const std::string& modelName);
    
    /*!
      \brief Get animation for given model name.

      \param modelName The name of the fish model.

      \return A pointer to an animation, if animation for model exist,
      0 otherwise.

    */
    FishAnimation* getAnimationForModel(const std::string& modelName);


    /*!
      \brief Remove an animation from manager.

      \param modelName The name of the fish model which animation will be deleted.

      \return true, if an animation was found and deleted. False if no animation
      was found for given model name.

    */
    bool removeAnimation(const std::string& modelName);


    /*!
      \brief Load animations from rosbag.

      \param filename The filename of the bag

      \return true, if loading was successful. False otherwise.

    */
    bool loadFromBag(const std::string& filename);



    /*!
      \brief Write animations to rosbag.

      \param filename The filename of the bag

      \return true, if animation was saved successfull. False otherwise.

    */
    bool saveAsBag(const std::string& filename);


    /*!
      \brief Clear the manager. Delete
      all animtions
    */
    void clear();



    /*!
      \brief Get all keyframes for the given index.

      E.g. get the 200th keyframe of all animation in a vector. Mainly used to
      publish them over ROS in fishsteering.

      \param animationCounter The index of the keyframe

      \return vector with all keyframes.

    */
    std::vector<FishSnapshot*> getAllKeyframesForCounter(int animationCounter);


    /*!
      \brief Get all animations.
      \return vector with all animations.

    */
    std::vector<FishAnimation*> getAllAnimations();


private:

    /// Map with all animations
    std::map<std::string,FishAnimation*> m_animations;


};


} // end namespace fishsim

#endif // H_ANIMATION_MANAGER_H
