#include "QIrrlichtWidget.h"
QIrrlichtWidget::QIrrlichtWidget(QWidget *parent) : QWidget(parent)
{
    device = 0;
    cena = 0;
    driver = 0;
    driverType = irr::video::EDT_OPENGL;
}

void QIrrlichtWidget::createIrrlichtDevice(QWidget *window, int winWidth, int winHeight)
{
   if ( device != 0 ) return;
   SIrrlichtCreationParameters params;
   params.AntiAlias = 32;
   params.WindowId = (void*)winId();
   params.WindowSize.Width = winWidth;
   params.WindowSize.Height = winHeight;
   //params.DriverType = driverType;
   params.Bits = 32;

   device = createDeviceEx( params );

   setAttribute( Qt::WA_PaintOnScreen );
   device->setResizable(true);
   driver  = device->getVideoDriver();
   cena = device->getSceneManager();
   env = device->getGUIEnvironment();
}

void QIrrlichtWidget::buildIrrlichtScene()
{
    no_cubo = cena->addCubeSceneNode(15);
    if (no_cubo)
    {
        no_cubo->setMaterialFlag(EMF_LIGHTING, false);
        no_cubo->setMaterialTexture( 0, driver->getTexture("media2/rockwall.jpg") );
        no_cubo->setPosition(core::vector3df(0,0,0) );
    }
//-----------------------------------iluminacao-------------------------------------//
    ILightSceneNode *light = cena->addLightSceneNode();
    light->setLightType( ELT_DIRECTIONAL );
    light->setRotation( vector3df( 45.0f, 45.0f, 0.0f ));
    light->getLightData().AmbientColor = SColorf( 0.2f, 0.2f, 0.2f, 1.0f );
    light->getLightData().DiffuseColor = SColorf( 0.8f, 0.8f, 0.8f, 1.0f );
//-----------------------------------camera------------------------------------------//
    camera = cena->addCameraSceneNode();
   // camera->setRotation(core::vector3df(0,90,0));
    camera->setPosition( core::vector3df(0,0,-60) );
    //camera->setTarget( core::vector3df(0,0,0) );
    device->getCursorControl()->setVisible(true);
}

void QIrrlichtWidget::paintEvent( QPaintEvent *event )
{
    //qDebug() << "MainWindow::paintEvent()";
    //drawIrrlichtScene();
}

void QIrrlichtWidget::resizeEvent( QResizeEvent *event )
{
    if ( device != 0 )
    {
        irr::core::dimension2d<unsigned int> size;
        size.Width = event->size().width();
        size.Height = event->size().height();
        device->getVideoDriver()->OnResize(size);
     }
    QWidget::resizeEvent(event);
}

QPaintEngine * QIrrlichtWidget::paintEngine() const
{
    qDebug() << "IrrQWidget::paintEngine()";
        return 0;
}


void QIrrlichtWidget::drawIrrlichtScene()
{
    if(device!=0)
    {
        driver->beginScene( true, true, SColor(255, 127, 191, 233));
        cena->drawAll();
        env->drawAll();
        driver->endScene();
    }
}
void QIrrlichtWidget::sendMouseEventToIrrlicht(QMouseEvent* event)
{
//    irr::SEvent irrEvent;
//    irrEvent.EventType = irr::EET_MOUSE_INPUT_EVENT;
         //no_cubo->setPosition(vector3df(xI+dx,yI+dy,0));
    //     if((MANIPULADOR & DESLOC) == DESLOC ){         //deslocamento
    //                      no_cubo->setPosition(vector3df(xI+dx,yI-dy,0));
    //       }
}

//void QIrrlichtWidget::SinalMouseEvent(double a)
//{a = 5;}

void QIrrlichtWidget::mouseMoveEvent(QMouseEvent *event){
    if(maniATIVA){
        dx = event->x()-mouseXi;
        dy = event->y()-mouseYi;
//        sendMouseEventToIrrlicht(event);
    }
}

void QIrrlichtWidget::mousePressEvent( QMouseEvent* event ){
    mouseXi = event->x();
    mouseYi = event->y();
}

void QIrrlichtWidget::mouseReleaseEvent( QMouseEvent* event ){
    maniATIVA = false;
}


