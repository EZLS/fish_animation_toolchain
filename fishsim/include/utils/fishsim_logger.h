/*! Copyright (C) 2017  	Department of Electrical Engineering & Computer Science, Institute of
 *			Real-Time Learning Systems, University of Siegen
 *
 *			Research Group of Ecology and Behavioral Biology, Institute of Biology,
 *			University of Siegen
 *
 * Contact: virtual.fish.project@gmail.com
 *
 * License: GNU General Public License  See LICENSE.txt for the full license.
 *
 * This file is part of the "FishSim Animation Toolchain".
 * "FishSim Animation Toolchain" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "FishSim Animation Toolchain" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "FishSim Animation Toolchain".  If not, see <http: *www.gnu.org/licenses/>.
 *
 * The "FishSim Animation Toolchain" was developed within the scope of the interdisciplinary,
 * DFG (Deutsche Forschungsgemeinschaft)-funded project “virtual fish” (KU
 * 689/11-1 and Wi 1531/12-1) of the Institute of Real-Time Learning Systems
 * (EZLS) and the Research Group of Ecology and Behavioral Biology at the
 * University of Siegen.
 *
 *  \file fishsim_logger.h
 *  \brief This file provides wrapper functions for logging.
 *  Right now we are using the rosout log macros which internally
 *  use log4cxx.
 *
*/

#ifndef H_FISHSIM_LOGGER_H
#define H_FISHSIM_LOGGER_H

#include "ros/console.h"
#include "string"

namespace fishsim
{


inline void logInfo(const char* message)
{
    ROS_INFO(message);
}

inline void logDebug(const char* message)
{
    ROS_DEBUG(message);
}

inline void logError(const char* message)
{
    ROS_ERROR(message);
}

inline void logWarn(const char* message)
{
    ROS_WARN(message);
}

inline void logFatal(const char* message)
{
    ROS_FATAL(message);
}

inline void logInfo(std::string message)
{
    ROS_INFO_STREAM(message);
}

inline void logDebug(std::string message)
{
    ROS_DEBUG_STREAM(message);
}

inline void logError(std::string message)
{
    ROS_ERROR_STREAM(message);
}

inline void logWarn(std::string message)
{
    ROS_WARN_STREAM(message);
}

inline void logFatal(std::string message)
{
    ROS_FATAL_STREAM(message);
}

} // end namespace fishsim
#endif // H_FISHSIM_LOGGER_H
