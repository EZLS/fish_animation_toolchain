/*! Copyright (C) 2017  	Department of Electrical Engineering & Computer Science, Institute of
 *			Real-Time Learning Systems, University of Siegen
 *
 *			Research Group of Ecology and Behavioral Biology, Institute of Biology,
 *			University of Siegen
 *
 * Contact: virtual.fish.project@gmail.com
 *
 * License: GNU General Public License  See LICENSE.txt for the full license.
 *
 * This file is part of the "FishSim Animation Toolchain".
 * "FishSim Animation Toolchain" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "FishSim Animation Toolchain" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "FishSim Animation Toolchain".  If not, see <http: *www.gnu.org/licenses/>.
 *
 * The "FishSim Animation Toolchain" was developed within the scope of the interdisciplinary,
 * DFG (Deutsche Forschungsgemeinschaft)-funded project “virtual fish” (KU
 * 689/11-1 and Wi 1531/12-1) of the Institute of Real-Time Learning Systems
 * (EZLS) and the Research Group of Ecology and Behavioral Biology at the
 * University of Siegen.
 *
 *   \file   curve_animator.h
 *   \brief  Implements bone rotations while swimming foreward.
 *
*/

#ifndef H_CURVE_ANIMATOR_H
#define H_CURVE_ANIMATOR_H

// For ubuntu 14.04
#include <opencv/cv.h>

// For ubuntu 16.04
//#include "opencv2/core.hpp"
//#include "opencv2/imgproc.hpp"
//#include "opencv2/highgui.hpp"

#include "irrlicht.h"
#include <iostream>


namespace fishsim
{

// Struct that extends the irrlicht bone scene
// node with additional attributes.
class SBoneExtension
{
public:

    enum Axis
    {
        x = 0,
        y = 1,
        z = 2
    };


    // Reference to irrlicht bone scene node
    irr::scene::IBoneSceneNode* irrBonePtr;

    // Length of the bone
    float length;

    // Axis that will be rotated around
    Axis axis;

    // current rotation on this axis
    float rotation;

    // Default rotation when loading mesh
    irr::core::vector3df defaultRotation;
};


class CurveAnimator
{

public:

    /// Constructor
    CurveAnimator();

    /// Destructor
    ~CurveAnimator();

    /// Append a bone to the curve animator.
    bool appendBone(irr::scene::IBoneSceneNode* bone, SBoneExtension::Axis axis, float boneLength);

    /// Update function that should be called every frame when movement happens.
    void animateBones(float forewardAmount);

    /// Normalize length of bones.
    void normalizeBones();

    /// Set amplitude of fin rotation
    void setAmplitude(float amplitude)
    {
        m_amplitude = amplitude;
    }

    /// Set speed of fin rotation
    void setSpeed(float speed)
    {
        m_speedMultiplier = speed;
    }


private:

    /// All bones that will be modified.
    std::vector<SBoneExtension> m_bones;

    /// If true, a window will created that shows the
    /// curve.
    bool m_doDebugDrawing;

    /// Rotation speed of the bones
    float m_previousSpeed;

    /// Rotation speed of the bones
    float m_speedMultiplier;

    /// Current sinus phase.
    float m_sinusPhase;

    /// Gets increased, whenever modify is called.
    unsigned int m_frameCounter;

    /// Amplitude of the fin rotations
    float m_amplitude;


    /// Calculates the length of a given curve by iterativly
    /// calculate the euklidean distance between two consecutive points.
    float getCurveLength(std::vector<cv::Point2f> curve);

    /// Apply rotation angles to the bones.
    void applyAngles();

    /// Reset bones rotation to 0
    void resetBones();

    std::vector<std::vector<cv::Point2f> > getBonesPoints(std::vector<cv::Point2f> &curve);

    cv::Mat solveLinearEquation(std::vector<cv::Point2f> curve, int polyPower);

    void getBonesAngles(std::vector<cv::Point2f> &curve);

    /// calculate y = f(x,t) where f is either motion curve 1 or motion curve 2 multiplied with a sinus.
    /// Returns a point p = (x,y)
    cv::Point2f calculatCurvePoint(float bone, float t, float s);

    /// Eukledian distance between two points.
    float dist_point2point(cv::Point2f a, cv::Point2f b = cv::Point2f(0,0));



};


} // end namespace fishsim

#endif // H_CURVE_ANIMATOR_H
