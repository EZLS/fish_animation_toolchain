#include "fish/bone_modifier/idle_modifier.h"

using namespace irr;
using namespace scene;


// Maximum rotation of tail
#define STRENGTH_TAIL 20.0

// Maximum rotation of vertical fins
#define STRENGTH_SIDE 5.0

namespace fishsim
{

IdleModifier::IdleModifier(IAnimatedMeshSceneNode *fishNode, std::string name,
                           float speed, bool isActive)
        : BoneModifier(fishNode, name, speed, isActive)
{

}

IdleModifier::~IdleModifier(){

    if(m_tailfin_up1)
        m_tailfin_up1->drop();
    if(m_tailfin_up2)
        m_tailfin_up2->drop();
    if(m_tailfin_up3)
        m_tailfin_up3->drop();

    if(m_tailfin_middle1)
        m_tailfin_middle1->drop();
    if(m_tailfin_middle2)
        m_tailfin_middle2->drop();
    if(m_tailfin_middle3)
        m_tailfin_middle3->drop();

    if(m_tailfin_down1)
        m_tailfin_down1->drop();
    if(m_tailfin_down2)
        m_tailfin_down2->drop();
    if(m_tailfin_down3)
        m_tailfin_down3->drop();

    if(m_vertical_fin_up_right1)
        m_vertical_fin_up_right1->drop();
    if(m_vertical_fin_up_right2)
        m_vertical_fin_up_right2->drop();

    if(m_vertical_fin_middle_right1)
        m_vertical_fin_middle_right1->drop();
    if(m_vertical_fin_middle_right2)
        m_vertical_fin_middle_right2->drop();

    if(m_vertical_fin_down_right1)
        m_vertical_fin_down_right1->drop();
    if(m_vertical_fin_down_right2)
        m_vertical_fin_down_right2->drop();


    if(m_vertical_fin_up_left1)
        m_vertical_fin_up_left1->drop();
    if(m_vertical_fin_up_left2)
        m_vertical_fin_up_left2->drop();

    if(m_vertical_fin_middle_left1)
        m_vertical_fin_middle_left1->drop();
    if(m_vertical_fin_middle_left2)
        m_vertical_fin_middle_left2->drop();

    if(m_vertical_fin_down_left1)
        m_vertical_fin_down_left1->drop();
    if(m_vertical_fin_down_left2)
        m_vertical_fin_down_left2->drop();

}

bool IdleModifier::init(){


    // getAndGrabBone will return false if bone could not be found.
    // If one of the function calls returns false, allOk will
    // be false too.
    bool allOk = true;

    // Tail fins
    allOk = allOk && getAndGrabBone(m_tailfin_up1, "tail_fin_up1");
    allOk = allOk && getAndGrabBone(m_tailfin_up2, "tail_fin_up2");
    allOk = allOk && getAndGrabBone(m_tailfin_up3, "tail_fin_up3");

    allOk = allOk && getAndGrabBone(m_tailfin_middle1,"tail_fin_middle1");
    allOk = allOk && getAndGrabBone(m_tailfin_middle2,"tail_fin_middle2");
    allOk = allOk && getAndGrabBone(m_tailfin_middle3,"tail_fin_middle3");

    allOk = allOk && getAndGrabBone(m_tailfin_down1,"tail_fin_down1");
    allOk = allOk && getAndGrabBone(m_tailfin_down2,"tail_fin_down2");
    allOk = allOk && getAndGrabBone(m_tailfin_down3,"tail_fin_down3");


    // Init vertical fins right
    allOk = allOk && getAndGrabBone(m_vertical_fin_up_right1,"vertical_fin_up_right1");
    allOk = allOk && getAndGrabBone(m_vertical_fin_up_right2,"vertical_fin_up_right2");
    allOk = allOk && getAndGrabBone(m_vertical_fin_middle_right1,"vertical_fin_middle_right1");
    allOk = allOk && getAndGrabBone(m_vertical_fin_middle_right2,"vertical_fin_middle_right2");
    allOk = allOk && getAndGrabBone(m_vertical_fin_down_right1,"vertical_fin_down_right1");
    allOk = allOk && getAndGrabBone(m_vertical_fin_down_right2,"vertical_fin_down_right2");

    // Init vertical fins left
    allOk = allOk && getAndGrabBone(m_vertical_fin_up_left1,"vertical_fin_up_left1");
    allOk = allOk && getAndGrabBone(m_vertical_fin_up_left2,"vertical_fin_up_left2");
    allOk = allOk && getAndGrabBone(m_vertical_fin_middle_left1,"vertical_fin_middle_left1");
    allOk = allOk && getAndGrabBone(m_vertical_fin_middle_left2,"vertical_fin_middle_left2");
    allOk = allOk && getAndGrabBone(m_vertical_fin_down_left1,"vertical_fin_down_left1");
    allOk = allOk && getAndGrabBone(m_vertical_fin_down_left2,"vertical_fin_down_left2");

    m_prevTailSinus1 = 0.0;
    m_prevTailSinus2 = 0.0;
    m_timerTail = 0.0;

    m_prevSideSinus1 = 0.0;
    m_prevSideSinus2 = 0.0;
    m_timerSide = 0.0;

    if(!allOk)
    {
        std::cerr << "Idle modifier failed on init." << std::endl;
        // Error handling: just disable moifier for now
        m_isActive = false;
        return false;
    }

    return true;

}


void IdleModifier::update(float inputVal1, float inputVal2, float inputVal3){

    if(!m_isActive)
        return;

    // Vertical fins play all the time, no matter what input
    // is comming
    animateVerticalFins();

    if(inputVal1 != 0.0)
    {
        // Reset tail fin animation
        m_prevTailSinus1 = 0.0;
        m_prevTailSinus2 = 0.0;
        m_timerTail = 0.0;
        return;
    }

    m_timerTail += m_speed;

    // Will be 1, if inputVal = 0.0 (no foreward force)
//    float amount = 1.0 - percentage( fabs(inputVal1), 0.1);

    float amount = 1.0;
    float sinusVal1 = sin(m_timerTail);


    // Second sinus has a phase shift of pi/2
    float sinusVal2 = sin(m_timerTail+ 1.5707);

    // dir > 0, if sinus is going up,
    //    < 0, if sinus is going down
    float dir1 = sinusVal1 - m_prevTailSinus1;
    m_prevTailSinus1 = sinusVal1;


    float dir2 = sinusVal2 - m_prevTailSinus2;
    m_prevTailSinus2 = sinusVal2;


    ///
    /// Tail fin
    ///
    irr::core::vector3df rot(0.0, 0.0, dir1 * STRENGTH_TAIL * amount);
    addRotationToBone(m_tailfin_up1, rot);
    addRotationToBone(m_tailfin_up2, rot);
    addRotationToBone(m_tailfin_up3, rot);

    rot = irr::core::vector3df(0.0, 0.0, -dir1 * STRENGTH_TAIL * amount);
    addRotationToBone(m_tailfin_down1, rot);
    addRotationToBone(m_tailfin_down2, rot);
    addRotationToBone(m_tailfin_down3, rot);

    rot = irr::core::vector3df(0.0, 0.0, dir2 * STRENGTH_TAIL * amount);
    addRotationToBone(m_tailfin_middle1, rot);
    addRotationToBone(m_tailfin_middle2, rot);
    addRotationToBone(m_tailfin_middle3, rot);

}

void IdleModifier::animateVerticalFins(){

    m_timerSide += m_speed;

    float sinusVal1 = sin(m_timerSide);

    // Second sinus has a phase shift of pi/2
    float sinusVal2 = sin(m_timerSide+ 1.5707);

    // dir > 0, if sinus is going up,
    //     < 0, if sinus is going down
    float dir1 = sinusVal1 - m_prevSideSinus1;
    m_prevSideSinus1 = sinusVal1;


    float dir2 = sinusVal2 - m_prevSideSinus2;
    m_prevSideSinus2 = sinusVal2;


    ///
    /// Vertical fin right
    ///
    irr::core::vector3df rot = irr::core::vector3df(0.0, 0.0, dir2 * STRENGTH_SIDE);
//    addRotationToBone(m_vertical_fin_up_right1, rot);
    addRotationToBone(m_vertical_fin_up_right2, rot);

    rot = irr::core::vector3df(0.0, 0.0, -dir2 * STRENGTH_SIDE);
//    addRotationToBone(m_vertical_fin_down_right1, rot);
    addRotationToBone(m_vertical_fin_down_right2, rot);

    rot = irr::core::vector3df(0.0, 0.0, dir1 * STRENGTH_SIDE);
//    addRotationToBone(m_vertical_fin_middle_right1, rot);
    addRotationToBone(m_vertical_fin_middle_right2, rot);


    ///
    /// Vertical fin left
    ///
    addRotationToBone(m_vertical_fin_up_left1, rot);
    addRotationToBone(m_vertical_fin_up_left2, rot);

    rot = irr::core::vector3df(0.0, 0.0, -dir2 * STRENGTH_SIDE);
    addRotationToBone(m_vertical_fin_down_left1, rot);
    addRotationToBone(m_vertical_fin_down_left2, rot);

    rot = irr::core::vector3df(0.0, 0.0, dir1 * STRENGTH_SIDE);
    addRotationToBone(m_vertical_fin_middle_left1, rot);
    addRotationToBone(m_vertical_fin_middle_left2, rot);


}

void IdleModifier::update(const sensor_msgs::Joy& joystickState){

    // Modifier does not show up in fishsteering. Its only used together
    // with the move function. -> Do nothing
    return;
}

void IdleModifier::modify(FishSnapshot* startState, FishSnapshot* endState,
            const sensor_msgs::Joy& joystickState ){

    // Modifier does not show up in fishsteering. Its only used together
    // with the move function. -> Do nothing
    return;
}


} // end namespace fishsim
