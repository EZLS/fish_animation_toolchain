#include "include/load_clip.h"

#include <QVBoxLayout>
#include <QLabel>

#include <boost/filesystem.hpp>

#include "include/utils.h"

namespace fishplayer
{
LoadClip::LoadClip(QWidget *parent, const YAML::Node& node) : Clip(parent, E_CLIP_TYPE::LOAD)
{
    load(node);

    verify();
    initUI();
}

LoadClip::LoadClip(QWidget *parent, std::string path) : Clip(parent, E_CLIP_TYPE::LOAD)
{
    m_path = path;
    verify();
    initUI();
}


LoadClip::~LoadClip()
{

}


void LoadClip::initUI()
{
    QHBoxLayout *mainLayout = new QHBoxLayout;

    QLabel *labelType = new QLabel("LOAD");
    labelType->setMaximumWidth(LABEL_TYPE_WIDTH);
    labelType->setStyleSheet("font-weight: bold;");
    mainLayout->addWidget(labelType);


    std::string base = getBaseFilename(m_path);
    QLabel* labelPath = new QLabel(QString::fromStdString(base));
    mainLayout->addWidget(labelPath);

//    QLabel* labelDuration = new QLabel("  00:02");
//    labelDuration->setMaximumWidth(LABEL_TIME_WIDTH);
//    mainLayout->addWidget(labelDuration);

    setLayout(mainLayout);
}

void LoadClip::paintEvent(QPaintEvent * event)
{

    // Only draw error rectangle if clip is invalid
    if(m_isValid)
        return;

    QRect rect = event->rect();
    // Outer border
    QPainter painter(this);
    painter.setRenderHint(QPainter::Antialiasing);
    QPen pen(Qt::red, 2);
    painter.setPen(pen);
    painter.drawRect(rect);


//    painter.drawText(rect, Qt::AlignCenter, "Could not find file");

//    // Line after type label
//    QPainterPath path;
//    path.moveTo(rect.topLeft() + QPoint(60,0));
//    path.lineTo(rect.bottomLeft() + QPoint(60,0));
//    path.closeSubpath();
//    painter.drawPath(path);

//    painter.drawText(rect, Qt::AlignCenter, "Data");
//    QPainter p(this);
//    p.setRenderHint(QPainter::Antialiasing);
//    QPainterPath path;
//    path.addRoundedRect(rect, 10, 5);
//    QPen pen(Qt::gray, 4);
//    p.setPen(pen);
////    p.fillPath(path, Qt::red);
//    p.drawPath(path);



}

bool LoadClip::verify()
{

    m_isValid = boost::filesystem::exists(m_path);
    return m_isValid;
}

float LoadClip::getDuration()
{
    //Load scene has fixed duration
    return 0.0;
}

void LoadClip::play(RosInterface* roshandler)
{
    std::cout << "playing load scene " << std::endl;
    roshandler->callLoadFishSimScene(m_path, true);
    ros::Duration(1.0).sleep();
}

void LoadClip::save(YAML::Emitter& emitter)
{
    emitter << YAML::BeginMap;

    emitter << YAML::Key << "type";
    emitter << YAML::Value << m_type;
    emitter << YAML::Key << "path";
    emitter << YAML::Value << m_path;

    emitter << YAML::EndMap;
}

bool LoadClip::load(const YAML::Node& node)
{
    if(! node.IsMap())
        return false;

    // Make sure all data is there
    YAML::Node path = node["path"];
    if(!path)
        return false;

    // Fill object
    m_path = path.as<std::string>();
    return true;
}
}
