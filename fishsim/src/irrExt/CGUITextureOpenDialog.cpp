// Copyright (C) 2002-2012 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

#include "irrExt/CGUITextureOpenDialog.h"
#ifdef _IRR_COMPILE_WITH_GUI_

#include <locale.h>

#include "IGUISkin.h"
#include "IGUIEnvironment.h"
#include "IVideoDriver.h"
#include "IGUIButton.h"
#include "IGUIStaticText.h"
#include "IGUIFont.h"
#include "IGUIFontBitmap.h"
#include "IFileList.h"
#include "irrlicht.h"

#include "iostream"

namespace irr
{
namespace gui
{

const s32 FOD_WIDTH = 350;
const s32 FOD_HEIGHT = 250;

//! constructor
CGUITextureOpenDialog::CGUITextureOpenDialog(const wchar_t* title,
        IGUIEnvironment* environment, IGUIElement* parent, ITimer *timer, s32 id,
        bool restoreCWD, io::path::char_type* startDir)
:IGUIElement(EGUIET_WINDOW, environment, parent,
             id, core::rect<s32>((parent->getAbsolutePosition().getWidth()-FOD_WIDTH)/2,
                                 (parent->getAbsolutePosition().getHeight()-FOD_HEIGHT)/2,
                                 (parent->getAbsolutePosition().getWidth()-FOD_WIDTH)/2+FOD_WIDTH,
                                 (parent->getAbsolutePosition().getHeight()-FOD_HEIGHT)/2+FOD_HEIGHT)),
    TextBoxPrefix(0), FileList(0), Dragging(false)
{
    Text = title;
    FileSystem = Environment?Environment->getFileSystem():0;

	if (FileSystem)
	{
		FileSystem->grab();

		if (restoreCWD)
			RestoreDirectory = FileSystem->getWorkingDirectory();
		if (startDir)
		{
			StartDirectory = startDir;
			FileSystem->changeWorkingDirectoryTo(startDir);
		}
	}
	else
        return;

	IGUISpriteBank* sprites = 0;
	video::SColor color(255,255,255,255);
	IGUISkin* skin = Environment->getSkin();
	if (skin)
	{
		sprites = skin->getSpriteBank();
		color = skin->getColor(EGDC_WINDOW_SYMBOL);
	}

	const s32 buttonw = skin->getSize(EGDS_WINDOW_BUTTON_WIDTH);
	const s32 posx = RelativeRect.getWidth() - buttonw - 4;

    CloseButton = Environment->addButton(core::rect<s32>(posx, 3, posx + buttonw, 3 + buttonw), this, -1,
        L"", skin ? skin->getDefaultText(EGDT_WINDOW_CLOSE) : L"Close");
    CloseButton->setSubElement(true);
    CloseButton->setTabStop(false);
    if (sprites)
    {
        CloseButton->setSpriteBank(sprites);
        CloseButton->setSprite(EGBS_BUTTON_UP, skin->getIcon(EGDI_WINDOW_CLOSE), color);
        CloseButton->setSprite(EGBS_BUTTON_DOWN, skin->getIcon(EGDI_WINDOW_CLOSE), color);
    }
    CloseButton->setAlignment(EGUIA_LOWERRIGHT, EGUIA_LOWERRIGHT, EGUIA_UPPERLEFT, EGUIA_UPPERLEFT);
    CloseButton->grab();




    //TextureBox = Environment->addListBox(core::rect<s32>(10, 55, RelativeRect.getWidth()-90, 230), this, -1, true);
    TextureBox = new CGUITextureListBox(environment, this, timer, fishsim::GUI_ID_LISTBOX_FISH_TEXTURES,core::rect<s32>(10, 55, RelativeRect.getWidth()-10, 230));
    TextureBox->setSubElement(true);
    TextureBox->setAlignment(EGUIA_UPPERLEFT, EGUIA_LOWERRIGHT, EGUIA_UPPERLEFT, EGUIA_LOWERRIGHT);
    TextureBox->grab();
    TextureBox->setItemHeight(256);
    TextureBox->setDrawBackground(true);


    TextBoxPrefix = Environment->addEditBox(0, core::rect<s32>(10, 30, RelativeRect.getWidth()-10, 50), true, this);
    TextBoxPrefix->setSubElement(true);
    TextBoxPrefix->setAlignment(EGUIA_UPPERLEFT, EGUIA_LOWERRIGHT, EGUIA_UPPERLEFT, EGUIA_UPPERLEFT);
    TextBoxPrefix->grab();
    TextBoxPrefix->setText(L"");

	setTabGroup(true);

    TexLoader = new fishsim::TextureLoader(Environment->getVideoDriver(), FileSystem);
	fillListBox();
}


//! destructor
CGUITextureOpenDialog::~CGUITextureOpenDialog()
{
	if (CloseButton)
		CloseButton->drop();

    if (TextureBox)
        TextureBox->drop();

    if (TextBoxPrefix)
        TextBoxPrefix->drop();

	if (FileSystem)
	{
		// revert to original CWD if path was set in constructor
		if (RestoreDirectory.size())
			FileSystem->changeWorkingDirectoryTo(RestoreDirectory);
		FileSystem->drop();
	}

	if (FileList)
		FileList->drop();
}



video::ITexture* CGUITextureOpenDialog::getSelectedTexture(){

    int selected = TextureBox->getSelected();
    return Textures.at(selected);

}

//! returns the filename of the selected file. Returns NULL, if no file was selected.
const wchar_t* CGUITextureOpenDialog::getTextureFileName() const
{
	return FileName.c_str();
}

//! called if an event happened.
bool CGUITextureOpenDialog::OnEvent(const SEvent& event)
{
	if (isEnabled())
	{
		switch(event.EventType)
		{
		case EET_GUI_EVENT:
			switch(event.GUIEvent.EventType)
			{
			case EGET_ELEMENT_FOCUS_LOST:
				Dragging = false;
				break;
			case EGET_BUTTON_CLICKED:
                if (event.GUIEvent.Caller == CloseButton)
				{
					sendCancelEvent();
					remove();
					return true;
				}

				break;

//			case EGET_LISTBOX_CHANGED:
//				{
//                    s32 selected = TextureBox->getSelected();
//                    sendSelectedEvent();
//                    return true;
//                }

            case EGET_EDITBOX_CHANGED:
                if (event.GUIEvent.Caller == TextBoxPrefix)
				{
                        fillListBox();
                        return true;
				}

			break;
			default:
				break;
			}
			break;

		case EET_MOUSE_INPUT_EVENT:
			switch(event.MouseInput.Event)
			{
			case EMIE_MOUSE_WHEEL:
                return TextureBox->OnEvent(event);
			case EMIE_LMOUSE_PRESSED_DOWN:
				DragStart.X = event.MouseInput.X;
				DragStart.Y = event.MouseInput.Y;
				Dragging = true;
				Environment->setFocus(this);
				return true;
			case EMIE_LMOUSE_LEFT_UP:
				Dragging = false;
				return true;
			case EMIE_MOUSE_MOVED:

				if ( !event.MouseInput.isLeftPressed () )
					Dragging = false;

				if (Dragging)
				{
					// gui window should not be dragged outside its parent
					if (Parent)
						if (event.MouseInput.X < Parent->getAbsolutePosition().UpperLeftCorner.X +1 ||
							event.MouseInput.Y < Parent->getAbsolutePosition().UpperLeftCorner.Y +1 ||
							event.MouseInput.X > Parent->getAbsolutePosition().LowerRightCorner.X -1 ||
							event.MouseInput.Y > Parent->getAbsolutePosition().LowerRightCorner.Y -1)

							return true;

					move(core::position2d<s32>(event.MouseInput.X - DragStart.X, event.MouseInput.Y - DragStart.Y));
					DragStart.X = event.MouseInput.X;
					DragStart.Y = event.MouseInput.Y;
					return true;
				}
				break;
			default:
				break;
			}
		default:
			break;
		}
	}

	return IGUIElement::OnEvent(event);
}


//! draws the element and its children
void CGUITextureOpenDialog::draw()
{
	if (!IsVisible)
		return;

	IGUISkin* skin = Environment->getSkin();

	core::rect<s32> rect = AbsoluteRect;

	rect = skin->draw3DWindowBackground(this, true, skin->getColor(EGDC_ACTIVE_BORDER),
		rect, &AbsoluteClippingRect);

	if (Text.size())
	{
		rect.UpperLeftCorner.X += 2;
		rect.LowerRightCorner.X -= skin->getSize(EGDS_WINDOW_BUTTON_WIDTH) + 5;

		IGUIFont* font = skin->getFont(EGDF_WINDOW);
		if (font)
			font->draw(Text.c_str(), rect,
					skin->getColor(EGDC_ACTIVE_CAPTION),
					false, true, &AbsoluteClippingRect);
	}

	IGUIElement::draw();
}



//! fills the listbox with files.
void CGUITextureOpenDialog::fillListBox()
{

    if (!FileSystem || !TextureBox )
        return;

    TextureBox->clear();


    std::string texturePrefix = core::stringc(TextBoxPrefix->getText()).c_str();
    Textures = TexLoader->getTextures(texturePrefix);


    video::ITexture* tex;
    io::path texName;

    for (u32 i=0; i < Textures.size(); ++i)
    {
        tex = Textures.at(i);
        texName = tex->getName().getPath();
        texName = FileSystem->getFileBasename(texName, false);

        TextureBox->addItem(core::stringw(texName).c_str(), tex);
    }
}


////! sends the event that the file has been selected.
//void CGUITextureOpenDialog::sendSelectedEvent()
//{

////	SEvent event;
////	event.EventType = EET_GUI_EVENT;
////	event.GUIEvent.Caller = this;
////	event.GUIEvent.Element = 0;
////	event.GUIEvent.EventType = type;
////	Parent->OnEvent(event);

//    SEvent event;

//    event.EventType = EET_USER_EVENT;

//    event.GUIEvent.Caller = this;

//    event.GUIEvent.Element = 0;

//    event.GUIEvent.EventType = EGET_FILE_SELECTED;


//    event.UserEvent.UserData1 = GUI_EVENT_TEXTURE_SELECTED;

//    if(Parent)
//    //Parent->OnEvent(event);


//    Environment->postEventFromUser(event);

//}


//! sends the event that the file choose process has been canceld
void CGUITextureOpenDialog::sendCancelEvent()
{
	SEvent event;
	event.EventType = EET_GUI_EVENT;
	event.GUIEvent.Caller = this;
	event.GUIEvent.Element = 0;
	event.GUIEvent.EventType = EGET_FILE_CHOOSE_DIALOG_CANCELLED;
	Parent->OnEvent(event);
}

} // end namespace gui
} // end namespace irr

#endif // _IRR_COMPILE_WITH_GUI_
