/*! Copyright (C) 2017  	Department of Electrical Engineering & Computer Science, Institute of
 *			Real-Time Learning Systems, University of Siegen
 *
 *			Research Group of Ecology and Behavioral Biology, Institute of Biology,
 *			University of Siegen
 *
 * Contact: virtual.fish.project@gmail.com
 *
 * License: GNU General Public License  See LICENSE.txt for the full license.
 *
 * This file is part of the "FishSim Animation Toolchain".
 * "FishSim Animation Toolchain" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "FishSim Animation Toolchain" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "FishSim Animation Toolchain".  If not, see <http: *www.gnu.org/licenses/>.
 *
 * The "FishSim Animation Toolchain" was developed within the scope of the interdisciplinary,
 * DFG (Deutsche Forschungsgemeinschaft)-funded project “virtual fish” (KU
 * 689/11-1 and Wi 1531/12-1) of the Institute of Real-Time Learning Systems
 * (EZLS) and the Research Group of Ecology and Behavioral Biology at the
 * University of Siegen.
 *
*/
#ifndef H_STEERINGRECORDER_H
#define H_STEERINGRECORDER_H

#include <sensor_msgs/Joy.h>
#include <ros/package.h>
#include <ros/timer.h>

#include <fishsim/FishSnapshotStamped.h>
#include <boost/thread.hpp>
#include <boost/filesystem.hpp>
#include <boost/lexical_cast.hpp>

#include <time.h>
#include <irrlicht.h>
#include "fish/fish_animation.h"

#include <QObject>

#include "scene/fishsim_scene_manager.h"
#include "fish/fishsnapshot.h"
#include "include/steering_roshandler.h"
#include "fish/animation_manager.h"
#include "utils/joystick_mapping.h"

namespace fishsteering
{
enum E_RECORDER_STATE{
    ERS_IDLE = 0,
    ERS_PLACING_RUNNING,
    ERS_RECORDING_RUNNING,
    ERS_EDITING_RUNNING,
    ERS_PLAYING_RUNNING
};

class SteeringRecorder: public QObject
{

    Q_OBJECT

public:

    /// Constructor
    SteeringRecorder(fishsim::FishSimSceneManager *fishSimSmgr);

    /// Destructor
    ~SteeringRecorder();

    /// Load bag from file.
    bool loadBag(const std::string& filepath);

    /// Save animations as bag file.
    bool saveBag(const std::string& fileName);

    /// Get length of animation.
    int getAnimationLength();

    /// Start a new recording.
    void startRecording(std::string modelName);
    void recording(std::string modelName);

    /// Stop the recording.
    void stopRecording();

    /// Start editing an existing animation.
    void startEditing(std::string modelName, int edtOpt);
    void editing(std::string modelName, int edtOpt);

    /// Stop editing.
    void stopEditing();

    /// Start placing model.
    void startPlacingModel(std::string modelName);
    void placingModel(std::string modelName);

    /// Stop placing model.
    void stopPlacingModel();

    /// Play the animations.
    void play();

    /// Change model to steer.
    bool changeCurrentModel(const std::string &modelName);

    /// Returns true, if model already has an animation recorded
    bool modelAnimationExist(std::string modelName);

    /// All possible edit modifier of the current fish.
    std::vector<std::string> getCurentEditingOptions();

    /// Change speed settings of fishsteering
    void changeSettings(float forewardSpeed, float yawSpeed, float pitchSpeed);

    /// Get the current state of the recorder
    E_RECORDER_STATE getState(){
        return m_state;
    }

    bool isWaitingForJoystick()
    {
        return m_waitForJoy;
    }


public slots:
    void setbagCounter(int msec);
    void startPlaying();
    void stopPlaying();

signals:
    void bagFileReady();
    void bagCounterChanged(int msec);
    void bagRevisonListUpdate();
    void setBagAndRevision();
    void playingDone();
    void stateChanged();
    void waitingForJoystick();

private:

    // Fishsim scene manager.
    fishsim::FishSimSceneManager* m_fishSimSmgr;

    // Pointer to fish that is currently edited.
    fishsim::Fish* m_currentFish;

    // Pointer to animation manager which keeps track of
    // all animations and allows to edit animations after
    // recording.
    fishsim::AnimationManager* m_animManager;

    // First animation that was recorded. Used as a reference
    // for the other ones. e.g. the timestamps are used from this
    // animation.
    fishsim::FishAnimation* firstAnimation;

    // Joystick mapping
    JoystickMapping* m_joystickMapping;

    // Interface to ROS.
    SteeringROSHandler *m_roshandler;

    // True if record is running.
    bool recordingIsRunning;

    // True if placing is running.
    bool placingIsRunning;

    // True if editing is running
    bool editingIsRunning;

    // True if playing is running
    bool playingIsRunning;

    // Current foreward speed. Gets multiplied with joy input.
    float forewardSpeed;

    // Current yaw speed. Gets multiplied with joy input.
    float yawSpeed;

    // Current roll speed. Gets multiplied with joy input.
    float rollSpeed;

    bool m_waitForJoy;

    // Where to save bags.
    std::string bagFolder;

    // Keeps track of the animation progress.
    int animationCounter;

    // All animations have the same maximum length,
    // the global length.
    int globalAnimationLength;

    // Threads
    boost::thread placingThread;
    boost::thread recordThread;
    boost::thread editingThread;
    boost::thread playingThread;

    // Frequency of snapshot recording.
    int serverUpdateRate;

    // Current state of recorder
    E_RECORDER_STATE m_state;

    // Publish snapshot of current fish.
    void publishSnapshot();

    // Publish snapshot of all fish in scene.
    void publishAll(std::vector<fishsim::FishSnapshot*> snapshots);

    // Create a bag filename.
    std::string createBagFileName();

    // Move the current fish.
    void joyMove();

    // Load scene.
    void initScene();

    // True, if one of the tasks is running.
    bool isTaskRunning();

    // Get the time between to snapshots. Depends on serverUpdateRate.
    float getWaitTime();

    // Update state and inform GUI that state has changed
    void updateState(E_RECORDER_STATE newState);

    // Blocks thread until a joy message is available.
    void waitForJoyInput();

    // Given the keyframe counter, it returns the time since
    // the start of the animation in milliseconds.
    int keyframeCounterToMSec(int msgCounter);
};
}
#endif // H_STEERINGRECORDER_H
