#include "fish/bone_modifier/turn_swim_modifier.h"

using namespace irr;
using namespace scene;


// Maximum turn angle of bones in degree
#define MAX_TURN_ANGLE 25.0

// Input value gets scaledwith this value. Can
// increase/ degrease turn amount.
#define INPUT_MULTIPLIER 4.0

namespace fishsim
{
TurnSwimModifier::TurnSwimModifier(IAnimatedMeshSceneNode *fishNode, std::string name,
                                   float speed, bool isActive)
    : BoneModifier(fishNode, name, speed, isActive)
{
    m_currentTurnAngle = 0.0;
}

TurnSwimModifier::~TurnSwimModifier(){

    if(m_backbone1)
        m_backbone1->drop();

    if(m_backbone2)
        m_backbone2->drop();

    if(m_backbone3)
        m_backbone3->drop();

    if(m_backbone4)
        m_backbone4->drop();


    if(m_tailfin_middle1)
        m_tailfin_middle1->drop();

    if(m_tailfin_middle2)
        m_tailfin_middle2->drop();

    if(m_tailfin_middle3)
        m_tailfin_middle3->drop();


    if(m_tailfin_up1)
        m_tailfin_up1->drop();

    if(m_tailfin_up2)
        m_tailfin_up2->drop();

    if(m_tailfin_up3)
        m_tailfin_up3->drop();


    if(m_tailfin_down1)
        m_tailfin_down1->drop();

    if(m_tailfin_down2)
        m_tailfin_down2->drop();

    if(m_tailfin_down3)
        m_tailfin_down3->drop();

    m_turnBones.clear();
}

bool TurnSwimModifier::init(){
    // Get bones that are used for animation
    bool allBonesFound = true;

    allBonesFound = allBonesFound && getAndGrabBone(m_head, "head");
    allBonesFound = allBonesFound && getAndGrabBone(m_backbone1,"backbone_1");
    allBonesFound = allBonesFound && getAndGrabBone(m_backbone2,"backbone_2");
    allBonesFound = allBonesFound && getAndGrabBone(m_backbone3,"backbone_3");
    allBonesFound = allBonesFound && getAndGrabBone(m_backbone4,"backbone_4");

    allBonesFound = allBonesFound && getAndGrabBone(m_tailfin_middle1,"tail_fin_middle1");
    allBonesFound = allBonesFound && getAndGrabBone(m_tailfin_middle2,"tail_fin_middle2");
    allBonesFound = allBonesFound && getAndGrabBone(m_tailfin_middle3,"tail_fin_middle3");


    allBonesFound = allBonesFound && getAndGrabBone(m_tailfin_up1, "tail_fin_up1");
    allBonesFound = allBonesFound && getAndGrabBone(m_tailfin_up2, "tail_fin_up2");
    allBonesFound = allBonesFound && getAndGrabBone(m_tailfin_up3, "tail_fin_up3");

    allBonesFound = allBonesFound && getAndGrabBone(m_tailfin_down1, "tail_fin_down1");
    allBonesFound = allBonesFound && getAndGrabBone(m_tailfin_down2, "tail_fin_down2");
    allBonesFound = allBonesFound && getAndGrabBone(m_tailfin_down3, "tail_fin_down3");

    if(!allBonesFound)
    {

        std::cerr << "Turn swim modifier failed on init." << std::endl;

        // Error handling: just disable moifier for now
        m_isActive = false;
        return false;
    }

    m_turnBones.push_back(m_head);
    m_turnBones.push_back(m_backbone1);
    m_turnBones.push_back(m_backbone2);
    m_turnBones.push_back(m_backbone3);
    m_turnBones.push_back(m_backbone4);
    m_turnBones.push_back(m_tailfin_middle1);
    m_turnBones.push_back(m_tailfin_middle2);
    m_turnBones.push_back(m_tailfin_middle3);
    return true;
}

void TurnSwimModifier::update(float inputVal1, float inputVal2, float inputVal3){

    if(!m_isActive)
        return;

    turnAnimation(inputVal1);

    // Update the up and down tail fin parts too.
    m_tailfin_up1->setRotation(m_tailfin_middle1->getRotation());
    m_tailfin_up2->setRotation(m_tailfin_middle2->getRotation());
    m_tailfin_up3->setRotation(m_tailfin_middle3->getRotation());

    m_tailfin_down1->setRotation(m_tailfin_middle1->getRotation());
    m_tailfin_down2->setRotation(m_tailfin_middle2->getRotation());
    m_tailfin_down3->setRotation(m_tailfin_middle3->getRotation());
}

void TurnSwimModifier::turnAnimation(float turnAmount){

    // Simple P controller for turn animation
    float goalAngle = INPUT_MULTIPLIER * turnAmount;
    if(fabs(goalAngle) > MAX_TURN_ANGLE ) goalAngle = copysign(MAX_TURN_ANGLE, goalAngle);

    float error = goalAngle - m_currentTurnAngle;
    m_currentTurnAngle = m_currentTurnAngle + m_speed * error;

    // update rotations
    for(int i = 0; i < m_turnBones.size(); i++){
        addRotationToBone(m_turnBones.at(i), 0.0,0.0, m_currentTurnAngle);
    }
}

void TurnSwimModifier::modify(FishSnapshot *startState,
                         FishSnapshot *endState,
                         const sensor_msgs::Joy &joystickState)
{
    // Do nothing, since modifier wont show up in fishsteering
    return;
}

void TurnSwimModifier::update(const sensor_msgs::Joy &joystickState){

    // Do nothing, since modifier wont show up in fishsteering
    return;
}


} // end namespace fishsim
