#ifndef __C_GUI_TEXTURE_OPEN_DIALOG_H_INCLUDED__
#define __C_GUI_TEXTURE_OPEN_DIALOG_H_INCLUDED__

#include "IrrCompileConfig.h"
#ifdef _IRR_COMPILE_WITH_GUI_

#include "IGUIFileOpenDialog.h"
#include "IGUIButton.h"
#include "IGUIListBox.h"
#include "IGUIEditBox.h"
#include "IFileSystem.h"

#include "irrExt/CGUITextureListBox.h"

#include "scene/texture_loader.h"

#include "sim/fishsim_globals.h"

namespace irr
{
namespace gui
{

    class CGUITextureOpenDialog : public IGUIElement
	{
	public:

		//! constructor
        CGUITextureOpenDialog(const wchar_t* title, IGUIEnvironment* environment,
                IGUIElement* parent, ITimer* timer, s32 id, bool restoreCWD=false,
                io::path::char_type* startDir=0);

		//! destructor
        ~CGUITextureOpenDialog();

		//! returns the filename of the selected file. Returns NULL, if no file was selected.
        const wchar_t* getTextureFileName() const;

		//! called if an event happened.
        bool OnEvent(const SEvent& event);

		//! draws the element and its children
        void draw();

        video::ITexture* getSelectedTexture();

	protected:

		//! fills the listbox with files.
		void fillListBox();

		//! sends the event that the file choose process has been canceld
		void sendCancelEvent();

		core::position2d<s32> DragStart;
		core::stringw FileName;
		io::path FileDirectory;
		io::path RestoreDirectory;
		io::path StartDirectory;

		IGUIButton* CloseButton;
        CGUITextureListBox* TextureBox;
        IGUIEditBox* TextBoxPrefix;
		IGUIElement* EventParent;
		io::IFileSystem* FileSystem;
		io::IFileList* FileList;
        bool Dragging;

        fishsim::TextureLoader *TexLoader;
        std::vector<video::ITexture*> Textures;
	};


} // end namespace gui
} // end namespace irr

#endif // _IRR_COMPILE_WITH_GUI_

#endif // __C_GUI_TEXTURE_OPEN_DIALOG_H_INCLUDED__

