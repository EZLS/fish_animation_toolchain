#include "fish/gasterosteus/pectoral_modifier.h"

using namespace irr;
using namespace scene;

// Maximum rotation of vertical fins
#define STRENGTH_SIDE 15.0

namespace fishsim
{

PectoralModifier::PectoralModifier(IAnimatedMeshSceneNode *fishNode, std::string name,
                           float speed, bool isActive)
        : BoneModifier(fishNode, name, speed, isActive)
{

}

PectoralModifier::~PectoralModifier(){

    if(m_vertical_fin_up_right1)
        m_vertical_fin_up_right1->drop();

    if(m_vertical_fin_middle_right1)
        m_vertical_fin_middle_right1->drop();

    if(m_vertical_fin_down_right1)
        m_vertical_fin_down_right1->drop();

    if(m_vertical_fin_up_left1)
        m_vertical_fin_up_left1->drop();

    if(m_vertical_fin_middle_left1)
        m_vertical_fin_middle_left1->drop();

    if(m_vertical_fin_down_left1)
        m_vertical_fin_down_left1->drop();

}

bool PectoralModifier::init(){


    // getAndGrabBone will return false if bone could not be found.
    // If one of the function calls returns false, allOk will
    // be false too.
    bool allOk = true;

    // Init vertical fins right
    allOk = allOk && getAndGrabBone(m_vertical_fin_up_right1,"vertical_fin_up_right1");
    allOk = allOk && getAndGrabBone(m_vertical_fin_middle_right1,"vertical_fin_middle_right1");
    allOk = allOk && getAndGrabBone(m_vertical_fin_down_right1,"vertical_fin_down_right1");

    // Init vertical fins left
    allOk = allOk && getAndGrabBone(m_vertical_fin_up_left1,"vertical_fin_up_left1");
    allOk = allOk && getAndGrabBone(m_vertical_fin_middle_left1,"vertical_fin_middle_left1");
    allOk = allOk && getAndGrabBone(m_vertical_fin_down_left1,"vertical_fin_down_left1");

    m_prevSideSinus1 = 0.0;
    m_timerSide = 0.0;

    if(!allOk)
    {
        std::cerr << "Idle modifier stickleback failed on init." << std::endl;
        // Error handling: just disable moifier for now
        m_isActive = false;
        return false;
    }

    float offset = 30.0;
    addRotationToBone(m_vertical_fin_up_right1, 0.0, 0.0, offset);
    addRotationToBone(m_vertical_fin_down_right1, 0.0, 0.0, offset);
    addRotationToBone(m_vertical_fin_middle_right1, 0.0, 0.0, offset);

    addRotationToBone(m_vertical_fin_up_left1,0.0,0.0, -offset);
    addRotationToBone(m_vertical_fin_down_left1,0.0,0.0, -offset);
    addRotationToBone(m_vertical_fin_middle_left1,0.0,0.0, -offset);

    return true;


}


void PectoralModifier::update(float inputVal1, float inputVal2, float inputVal3){

    if(!m_isActive)
        return;

    m_timerSide += m_speed; // how fast

    float sinusVal1 = sin(m_timerSide);

    // Second sinus has a phase shift of pi/2
    float sinusVal2 = sin(m_timerSide+ 1.5707);

    // dir > 0, if sinus is going up,
    //     < 0, if sinus is going down
    float dir1 = sinusVal1 - m_prevSideSinus1;
    m_prevSideSinus1 = sinusVal1;


    float dir2 = sinusVal2 - m_prevSideSinus2;
    m_prevSideSinus2 = sinusVal2;


    if(false) // all the same, all fins
    {
    /// Vertical fin right
    irr::core::vector3df rot = irr::core::vector3df(0.0, 0.0, dir1 * STRENGTH_SIDE );
    addRotationToBone(m_vertical_fin_up_right1, rot);
    addRotationToBone(m_vertical_fin_down_right1, rot);
    addRotationToBone(m_vertical_fin_middle_right1, rot);

    /// Vertical fin left
    addRotationToBone(m_vertical_fin_up_left1, rot);
    addRotationToBone(m_vertical_fin_down_left1, rot);
    addRotationToBone(m_vertical_fin_middle_left1, rot);
    }
    else
    {
        /// Vertical fin right
        irr::core::vector3df rot = irr::core::vector3df(0.0, 0.0, dir1 * STRENGTH_SIDE);
        addRotationToBone(m_vertical_fin_up_right1, rot);

        rot = irr::core::vector3df(0.0, 0.0, -dir1 * STRENGTH_SIDE);
        addRotationToBone(m_vertical_fin_down_right1, rot);

        rot = irr::core::vector3df(0.0, 0.0, dir2 * STRENGTH_SIDE);
    //    addRotationToBone(m_vertical_fin_middle_right1, rot);

        /// Vertical fin left
        rot = irr::core::vector3df(0.0, 0.0, dir1 * STRENGTH_SIDE);
        addRotationToBone(m_vertical_fin_up_left1, rot);

        rot = irr::core::vector3df(0.0, 0.0, -dir2 * STRENGTH_SIDE);
        addRotationToBone(m_vertical_fin_down_left1, rot);

        rot = irr::core::vector3df(0.0, 0.0, dir1 * STRENGTH_SIDE);
    //    addRotationToBone(m_vertical_fin_middle_left1, rot);

    }
}



void PectoralModifier::update(const sensor_msgs::Joy& joystickState){

    // Modifier does not show up in fishsteering. Its only used together
    // with the move function. -> Do nothing
    return;
}

void PectoralModifier::modify(FishSnapshot* startState, FishSnapshot* endState,
            const sensor_msgs::Joy& joystickState ){

    // Modifier does not show up in fishsteering. Its only used together
    // with the move function. -> Do nothing
    return;
}


} // end namespace fishsim
