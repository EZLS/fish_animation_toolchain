#include "gui/camera_editor.h"

#include <string>

#include "IGUIButton.h"

#include "sim/fishsim_globals.h"
#include "sim/fishsim_helper_routines.h"

using namespace irr;
using namespace core;
using namespace scene;
using namespace gui;

namespace fishsim
{

CameraEditor::CameraEditor(irr::IrrlichtDevice *device,
                           irr::scene::ICameraSceneNode *mayaCam,
                           ICameraSceneNode *staticCam)
{
    m_device = device;
    m_device->grab();

    m_guiEnv = m_device->getGUIEnvironment();
    m_guiEnv->grab();

    m_mayaCam = mayaCam;
    m_mayaCam->grab();

    m_staticCam = staticCam;
    m_staticCam->grab();

    createSettingsWindow();
}


CameraEditor::~CameraEditor()
{
    if (m_device)
        m_device->drop();

    if (m_mayaCam)
        m_mayaCam->drop();

    if(m_guiEnv)
        m_guiEnv->drop();

    if(m_winCameraSettings)
    {
        m_winCameraSettings->remove();
        m_winCameraSettings->drop();
    }

    if(m_cameraTab)
        delete m_cameraTab;
}

void CameraEditor::applyCameraSettings(){

    stringc text = m_edtBoxFOV->getText();
    f32 fov = atof(text.c_str());

    m_mayaCam->setFOV(irr::core::degToRad(fov));

    text = m_edtBoxAspectRatio->getText();
    f32 aspectRatio = atof(text.c_str());

    m_mayaCam->setAspectRatio(aspectRatio);

    updateCameraTab();
}


void CameraEditor::updateCameraTab(){

    // fov
    f32 fov = m_mayaCam->getFOV();
    fov = irr::core::radToDeg(fov);
    m_edtBoxFOV->setText( stringw(fov).c_str() );

    // aspect ratio
    f32 aspectRatio = m_mayaCam->getAspectRatio();

    m_edtBoxAspectRatio->setText( stringw(aspectRatio).c_str() );
}


void CameraEditor::createSettingsWindow(){


    ///
    /// Create toolbox
    ///
    ///
    const core::vector2di windowStartPos = core::vector2di(TOOLBOX_START_X,
                                                          TOOLBOX_START_Y);

    rect<s32> toolBoxRect(0,0, TOOLBOX_WITDH, TOOLBOX_HEIGHT);
    toolBoxRect.UpperLeftCorner +=  windowStartPos;
    toolBoxRect.LowerRightCorner += windowStartPos;

    rect<s32> tabCtrlRect(2, 20,TOOLBOX_WITDH -2,TOOLBOX_HEIGHT -2);

    // Create toolbox window
    m_winCameraSettings = m_guiEnv->addWindow(toolBoxRect, false,
                                             L"Camera settings", 0,
                                              GUI_ID_WIN_CAMERA_SETTINGS);
    m_winCameraSettings->grab();
    // create tab control
    IGUITabControl* tabCtrl = m_guiEnv->addTabControl( tabCtrlRect,
                                                       m_winCameraSettings,
                                                       true, true);
    ///
    /// Create position tab
    ///
    IGUITab* tabCamera = tabCtrl->addTab(L"Camera");
    m_cameraTab = new FishSimToolboxTab(m_guiEnv, tabCamera, TOOLBOX_WITDH, TOOLBOX_HEIGHT);

    m_cameraTab->appendStaticText(L"FOV");
    m_edtBoxFOV = m_cameraTab->appendEditBox(L"");

    m_cameraTab->appendSeparator();

    m_cameraTab->appendStaticText(L"Aspect ratio");
    m_edtBoxAspectRatio = m_cameraTab->appendEditBox(L"");

    m_cameraTab->appendButton(L"Set", GUI_ID_BTN_SET_CAMERA_SETTINGS,
                              L"Apply all settings");

    m_cameraTab->appendSeparator();
    m_cameraTab->appendStaticText(L"Copy settings to static cam");
    m_cameraTab->appendButton(L"Copy",GUI_ID_BTN_SET_STATIC_CAM,
                              L"Copy the state of the free camera (positon, orientation, fov.. to the static camera");

    updateCameraTab();
}

} // end namespace fishsim
