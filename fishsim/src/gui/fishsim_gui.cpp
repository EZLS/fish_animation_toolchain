#include "gui/fishsim_GUI.h"
#include "sim/fishsim_globals.h"
#include "irrExt/CGUICustomModalScreen.h"


using namespace irr;
using namespace scene;
using namespace video;
using namespace gui;

namespace fishsim
{

FishSimGUI::FishSimGUI(IrrlichtDevice* device, FishSimSceneManager* fishsimSmgr,
                       const std::string& pathToFishSim)
{

    m_device = device;
    m_guiEnv = m_device->getGUIEnvironment();
    m_driver = device->getVideoDriver();

    m_device->grab();
    m_guiEnv->grab();
    m_driver->grab();

    m_screenHeight = m_device->getVideoDriver()->getScreenSize().Height;
    m_screenWidth = m_device->getVideoDriver()->getScreenSize().Width;

    m_isVisible = false;

    m_fishsimSmgr = fishsimSmgr;

    m_fishSimPath = pathToFishSim;

    m_fishEditor = 0;
    m_lightEditor = 0;
    m_cameraEditor = 0;
    m_helpWindow = 0;
    m_loadingInfo = 0;

    createStaticGUI();
}

FishSimGUI::~FishSimGUI(){

    if(m_device)
        m_device->drop();

    if(m_guiEnv)
        m_guiEnv->drop();

    if(m_driver)
        m_driver->drop();

    if(m_fishEditor)
        delete m_fishEditor;

    if(m_lightEditor)
        delete m_lightEditor;

    if(m_cameraEditor)
        delete m_cameraEditor;

    if(m_helpWindow)
        delete m_helpWindow;
}


void FishSimGUI::draw(){

    if(m_isVisible)
        m_guiEnv->drawAll();
}

bool FishSimGUI::closeAllWindows()
{

    bool windowClosed = false;

    if(m_cameraEditor)
    {
        onCloseCameraSettings();
        windowClosed = true;
    }

    if(m_lightEditor)
    {
        onCloseLightSettings();
        windowClosed = true;
    }

    if(m_fishEditor)
    {
        onCloseFishEditToolbox();
        windowClosed = true;
    }

    if(m_helpWindow)
    {
        onCloseHelpWindow();
        windowClosed = true;
    }

    return windowClosed;
}

void FishSimGUI::showLoadingInfo()
{
    m_loadingInfo->setVisible(true);
}

void FishSimGUI::hideLoadingInfo()
{
    m_loadingInfo->setVisible(false);
}

CGUIFileSelector* FishSimGUI::addFileSelector(const wchar_t* title,
                                              s32 id, E_FILESELECTOR_TYPE type)
{

    CGUIFileSelector *selector = new CGUIFileSelector(title,
                                                      m_device->getGUIEnvironment(),
                                                      m_device->getGUIEnvironment()->getRootGUIElement(),
                                                      id, type);

    CGUICustomModalScreen* modalScreen = new CGUICustomModalScreen(m_device->getGUIEnvironment(),
                                                       m_device->getGUIEnvironment()->getRootGUIElement(),
                                                       -1, m_device->getTimer());
    modalScreen->drop();
    modalScreen->addChild(selector);

    return selector;
}

void FishSimGUI::onCloseCameraSettings(){

    if(m_cameraEditor)
        delete m_cameraEditor;

    m_cameraEditor = 0;
    m_guiEnv->setFocus(m_guiEnv->getRootGUIElement());

}

void FishSimGUI::openCameraSettings()
{

    if(m_cameraEditor)
        delete m_cameraEditor;

    m_cameraEditor = new CameraEditor(m_device,
                                      m_fishsimSmgr->getMayaCam(),
                                      m_fishsimSmgr->getStaticCam());
}

//bool FishSimGUI::OnEvent(const SEvent& event)
//{

//}


void FishSimGUI::openFishEditToolbox(){

    if(m_fishsimSmgr->getSelected() == 0)
        return;

    if(m_fishEditor)
        delete m_fishEditor;

    m_fishEditor = new FishEditor(m_device,
                                  m_fishsimSmgr->getSelected(),
                                  m_fishSimPath);


}

void FishSimGUI::onCloseFishEditToolbox(){

    if(m_fishEditor)
        delete m_fishEditor;

    m_fishEditor = 0;
    m_guiEnv->setFocus(m_guiEnv->getRootGUIElement());
    m_fishsimSmgr->deselect();
}

void FishSimGUI::onCloseLightSettings(){

    if(m_lightEditor)
        delete m_lightEditor;

    m_lightEditor = 0;
    m_guiEnv->setFocus(m_guiEnv->getRootGUIElement());
}

void FishSimGUI::openHelpWindow()
{
    if(m_helpWindow)
        delete m_helpWindow;

    m_helpWindow = new HelpWindow(m_device);
}

void FishSimGUI::onCloseHelpWindow()
{
    if(m_helpWindow)
        delete m_helpWindow;

    m_helpWindow = 0;
    m_guiEnv->setFocus(m_guiEnv->getRootGUIElement());
}



void FishSimGUI::openLightSettings(){

    if(m_lightEditor)
        delete m_lightEditor;

    // Struct that holds lighting informations
    SFishSimLightEnv* light = new SFishSimLightEnv;
    light->shadowLight = m_fishsimSmgr->getShadowLight();
    light->shadowManager = m_fishsimSmgr->getShadowManager();

    m_lightEditor = new LightEditor(m_device, light);

}


void FishSimGUI::createStaticGUI(){

    // New font. Default one is too small
    IGUISkin* skin = m_guiEnv->getSkin();

    const std::string fontPath = m_fishSimPath + GLOBAL_FONT_PATH;
    IGUIFont* font = m_guiEnv->getFont(fontPath.c_str());
    if (font)   skin->setFont(font);



    // Create menu
    IGUIContextMenu* menu = m_guiEnv->addMenu();
    menu->addItem(L"File", -1, true, true);
    menu->addItem(L"Settings", -1, true, true);
    menu->addItem(L"Help", -1, true, true);

    // Submenu: "File"
    IGUIContextMenu* file = menu->getSubMenu(0);
    file->addItem(L"Load static object...", GUI_ID_MENU_ADD_STATIC_OBJ, true);
    file->addItem(L"Load fish model...", GUI_ID_MENU_LOAD_FISH, true);
    file->addItem(L"Save Scene...", GUI_ID_MENU_SAVE_SCENE, true);
    file->addItem(L"Load Scene...", GUI_ID_MENU_LOAD_SCENE, true);
    file->addSeparator();
    file->addItem(L"Quit", GUI_ID_MENU_QUIT, true);

    // Submenu: "Settings"
    IGUIContextMenu* settings = menu->getSubMenu(1);
    settings->addItem(L"Camera Settings", GUI_ID_MENU_OPEN_CAMERA_SETTINGS,true, false);
    settings->addItem(L"Light Settings", GUI_ID_MENU_OPEN_LIGHT_SETTINGS, true, false);
    settings->addItem(L"Lights", -1, true, true);

    // Sub- Sub menu: Settings-> Lights
    IGUIContextMenu* lights = settings->getSubMenu(2);
    lights->addItem(L"Disable" , GUI_ID_MENU_DISABLE_LIGHTS, true);
    lights->addItem(L"Enable" , GUI_ID_MENU_ENABLE_LIGHTS, true);

    // Submenue Help
    IGUIContextMenu* help = menu->getSubMenu(2);
    help->addItem(L"About fishsim", GUI_ID_MENU_HELP, true);

    // Create Toolbar
    gui::IGUIToolBar* bar = m_guiEnv->addToolBar();

    std::string iconPath = m_fishSimPath + MEDIA_PATH + "/icon_file_open.png";
    ITexture* image = m_driver->getTexture(iconPath.c_str());
    bar->addButton(GUI_ID_MENU_LOAD_SCENE, 0, L"Load scene",image, 0, false, true);

    iconPath = m_fishSimPath + MEDIA_PATH + "/icon_file_save.png";
    image = m_driver->getTexture(iconPath.c_str());
    bar->addButton(GUI_ID_MENU_SAVE_SCENE, 0, L"Save scene",image, 0, false, true);

    iconPath = m_fishSimPath + MEDIA_PATH + "/icon_tools_2.png";
    image = m_driver->getTexture(iconPath.c_str());
    bar->addButton(GUI_ID_MENU_EDIT_FISH, 0, L"Open fish toolset",image, 0, false, true);

    iconPath = m_fishSimPath + MEDIA_PATH + "/icon_edit_light_2.png";
    image = m_driver->getTexture(iconPath.c_str());
    bar->addButton(GUI_ID_MENU_OPEN_LIGHT_SETTINGS, 0, L"Open light settings",image, 0, false, true);

    iconPath = m_fishSimPath + MEDIA_PATH +"/icon_edit_camera.png";
    image = m_driver->getTexture(iconPath.c_str());
    bar->addButton(GUI_ID_MENU_OPEN_CAMERA_SETTINGS, 0, L"Open camera settings",image, 0, false, true);

    iconPath = m_fishSimPath + MEDIA_PATH + "/icon_delete.png";
    image = m_driver->getTexture(iconPath.c_str());
    bar->addButton(GUI_ID_MENU_DELETE_NODE, 0, L"Delete selected node",image, 0, false, true);


//    irr::core::rect<s32> rect = irr::core::rect<s32>((m_screenWidth / 2) - 140,
//                                                     (m_screenHeight / 2) - 15,
//                                                     (m_screenWidth / 2) + 140,
//                                                     (m_screenHeight / 2) + 15
//                                                     );
    irr::core::rect<s32> rect = irr::core::rect<s32>(0,
                                                     0,
                                                     m_screenWidth,
                                                     m_screenHeight);

    m_loadingInfo = m_guiEnv->addStaticText(L" Loading Textures. May take a while...", rect,true,false,0,-1, true);
    m_loadingInfo->setOverrideColor(video::SColor(255,255,255,0));
    m_loadingInfo->setBackgroundColor(video::SColor(100,50,50,50));
    m_loadingInfo->setTextAlignment(EGUIA_CENTER,EGUIA_CENTER);
    m_loadingInfo->setVisible(false);
}

//void FishSimGUI::openModelSelectionWindow()
//{
//    if(m_modelSelectionWin)
//        delete m_modelSelectionWin;

//    m_modelSelectionWin = new ModelSelectionWindow(m_device, m_fishSimPath);

//}

//void FishSimGUI::onCloseModelSelectionWindow()
//{
//    if(m_modelSelectionWin)
//        delete m_modelSelectionWin;

//    m_modelSelectionWin = 0;
//}


} // end namespace fishsim
