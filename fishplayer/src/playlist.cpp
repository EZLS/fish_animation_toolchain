#include "include/playlist.h"
namespace fishplayer
{
Playlist::Playlist(QObject *parent, RosInterface *roshandler) : QObject(parent)
{
    m_playingIsRunning = false;
    m_roshandler = roshandler;
    m_currentPlayback = 0;
}

Playlist::~Playlist(){

    // Just in case we are still playing
    stop();

}

void Playlist::play(std::vector<Clip*> clips)
{
    if(!m_playingIsRunning)
    {
        m_playingIsRunning = true;
        m_playingThread = boost::thread(&Playlist::playback, this, clips);
    }
}


void Playlist::playback(std::vector<Clip*> clips)
{
    std::vector<Clip*>::iterator it = clips.begin();
    int clipNum = 0;
    for(it; it != clips.end() && m_playingIsRunning; ++it)
    {
        emit(startNewClip(clipNum++));
        m_currentPlayback = *it;
        m_currentPlayback->play(m_roshandler);
    }

    m_playingIsRunning = false;
    emit(playbackFinished());
}

void Playlist::stop()
{
    if(m_playingIsRunning)
    {
        m_playingIsRunning = false;

        if(m_currentPlayback)
            m_currentPlayback->stop();

        m_playingThread.join();
        m_playingThread.detach();
    }
    m_currentPlayback = 0;
}


bool Playlist::savePlaylist(std::vector<Clip*> clips, const std::string& filePath)
{
    if(m_playingIsRunning)
        return false;

    if(clips.size() == 0)
        return false;

    YAML::Emitter out;
    out << YAML::BeginSeq;

    std::vector<Clip*>::iterator it = clips.begin();
    for(it; it != clips.end(); ++it)
        (*it)->save(out);

    out << YAML::EndSeq;



    std::ofstream fout(filePath + ".yaml");
    fout << out.c_str();

    return true;
}

 bool Playlist::loadPlaylist(const std::string& filePath, std::vector<Clip *> &clips)
{

    if(m_playingIsRunning)
        return false;

    YAML::Node playlist = YAML::LoadFile(filePath);

    if(playlist.size() == 0)
        return false;

    bool success = true;

    for (std::size_t i = 0 ; i < playlist.size() ; i++) {

        YAML::Node node = playlist[i];

        // Node has to be a map
        if(!node || !node.IsMap())
        {
            success = false;
            break;
        }

        // A type is needed
        int type = node["type"].as<int>();
        if(!type)
        {
            success = false;
            break;
        }

        // Let's see what we got and create the right clip object
        Clip* newClip;
        switch (type) {

#ifdef USE_AUTOMVOER
        case E_CLIP_TYPE::AUTO:
            newClip = new AutoClip(0, node);
            break;
#endif

        case E_CLIP_TYPE::BAG:
            newClip = new BagClip(0, node);
            break;

        case E_CLIP_TYPE::LOAD:
            newClip = new LoadClip(0, node);
            break;

        case E_CLIP_TYPE::PAUSE:
            newClip = new PauseClip(0, node);
            break;
        default:
            success = false;
            break;
        }

//        if(!newClip->verify())
//        {
//            success = false;
//            break;
//        }
        clips.push_back(newClip);
    }

    return success;
}
}
