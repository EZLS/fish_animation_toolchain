/*! Copyright (C) 2017  	Department of Electrical Engineering & Computer Science, Institute of
 *			Real-Time Learning Systems, University of Siegen
 *
 *			Research Group of Ecology and Behavioral Biology, Institute of Biology,
 *			University of Siegen
 *
 * Contact: virtual.fish.project@gmail.com
 *
 * License: GNU General Public License  See LICENSE.txt for the full license.
 *
 * This file is part of the "FishSim Animation Toolchain".
 * "FishSim Animation Toolchain" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "FishSim Animation Toolchain" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "FishSim Animation Toolchain".  If not, see <http: *www.gnu.org/licenses/>.
 *
 * The "FishSim Animation Toolchain" was developed within the scope of the interdisciplinary,
 * DFG (Deutsche Forschungsgemeinschaft)-funded project “virtual fish” (KU
 * 689/11-1 and Wi 1531/12-1) of the Institute of Real-Time Learning Systems
 * (EZLS) and the Research Group of Ecology and Behavioral Biology at the
 * University of Siegen.
 *
 *   \file   bone_modifier.h
 *   \brief  A BoneModifier takes the current joystick input and turns it into a bone
 *           movement.
 *
 *   BoneModifier are able to work in two different ways. The first one is a direct mode,
 *   where the bones of the IAnimatedMeshSceneNode -that is passed to constructor- are modified
 *   directly. This is used in recording mode in fishsteering.
 *   See update(...) for more informations
 *
 *   The second mode is a indirect mode. The bones of the IAnimatedMeshSceneNode are not changed
 *   at all. This mode only opperates on two snapshots and will be used in modify mode in fishsteering.
 *   See modify(...) for more informations.
 *
*/

#ifndef H_BONE_MODIFIER_H
#define H_BONE_MODIFIER_H


#include <irrlicht.h>
#include <string>
#include <sensor_msgs/Joy.h>

#include "fish/fishsnapshot.h"


namespace fishsim
{
class BoneModifier
{

public:

    /// Constructor
    BoneModifier(irr::scene::IAnimatedMeshSceneNode* fishNode, std::string name,
                 float speed = 1.0, bool isActive = true) : m_fishNode(fishNode),
        m_name(name), m_speed(speed), m_isActive(isActive){

        m_fishNode->grab();
    }

    /// Destructor
    ~BoneModifier(){
        m_fishNode->drop();
    }

    /// Init is called when bone modifier is created. Use it to grab the bone pointers that
    /// are modified by this BoneModifier
    virtual bool init() = 0;    

    /// Update with joy message. Will change bone rotation of the irrlicht node
    /// Normally used in fishsteering recoding mode.
    virtual void update(const sensor_msgs::Joy& joystickState) = 0;

    /// Update with 3 single values, one for each axis. Will change bone rotation of the irrlicht node
    /// Normally used in move function of the fish.
    virtual void update(float inputVal1, float inputVal2 = 0, float inputVal3 = 0) = 0;

    /// Modify based on an existing snapshot. Will not effect bone rotation of irrlicht node
    /// startState is the state before modification takes places. The result of the modification
    /// will be written to end state.
    /// Normally used in fishsteering modify mode.
    virtual void modify(FishSnapshot* startState,
                        FishSnapshot* endState,
                        const sensor_msgs::Joy& joystickState ) = 0;

    /// Activate / Deactivate the modifier
    void setActive(bool isActive)
    {
        m_isActive = isActive;
    }

    /// Set name
    void setName(const std::string& name){
        m_name = name;
    }

    /// Set speed
    void setSpeed(float speed){
        m_speed = speed;
    }

    /// Return name, which includes in most cases
    /// a description.
    std::string getName(){
        return m_name;
    }

    /// Returns active state
    bool isActive(){
        return m_isActive;
    }

    /// Helper function:
    /// Tries to find the BoneSceneNode with name boneName in the irrlicht node.
    /// If bone can be found, it is then grabbed and true is returned.
    /// Otherwise warining is printed and false will be returned.
    bool getAndGrabBone(irr::scene::IBoneSceneNode*& bone, const std::string& boneName)
    {
        bone = m_fishNode->getJointNode(boneName.c_str());

        if(bone)
        {
            bone->grab();
            return true;
        }

        std::cerr <<m_fishNode->getName() << " has no bone with name: " <<boneName << std::endl;
        return false;
    }


    /// Helper function:
    /// Add a rotation vector to a bone.
    void addRotationToBone(irr::scene::IBoneSceneNode* bone, irr::core::vector3df rotationToAdd)
    {
        bone->setRotation(bone->getRotation() + rotationToAdd);
    }

    /// Helper function:
    /// Add rotation to bone given as euler angles
    void addRotationToBone(irr::scene::IBoneSceneNode *bone, float X, float Y, float Z)
    {
        irr::core::vector3df toAdd(X,Y,Z);
        bone->setRotation(bone->getRotation() + toAdd);
    }


protected:

    /// Name of the modifier.
    std::string m_name;

    /// Active state.
    bool m_isActive;

    /// Speed of the modifier. Usage depends on modifier implementation.
    float m_speed;

    /// Pointer to scene node.
    irr::scene::IAnimatedMeshSceneNode* m_fishNode;



};


} // end namespace fishsim

#endif // H_BONE_MODIFIER_H
