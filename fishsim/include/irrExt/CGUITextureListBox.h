// Copyright (C) 2002-2012 Nikolaus Gebhardt
// This file is part of the "Irrlicht Engine".
// For conditions of distribution and use, see copyright notice in irrlicht.h

#ifndef __C_GUI_TEXTURE_LIST_BOX_H_INCLUDED__
#define __C_GUI_TEXTURE_LIST_BOX_H_INCLUDED__

#include "IrrCompileConfig.h"
#ifdef _IRR_COMPILE_WITH_GUI_

#include "IGUIListBox.h"
#include "irrArray.h"
#include "IGUIListBox.h"
#include "IGUISkin.h"
#include "IGUIEnvironment.h"
#include "IVideoDriver.h"
#include "IGUIFont.h"
#include "IGUISpriteBank.h"
#include "IGUIScrollBar.h"
#include "irrlicht/coreutil.h"
#include "ITimer.h"

namespace irr
{
namespace gui
{

	class IGUIFont;
	class IGUIScrollBar;

    class CGUITextureListBox : public IGUIElement
	{
	public:
		//! constructor
        CGUITextureListBox(IGUIEnvironment* environment, IGUIElement* parent, ITimer *timer,
			s32 id, core::rect<s32> rectangle, bool clip=true,
			bool drawBack=false, bool moveOverSelect=false);

		//! destructor
        virtual ~CGUITextureListBox();

		//! returns amount of list items
        u32 getItemCount() const;


		//! clears the list
        void clear();

		//! returns id of selected item. returns -1 if no item is selected.
        s32 getSelected() const;

        //! sets the selected item. Set this to -1 if no item should be selected
        void setSelected(s32 id);

		//! called if an event happened.
        bool OnEvent(const SEvent& event);

		//! draws the element and its children
        void draw();

        u32 addItem(const wchar_t* text, video::ITexture* texture);

//		//! adds an list item with an icon
//		//! \param text Text of list entry
//		//! \param icon Sprite index of the Icon within the current sprite bank. Set it to -1 if you want no icon
//		//! \return
//		//! returns the id of the new created item
//        u32 addItem(const wchar_t* text, s32 icon);


//		//! removes an item from the list
//		virtual void removeItem(u32 id);

//		//! get the the id of the item at the given absolute coordinates
//		virtual s32 getItemAt(s32 xpos, s32 ypos) const;

		//! Sets the sprite bank which should be used to draw list icons. This font is set to the sprite bank of
		//! the built-in-font by default. A sprite can be displayed in front of every list item.
		//! An icon is an index within the icon sprite bank. Several default icons are available in the
		//! skin through getIcon
        void setSpriteBank(IGUISpriteBank* bank);

		//! Update the position and size of the listbox, and update the scrollbar
        void updateAbsolutePosition();



        s32 getItemAt(s32 xpos, s32 ypos) const;

		//! set global itemHeight
        void setItemHeight( s32 height );

        //! Sets whether to draw the background
        void setDrawBackground(bool draw);


	private:

		struct ListItem
		{
            ListItem() : texture(0)
			{}

            core::stringw text;
            video::ITexture* texture;

		};

		void recalculateItemHeight();
		void selectNew(s32 ypos, bool onlyHover=false);
		void recalculateScrollPos();

		// extracted that function to avoid copy&paste code
        void recalculateItemWidth();


		core::array< ListItem > Items;
		s32 Selected;
        s32 ItemHeight;
		s32 TotalItemHeight;
        s32 ItemsTextureWidth;
		gui::IGUIFont* Font;
        //gui::IGUISpriteBank* IconBank;
        IGUIScrollBar* ScrollBar;
        ITimer *Timer;
		u32 selectTime;
		u32 LastKeyTime;
		core::stringw KeyBuffer;
		bool Selecting;
		bool DrawBack;
		bool MoveOverSelect;
		bool AutoScroll;
		bool HighlightWhenNotFocused;
	};


} // end namespace gui
} // end namespace irr

#endif // _IRR_COMPILE_WITH_GUI_

#endif
