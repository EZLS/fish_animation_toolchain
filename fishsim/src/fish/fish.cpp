#include "fish/fish.h"
#include <math.h>

#include "sim/fishsim_helper_routines.h"

#include "ros/joy_indices.h"

using namespace irr;
using namespace scene;
using namespace core;

// Size of snapshot buffer
#define DEFAULT_SNAPSHOT_BUFFER_SIZE 20

// Slide val of CollisionResponseAnimator.
#define SLIDE_VAL 0.0

namespace fishsim
{
Fish::Fish(IAnimatedMeshSceneNode *fishNode){

    m_irrNode = fishNode;
    m_irrNode->grab();
    m_snapshotBufferSize = DEFAULT_SNAPSHOT_BUFFER_SIZE;
    m_collisionAnimator = 0;
    m_forewardSmoothFilter = 0;
    m_currentYaw = 0.0;

}


Fish::~Fish()
{
    unload();
}


bool Fish::unload()
{

    if(m_irrNode){
        m_irrNode->drop();
        m_irrNode->remove();
        m_irrNode = 0;
    }

    if(m_collisionAnimator)
    {
        m_collisionAnimator->drop();
        m_collisionAnimator = 0;
    }
    for(int i = 0; i < m_editableMaterials.size(); i++)
        delete m_editableMaterials.at(i);

    for(int i = 0; i < m_boneModifiers.size(); i++)
        delete m_boneModifiers.at(i);

    if(m_forewardSmoothFilter)
    {
        delete m_forewardSmoothFilter;
        m_forewardSmoothFilter = 0;
    }
    m_snapshotBuffer.clear();
    m_editableMaterials.clear();
    m_boneModifiers.clear();

    return true;
}


bool Fish::load()
{

    // To controll joints directly
    m_irrNode->setJointMode(EJUOR_CONTROL);

    m_irrNode->setMaterialFlag(irr::video::EMF_LIGHTING, true);
    m_irrNode->setMaterialType(irr::video::EMT_TRANSPARENT_ALPHA_CHANNEL);
    m_irrNode->setMaterialFlag(irr::video::EMF_NORMALIZE_NORMALS, true);

    /// Init foreward smoothing filter
    std::vector<float> boxBlurKernel;
    for(int i = 0; i < 14; i++)
        boxBlurKernel.push_back(0.1);

    m_forewardSmoothFilter = new FIRFilter(boxBlurKernel);

    return true;
}

void Fish::updateBoneModifiers(const sensor_msgs::Joy &joyState){

    for(int i = 0; i < m_boneModifiers.size(); i++){
        m_boneModifiers[i]->update(joyState);
    }
}

void Fish::move(const sensor_msgs::Joy &joystickState){

    return;
}

void Fish::move(float forewardAmount, float sidewardAmount, float upwardAmount,
                float yawAmount, float pitchAmount, float rollAmount){

    // Rotate around z axis (pitch fish).
    pitch(pitchAmount);

    // Rotate around y axis (yaw fish).
    yaw(yawAmount);
    m_currentYaw -= yawAmount;
    /// TODO: roll

    // Translate.
    translate(forewardAmount, sidewardAmount, upwardAmount);

    doCollisionCheck();
}

float Fish::getForewardSmoothed(float inputVal)
{
    m_forewardSmoothFilter->addNewInputVal(inputVal);
    return m_forewardSmoothFilter->getFilterValue() * 2;
}

void Fish::doCollisionCheck(){

    updateCollisionBoundingBox();
    m_collisionAnimator->animateNode(m_irrNode,0);
}

void Fish::translate(float foreward, float sideward, float upward){

    irr::core::vector3df
            translation = irr::core::vector3df(foreward,upward, sideward );

    irr::core::matrix4 transformationMat;
    transformationMat.makeIdentity();
    transformationMat.setRotationDegrees(m_irrNode->getRotation());
    transformationMat.setTranslation(m_irrNode->getPosition());

    // Do transformation
    transformationMat.transformVect(translation);

    m_irrNode->setPosition(translation);
}

void Fish::yaw(float degs){

    irr::core::matrix4 m,n;
    m.setRotationDegrees(m_irrNode->getRotation());
    irr::core::vector3df yAxis(0,1,0);
    m.inverseRotateVect(yAxis);


    //rotate model around global y axis
    n.setRotationAxisRadians(-degs * (float)irr::core::DEGTORAD, yAxis);
    m *= n;

    m_irrNode->setRotation(m.getRotationDegrees());

}

void Fish::pitch(float degs){


    irr::core::vector3df zRotation(0.0,0.0,-degs);

    //calculate rotation
    irr::core::matrix4 m, n;
    m.setRotationDegrees(m_irrNode->getRotation());
    n.setRotationDegrees(zRotation);
    m *= n;

    m_irrNode->setRotation(m.getRotationDegrees());
}

void Fish::slide(float slideFwdAmount, float slideSideAmount)
{
    irr::core::vector3df
            translation = irr::core::vector3df(slideFwdAmount, 0.0, slideSideAmount);

    std::cout << m_currentYaw << std::endl;
    irr::core::matrix4 transformationMat;
    transformationMat.makeIdentity();
    transformationMat.setRotationDegrees(irr::core::vector3df(0,m_currentYaw,0));
    transformationMat.setTranslation(m_irrNode->getPosition());

    // Do transformation
    transformationMat.transformVect(translation);

    m_irrNode->setPosition(translation);

}


std::vector<std::string> Fish::getBones(){

    std::vector<std::string> res;
    res.reserve(m_irrNode->getJointCount());

    for(uint i = 0; i < m_irrNode->getJointCount(); ++i)
    {
        res.push_back(m_irrNode->getJointNode(i)->getName());
    }

    return res;
}

void Fish::createTriangleSelector(ISceneManager* sceneMngr){

    // Add triangle selector to fish so that user can select it by mouseclick
    scene::ITriangleSelector* selector =
            sceneMngr->createOctreeTriangleSelector(m_irrNode->getMesh(),m_irrNode);

    m_irrNode->setTriangleSelector(selector);
    selector->drop();
}

void Fish::createCollisionResponseAnimator(ISceneManager *smgr, ITriangleSelector *worldTriangles){

    core::aabbox3df bb = m_irrNode->getTransformedBoundingBox();
    core::vector3df r = bb.MaxEdge - bb.getCenter();

    m_collisionAnimator =
            new CSceneNodeAnimatorCollisionResponse(smgr, worldTriangles,
                                                    m_irrNode,r ,
                                                    vector3df(0.0,0.0,0.0),
                                                    vector3df(0.0,0.0,0.0),SLIDE_VAL
                                                    );


    m_irrNode->addAnimator(m_collisionAnimator);
}

void Fish::updateCollisionBoundingBox(){

    core::aabbox3df bb = m_irrNode->getTransformedBoundingBox();
    core::vector3df r = bb.MaxEdge - bb.getCenter();

    m_collisionAnimator->setEllipsoidRadius(r);
}

SNamedMaterial* Fish::makeMaterialEditable(int materialIndex,
                                           const wchar_t* materialName,
                                           bool hasDiffuseTexture)
{

    if(materialIndex < 0 || materialIndex > m_irrNode->getMaterialCount())
        return 0;

    video::SMaterial& mat = m_irrNode->getMaterial(materialIndex);

    SNamedMaterial* namedMat = new SNamedMaterial;
    namedMat->material = &mat;
    namedMat->name = materialName;
    namedMat->hasDiffuseTexture = hasDiffuseTexture;

    m_editableMaterials.push_back(namedMat);

    return namedMat;
}

void Fish::removeBonePrefix(std::string nameOfHeadBone)
{
    /// Search for the head bone and use this bone to identify the
    /// the bones name prefix.
    std::string bonePrefix = "";

    for(uint i = 0; i < m_irrNode->getJointCount(); ++i)
    {
        std::string boneName = m_irrNode->getJointNode(i)->getName();
        std::size_t pos = boneName.find(nameOfHeadBone);
        if(pos != std::string::npos)
        {
            bonePrefix = boneName.substr(0, pos);
            break;
        }
    }

   std::cout << "Bone prefix is " << bonePrefix << std::endl;

    /// Remove the prefix from all bones.
    /// NOTE: SJoint contains the INITIAL informations about the joints.
    /// IBoneSceneNode contain all the CURRENT informations about the joints.

    ISkinnedMesh* Smesh = (ISkinnedMesh*)  m_irrNode->getMesh();
    irr::core::array<ISkinnedMesh::SJoint*> allJoints = Smesh->getAllJoints();

    for(uint i = 0; i < m_irrNode->getJointCount(); ++i)
    {

        IBoneSceneNode* bone = m_irrNode->getJointNode(i);

        std::string boneName =  bone->getName();
        std::cout << "Found bone in " << m_irrNode->getName() << ": " << boneName;

        // Find and remove prefix of model.
        std::size_t found = boneName.find(bonePrefix);
        if (found!=std::string::npos)
            boneName.replace (found,bonePrefix.length(),"");

        // Change bone name.
        bone->setName(boneName.c_str());
//        std::cout << " - change bonename to " <<boneName <<std::endl;

        // Change name of joints.
        ISkinnedMesh::SJoint* joint = allJoints[i];
        joint->Name = boneName.c_str();

    }
}




void Fish::addSnapshot(FishSnapshot &snapshot){

    // Always add snapshot if buffer is empty.
    if(m_snapshotBuffer.size() == 0)
    {
        // Store time difference between Server and Client. This is just a very(!) basic clock sync.
        m_timeDiff = snapshot.getTime().toNSec() - ros::Time::now().toNSec();
        m_snapshotBuffer.push_front(snapshot);
        return;
    }

    // Only add new snapshot if it's timestamp is greater than all other timestamps.
    // Otherwise ignore the snapshot, since it is most likely too late. Because buffer is always in order,
    // its only neccessary to compare the first snapshot in the buffer with the new one.
    else if (m_snapshotBuffer[0].getTime().toNSec() < snapshot.getTime().toNSec())
    {
        m_snapshotBuffer.push_front(snapshot);

        // Limit buffer size
        if(m_snapshotBuffer.size() > m_snapshotBufferSize)
            m_snapshotBuffer.pop_back();

    }
    else{
        std::cout << "Snapshot too old. Ignore it." << std::endl;
    }

}

void Fish::updateState(ros::Time currTime){


    // Correct time
    int64_t clientTime = currTime.toNSec() + m_timeDiff;

    if(m_snapshotBuffer.size() == 0){
        return;
    }

    // If current time is already greater than the latest snapshot, teleport the fish to the
    // last known state. Could also use extrapolation here.
    else if (clientTime >= m_snapshotBuffer[0].getTime().toNSec()){

        setFishStateFromSnapshot(m_snapshotBuffer[0]);
        return;
    }

    else if(m_snapshotBuffer.size() >= 2){

        for (unsigned int i = 0; i < m_snapshotBuffer.size() -1; i++){

            //           std::cout << m_snapshotBuffer[i].getTime().toNSec() << std::endl << clientTime
            //                                                                  << std::endl << m_snapshotBuffer[i+1].getTime().toNSec()
            //                                                                  << std::endl << std::endl;


            // Snapshot buffer is in order. Find the two snapshots where the current time is in between their period of time.
            // It should satisfy the condition from.time <= currTime <= to.time
            if(m_snapshotBuffer[i].getTime().toNSec() >= clientTime && clientTime >= m_snapshotBuffer[i+1].getTime().toNSec()){

                //                std::cout << "found snapshots at index " << i << std::endl;

                FishSnapshot from = m_snapshotBuffer[i+1];
                FishSnapshot to = m_snapshotBuffer[i];

                double t1 = static_cast<double>(to.getTime().toNSec() -  clientTime);
                double t2 = static_cast<double>(to.getTime().toNSec() - from.getTime().toNSec());

                double fraction = 1.0 - (t1 / t2 );

                transformFish(from, to, fraction);

                return;
            }

        }
        // This case happens if client time is smaller than all snapshots in buffer.
        // Either interpolation delay is too large, buffer size too small or clocks are not synced.
        // Usually this error can be fixed by tuning these settings. Do nothing and inform the user.
        std::cout << "Client time is smaller than all snapshots in buffer." << std::endl;
    }
}

void Fish::setFishStateFromSnapshot(const FishSnapshot &to){


    m_irrNode->setPosition(to.getPos());
    m_irrNode->setRotation(to.getRot());

    for(auto it : to.getBoneRotationMap())
    {
        std::string boneName = it.first;
        irr::core::vector3df boneRot = it.second;

        irr::scene::IBoneSceneNode* bone = m_irrNode->getJointNode(boneName.c_str());

        if(bone)
        {
            bone->setRotation(boneRot);
        }
    }

}


void Fish::transformFish(const FishSnapshot &from, const FishSnapshot &to, float frac){

    // Clamp fraction for safety
    frac = irr::core::clamp(frac, 0.0f, 1.0f);

    irr::core::vector3df newPos = irr::core::vector3df(0.0,0.0,0.0);
    newPos.interpolate(to.getPos(), from.getPos(), frac);
    m_irrNode->setPosition(newPos);

    //  irr::core::vector3df newScale = irr::core::vector3df(0.0,0.0,0.0);
    //newScale.interpolate(to.getRot(), from.getRot(), frac);
    //m_fish->setScale(newScale);

    // Use quaternions for rotation.
    irr::core::quaternion fromQuaternion(from.getRot().X * irr::core::DEGTORAD,
                                         from.getRot().Y * irr::core::DEGTORAD,
                                         from.getRot().Z * irr::core::DEGTORAD);

    irr::core::quaternion toQuaternion(to.getRot().X * irr::core::DEGTORAD,
                                       to.getRot().Y * irr::core::DEGTORAD,
                                       to.getRot().Z * irr::core::DEGTORAD);

    irr::core::quaternion rotQuaternion;

    // Spherical interpolation
    rotQuaternion.slerp(fromQuaternion, toQuaternion, frac, 0.05);


    irr::core::vector3df newRot = irr::core::vector3df(0.0,0.0,0.0);
    rotQuaternion.toEuler(newRot);

    newRot.X = newRot.X * irr::core::RADTODEG;
    newRot.Y = newRot.Y * irr::core::RADTODEG;
    newRot.Z = newRot.Z * irr::core::RADTODEG;

    m_irrNode->setRotation(newRot);

    /* Per snapshot bones*/
    for(auto it : from.getBoneRotationMap())
    {
        std::string boneName = it.first;
        irr::core::vector3df boneRot = it.second;

        irr::scene::IBoneSceneNode* bone = m_irrNode->getJointNode(boneName.c_str());

        if(bone != 0 && to.hasBoneRotationValue(boneName))
        {
            irr::core::vector3df newBoneRot = irr::core::vector3df(0.0, 0.0, 0.0);
            newBoneRot.interpolate(to.getSingleBoneRot(boneName), boneRot, frac);
            bone->setRotation(newBoneRot);
        }
    }

}


FishSnapshot* Fish::createNewSnapshotFromCurrentState() {


    FishSnapshot* snapshot = new FishSnapshot();

    m_irrNode->updateAbsolutePosition();

    snapshot->setName(m_irrNode->getName());
    snapshot->setPos(m_irrNode->getPosition());
    snapshot->setRot(m_irrNode->getRotation());
    //    m_fishState->setScale(m_irrNode->getScale());

    for(uint i = 0; i < m_irrNode->getJointCount(); ++i)
    {
        IBoneSceneNode* joint = m_irrNode->getJointNode(i);
        snapshot->setSingleBoneRot(joint->getName(), joint->getRotation());
    }

    return snapshot;
}



} // end namespace fishsim
