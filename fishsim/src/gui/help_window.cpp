#include "gui/help_window.h"
#include "sim/fishsim_globals.h"

//#include <string>

using namespace irr;
using namespace core;
using namespace scene;
using namespace gui;

namespace fishsim
{

HelpWindow::HelpWindow(irr::IrrlichtDevice *device)
{
    m_device = device;
    m_device->grab();

    m_guiEnv = m_device->getGUIEnvironment();
    m_guiEnv->grab();

    createHelpWindow();
}


HelpWindow::~HelpWindow()
{
    if (m_device)
        m_device->drop();

    if(m_guiEnv)
        m_guiEnv->drop();

    if(m_helpWindow)
    {
        m_helpWindow->remove();
        m_helpWindow->drop();
    }
}

void HelpWindow::createHelpWindow(){

    const core::vector2di windowPos = core::vector2di(HELP_WINDOW_POS_X,
                                                      HELP_WINDOW_POS_Y);

    rect<s32> helpWindowRect(0,0, HELP_WINDOW_WIDTH, HELP_WINDOW_HEIGHT);
    helpWindowRect.UpperLeftCorner +=  windowPos;
    helpWindowRect.LowerRightCorner += windowPos;

    // Create help window
    m_helpWindow = m_guiEnv->addWindow(helpWindowRect, true,
                                       L"About FishSim", 0,
                                       GUI_ID_WIN_HELP);
    m_helpWindow->grab();

    // Add logo
    const io::path workDir = m_device->getFileSystem()->getWorkingDirectory();
    video::ITexture* logoTexture = m_device->getVideoDriver()->getTexture(workDir + "/media/fishsim_logo.png");
    m_guiEnv->addImage(logoTexture, vector2di(20,20),true, m_helpWindow);

    core::rect<s32> rect = core::rect<s32>((HELP_WINDOW_WIDTH / 2) - 50,
                                           130,
                                           (HELP_WINDOW_WIDTH / 2) + 50,
                                           160);

    std::wstring v = L"FishSim ";
    v = v + VERSION;
    IGUIStaticText* versionText = m_guiEnv->addStaticText(v.c_str(),rect,false,false,m_helpWindow);

    rect = core::rect<s32>(20,
                           160,
                           HELP_WINDOW_WIDTH - 20,
                           HELP_WINDOW_HEIGHT);

    std::wstring descr = L"Computer-animated 3D fish stimuli for research in animal behaivior.\n\n"
                          "For more informations and help visit: \n"
                         "bitbucket.org/EZLS/fish_animation_toolchain\n\n"
                         "Contact: virtual.fish.project@gmail.com\n\n"
                         "Developer team:\n"
                         "The Virtual Fish Project at University of Siegen (Germany)\n"
                         "Klaus Müller\n"
                         "Jan-Marco Hütwohl\n"
                         "Ievgen Smielik\n"
                         "Klaus-Dieter Kuhnert\n"
                         "Stefanie Gierszewski\n"
                         "Klaudia Witte\n\n\n"
                         "Acknowledgements:\n"
                         "We thank the DFG (Deutsche Forschungsgemeinschaft) for funding "
                         "the Virtual Fish Project. We thank Sanu Shrestha, "
                         "Maria Mastoras, Shumail B. Ahmad and "
                         "Derek Baker for their contributions to the Virtual Fish Project.\n\n"
                         "Icons made by http://www.freepik.com\n"
                         "Stickleback texture by Kókay Szabolcs.\n"
                         "Haplochromis texture by Oliver Selz";

    IGUIStaticText* description = m_guiEnv->addStaticText(descr.c_str(),rect,false,true,m_helpWindow);
    description->setTextAlignment(EGUIA_CENTER,EGUIA_CENTER);

    // Add sponsors logos
    // Add logo
    video::ITexture* sponsorsTexture = m_device->getVideoDriver()->getTexture(workDir + "/media/sponsors_logos.png");
    m_guiEnv->addImage(sponsorsTexture, vector2di(15,HELP_WINDOW_HEIGHT - 50),true, m_helpWindow);
}

} // end namespace fishsim
