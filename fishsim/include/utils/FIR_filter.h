/*! Copyright (C) 2017  	Department of Electrical Engineering & Computer Science, Institute of
 *			Real-Time Learning Systems, University of Siegen
 *
 *			Research Group of Ecology and Behavioral Biology, Institute of Biology,
 *			University of Siegen
 *
 * Contact: virtual.fish.project@gmail.com
 *
 * License: GNU General Public License  See LICENSE.txt for the full license.
 *
 * This file is part of the "FishSim Animation Toolchain".
 * "FishSim Animation Toolchain" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "FishSim Animation Toolchain" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "FishSim Animation Toolchain".  If not, see <http: *www.gnu.org/licenses/>.
 *
 * The "FishSim Animation Toolchain" was developed within the scope of the interdisciplinary,
 * DFG (Deutsche Forschungsgemeinschaft)-funded project “virtual fish” (KU
 * 689/11-1 and Wi 1531/12-1) of the Institute of Real-Time Learning Systems
 * (EZLS) and the Research Group of Ecology and Behavioral Biology at the
 * University of Siegen.
 *
 *  \file FIR_filter.h
 *  \brief Implementation of a simple finite impulse response filter using vectors and deque. Not
 *       very performant, but ok for small filter sizes.
 *
*/


#ifndef FIR_FILTER_H
#define FIR_FILTER_H

#include <vector>
#include <deque>


class FIRFilter
{


public:

    /*!
        Default Constructor
    */
    FIRFilter();

    /*!
       Custom Constructor

       \param[in] coefficients the coefficients of the filter as a vector, where
                  the first value in the vector coefficients[0] will be multiplied with
                  the most recent value, the second value coefficients[1] with the second 
                  recent value and so on. The size of the vector also tells the filter how
                  many values have to be stored.
     */
    FIRFilter(std::vector<float> coefficients);



    /*!
        Destructor
    */
    ~FIRFilter();

     /*!
        Getter for filter value

        \return the filtered value
    */
    float getFilterValue();


    /*!
       Pass a new input value to the filter.

       \param[in] inputVal the new input value
     */
    void addNewInputVal(float inputVal);

    /*!
       set new coefficients for the filter. The value history will be cleared, so that the filter has 
       to start over

       \param[in] coefficients the coefficients of the filter as a vector, where
                  the first value in the vector coefficients[0] will be multiplied with
                  the most recent value, the second value coefficients[1] with the second 
                  recent value and so on
     */
    void setCoefficients(std::vector<float> coefficients);

	
    /*!
       Clear the filter history. Filter starts over
     */
    void clear();


private:

    /// Coefficients of the filter
	std::vector<float> m_coefficients;

    /// Values of the filter over "time"
	std::deque<float> m_valueHistory;

    /// Validation flag for m_result
    bool m_updateFilter;

    /// The result of the filter. Only valid, if updateFilter is false. Used to avoid unnecessary calculations when
    /// no new value was passed to the filter.
    float m_result;

};


#endif // FIR_FILTER_H
