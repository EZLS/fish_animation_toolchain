#include <qt5/QtWidgets/QApplication>
#include "gui/mainwindow.h"

int main(int argc, char *argv[])
{
    ros::init( argc, argv, "FishSteering" );

    QApplication a(argc, argv);
    fishsteering::MainWindow w;

    w.show();

    return a.exec();
}

