#include "fish/rigid_fish/rigid_fish.h"

#include "sim/fishsim_globals.h"

#include "ros/joy_indices.h"

using namespace irr;
using namespace scene;
using namespace core;

// Gets multiplied with foreward input
#define FOREWARD_MULTIPLIER 0.8

// Gets multiplied with yaw input
#define YAW_MULTIPLIER 6.0

// Gets multiplied with pitch input
#define PITCH_MULTIPLIER 2.0

namespace fishsim
{

RigidFish::RigidFish(IAnimatedMeshSceneNode *node) : Fish(node)
{

    m_forewardMultiplier = FOREWARD_MULTIPLIER;
    m_yawMultiplier = YAW_MULTIPLIER;
    m_pitchMultiplier = PITCH_MULTIPLIER;
}

RigidFish::~RigidFish()
{
    unload();
}

bool RigidFish::unload()
{
    Fish::unload();
    return true;
}

bool RigidFish::load()
{

    /// 1. Mesh scene node settings like material flags, debug stuff etc..
    Fish::load();

    /// 2. Init editable body parts of the fish.
    initEditableMaterials();

    m_irrNode->setID(RIGID_FISH);

    return true;
}

void RigidFish::initEditableMaterials(){

    // Body should not be transparent
    m_irrNode->getMaterial(0).MaterialType = irr::video::EMT_SOLID;

    makeMaterialEditable(0, L"body", true);
}

void RigidFish::move(const sensor_msgs::Joy& joyState){

    float forewardAmount = joyState.axes.at(PS3_AXIS_STICK_LEFT_UPWARDS) * m_forewardMultiplier;
    float yawAmount = joyState.axes.at(PS3_AXIS_STICK_RIGHT_LEFTWARDS) * m_yawMultiplier;
    float pitchAmount = joyState.axes.at(PS3_AXIS_STICK_RIGHT_UPWARDS) * m_pitchMultiplier;

    move(forewardAmount, 0.0, 0.0, yawAmount, pitchAmount, 0.0);
}

void RigidFish::move(float forewardAmount, float sidewardAmount, float upwardAmount, float yawAmount, float pitchAmount, float rollAmount)
{

    forewardAmount = Fish::getForewardSmoothed(forewardAmount);
    Fish::move(forewardAmount, sidewardAmount,  upwardAmount,  yawAmount,  pitchAmount,  rollAmount);
}


} // end namespace fishsim
