#include "include/steeringrecorder.h"

using namespace fishsim;
using namespace boost::posix_time;

namespace fishsteering
{
SteeringRecorder::SteeringRecorder(FishSimSceneManager *fishSimSmgr)
{

    std::string pathToFishSteering = ros::package::getPath("fishsteering");
    bagFolder = pathToFishSteering + "/records/";

    m_fishSimSmgr = fishSimSmgr;
    m_roshandler = new SteeringROSHandler();
    m_animManager = new AnimationManager();
    m_joystickMapping = new JoystickMapping();

    recordingIsRunning = false;
    editingIsRunning = false;
    playingIsRunning = false;
    placingIsRunning = false;

    globalAnimationLength = 0;
    firstAnimation = 0;
    m_currentFish = 0;

    // Server runs with 50 hz
    serverUpdateRate = 50;

    updateState(ERS_IDLE);
    initScene();


}

SteeringRecorder::~SteeringRecorder()
{
    recordingIsRunning = false;
    editingIsRunning = false;
    playingIsRunning = false;
    placingIsRunning = false;

    delete m_roshandler;
    delete m_animManager;

    if(m_joystickMapping)
        delete m_joystickMapping;
}

void SteeringRecorder::initScene(){

    m_fishSimSmgr->loadTempScene();
    m_fishSimSmgr->updateTriangleWorld();
    m_fishSimSmgr->updateCollisionAnimators();

}

bool SteeringRecorder::isTaskRunning()
{
    if( placingIsRunning || recordingIsRunning ||
            playingIsRunning  || editingIsRunning)
        return true;
    else
        return false;
}

float SteeringRecorder::getWaitTime()
{
    return static_cast<float>( (1.0 / serverUpdateRate) * 1000000 );
}

void SteeringRecorder::joyMove(){

    sensor_msgs::Joy joyState = m_roshandler->getJoystickState();
    m_currentFish->move(m_joystickMapping->remap(joyState));
}

void SteeringRecorder::startPlacingModel(std::string modelName)
{

    if(isTaskRunning())
        return;

    if(!m_currentFish)
    {
        std::cerr << "No model selected" << std::endl;
        return;
    }

    updateState(ERS_PLACING_RUNNING);
    placingIsRunning = true;
    placingThread = boost::thread(&SteeringRecorder::placingModel, this, modelName);
}

void SteeringRecorder::stopPlacingModel()
{
    placingIsRunning = false;
    updateState(ERS_IDLE);
}

void SteeringRecorder::placingModel(std::string modelName)
{

    // Will freeze thread until joy input is available
    waitForJoyInput();

    // Tell GUI to update. Maybe something has changed in the time where
    // we have waited for joystick data (like user stoped placing).
    emit stateChanged();

    // Time to wait between two snapshots, in microseconds
    float waitTime = getWaitTime();

    while(placingIsRunning)
    {
        ptime start = microsec_clock::universal_time();

        joyMove();
        publishSnapshot();
        //ros::spinOnce();

        ptime end = microsec_clock::universal_time();
        time_duration dur = end - start;
        boost::this_thread::sleep( microseconds(waitTime - dur.total_microseconds() ));
    }
}

void SteeringRecorder::startRecording(std::string modelName)
{

    if(isTaskRunning())
        return;

    if(!m_currentFish)
    {
        std::cerr << "Could not find model" << std::endl;
        return;
    }

    updateState(ERS_RECORDING_RUNNING);
    recordingIsRunning = true;
    recordThread = boost::thread(&SteeringRecorder::recording, this, modelName);

}

void SteeringRecorder::stopRecording()
{
    recordingIsRunning = false;
    updateState(ERS_IDLE);
}

void SteeringRecorder::recording(std::string modelName)
{

    // Will freeze thread until joy input is available
    waitForJoyInput();

    // Tell GUI to update. Maybe something has changed in the time where
    // we have waited for joystick data (like user stoped placing).
    emit stateChanged();

    // Time to wait between two snapshots, in microseconds
    float waitTime = getWaitTime();

    FishAnimation *newAnim = m_animManager->createNewAnimation(modelName);
    animationCounter = 0;

    ros::Time keyframeTime = ros::Time::now();

    while(recordingIsRunning)
    {
        ptime start = microsec_clock::universal_time();
        if(firstAnimation)
        {
            // First animation sets the maximum animation time
            if(firstAnimation->getKeyframeCount() == animationCounter){
                recordingIsRunning = false;
                break;
            }

            // If an animation already exist, use this as a time reference.
            // All keyframes should have the same timestamp.
            keyframeTime = firstAnimation->getKeyframe(animationCounter)->getTime();
        }
        else
        {
            keyframeTime = ros::Time::now();
        }
        // Move the fish
        joyMove();

        // Save snapshot in animation
        FishSnapshot* keyframe = m_currentFish->createNewSnapshotFromCurrentState();
        keyframe->setTime(keyframeTime);
        newAnim->addKeyframe(keyframe);

        publishAll(m_animManager->getAllKeyframesForCounter(animationCounter));

        animationCounter++;

        // Tell gui to update
        if(!firstAnimation)
            emit bagCounterChanged(getAnimationLength());
        else
            emit bagCounterChanged(keyframeCounterToMSec(animationCounter));

        ptime end = microsec_clock::universal_time();
        time_duration dur = end - start;
        if(dur.total_microseconds() < waitTime)
            boost::this_thread::sleep(microseconds(waitTime - dur.total_microseconds() ));
    }

    // Make sure we really recorded something. If not, delete animation.
    if(animationCounter < 2)
    {
        m_animManager->removeAnimation(modelName);
        return;
    }

    // If recording was sucessfull, store pointer to this animation as a reference for the other ones
    if(!firstAnimation)
        firstAnimation = newAnim;

    globalAnimationLength = animationCounter;
    animationCounter = 0;

    emit bagFileReady();
    emit stateChanged();
}

void SteeringRecorder::startEditing(std::string modelName, int edtOpt)
{
    if(isTaskRunning())
        return;

    if(!m_currentFish)
    {
        std::cerr << "Could not find model" << std::endl;
        return;
    }

    updateState(ERS_EDITING_RUNNING);
    editingIsRunning = true;
    recordThread = boost::thread(&SteeringRecorder::editing, this, modelName, edtOpt );
}


void SteeringRecorder::stopEditing()
{
    editingIsRunning = false;
    updateState(ERS_IDLE);
}

void SteeringRecorder::editing(std::string modelName, int edtOpt){

    FishAnimation* animation = m_animManager->getAnimationForModel(modelName);

    if(!animation)
    {
       editingIsRunning = false;
       updateState(ERS_IDLE);
       return;
    }

    if(!firstAnimation || firstAnimation->getKeyframeCount() < 2)
    {
        editingIsRunning = false;
        updateState(ERS_IDLE);
        return;
    }

    int numKeyframes = animation->getKeyframeCount();

    // Avoid starting at keyframe 0, since a previous
    // keyframe is needed.
    if(animationCounter == 0) animationCounter = 1;

    waitForJoyInput();

    // Tell GUI to update. Maybe something has changed in the time where
    // we have waited for joystick data (like user stoped placing).
    updateState(m_state);

    for(; animationCounter < numKeyframes -1 && editingIsRunning; animationCounter++)
    {
        ptime start = microsec_clock::universal_time();

        emit bagCounterChanged(keyframeCounterToMSec(animationCounter));

        FishSnapshot* prevKeyframe = animation->getKeyframe(animationCounter -1);
        FishSnapshot* thisKeyframe = animation->getKeyframe(animationCounter);

        if(edtOpt == 0) // All
        {
            m_currentFish->setFishStateFromSnapshot(*prevKeyframe);
            joyMove();
            FishSnapshot* snap = m_currentFish->createNewSnapshotFromCurrentState();
            animation->overrideKeyframe(animationCounter, snap);
            thisKeyframe = animation->getKeyframe(animationCounter);
        }

        else // One of the bone modifiers
        {
            m_currentFish->setFishStateFromSnapshot(*prevKeyframe);
            BoneModifier* boneMod = m_currentFish->getBoneModifiers().at(edtOpt-1);
            boneMod->modify(prevKeyframe, thisKeyframe, m_joystickMapping->remap(m_roshandler->getJoystickState()));
        }

        thisKeyframe->setTime(firstAnimation->getKeyframe(animationCounter)->getTime());

        publishAll(m_animManager->getAllKeyframesForCounter(animationCounter));

        ros::spinOnce();

        // Timer for get the runtime
        ptime end = microsec_clock::universal_time();
        time_duration dur = end - start;

        // Sleep time to next keyframe
        if(animationCounter != numKeyframes -1)
        {
            ptime nextKey = animation->getKeyframe(animationCounter+1)->getTime().toBoost();
            ptime  thisKey = thisKeyframe->getTime().toBoost();

            time_duration timeToSleep = nextKey - thisKey;

            boost::this_thread::sleep(microseconds(timeToSleep.total_microseconds() - dur.total_microseconds()) ) ; //nano to micro sec divided by 1000
        }
    }

    editingIsRunning = false;
    updateState(ERS_IDLE);
}

int SteeringRecorder::getAnimationLength()
{

    // Currently no reference animation there, so use current animation
    // (No recording yet)
    if(globalAnimationLength == 0)
    {
        if(m_currentFish == 0)
            return 0;

        FishAnimation* currentAnimation = m_animManager->getAnimationForModel(m_currentFish->getName());
        if(currentAnimation == 0)
            return 0;

        int totalMsec = currentAnimation->getAnimationDuration().toNSec() / 100000;
        return totalMsec;
    }

    // A reference animation exist
    else
    {
        int totalMsec = firstAnimation->getAnimationDuration().toNSec() / 100000;
        return totalMsec;
    }
}

bool SteeringRecorder::loadBag(const std::string &filename)
{
    // Stop everything
    playingIsRunning = false;
    recordingIsRunning = false;
    editingIsRunning = false;
    placingIsRunning = false;

    playingThread.join();
    recordThread.join();
    editingThread.join();
    placingThread.join();

    // Delete all animations
    m_animManager->clear();
    bool success = m_animManager->loadFromBag(filename);

    if(success)
    {
        // Right now: all animations have same length. Just pick first one as time reference
        if(m_animManager->getAllAnimations().size() > 0)
        {
            firstAnimation = m_animManager->getAllAnimations().at(0);
            globalAnimationLength = firstAnimation->getKeyframeCount();
        }
    }
    else
    {
        firstAnimation = 0;
        globalAnimationLength = 0;
    }


    updateState(ERS_IDLE);
    emit bagFileReady();
    return success;
}

std::string SteeringRecorder::createBagFileName(){

    time_t now = time(0);
    tm *ltm = localtime(&now);
    std::string fileName = boost::lexical_cast<std::string>(ltm->tm_year + 1900) + "-"
            + boost::lexical_cast<std::string>(ltm->tm_mon + 1) + "-"
            + boost::lexical_cast<std::string>(ltm->tm_mday) + "_"
            + boost::lexical_cast<std::string>(ltm->tm_hour) + "-"
            + boost::lexical_cast<std::string>(ltm->tm_min) + "-"
            + boost::lexical_cast<std::string>(ltm->tm_sec) + ".bag";

    return fileName;
}

bool SteeringRecorder::saveBag(const std::string &filename)
{
    std::string savePath;
    if(filename == "")
    {
       savePath = bagFolder + createBagFileName();
    }
    else
    {
        savePath = filename + ".bag";
    }

    return m_animManager->saveAsBag(savePath);
}


void SteeringRecorder::setbagCounter(int msec)
{
    int numKeyframes = firstAnimation->getKeyframeCount();
    if(numKeyframes == 0)
        return;

    animationCounter =  ((float)msec / getAnimationLength()) * numKeyframes;

    if (animationCounter >= numKeyframes)
        animationCounter = numKeyframes - 1;

    if(!editingIsRunning && !recordingIsRunning)
    {
        publishAll(m_animManager->getAllKeyframesForCounter(animationCounter));
    }
}

int SteeringRecorder::keyframeCounterToMSec(int msgCounter)
{
    int numKeyframes = firstAnimation->getKeyframeCount();
    return ((float)msgCounter / numKeyframes) * getAnimationLength();
}

void SteeringRecorder::startPlaying()
{

    if(isTaskRunning())
        return;


    updateState(ERS_PLAYING_RUNNING);
    playingIsRunning = true;
    recordThread = boost::thread(&SteeringRecorder::play, this);
}

void SteeringRecorder::stopPlaying()
{
    playingIsRunning = false;
    updateState(ERS_IDLE);

}

void SteeringRecorder::play()
{
    if(firstAnimation == 0)
    {
        playingIsRunning = false;
        updateState(ERS_IDLE);
        return;
    }

    int numKeyframes = firstAnimation->getKeyframeCount();
    if(numKeyframes == 0)
    {
        playingIsRunning = false;
        updateState(ERS_IDLE);
        return;
    }


    for(; animationCounter < numKeyframes && playingIsRunning; animationCounter++)
    {
        emit bagCounterChanged(keyframeCounterToMSec(animationCounter));

        // Publish topic via ROS
        publishAll(m_animManager->getAllKeyframesForCounter(animationCounter));

        // Time between two messages
        if(animationCounter != numKeyframes -1)
        {

            float sleepTime = firstAnimation->getDurationBetweenKeyframes(animationCounter,
                                                                       animationCounter +1).toNSec() / 1000;
            usleep(sleepTime);
        }
    }

    playingIsRunning = false;
    updateState(ERS_IDLE);

}

void SteeringRecorder::publishSnapshot(){

    FishSnapshot* snap = m_currentFish->createNewSnapshotFromCurrentState();
    m_roshandler->publishSnapshot(snap);
    delete snap;
}

void SteeringRecorder::publishAll(std::vector<FishSnapshot*> snapshots){

    for(int i = 0; i < snapshots.size(); i++)
    {
        m_roshandler->publishSnapshot(snapshots[i]);
    }
}

void SteeringRecorder::waitForJoyInput()
{
    emit waitingForJoystick();

    // Wait for joy input
    while(!m_roshandler->isJoystickDataAvailable() && m_state != ERS_IDLE)
    {
        boost::this_thread::sleep(boost::posix_time::milliseconds(250));
    }

}

void SteeringRecorder::changeSettings(float forewardSpeed, float yawSpeed, float pitchSpeed)
{
    for (std::string fishName : m_fishSimSmgr->getCurrentFishModels())
    {
        Fish* fish = m_fishSimSmgr->getFishByName(fishName);
        fish->setForewardSpeed(forewardSpeed);
        fish->setYawSpeed(yawSpeed);
        fish->setPitchSpeed(pitchSpeed);
    }

}

std::vector<std::string> SteeringRecorder::getCurentEditingOptions(){

   std::vector<std::string> toReturn;

    toReturn.push_back("All");

    std::vector<BoneModifier*> boneModifiers = m_currentFish->getBoneModifiers();

    for(int i = 0; i < boneModifiers.size(); i++)
    {
        toReturn.push_back(boneModifiers.at(i)->getName());
    }
    return toReturn;
}

bool SteeringRecorder::changeCurrentModel(const std::string& modelName){

     if( isTaskRunning() )
     {
         std::cout << "Not able to change model if task is running." << std::endl;
         return false;
     }

     std::cout << " Change model to: " << modelName << std::endl;
     m_currentFish = m_fishSimSmgr->getFishByName(modelName);

     if(!m_currentFish)
     {
         std::cout << "Could not find model in scene." << std::endl;
         return false;
     }

     return true;
 }

 bool SteeringRecorder::modelAnimationExist(std::string modelName)
 {
     if(m_animManager->getAnimationForModel(modelName))
         return true;
     else
         return false;
 }

void SteeringRecorder::updateState(E_RECORDER_STATE newState){
    m_state = newState;
    emit stateChanged();
}
}
