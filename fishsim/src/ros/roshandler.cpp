#include "ros/roshandler.h"

#include "fish/fishsnapshot.h"

namespace fishsim
{

RosHandler::RosHandler(IrrHandler* irrhandler)
{
    m_nh = new ros::NodeHandle("~");

    // Subscriber
    m_fishSnapshotSub = m_nh->subscribe("fishSnapshot", 10, &RosHandler::parseSnapshot, this);
    m_dieSub = m_nh->subscribe("exit", 5, &RosHandler::die, this);
    realFishPosSub = m_nh->subscribe("/FishPosVec", 5, &RosHandler::fishPositionCallback, this);

    // Services
    m_modelSrv = m_nh->advertiseService("getModels", &RosHandler::getModelsSrv, this);
    m_loadSceneSrv = m_nh->advertiseService("loadScene", &RosHandler::loadSceneSrv, this);


    m_irrhandler = irrhandler;
    m_keepRunning = true;
}

RosHandler::~RosHandler()
{
    if(m_dyn_server != 0)
	{
        delete m_dyn_server;
    }

    if(m_nh != 0)
	{
        m_nh->shutdown();
        delete m_nh;
	}

}

void RosHandler::initDynReconfigure()
{
    // dynamic reconfigure
    m_dyn_server = new dynamic_reconfigure::Server<fishsim::FishSimConfig>(*(m_nh));
    m_dyn_server->setCallback(boost::bind(&RosHandler::dyncfgCallback, this, _1, _2));
}

void RosHandler::spinOnce()
{

	ros::spinOnce();
}

bool RosHandler::isRunning()
{
    return m_keepRunning;
}

void RosHandler::die()
{
    m_keepRunning = false;
}


void RosHandler::die(const std_msgs::EmptyConstPtr& empty)
{
	die();
}

void RosHandler::parseSnapshot(const fishsim::FishSnapshotStampedConstPtr &rosSnapshot){

    FishSnapshot irrSnapshot;

    irrSnapshot.fromRosSnapshot(rosSnapshot);

    m_irrhandler->addSnapshot(irrSnapshot, rosSnapshot->modelName);


}

void RosHandler::fishPositionCallback(const geometry_msgs::TwistConstPtr& fishPos){

    static irr::core::vector3df minPosition(-21.0, 3.0, -8.5);
    static irr::core::vector3df maxPosition(21.0, 30.0, 10.5);

    static irr::core::vector3df minRealPosition(0, 0, 0);
    static irr::core::vector3df maxRealPosition(1000, 400, 500);

    float fishX, fishY, fishZ;

    float rx = fishPos->linear.z / (abs(minRealPosition.Z) + abs(maxRealPosition.Z));
    fishX = minPosition.X + (1.0 - rx) * (maxPosition.X - minPosition.X);

    float r = fishPos->linear.y / (abs(minRealPosition.Y) + abs(maxRealPosition.Y));
    fishY = minPosition.Y + (1.0 - r) * (maxPosition.Y - minPosition.Y);


    fishZ = (fishPos->linear.x / 10.0) + maxPosition.Z ;

    m_irrhandler->setRealFishPosition(irr::core::vector3df(fishX, fishY, fishZ));



}

bool RosHandler::getModelsSrv(fishsim::getModelsRequest& req, fishsim::getModelsResponse& resp)
{
    resp.models = m_irrhandler->getFishModels();
	return true;
}

bool RosHandler::loadSceneSrv(fishsim::loadSceneRequest &req, fishsim::loadSceneResponse &resp)
{
    std::string path = req.scenePath;
    bool hideFishModels = req.hideFishModelsOnStart;

    bool loadSuccess = m_irrhandler->loadScene(path, hideFishModels);
    resp.result = loadSuccess;
    return loadSuccess;
}

void RosHandler::dyncfgCallback(fishsim::FishSimConfig& config, uint32_t level)
{
    if(level & 1)
    {
        m_irrhandler->resizeWindow(config.win_width, config.win_height);
    }

    if(level & 2)
    {
        m_irrhandler->moveWindow(config.win_x_pos, config.win_y_pos);
    }

    if(level & 4)
    {
        m_irrhandler->setTitle(config.win_title);
    }


}
} // end namespace fishsim
