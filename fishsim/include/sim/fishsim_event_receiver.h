/*! Copyright (C) 2017  	Department of Electrical Engineering & Computer Science, Institute of
 *			Real-Time Learning Systems, University of Siegen
 *
 *			Research Group of Ecology and Behavioral Biology, Institute of Biology,
 *			University of Siegen
 *
 * Contact: virtual.fish.project@gmail.com
 *
 * License: GNU General Public License  See LICENSE.txt for the full license.
 *
 * This file is part of the "FishSim Animation Toolchain".
 * "FishSim Animation Toolchain" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "FishSim Animation Toolchain" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "FishSim Animation Toolchain".  If not, see <http: *www.gnu.org/licenses/>.
 *
 * The "FishSim Animation Toolchain" was developed within the scope of the interdisciplinary,
 * DFG (Deutsche Forschungsgemeinschaft)-funded project “virtual fish” (KU
 * 689/11-1 and Wi 1531/12-1) of the Institute of Real-Time Learning Systems
 * (EZLS) and the Research Group of Ecology and Behavioral Biology at the
 * University of Siegen.
 *
 *  \file fishsim_event_receiver.h
 *  \brief Custom irrlicht event receiver.
 *
*/

#ifndef H_FISHSIM_EVENT_RECEIVER_H
#define H_FISHSIM_EVENT_RECEIVER_H

#include <irrlicht.h>

#include "gui/fishsim_GUI.h"
#include "scene/fishsim_scene_manager.h"


namespace fishsim
{
class FishSimEventReceiver : public irr::IEventReceiver
{

public:

    /*!
     * \brief Custom constructor.
     * \param device pointer to irrlicht device.
     * \param fishsimSmgr pointer to fishsim scene manager.
     * \param fishsimGUI pointer to fishsim GUI.
     */
    FishSimEventReceiver(irr::IrrlichtDevice* device,
                         FishSimSceneManager* fishsimSmgr,
                         FishSimGUI* fishsimGUI);

    /*!
     * \brief Destructor.
     */
    ~FishSimEventReceiver();

    /*!
     * \brief Callback for all events.
     * \param event the irrlicht event.
     * \return true, if event was handeld by fishsim. False if event was ignored.
     */
    bool OnEvent(const irr::SEvent& event);

    /*!
     * \brief Handle key events.
     * \param event the irrlicht event.
     * \return true, if key press was handled. False if key press was ignored.
     */
    bool onKeyEvents(const irr::SEvent& event);

    /*!
     * \brief Handle mouse events. Mainly used to select objects.
     * \param event the irrlicht event.
     * \return true, if an object was selected. False otherwise.
     */
    bool onMouseEvent(const irr::SEvent& event);

    /*!
     * \brief Handle GUI events.
     * \param event the irrlicht event.
     * \return true, if event was handled by fishsim. False if event was ignored.
     */
    bool onGUIEvents(const irr::SEvent& event);

    /*!
     * \brief handle menue events.
     * \param event the irrlicht event.
     * \return true, if event was handled by fishsim. False if event was ignored.
     */
    bool onMenuEvents(const irr::SEvent& event);

    /*!
     * \brief Handle button clicks.
     * \param event the irrlicht event.
     * \return true, if button click was handled by fishsim. False if event was ignored.
     */
    bool onButtonEvents(const irr::SEvent& event);

    /*!
     * \brief Handle file dialog events.
     * \param event the irrlicht event.
     * \return true, if event was handled by fishsim. False if event was ignored.
     */
    bool onFileDialogEvents(const irr::SEvent& event);

    /*!
     * \brief Handle close window events.
     * \param event the irrlicht event.
     * \return true, if event was handled by fishsim. False if event was ignored.
     */
    bool onCloseEvents(const irr::SEvent& event);


private:

    /// Pointer to irrlicht device.
    irr::IrrlichtDevice* m_device;

    /// Pointer to fishsim scene manager.
    FishSimSceneManager* m_fishSimSmgr;

    /// Pointer to fishsim main gui.
    FishSimGUI* m_gui;

    /// Last time the left mouse was pressed.
    irr::u32 m_lastTimeLeftMousePressed;
};


} // end namespace fishsim

#endif // H_FISHSIM_EVENT_RECEIVER_H
