#include "gui/light_editor.h"

#include "sim/fishsim_globals.h"
#include "sim/fishsim_helper_routines.h"

using namespace irr;
using namespace core;
using namespace gui;
using namespace video;

namespace fishsim
{

LightEditor::LightEditor(IrrlichtDevice* device, SFishSimLightEnv* lightEnv)
{

    m_device = device;
    m_guiEnv = m_device->getGUIEnvironment();

    m_device->grab();
    m_guiEnv->grab();

    m_lightEnv = lightEnv;

    m_qualityTab = 0;
    m_positionTab = 0;
    m_colorTab = 0;

    createSettingsWindow();

}

LightEditor::~LightEditor()
{

    if(m_device)
        m_device->drop();

    if(m_guiEnv)
        m_guiEnv->drop();

    if(m_colorTab)
        delete m_colorTab;

    if(m_positionTab)
        delete m_positionTab;

    if(m_qualityTab)
        delete m_qualityTab;

    if(m_winLightSettings)
    {
        m_winLightSettings->remove();
        m_winLightSettings->drop();
    }
}

void LightEditor::applyQualitySettings(){

    stringc text = m_edtBoxShadowRes->getText();
    u32 shadowRes = atof(text.c_str());

    text = m_edtBoxNearVal->getText();
    f32 nearVal = atof(text.c_str());

    text = m_edtBoxFarVal->getText();
    f32 farVal = atof(text.c_str());

    m_lightEnv->shadowLight->setShadowMapResolution(shadowRes);
    m_lightEnv->shadowLight->setNearValue(nearVal);
    m_lightEnv->shadowLight->setFarValue(farVal);

    updateQualityTab();

}

void LightEditor::applyPositionSettings(){

    /// Position
    vector3df newPos = makeVec3df(m_edtBoxLightPosX->getText(),
                                  m_edtBoxLightPosY->getText(),
                                  m_edtBoxLightPosZ->getText());
    m_lightEnv->shadowLight->setPosition(newPos);

    /// Target
    vector3df newTargetPos = makeVec3df(m_edtBoxTargetPosX->getText(),
                                        m_edtBoxTargetPosY->getText(),
                                        m_edtBoxTargetPosZ->getText());
    m_lightEnv->shadowLight->setTarget(newTargetPos);


    /// FOV
    stringc text = m_edtBoxFOV->getText();
    f32 fov = atof(text.c_str());
    m_lightEnv->shadowLight->setFOV(fov);

    updatePositionTab();
}

void LightEditor::applyColorSettings()
{
    u32 minCol = 0;
    u32 maxCol = 255;

    // Light color
    stringc text = m_edtBoxLightColR->getText();
    u32 lightColorR= atoi(text.c_str());
    lightColorR = core::clamp(lightColorR, minCol, maxCol);

    text = m_edtBoxLightColG->getText();
    u32 lightColorG= atoi(text.c_str());
    lightColorG = core::clamp(lightColorG, minCol, maxCol);

    text = m_edtBoxLightColB->getText();
    u32 lightColorB = atoi(text.c_str());
    lightColorB = core::clamp(lightColorB, minCol, maxCol);
    video::SColor newColor(1.0, lightColorR, lightColorG, lightColorB);

    m_lightEnv->shadowLight->setLightColor(newColor);

    // Ambient color
    text = m_etdBoxAmbientIntensity->getText();
    u32 ambientIntensity = atoi(text.c_str());
    ambientIntensity = core::clamp(ambientIntensity, minCol, maxCol);
    newColor = video::SColor(1.0, ambientIntensity, ambientIntensity, ambientIntensity);
    m_lightEnv->shadowManager->setAmbientColor(newColor);

    // Background color
    text = m_edtBoxBackColR->getText();
    u32 backColorR= atoi(text.c_str());
    backColorR = core::clamp(backColorR, minCol, maxCol);

    text = m_edtBoxBackColG->getText();
    u32 backColorG= atoi(text.c_str());
    backColorG = core::clamp(backColorG, minCol, maxCol);

    text = m_edtBoxBackColB->getText();
    u32 backColorB = atoi(text.c_str());
    backColorB = core::clamp(backColorB, minCol, maxCol);

    m_lightEnv->shadowManager->setClearColour(
                SColor(1.0, backColorR, backColorG, backColorB));

    updateColorTab();
}



void LightEditor::updatePositionTab(){

    // position
    vector3df pos = m_lightEnv->shadowLight->getPosition();

    m_edtBoxLightPosX->setText( stringw(pos.X).c_str() );
    m_edtBoxLightPosY->setText( stringw(pos.Y).c_str() );
    m_edtBoxLightPosZ->setText( stringw(pos.Z).c_str() );

    // Target
    vector3df target = m_lightEnv->shadowLight->getTarget();

    m_edtBoxTargetPosX->setText( stringw(target.X).c_str() );
    m_edtBoxTargetPosY->setText( stringw(target.Y).c_str() );
    m_edtBoxTargetPosZ->setText( stringw(target.Z).c_str() );

    // FOX
    f32 fov = m_lightEnv->shadowLight->getFOV();
    fov = irr::core::radToDeg(fov);
    m_edtBoxFOV->setText(stringw(fov).c_str());
}

void LightEditor::updateQualityTab(){

    u32 shadowRes = m_lightEnv->shadowLight->getShadowMapResolution();
    u32 nearVal = m_lightEnv->shadowLight->getNearValue();
    u32 farVal = m_lightEnv->shadowLight->getFarValue();

    m_edtBoxShadowRes->setText(stringw(shadowRes).c_str());
    m_edtBoxNearVal->setText(stringw(nearVal).c_str());
    m_edtBoxFarVal->setText(stringw(farVal).c_str());

}


void LightEditor::updateColorTab(){

    SColor color = m_lightEnv->shadowLight->getLightColor().toSColor();

    m_edtBoxLightColR->setText(stringw(color.getRed()).c_str());
    m_edtBoxLightColG->setText(stringw(color.getGreen()).c_str());
    m_edtBoxLightColB->setText(stringw(color.getBlue()).c_str());

    color = m_lightEnv->shadowManager->getAmbientColor();
    m_etdBoxAmbientIntensity->setText(stringw(color.getRed()).c_str());

    color = m_lightEnv->shadowManager->getClearColor();

    m_edtBoxBackColR->setText(stringw(color.getRed()).c_str());
    m_edtBoxBackColG->setText(stringw(color.getGreen()).c_str());
    m_edtBoxBackColB->setText(stringw(color.getBlue()).c_str());
}




void LightEditor::createSettingsWindow(){

    ///
    /// Create toolbox
    ///

    const core::vector2di windowSartPos = core::vector2di(TOOLBOX_START_X,
                                                          TOOLBOX_START_Y);

    rect<s32> toolBoxRect(0,0, TOOLBOX_WITDH, TOOLBOX_HEIGHT);
    toolBoxRect.UpperLeftCorner +=  windowSartPos;
    toolBoxRect.LowerRightCorner += windowSartPos;

    rect<s32> tabCtrlRect(2, 20,TOOLBOX_WITDH -2,TOOLBOX_HEIGHT -2);

    // Create toolbox
    m_winLightSettings = m_guiEnv->addWindow(toolBoxRect, false,
                                             L"Light settings", 0, GUI_ID_WIN_LIGHT_SETTINGS);
    m_winLightSettings->grab();

    // create tab control
    IGUITabControl* tabCtrl = m_guiEnv->addTabControl( tabCtrlRect, m_winLightSettings,
                                                       true, true);


    ///
    /// Create position tab
    ///
    IGUITab* tabPosition = tabCtrl->addTab(L"Position");
    m_positionTab = new FishSimToolboxTab(m_guiEnv, tabPosition, TOOLBOX_WITDH, TOOLBOX_HEIGHT);


    m_positionTab->appendStaticText(L"Light position:");

    m_edtBoxLightPosX = m_positionTab->appendEditBoxWithDescription(L"X:", L"1.0");
    m_edtBoxLightPosY = m_positionTab->appendEditBoxWithDescription(L"Y:", L"1.0");
    m_edtBoxLightPosZ = m_positionTab->appendEditBoxWithDescription(L"Z:", L"1.0");
    m_positionTab->appendSeparator();


    m_positionTab->appendStaticText(L"Light target:");

    m_edtBoxTargetPosX = m_positionTab->appendEditBoxWithDescription(L"X:", L"1.0");
    m_edtBoxTargetPosY = m_positionTab->appendEditBoxWithDescription(L"Y:", L"1.0");
    m_edtBoxTargetPosZ = m_positionTab->appendEditBoxWithDescription(L"Z:", L"1.0");
    m_positionTab->appendSeparator();

    m_positionTab->appendStaticText(L"FOV:");
    m_edtBoxFOV = m_positionTab->appendEditBox(L"1.0");
    m_positionTab->appendSeparator();

    m_positionTab->appendButton(L"Set", GUI_ID_BTN_SET_LIGHT_POSITION, L"Apply changes");

    updatePositionTab();

    ///
    /// Create color tab
    ///

    IGUITab* tabColor = tabCtrl->addTab(L"Color");
    m_colorTab = new FishSimToolboxTab(m_guiEnv, tabColor, TOOLBOX_WITDH, TOOLBOX_HEIGHT);

    m_colorTab->appendStaticText(L"Light Color");
    m_edtBoxLightColR = m_colorTab->appendEditBoxWithDescription(L"R:", L"255");
    m_edtBoxLightColG = m_colorTab->appendEditBoxWithDescription(L"G:", L"255");
    m_edtBoxLightColB = m_colorTab->appendEditBoxWithDescription(L"B:", L"255");
    m_colorTab->appendSeparator();

    m_colorTab->appendStaticText(L"Background Color");
    m_edtBoxBackColR = m_colorTab->appendEditBoxWithDescription(L"R:", L"255");
    m_edtBoxBackColG = m_colorTab->appendEditBoxWithDescription(L"G:", L"255");
    m_edtBoxBackColB = m_colorTab->appendEditBoxWithDescription(L"B:", L"255");
    m_colorTab->appendSeparator();

    m_colorTab->appendStaticText(L"Ambient Intensity");
    m_etdBoxAmbientIntensity = m_colorTab->appendEditBox(L"255");
    m_colorTab->appendSeparator();

    m_colorTab->appendButton(L"Set",GUI_ID_BTN_SET_LIGHT_COLOR, L"Apply changes");
    updateColorTab();

    ///
    /// Create quality tab
    ///
    IGUITab* tabQuality = tabCtrl->addTab(L"Quality");
    m_qualityTab= new FishSimToolboxTab(m_guiEnv, tabQuality, TOOLBOX_WITDH, TOOLBOX_HEIGHT);

    m_qualityTab->appendStaticText(L"Shadow resolution");
    m_edtBoxShadowRes = m_qualityTab->appendEditBoxWithDescription(L"", L"1024");
    m_qualityTab->appendSeparator();

    m_qualityTab->appendStaticText(L"Near value");
    m_edtBoxNearVal = m_qualityTab->appendEditBoxWithDescription(L"", L"1.0");
    m_qualityTab->appendSeparator();

    m_qualityTab->appendStaticText(L"Far value");
    m_edtBoxFarVal = m_qualityTab->appendEditBoxWithDescription(L"", L"1.0");
    m_qualityTab->appendSeparator();

    m_qualityTab->appendButton(L"Set",GUI_ID_BTN_SET_LIGHT_QUALITY, L"Apply changes");
    updateQualityTab();

}


} // end namespace fishsim
