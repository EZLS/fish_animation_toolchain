#include <signal.h>

#include <boost/lexical_cast.hpp>

#include <ros/rate.h>

#include "sim/irrhandler.h"
#include "ros/roshandler.h"

#include "ros/console.h"

#include "sim/fishsim_globals.h"

#include "irrlicht.h"

static fishsim::RosHandler* rosHandle = 0;
static void sighandler(int signal)
{
    if(!rosHandle)
	{
		return;
	}

    rosHandle->die();
}

int main(int argc, char* argv[])
{


    ros::init( argc, argv, "fishsim");

    fishsim::IrrHandler* irrhandler = new fishsim::IrrHandler();
    fishsim::RosHandler* roshandler = new fishsim::RosHandler(irrhandler);
    rosHandle = roshandler;

	bool fullscreen = false;
    int width = 1920;
    int height = 1080;

	if(argc == 2)
	{
		if(strcmp(argv[1], "-f") == 0)
		{
			fullscreen = true;
		}
	}
	else if(argc >= 3)
	{
		width = boost::lexical_cast<int>(argv[1]);
		height = boost::lexical_cast<int>(argv[2]);

		if(argc == 4)
		{
			if(strcmp(argv[3], "-f") == 0)
			{
				fullscreen = true;
			}
		}
	}

    if(!irrhandler->init(width, height, fullscreen))
	{
		return -1;
	}

    if(!irrhandler->load())
	{
		return -2;
    }


    roshandler->initDynReconfigure();

	signal(SIGINT, &sighandler);
	signal(SIGTERM, &sighandler);

    ros::Rate t(60);

    while(irrhandler->run() && roshandler->isRunning())
    {
        roshandler->spinOnce();
        irrhandler->exec();

        t.sleep();
    }

    irrhandler->drop();

    delete irrhandler;
    delete roshandler;

    rosHandle = 0;
    irrhandler = 0;

	return 0;
}
