#!/usr/bin/env python
# Copyright (C) 2017  	Department of Electrical Engineering & Computer Science, Institute of
#			Real-Time Learning Systems, University of Siegen
#
#			Research Group of Ecology and Behavioral Biology, Institute of Biology,
#			University of Siegen
#
# Contact: virtual.fish.project@gmail.com
#
# License: GNU General Public License  See LICENSE.txt for the full license.
#
# This file is part of the "FishSim Animation Toolchain".
# "FishSim Animation Toolchain" is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# "FishSim Animation Toolchain" is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with "FishSim Animation Toolchain".  If not, see <http: *www.gnu.org/licenses/>.
#
# The "FishSim Animation Toolchain" was developed within the scope of the interdisciplinary,
# DFG (Deutsche Forschungsgemeinschaft)-funded project "virtual fish" (KU
# 689/11-1 and Wi 1531/12-1) of the Institute of Real-Time Learning Systems
# (EZLS) and the Research Group of Ecology and Behavioral Biology at the
# University of Siegen.
#


import subprocess
import roslaunch
from PyQt5.QtWidgets import (QWidget, QLineEdit, QLabel, QComboBox, QHBoxLayout, QCheckBox)



class FishSimInstance(QWidget):
  
    def __init__(self, namespace, monitor, isActive, monitor_count, roslaunch):      
        super(FishSimInstance, self).__init__()

        self._roslaunch = roslaunch
        self._process = None

        # Horizontal box layout
        hbox = QHBoxLayout()
        hbox.setSpacing(25)
        self.setLayout(hbox)    

        self._checkbox_enabled = QCheckBox("")
        self._checkbox_enabled.setChecked(isActive)
        self._checkbox_enabled.stateChanged.connect(self._checkbox_enabled_changed)
        hbox.addWidget(self._checkbox_enabled)

        if (namespace == "player"):
            self._checkbox_enabled.setEnabled(False)
  
        # Namespace textbox
        self._namespace_edit = QLabel()# QLineEdit(self)
        self._namespace_edit.setText(namespace)
        hbox.addWidget(self._namespace_edit)
    
        # Monitor dropdown selection
        self._monitor_comboBox = QComboBox(self)
        for i in range(1, monitor_count +1):
            self._monitor_comboBox.addItem(str(i))
        self._monitor_comboBox.setCurrentIndex(monitor-1)
        hbox.addWidget(self._monitor_comboBox)
        
        # Update gui elements
        self._checkbox_enabled_changed(0)

    def __del__(self):
        self.stop_process()
    
 
    def get_namespace(self):
        return str(self._namespace_edit.text())

    def get_monitor(self):

        try:
            return int(self._monitor_comboBox.currentText())
        except ValueError:
            return 0 

    def launch(self):

        if self._namespace_edit.text() == "player":
            return
        
        if not self._checkbox_enabled.checkState():
            return            

        node =  roslaunch.core.Node('fishsim', 'FishSim', 'fishsim', str(self.get_namespace()))
        self._process = self._roslaunch.launch(node)

    def is_running(self):

        if self._namespace_edit.text() == "player":
            return True

        if self._process is None:
            return False
        else:
            # poll() will return None as long as process is running
            #return (self._process.poll() is None)
            return self._process.is_alive()

    def stop_process(self):
        if(self._process is not None):
            self._process.stop()

    
    def _checkbox_enabled_changed(self, int):
        
        enabled = self._checkbox_enabled.checkState()
        self._namespace_edit.setEnabled(enabled)
        self._monitor_comboBox.setEnabled(enabled)

    def is_active(self):
        if self._checkbox_enabled.checkState():
            return True
        else:
            return False
    
