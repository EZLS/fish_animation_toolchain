#include "scene/fishsim_scene_manager.h"

#include <boost/filesystem.hpp>

#include "sim/fishsim_globals.h"
#include "fish/poecilia/poecilia.h"
#include "fish/rigid_fish/rigid_fish.h"
#include "fish/gasterosteus/gasterosteus.h"
#include "fish/cynoscion/cynoscion.h"
#include "utils/fishsim_logger.h"

using namespace irr;
using namespace scene;
using namespace core;
using namespace video;


namespace fishsim
{

FishSimSceneManager::FishSimSceneManager(irr::scene::ISceneManager *sceneManager,
                                         const std::string pathToFishSim,
                                         EffectHandler* shadowManager){
    m_smgr = sceneManager;
    m_smgr->grab();

    m_shadowLevel = SHADOW_SETTING_SOFT;

    // Create the meta triangle selector which is used for collision detection
    m_selector = m_smgr->createMetaTriangleSelector();

    // Maya camera
    m_mayaCam = m_smgr->addCameraSceneNodeMaya(0, -500.0, 200.0, 250.0,-1 );
    m_mayaCam->setName("maya_camera");
    m_mayaCam->setID(MAYA_CAM);
    m_mayaCam->bindTargetAndRotation(true);

    // Static camera
    m_staticCam = m_smgr->addCameraSceneNode();
    m_staticCam->setName("static_camera");
    m_staticCam->setID(STATIC_CAM);


    m_smgr->setActiveCamera(m_staticCam);

    m_smgr->setAmbientLight(DEFAULT_AMBIENT_COLOR);

    m_sceneSettingsNode = m_smgr->addEmptySceneNode(0, SCENE_SETTINGS_NODE);
    m_sceneSettingsNode->setName("scene_settings_node");

    // Shadow manager. Not needed in fishsteering or fish_automover
    m_shadowMgr = shadowManager;
    if(m_shadowMgr != 0){
        initXEffect();
        setShadowLevel(SHADOW_SETTING_SOFT);
    }

    m_selectedNode = 0;


    // TODO
    // Add a emtpy scene node which is parent of all nodes (fish and env models)
    // It's used to save scene. Only scene elements that have this node as a
    // parent will be saved to .irr file
//    m_parentSaveNode = m_smgr->addEmptySceneNode();
//    m_parentSaveNode->addChild(m_staticCam);

    // Fix the transparency sorting problem. Without this attribute,
    // strange transparency problems would occure.
    m_smgr->getParameters()->setAttribute(ALLOW_ZWRITE_ON_TRANSPARENT, true);

    m_smgr->getRootSceneNode()->setName("root_scene_node");
    m_smgr->getRootSceneNode()->setID(ROOT_SCENE_NODE);

    m_fishSimPath = pathToFishSim;
    m_editMode = false;

}


FishSimSceneManager::~FishSimSceneManager()
{
    if(m_smgr)
        m_smgr->drop();

    if(m_shadowMgr)
    {
        m_shadowMgr->removeAllShadowLights();
    }
}


// XEffect setup for soft shadows
void FishSimSceneManager::initXEffect(){

    // Set ambient color
    m_shadowMgr->setAmbientColor(DEFAULT_AMBIENT_COLOR);

    // Shadow map dimension
    u32 shadowDimen = 1024;


    // Create a shadow light
    SShadowLight light = SShadowLight(shadowDimen,
                                      vector3df(0.0, 40.0, 0.0),
                                      vector3df(0.0, 0.0, 0.0),
                                      DEFAULT_LIGHT_COLOR,
                                      1.0f, 60.0f, 180.0 * irr::core::DEGTORAD,
                                      false);

    m_shadowMgr->addShadowLight(light);
    m_shadowLight = &m_shadowMgr->getShadowLight(0);

    // Background color
    m_shadowMgr->setClearColour(DEFAULT_BACKGROUND_COLOR);
}


IMeshSceneNode* FishSimSceneManager::getEnvNodeByName(const std::string name){

    for(auto it: m_envModels){

        if(it->getName() == name)
            return it;
    }

    return 0;

}


void FishSimSceneManager::focusSelected(){

    if(!m_selectedNode)
        return;
    irr::core::vector3df nodePos =
            m_selectedNode->getTransformedBoundingBox().getCenter();
    m_mayaCam->setTarget(nodePos);

}

void FishSimSceneManager::setStaticCamToCurrentView(){

    m_mayaCam->updateAbsolutePosition();

    m_staticCam->setPosition(m_mayaCam->getPosition());
    m_staticCam->setRotation(m_mayaCam->getRotation());
    m_staticCam->setFOV(m_mayaCam->getFOV());
    m_staticCam->setAspectRatio(m_mayaCam->getAspectRatio());
    m_staticCam->setUpVector(m_mayaCam->getUpVector());
    m_staticCam->setTarget(m_mayaCam->getTarget());
    m_staticCam->updateAbsolutePosition();

}

// Extract filename as modelname
bool FishSimSceneManager::loadEnvModel(const std::string &filePath)
{

    boost::filesystem::path fp(filePath);
    std::string modelName = fp.stem().string();

    return loadEnvModel(filePath, modelName);

}


// Load scene node and apply default settings
bool FishSimSceneManager::loadEnvModel(const std::string &path,
                                       const std::string &name){

    logInfo("Load new env model: " + path);
    // Create a unique name
    std::string uniqueName = makeUniqueName(name);

    /// Try to load model
    IMesh* mesh = m_smgr->getMesh(path.c_str());
    if(!mesh)
    {
        logError("Could not find mesh.");
        return false;
    }

    IMeshSceneNode* node = m_smgr->addMeshSceneNode(mesh);
    node->setName(uniqueName.c_str());


    /// Apply default settings and add node to mesh map

    // Default: no transparency for env models
    node->setMaterialType(irr::video::EMT_SOLID);

    // Default: Normalize face normals
    node->setMaterialFlag(irr::video::EMF_NORMALIZE_NORMALS, true);

    // Default: no lighting. XEffect will handle it
    node->setMaterialFlag(irr::video::EMF_LIGHTING, false);

    node->setID(ENVIRONMENT);

//    node->addShadowVolumeSceneNode();

    return addEnvNode(node);
}


// Add a existing node to scene
bool FishSimSceneManager::addEnvNode(IMeshSceneNode* node){

    if(node == 0)
        return false;

//    node->grab();

    // Add it to the mesh map
    m_envModels.push_back(node);

    recalculateBoundingBox();
    addShadowToNode(node, ESM_SOLID_NODE_RECEIVE);

//    m_parentSaveNode->addChild(node);

    return true;

}


// Remove node from scene by name
bool FishSimSceneManager::removeEnvNodeByName(const std::string &name){

    IMeshSceneNode* node = getEnvNodeByName(name);
    return removeEnvNode(node);


}


bool FishSimSceneManager::removeEnvNode(IMeshSceneNode* node){

    if(node == 0)
    {
        return false;
    }

    std::vector<IMeshSceneNode*>::iterator pos =
            std::find(m_envModels.begin(), m_envModels.end(), node);

    if (pos != m_envModels.end()){
        m_envModels.erase(pos);

        removeShadowFromNode(node);
        m_selector->removeTriangleSelector(node->getTriangleSelector());

//        node->drop();
        node->remove();
        recalculateBoundingBox();


//        m_parentSaveNode->removeChild(node);

        return true;
    }

    return false;
}


void FishSimSceneManager::removeAllEnvModels(){

    for(auto it: m_envModels)
    {
        IMeshSceneNode *node = it;

        m_selector->removeTriangleSelector(node->getTriangleSelector());
        removeShadowFromNode(node);
        node->remove();
    }

    m_envModels.clear();
    recalculateBoundingBox();
}


Fish* FishSimSceneManager::getFishByName(const std::string &name){

    for(auto it: m_fishModels){

        if(it->getName() == name)
            return it;
    }

    return 0;
}


bool FishSimSceneManager::loadFishModel(const io::path& filePath){


    io::path extension;
    core::getFileNameExtension(extension,filePath);

    if(extension == ".x")
    {
        io::path modelName = m_smgr->getFileSystem()->getFileBasename(filePath, false);
        return loadXFishModel(filePath.c_str(), modelName.c_str());
    }

    if(extension == ".fish")
    {
         return loadIrrFishModel(filePath);
    }

    std::cout << "Not a valid file extension." << std::endl;
    return false;
}


std::string FishSimSceneManager::makeUniqueName(const std::string &name){

    std::string unique = name;
    int suffix = 0;

    // As long as we can find a fish in scene, increase name suffix
    while(getFishByName(unique) != 0 || getEnvNodeByName(unique) != 0)
    {
        unique = name + std::to_string(suffix);
        suffix++;
    }

    return unique;
}


int FishSimSceneManager::getTypeOfModel(const std::string& modelName)
{

    if(modelName.length() < 3 )
        return RIGID_FISH;

    // Type of model is defined through last 3 characters
    std::string type = modelName.substr( modelName.length() - 3);

    if(type == "BOX")
        return RIGID_FISH;
    else if(type == "PLF")
        return POECILIA_LATIPINNA_F;
    else if(type ==  "PLM")
        return POECILIA_LATIPINNA_M;
    else if(type ==  "PMM")
        return POECILIA_MEXICANA_M;
    else if(type ==  "PMF")
        return POECILIA_MEXICANA_F;
    else if(type ==  "PRF")
        return POECILIA_RETICULATA_F;
    else if(type == "PRM")
        return POECILIA_RETICULATA_M;
    else if(type == "GAM")
        return GASTEROSTEUS_ACULEATUS_M;
    else if(type == "PPM")
        return PUNGITIUS_PUNGITIUS_M;
    else if(type == "GAF")
        return GASTEROSTEUS_ACULEATUS_F;
    else if(type == "MSM")
        return MARINE_STICKLEBACK;
    else if(type == "CNM")
        return CYNOSCION_NEBULOSUS_M;
    else
        return RIGID_FISH;
}

bool FishSimSceneManager::loadXFishModel(const std::string& path,const std::string& name)
{
    int type = getTypeOfModel(name);

    /// Name already in scene?
    std::string uniqueName = makeUniqueName(name);

    /// Load mesh
    IAnimatedMesh* mesh = m_smgr->getMesh(path.c_str());

    if(!mesh)
        return false;

    IAnimatedMeshSceneNode* node = m_smgr->addAnimatedMeshSceneNode(mesh);
    node->setName(uniqueName.c_str());
    node->setID(type);

    node->setPosition(vector3df(0.0, 5.0,0.0));

    return addFishNode(node, type);
}

bool FishSimSceneManager::loadIrrFishModel(const io::path& path){

    logInfo("Load new .irr fish model from: " + std::string(path.c_str()));

    m_smgr->loadScene(path,0);

    core::array<ISceneNode*> allNodes;
    m_smgr->getSceneNodesFromType(ESNT_ANY, allNodes);

    ISceneNode *node = allNodes.getLast();
    node->setName(makeUniqueName(node->getName()).c_str());
    node->setDebugDataVisible(EDS_OFF);

    IAnimatedMeshSceneNode* fishNode = (IAnimatedMeshSceneNode*) node;

    logInfo("Found fish node: " + std::string(node->getName()));
    if(!addFishNode(fishNode, node->getID()))
    {
        logError("Error: Could not find load fish. Will remove node.");
        fishNode->remove();
        return false;
    }
    return true;

}

bool FishSimSceneManager::addFishNode(IAnimatedMeshSceneNode* node, int fishType)
{

    if(node == 0)
        return false;

    if(node->getMesh() == 0)
    {
        logError("Could not find mesh of " + std::string(node->getName()));
        return false;
    }

    Fish* newFish = 0;

    if(fishType == POECILIA_LATIPINNA_M
            || fishType == POECILIA_MEXICANA_M
            || fishType == POECILIA_RETICULATA_M)
    {
        Poecilia* sfm = new Poecilia(node, true);
        newFish = sfm;
    }

    else if(fishType == POECILIA_LATIPINNA_F
            || fishType == POECILIA_MEXICANA_F
            || fishType == POECILIA_RETICULATA_F)
    {
        Poecilia* sfm = new Poecilia(node, false);
        newFish = sfm;
    }

    else if(fishType == RIGID_FISH)
    {
        RigidFish* rigid = new RigidFish(node);
        newFish = rigid;
    }
    else if(fishType == GASTEROSTEUS_ACULEATUS_M
            || fishType == GASTEROSTEUS_ACULEATUS_F
            || fishType == PUNGITIUS_PUNGITIUS_M)
    {
        Gasterosteus* gam = new Gasterosteus(node);
        newFish = gam;
    }
    else if(fishType == MARINE_STICKLEBACK)
    {
        Gasterosteus* marineGAM = new Gasterosteus(node);
        IBoneSceneNode* spineLeft = node->getJointNode("GAM_pelvic_spine_left");
        IBoneSceneNode* spineRight = node->getJointNode("GAM_pelvic_spine_right");

        if(spineLeft && spineRight)
        {
            float scale = 2.0;
            spineLeft->setScale(irr::core::vector3df(1.5,scale,1.5));
            spineRight->setScale(irr::core::vector3df(1.5,scale,1.5));
        }
        newFish =marineGAM;
    }
    else if(fishType == CYNOSCION_NEBULOSUS_M)
    {
        Cynoscion* cnm = new Cynoscion(node);
        newFish = cnm;
    }
    else
    {
        logError("Error: Not a fish node. ID unknown: " + node->getID());
        return false;
    }

    if(!newFish)
        return false;

    // Load fish
    if(! newFish->load())
    {
        logError("Error loading fish.");
        return false;
    }

    // Add triangle selector to be able to select it by mouse click
    newFish->createTriangleSelector(m_smgr);

    newFish->createCollisionResponseAnimator(m_smgr, m_selector);

    // Add a shadow to the node
    addShadowToNode(node,ESM_TRANSPARENT_NODE_BOTH);

    // Add new fish to fish vector
    m_fishModels.push_back(newFish);

    logInfo("New fish added: " + std::string(newFish->getName()));

    return true;
}

bool FishSimSceneManager::removeFishByName(const std::string& name){


    Fish* fish = getFishByName(name);

    if(fish == 0)
        return false;

    std::vector<Fish*>::iterator pos =
            std::find(m_fishModels.begin(), m_fishModels.end(), fish);

    if (pos != m_fishModels.end()){

        IAnimatedMeshSceneNode *node = (*pos)->getAnimatedMeshSceneNodePtr();
        removeShadowFromNode(node);

//        m_parentSaveNode->removeChild(node);
        (*pos)->unload();
        delete *pos;
        m_fishModels.erase(pos);

        return true;
    }
    else
        return false;

}



void FishSimSceneManager::removeAllFishModels()
{

    for(auto it: m_fishModels){

        IAnimatedMeshSceneNode *node = it->getAnimatedMeshSceneNodePtr();
        removeShadowFromNode(node);
//        m_parentSaveNode->removeChild(node);
//        m_selector->removeTriangleSelector(node->getTriangleSelector());
        it->unload();
    }
    m_fishModels.clear();

}

bool FishSimSceneManager::loadTempScene(){

    return loadScene(m_fishSimPath + TEMP_SCENE_PATH, false);
}

void FishSimSceneManager::saveTempScene(){

    saveScene(m_fishSimPath + TEMP_SCENE_PATH);
}

void FishSimSceneManager::saveScene(const std::string& filepath){

    logInfo("Save scene to " + filepath);
    m_smgr->getFileSystem()->changeWorkingDirectoryTo(m_fishSimPath.c_str());

    io::IWriteFile* file = m_smgr->getFileSystem()->createAndWriteFile(filepath.c_str());

    if (file)
    {
        io::IXMLWriter* writer = m_smgr->getFileSystem()->createXMLWriter(file);

        if(writer)
        {
            // Workaround to store shadow lights, until we got a custom xml writer/reader:
            // store the informations of the shadow light in an irrlicht light, add it to scene,
            // save file and remove that light afterwards.
            ILightSceneNode* tempShadowLight = 0;

            if(m_shadowMgr)
            {
                tempShadowLight = m_shadowMgr->getLightParamsAsIrrLight(0);
                tempShadowLight->setName("shadow_light");
                tempShadowLight->setID(SHADOW_LIGHT);

                // Another workaround: save background color of scene as specular color
                // save ambient color of scene as ambient color of light.
                video::SLight lightData = tempShadowLight->getLightData();

                lightData.AmbientColor = m_shadowMgr->getAmbientColor();
                lightData.SpecularColor = m_shadowMgr->getClearColor();
                tempShadowLight->setLightData(lightData);
            }

            // m_fishSimPath is the path which is used for relative file names.
            // all filenames in the resulting save file will be relative to
            // m_fishSimPath. Before loading, its neccessary to change the
            // Working directory of irrlicht to m_fishSimPath. Then all files will
            // be found corretly.
            m_smgr->saveScene(writer, m_fishSimPath.c_str());

            if(tempShadowLight)
                tempShadowLight->remove();

            writer->drop();
        }
        else
        {
            logError("Failed to save scene.");
        }

        file->drop();
    }

    logInfo("Save scene success.");

}

void FishSimSceneManager::printNodes()
{

    core::array<ISceneNode*> allNodes;
    m_smgr->getSceneNodesFromType(ESNT_ANY, allNodes);
    logInfo( "Printing " +std::to_string(allNodes.size()) + " nodes." );



    for(int i = 0; i < allNodes.size(); i++){

        ISceneNode *node = allNodes[i];
        logInfo("---" + std::string(node->getName()));
    }


}


void FishSimSceneManager::clearScene(){

    removeAllEnvModels();
    removeAllFishModels();

    if(m_staticCam){
        m_staticCam->remove();
        m_staticCam =0;
    }

    if(m_mayaCam){
        m_mayaCam->remove();
        m_mayaCam = 0;
    }

    m_selectedNode = 0;

    if(m_shadowMgr)
        m_shadowMgr->removeAllShadowLights();
//        m_shadowMgr->removeShadowLight(0); TODO: does not delete it.




}

bool FishSimSceneManager::loadScene(const std::string& filepath, bool hideFishModels)
{
    m_smgr->getFileSystem()->changeWorkingDirectoryTo(m_fishSimPath.c_str());
    irr::core::LOCALE_DECIMAL_POINTS = ".,";

    /// Clear scene
    clearScene();
    m_smgr->clear();


    /// Let scene manager do its job
    logInfo("Irrlicht scene manager starts loading scene " + filepath);
    if(m_smgr->loadScene(filepath.c_str()))
    {
        /// init this scene manager
        logInfo("Success. Now init FishSim scene manager");

        core::array<ISceneNode*> allNodes;
        m_smgr->getSceneNodesFromType(ESNT_ANY, allNodes);

        for(int i = 0; i < allNodes.size(); i++){

            ISceneNode *node = allNodes[i];

            node->setDebugDataVisible(EDS_OFF);

            switch(node->getID())
            {

            /// Root
            case ROOT_SCENE_NODE:
            {
                logInfo("Found root scene node");
                break;
            }

            case SCENE_SETTINGS_NODE:
            {
                logInfo("Found scene settings node");
                m_sceneSettingsNode = node;
                break;
            }


            /// Static env model
            case ENVIRONMENT:
            {
                IMeshSceneNode* envNode = (IMeshSceneNode*) node;

                logInfo("Found env node: " + std::string(node->getName()));

                if(envNode->getMesh())
                    addEnvNode(envNode);
                else
                {

                    logError("Could not find mesh of " + std::string(node->getName())
                             + ". Will delete node.");
                    envNode->remove();
                }

                break;
            }


            /// Fish nodes
            case POECILIA_LATIPINNA_M:
            case POECILIA_LATIPINNA_F:
            case POECILIA_MEXICANA_M:
            case POECILIA_MEXICANA_F:
            case POECILIA_RETICULATA_F:
            case POECILIA_RETICULATA_M:
            case RIGID_FISH:
            case GASTEROSTEUS_ACULEATUS_M:
            case CYNOSCION_NEBULOSUS_M:
            case PUNGITIUS_PUNGITIUS_M:
            {
                IAnimatedMeshSceneNode* fishNode = (IAnimatedMeshSceneNode*) node;
                logInfo("Found fish node: " + std::string(node->getName()));
                if(addFishNode(fishNode, node->getID()))
                {
                    if(hideFishModels)
                        fishNode->setPosition(vector3df(0.0,9000.0,0.0));
                }
                else
                {
                    fishNode->remove();
                }
                break;


            }

            /// Maya camera
            case MAYA_CAM:
            {
                logInfo("Found maya cam: " + std::string(node->getName()) );
                ICameraSceneNode* mayaCam = (ICameraSceneNode*) node;

                m_mayaCam = m_smgr->addCameraSceneNodeMaya(0, -500.0, 200.0, 250.0,-1 );
                m_mayaCam->setName("maya_camera");
                m_mayaCam->setID(MAYA_CAM);
                m_mayaCam->bindTargetAndRotation(true);
                m_mayaCam->setPosition(mayaCam->getPosition());
                m_mayaCam->setTarget(vector3df(0.0,0.0,0.0));
                m_mayaCam->setAspectRatio(mayaCam->getAspectRatio());
                m_mayaCam->setFOV(mayaCam->getFOV());
                m_mayaCam->setUpVector(mayaCam->getUpVector());
                m_mayaCam->setTarget(mayaCam->getTarget());

                if(m_editMode){
                    m_smgr->setActiveCamera(m_mayaCam);
                }
                mayaCam->remove();
                break;
            }


            /// Static camera
            case STATIC_CAM:
            {
                logInfo("Found static cam: " + std::string(node->getName()) );
                m_staticCam = (ICameraSceneNode* ) node;

                if(!m_editMode){
                    m_smgr->setActiveCamera(m_staticCam);
                }
                break;
            }


            /// XEffect light scene node
            case SHADOW_LIGHT:
            {
                ILightSceneNode* lightNode = (ILightSceneNode*) node;

                if(m_shadowMgr)
                {
                    logInfo("Found shadow light: " + std::string(node->getName()));

                    // add a shadow light from light node
                    m_shadowMgr->addShadowLightFromLightSceneNode(
                                (ILightSceneNode*) node);

                    m_shadowLight = &m_shadowMgr->getShadowLight(0);

                    //Set Background and ambient color
                    if(m_shadowMgr)
                    {
                        m_shadowMgr->setClearColour( lightNode->getLightData().SpecularColor.toSColor());
                        m_shadowMgr->setAmbientColor(lightNode->getLightData().AmbientColor.toSColor());
                    }

                }
                // Then delete it. We don't need it anymore.
                lightNode->remove();

                break;
            }
            }
        }

        // Change shadow settings
        setShadowLevel(m_sceneSettingsNode->getAutomaticCulling());

        updateTriangleWorld();
        logInfo("Loading scene success.");

        return true;
    }
    else
    {
        logError("Irrlicht scene manager failed loading scene.");
    }



    return false;
}

void FishSimSceneManager::recalculateBoundingBox(){

    m_boundingBox.reset(0,0,0);

    for(auto it: m_envModels){

        m_boundingBox.addInternalBox(it->getTransformedBoundingBox());
    }

//    std::cout << "Boundingbox:" << m_boundingBox.MaxEdge.X << "/" << m_boundingBox.MaxEdge.Y << "/" << m_boundingBox.MaxEdge.Z << std::endl;
//    std::cout << "Boundingbox:" << m_boundingBox.MinEdge.X << "/" << m_boundingBox.MinEdge.Y << "/" << m_boundingBox.MinEdge.Z << std::endl;
//    std::cout << "Boundingbox center:" << m_boundingBox.getCenter().X << "/" << m_boundingBox.getCenter().Y << "/" << m_boundingBox.getCenter().Z << std::endl;

}

std::vector<std::string>  FishSimSceneManager::getCurrentFishModels(){

    std::vector<std::string> allModels;
    allModels.reserve(m_fishModels.size());

    for(auto fish : m_fishModels)
    {
        allModels.push_back(fish->getName());
    }
    return allModels;

}

void FishSimSceneManager::updateCollisionAnimators(){

    for(auto fish : m_fishModels){
        fish->setNewCollisionWorld(m_selector);
    }
}

void FishSimSceneManager::updateTriangleWorld(){

    // Remove old triangle selector
    m_selector->removeAllTriangleSelectors();

    // Create new one
    for(auto it = m_envModels.begin(); it != m_envModels.end(); it++)
    {

        IMeshSceneNode *node = *it;

        // Add triangle selector for collsion detection
        scene::ITriangleSelector *selector =
                m_smgr->createOctreeTriangleSelector(node->getMesh(),node);
        node->setTriangleSelector(selector);

        // Add selector to meta triangle selector
        m_selector->addTriangleSelector(selector);
        selector->drop();
    }
}

bool FishSimSceneManager::deleteSelectedNode(){

    if(m_selectedNode == 0)
        return false;

    logInfo("Will delete node: " + std::string(m_selectedNode->getName()));

    bool result = false;
    if(m_selectedNode->getID() == ENVIRONMENT)
        result = removeEnvNodeByName(m_selectedNode->getName());
    else
        result = removeFishByName(m_selectedNode->getName());

    m_selectedNode = 0;

    return result;
}

void FishSimSceneManager::deselect()
{

    if(m_selectedNode)
    {
        m_selectedNode->setDebugDataVisible(EDS_OFF);
    }

    m_selectedNode = 0;
}

void FishSimSceneManager::selectNodeAtScreenCoordinates(int screenX, int screenY){

    if(m_selectedNode != 0){
        m_selectedNode->setDebugDataVisible(EDS_OFF);
    }


    scene::ISceneCollisionManager* cmgr =  m_smgr->getSceneCollisionManager();

    core::line3df ray = cmgr->getRayFromScreenCoordinates(
                irr::core::vector2di(screenX, screenY));

    // Tracks the current intersection point with a mesh
    core::vector3df intersection;

    // Used to show which triangle has been hit
    core::triangle3df hitTriangle;


    m_selectedNode = cmgr->getSceneNodeAndCollisionPointFromRay(
                ray, intersection, hitTriangle );

    // Nothing selected
    if(!m_selectedNode)
        return;

    // Check if normal of triangle we hit points away from screen.
    // In this case we hit a triangle we cannot see (backface culling)
    int dir = hitTriangle.getNormal().dotProduct(ray.getVector());
    if(dir > 0)
    {
        ray.start = intersection;

        // If so, do another test, but this time start from
        // the triangle we hit.
        m_selectedNode = cmgr->getSceneNodeAndCollisionPointFromRay(
                    ray, intersection, hitTriangle );
    }

    if(!m_selectedNode)
        return;

    m_selectedNode->setDebugDataVisible(EDS_MESH_WIRE_OVERLAY);
//    IAnimatedMesh* arrowMesh = m_smgr->addArrowMesh("X-Axis", 0xFFFFFFFF,0xFFFFFFFF,4,8, 1.0, 20);
//    IAnimatedMeshSceneNode* arrowNode = m_smgr->addAnimatedMeshSceneNode (arrowMesh);

//    arrowNode->setPosition(m_selectedNode->getPosition());


}

void FishSimSceneManager::disableSoftShadows(){

    if(!m_shadowMgr)
        return;

    for(auto it: m_envModels){

        ISceneNode *node = it;
        m_shadowMgr->removeShadowFromNode(node);
    }

    for(auto it: m_fishModels){
        IAnimatedMeshSceneNode *node = it->getAnimatedMeshSceneNodePtr();
        m_shadowMgr->removeShadowFromNode(node);
    }


}


void FishSimSceneManager::removeShadowFromNode(ISceneNode* node)
{

    if(m_shadowMgr)
        m_shadowMgr->removeShadowFromNode(node);
}

void FishSimSceneManager::addShadowToNode(ISceneNode* node, E_SHADOW_MODE mode)
{
    if(m_shadowMgr)
    {
        // Add env nodes the shadow list
        // (It is recommended to only use EFT_NONE when VSM filtering is enabled.)
        E_FILTER_TYPE filterType = EFT_16PCF;
        m_shadowMgr->addShadowToNode(node, filterType, mode);
    }
}


void FishSimSceneManager::enableSoftShadows(){

    for(auto it: m_envModels){
        ISceneNode *node = it;
        addShadowToNode(node, ESM_SOLID_NODE_RECEIVE);
    }

    for(auto it: m_fishModels){
        IAnimatedMeshSceneNode *node = it->getAnimatedMeshSceneNodePtr();
        addShadowToNode(node, ESM_TRANSPARENT_NODE_BOTH);
    }

}


void FishSimSceneManager::setLightingOnNodes(bool doLighting){

    //Env models
    for(auto it: m_envModels){
        ISceneNode *node = it;
        for(u32 i = 0;i < node->getMaterialCount();++i)
            node->getMaterial(i).Lighting = doLighting;
    }

    // Fish models
    for(auto it: m_fishModels){
        IAnimatedMeshSceneNode *node = it->getAnimatedMeshSceneNodePtr();
        for(u32 i = 0;i < node->getMaterialCount();++i)
            node->getMaterial(i).Lighting = doLighting;
    }

}


void FishSimSceneManager::setShadowLevel(int level)
{

    if(level == SHADOW_SETTING_NONE)
    {
        disableSoftShadows();
    }

    if(level == SHADOW_SETTING_SOFT)
    {
        enableSoftShadows();
    }

    m_shadowLevel = level;
    m_sceneSettingsNode->setAutomaticCulling(level);
}

void FishSimSceneManager::enableMayaCamera(bool enableMayaCam){

    if(enableMayaCam){

        m_editMode = true;
        m_smgr->setActiveCamera(m_mayaCam);
    }
    else{
        m_editMode = false;
        m_smgr->setActiveCamera(m_staticCam);
    }

}

void FishSimSceneManager::drawAll(){

    /// Update all fish nodes depending on current client time
    for(auto it: m_fishModels)
    {
        Fish* fish = it;

        fish->getAnimatedMeshSceneNodePtr()->removeAnimators();
        fish->updateState(ros::Time::now() - ros::Duration(0.1));// 100 ms back in time
//        fish->move(0.0,0.0,0.0,0.0,0.0,0.0);
    }

    /// Draw
    if(m_shadowMgr)
        m_shadowMgr->update();
    else
        m_smgr->drawAll();
}

} // end namespace fishsim
