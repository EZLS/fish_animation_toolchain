#!/bin/bash
#
# Script will install all software components that are necessary for using the fishsim animation toolchain.
#

####################
# Global variables #
####################

INSTALL_DIR=~/fishsim_animation_toolchain
CATKIN_WS_DIR=${INSTALL_DIR}/ros_ws
GIT_HOME=https://bitbucket.org/EZLS/fish_animation_toolchain

#####################
# Helper functions  #
#####################

# Will try to do apt-get update.
# When administration directory is locked, it will retry every 5 seconds
function safe_apt_update {
apt-get update
while [ "$?" != "0" ]; do
  sleep 5
  apt-get update
done
}

# Install software but will wait if administration directory is locked
function safe_install {
    yes | aptdcon --hide-terminal --install $1
}

##########################
# Check and preparation. #
##########################

# Are you really sure?
echo -e "\nThis Script will install the FishSim Animation Toolchain."
echo -e "Continue [Y/n]?"
read RESPONSE
echo -e "\n"
case "$RESPONSE" in
    n*|N*)
    echo "Install Terminated"
    exit 0;
esac

# Make sure this script runs on the right ubuntu version (16.04 xenail)
UBUNTU_VERSION=$(lsb_release -sc)
if [ $UBUNTU_VERSION != "xenial" ]; then
    echo "Found wrong version of Ubuntu. Please make sure to run this script only on Ubuntu 16.04. On other systems please use the manual installation."
    exit -1;
fi

# Make sure that you have root permissions
if [ $EUID -ne 0 ]; then
    echo "Error: Please run this script using sudo."
    exit -1
fi

# Check if installation directory exists
if [ -d "$INSTALL_DIR" ]; then
  echo "Error: installation directory ${INSTALL_DIR} exists. Please remove it before installation."
  exit -1
fi

sudo add-apt-repository universe
sudo add-apt-repository multiverse

####################
# Remove old files #
####################

sed -i "/^export FISHSIM_HOME=/d" ~/.bashrc
sed -i "/^export FISHSIM_CATKIN_WS=/d" ~/.bashrc
unset FISHSIM_HOME
unset FISHSIM_CATKIN_WS

################
# Install ROS. #
################

# Set up sources
sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'

# Set up keys
sudo apt-key adv --keyserver hkp://ha.pool.sks-keyservers.net:80 --recv-key 421C365BD9FF1F717815A3895523BAEEB01FA116

# Install ros
safe_apt_update
safe_install ros-kinetic-desktop-full

# install joystick drivers
safe_install ros-kinetic-joystick-drivers ros-kinetic-joy

source /opt/ros/kinetic/setup.bash

#create fishsim workspace
mkdir -p ${CATKIN_WS_DIR}/src
cd ${CATKIN_WS_DIR}

catkin_make

# Add workspace to .bashrc
#echo "source ${CATKIN_WS_DIR}/devel/setup.bash" >> ~/.bashrc
echo "export FISHSIM_HOME=${INSTALL_DIR}" >> ~/.bashrc
echo "export FISHSIM_CATKIN_WS=${CATKIN_WS_DIR}" >> ~/.bashrc


###############################
# Install irrlicht and bullet #
###############################

safe_apt_update
safe_install libirrlicht-dev libirrlicht1.8 libbullet-dev libbullet-extras-dev libbulletcollision2.83.6 libbulletdynamics2.83.6

##############################
# Build the fishsim project. #
##############################

# Install git
safe_install git

# Clone repo
cd ${CATKIN_WS_DIR}/src
git clone ${GIT_HOME}

cd ..
rospack profile

# Build toolchain
catkin_make

# And install
catkin_make install

# Add icons to launcher
gsettings set com.canonical.Unity.Launcher favorites "$(gsettings get com.canonical.Unity.Launcher favorites | sed "s/]/,'fishsim.desktop']/")"
gsettings set com.canonical.Unity.Launcher favorites "$(gsettings get com.canonical.Unity.Launcher favorites | sed "s/]/,'fishsteering.desktop']/")"
gsettings set com.canonical.Unity.Launcher favorites "$(gsettings get com.canonical.Unity.Launcher favorites | sed "s/]/,'fishplayer.desktop']/")"

# Set owner
#sudo chown $SUDO_USER /home/${SUDO_USER}/.local/share/applications/fishsim_icon.png
#sudo chown $SUDO_USER /home/${SUDO_USER}/.local/share/applications/fishsteering_icon.png
#sudo chown $SUDO_USER /home/${SUDO_USER}/.local/share/applications/fishplayer_icon.png

sudo update-desktop-database

# Create links
cd ${INSTALL_DIR}
ln -s ${CATKIN_WS_DIR}/src/fish_animation_toolchain/fishsim/models/ models
ln -s ${CATKIN_WS_DIR}/src/fish_animation_toolchain/fishsim/envModel/ envModel
ln -s ${CATKIN_WS_DIR}/src/fish_animation_toolchain/fishsim/scenes/ scenes

wget https://bitbucket.org/EZLS/fish_animation_toolchain/downloads/update.sh
chmod +x update.sh

# Set read and write permissions for your user account
sudo chown -R $SUDO_USER  $INSTALL_DIR
sudo chgrp -R $SUDO_USER  $INSTALL_DIR
sudo chmod -R g+w $INSTALL_DIR


