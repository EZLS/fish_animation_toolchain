#include "playlist_widget.h"
#include "include/utils.h"

namespace fishplayer
{
PlaylistWidget::PlaylistWidget(QWidget *parent, std::string rosNamespace)
    :QWidget(parent), m_ui(new Ui::PlaylistWidget())
{
    m_ui->setupUi(this);

    m_listWidget = m_ui->listWidgetPlaylist;

    m_roshandler = new RosInterface(rosNamespace);
    m_playlist = new Playlist(this, m_roshandler);
    m_playlistDuration = 0.0;
    m_playTime = 0.0;
    connectSignalsAndSlots();

    m_playTimer = new QTimer(this);
    connect(m_playTimer, SIGNAL(timeout()), this, SLOT(updatePlaytime()));

m_ui->groupBoxGlobal->setTitle(QString::fromStdString(rosNamespace));
}

PlaylistWidget::~PlaylistWidget()
{
    delete m_ui;
}

void PlaylistWidget::connectSignalsAndSlots()
{
    // GUI Elements to PlaylistWidget
    connect(m_ui->buttonChangeScene, SIGNAL(clicked()), this, SLOT(changeCurrentScene()));
    connect(m_ui->buttonAddLoadScene, SIGNAL(clicked()), this, SLOT(addLoadScene()));
    connect(m_ui->buttonAddPause, SIGNAL(clicked()), this, SLOT(addPause()));
    connect(m_ui->buttonPlayBag, SIGNAL(clicked()), this, SLOT(addPlayBag()));
    connect(m_ui->buttonPlayAuto, SIGNAL(clicked()), this, SLOT(addPlayAuto()));
    connect(m_ui->buttonSave, SIGNAL(clicked()), this, SLOT(save()));
    connect(m_ui->buttonLoad, SIGNAL(clicked()), this, SLOT(load()));
    connect(m_ui->buttonDelete,SIGNAL(clicked()), this, SLOT(deleteEntry()) );

    // Playlist to PlaylistWidget
    connect(m_playlist, SIGNAL(playbackFinished()), this, SLOT(playbackFinished()));
    connect(m_playlist, SIGNAL(startNewClip(int)), this, SLOT(startNewClip(int)));
}

void PlaylistWidget::changeCurrentScene(){

    if(m_playlist->isPlaying())
        return;

    QString scenePath = QFileDialog::getOpenFileName(0, "Choose scene file.", 0,
                                                     "*.scene");
    if( scenePath != "")
    {
        if( !m_roshandler->loadFishScene(scenePath.toStdString()))
            showErrorBox("Error: Could not load scene.");
    }
}

void PlaylistWidget::addLoadScene()
{

    if(m_playlist->isPlaying())
        return;

    QString scenePath = QFileDialog::getOpenFileName(0, "Choose scene file.", 0,
                                                     "*.scene");
    if( scenePath != "")
    {
        //m_playlist->appendLoadScene(scenePath.toStdString());

        LoadClip* clip = new LoadClip(m_listWidget, scenePath.toStdString());
        addClipToList(clip);
    }
}

void PlaylistWidget::addPause()
{

    if(m_playlist->isPlaying())
        return;

    PauseClip *clip = new PauseClip(m_listWidget, 60.0);
    addClipToList(clip);
}

void PlaylistWidget::addPlayAuto()
{
#ifdef USE_AUTOMOVER
    if(m_playlist->isPlaying())
        return;

    AutoClip *clip = new AutoClip(m_listWidget, 120.0);
    addClipToList(clip);
#endif
}

void PlaylistWidget::addPlayBag()
{

    if(m_playlist->isPlaying())
        return;

    QString bagPath = QFileDialog::getOpenFileName(0, "Choose bag file.", 0,
                                                     "*.bag");
    if( bagPath != "")
    {
        BagClip * clip = new BagClip(m_listWidget, bagPath.toStdString());
        addClipToList(clip);
    }
}


void PlaylistWidget::play()
{
    if(m_playlist->isPlaying())
        return;

    m_playTime = 0.0;
    m_playlist->play(getClips());
    m_playTimer->start(1000);

    m_listWidget->setEnabled(false);
}

void PlaylistWidget::stop()
{
    m_playlist->stop();
}

bool PlaylistWidget::checkPlaylist()
{
    if(m_playlist->isPlaying())
        return true;

    bool playlistValid = true;
    for(int i = 0; i < m_listWidget->count(); i++)
    {
        // Cast the items in the listWidget to custom widget type
        Clip *c = qobject_cast<Clip*>(m_listWidget->itemWidget(m_listWidget->item(i)));
        playlistValid = playlistValid && c->verify();
    }

    return playlistValid;

}

void PlaylistWidget::save()
{

    if(m_playlist->isPlaying())
        return;

    QString filePath = QFileDialog::getSaveFileName(0, "Choose file to save.", 0,
                                                     "*.yaml");
    if( filePath != "")
    {
        if(!m_playlist->savePlaylist(getClips(), filePath.toStdString()))
        {
            showErrorBox("Error: Saving playlist failed.");
        }
    }

}

void PlaylistWidget::load()
{
    if(m_playlist->isPlaying())
        return;

    QString filePath = QFileDialog::getOpenFileName(0, "Choose file to load.", 0,
                                                     "*.yaml");
    if( filePath != "")
    {
        std::vector<Clip*> clips;
        m_listWidget->clear();

        if(m_playlist->loadPlaylist(filePath.toStdString(), clips))
        {
            for(int i = 0; i < clips.size(); i++)
            {
                clips[i]->setParent(m_listWidget);
                addClipToList(clips[i]);
            }
        }
        else
        {
            showErrorBox("Error: Loading playlist failed.");
        }

        updateUI();
    }
}

void PlaylistWidget::addClipToList(Clip *widget)
{
    QListWidgetItem *item = new QListWidgetItem();
    item->setSizeHint(widget->sizeHint());
    m_listWidget->addItem(item);
    m_listWidget->setItemWidget(item, widget);

    connect(widget, SIGNAL(clipHasChanged()), this, SLOT(updateUI()));
    updateUI();
}


void PlaylistWidget::updateUI()
{
    // Update time label
    m_playlistDuration = 0.0;
    for(int i = 0; i < m_listWidget->count(); i++)
    {
        // Cast the items in the listWidget to custom widget type
        Clip *c = qobject_cast<Clip*>(m_listWidget->itemWidget(m_listWidget->item(i)));
        m_playlistDuration += c->getDuration();
    }
    updateTimeLabel();
}

void PlaylistWidget::deleteEntry()
{

    if(m_playlist->isPlaying())
        return;

    if( m_listWidget->count() == 0)
        return;

    m_listWidget->takeItem(m_listWidget->currentIndex().row());

    updateUI();
}

std::vector<Clip*> PlaylistWidget::getClips()
{
    std::vector<Clip*> clips;
    for(int i = 0; i < m_listWidget->count(); i++)
    {
        // Cast the items in the listWidget to custom widget type
        Clip *c = qobject_cast<Clip*>(m_listWidget->itemWidget(m_listWidget->item(i)));
        clips.push_back(c);
    }
    return clips;
}

void PlaylistWidget::showErrorBox(std::string message)
{
    QMessageBox msgBox;
    msgBox.setText(QString::fromStdString(message));
    msgBox.exec();
}

void PlaylistWidget::updateTimeLabel()
{
   QString timeString = generateTimeString(m_playTime)
           + " / "
           + generateTimeString(m_playlistDuration);
    m_ui->labelTime->setText(timeString);
}


void PlaylistWidget::updatePlaytime()
 {
    m_playTime += 1.0;
    updateTimeLabel();
}


void PlaylistWidget::playbackFinished()
{
    m_playTimer->stop();

    m_playTime = 0.0;
    updateTimeLabel();

    m_listWidget->setEnabled(true);
    // Tell main window that we are done.
    emit(donePlaying());
}

void PlaylistWidget::startNewClip(int index)
{
    m_listWidget->setCurrentRow(index);
}

} // end namespace fishplayer
