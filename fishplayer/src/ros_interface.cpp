#include "include/ros_interface.h"

namespace fishplayer
{
RosInterface::RosInterface(std::string ROSNamespace)
{
    m_namespace = ROSNamespace;

    m_spinner = new ros::AsyncSpinner(2);
    m_spinner->start();

    m_snapshotPublisher =
            m_nodeHandle.advertise<fishsim::FishSnapshotStamped>("/" + m_namespace +
                                                                 "/fishsim/fishSnapshot", 2);
    m_loadSceneFishSimClient =
             m_nodeHandle.serviceClient<fishsim::loadScene>("/" + m_namespace +
                                                            "/fishsim/loadScene");
#ifdef USE_AUTOMOVER

    m_enableAutoMoverClient =
             m_nodeHandle.serviceClient<fish_automover::enableAutoMover>("/" +  m_namespace +
                                                                         "/fish_automover/enableAutoMover");
    m_loadSceneAutomoverClient =
             m_nodeHandle.serviceClient<fishsim::loadScene>("/" + m_namespace +
                                                           "/fish_automover/loadScene");
#endif
}

RosInterface::~RosInterface()
{
    m_spinner->stop();

}

bool RosInterface::callLoadFishSimScene(const std::string& scenePath, bool hideModels){

    return callLoadSceneSrv(scenePath,m_loadSceneFishSimClient,hideModels);
}

bool RosInterface::loadFishScene(const std::string& scenePath){

    bool success = true;

    // FishSim
    success = success && callLoadSceneSrv(scenePath, m_loadSceneFishSimClient, false);

    // Automover
#ifdef USE_AUTOMOVER
    success = success && callLoadSceneSrv(scenePath, m_loadSceneAutomoverClient, false);
#endif

    return success;
}
#ifdef USE_AUTOMOVER
bool RosInterface::callLoadAutomoverScene(const std::string& scenePath){
    return callLoadSceneSrv(scenePath,m_loadSceneAutomoverClient,false);
}


bool RosInterface::callEnableAutoMoverSrv(bool autoMove, bool doMimic, 
                                          bool doDorsalFinMove, bool doGonopodiumMove)
{


    fish_automover::enableAutoMover srv;

    srv.request.enableAutoMove = autoMove;
    srv.request.enableMimic = doMimic;
    srv.request.enableDorsalFin = doDorsalFinMove;
    srv.request.enableGonopodium = doGonopodiumMove;

    if (m_enableAutoMoverClient.call(srv))
    {
      return srv.response.result;
    }
    else
    {
      ROS_ERROR("Failed to call service enableAutoMover");
      return false;
    }
}
#endif

bool RosInterface::callLoadSceneSrv(const std::string& scenePath,
                                    ros::ServiceClient server,
                                    bool hideFishModelsOnStart){

    fishsim::loadScene srv;

    std::cout << scenePath << std::endl;

    std::cout << "/" + m_namespace +
                 "/fishsim/loadScene" << std::endl;

    srv.request.scenePath = scenePath;
    srv.request.hideFishModelsOnStart = hideFishModelsOnStart;
    if(server.call(srv))
    {
        return srv.response.result;
    }
    else
    {
        ROS_ERROR("Failed to call service loadFishScene");
        return false;
    }
}


void RosInterface::publishAll(std::vector<fishsim::FishSnapshot*> snapshots){

    for(int i = 0; i < snapshots.size(); i++)
    {
        // stamp message
        fishsim::FishSnapshotStamped rosSnapshot;
        rosSnapshot = snapshots.at(i)->toRosSnapshot();
        rosSnapshot.timestamp = ros::Time::now();

        //publish topic
        m_snapshotPublisher.publish(rosSnapshot);
    }

}
}
