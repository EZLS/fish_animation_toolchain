#include "gui/fish_editor.h"

#include "sim/fishsim_globals.h"
#include "sim/fishsim_helper_routines.h"

using namespace irr;
using namespace core;
using namespace scene;
using namespace video;
using namespace gui;

namespace fishsim
{

FishEditor::FishEditor(IrrlichtDevice *device, Fish *fish, const std::string& fishSimPath){


    m_device = device;
    m_guiEnv = m_device->getGUIEnvironment();
    m_fish = fish;

    m_device->grab();
    m_guiEnv->grab();

    m_configTab = 0;
    m_textureTab = 0;
    m_textureDialog = 0;
    m_edtBoxScaleX = 0;
    m_edtBoxScaleY = 0;
    m_edtBoxScaleZ = 0;
    m_edtBoxName = 0;
    m_winToolbox = 0;
    m_checkBoxShowMesh = 0;

    m_fishSimPath = fishSimPath;

    std::string notFoundTexture = m_fishSimPath + "/media/textureNotFound.png";
    m_notFoundTexture = m_device->getVideoDriver()->getTexture(notFoundTexture.c_str());

    createEditToolbox();
}

FishEditor::~FishEditor(){

    if(m_textureDialog)
    {
        m_textureDialog->remove();
        m_textureDialog->drop();
    }

    if(m_winToolbox)
    {
        m_winToolbox->remove();
        m_winToolbox->drop();
    }

    if(m_device)
        m_device->drop();

    if(m_guiEnv)
        m_guiEnv->drop();

    if(m_configTab)
        delete m_configTab;

    if(m_textureTab)
        delete m_textureTab;


    m_textureButtons.clear();
}
       

void FishEditor::applyConfigTabChanges(){


    irr::core::vector3df newPos = makeVec3df(m_edtBoxPosX->getText(),
                                               m_edtBoxPosY->getText(),
                                               m_edtBoxPosZ->getText());

    irr::core::vector3df newScale = makeVec3df(m_edtBoxScaleX->getText(),
                                               m_edtBoxScaleY->getText(),
                                               m_edtBoxScaleZ->getText());


    // TODO: conversion madness really neccessary?
    core::stringc cString = core::stringc(m_edtBoxName->getText());
    std::string stdString = cString.c_str();

    m_fish->setScale(newScale);
    m_fish->setPosition(newPos);
    m_fish->setName(stdString);

    if(m_checkBoxShowMesh->isChecked())
        m_fish->setDebugDataVisible(EDS_MESH_WIRE_OVERLAY);
    else
        m_fish->setDebugDataVisible(EDS_OFF);

    updateConfigTab();

}


void FishEditor::updateConfigTab(){

    // Position
    irr::core::vector3df pos = m_fish->getPosition();

    m_edtBoxPosX->setText(core::stringw(pos.X).c_str());
    m_edtBoxPosY->setText(core::stringw(pos.Y).c_str());
    m_edtBoxPosZ->setText(core::stringw(pos.Z).c_str());

    // Scale
    irr::core::vector3df scale = m_fish->getScale();

    m_edtBoxScaleX->setText(core::stringw(scale.X).c_str());
    m_edtBoxScaleY->setText(core::stringw(scale.Y).c_str());
    m_edtBoxScaleZ->setText(core::stringw(scale.Z).c_str());

    m_edtBoxName->setText(core::stringw(m_fish->getName().c_str()).c_str());


    // Show mesh
    if(m_fish->isDebugDataVisible() == EDS_MESH_WIRE_OVERLAY)
        m_checkBoxShowMesh->setChecked(true);
    else
        m_checkBoxShowMesh->setChecked(false);

}


void FishEditor::openTextureDialog(const core::stringc& materialIndex)
{

    core::rect<s32> position(1000,200,1400,900);

    if(m_textureDialog){
        position =  m_textureDialog->getRelativePosition();
        m_textureDialog->remove();
        m_textureDialog->drop();
    }

    m_selectedMaterialIndex = std::stoi(materialIndex.c_str());
    std::wstring matName = m_fish->getNamedMaterials().at(m_selectedMaterialIndex)->name;
    std::wstring text = L"Choose a texture for: ";

    m_textureDialog = new CGUITextureOpenDialog(text.append(matName).c_str(), m_guiEnv, m_guiEnv->getRootGUIElement(),
                                                      m_device->getTimer(), FILE_DIALOG_CHOOSE_TEXTURE, true);
    m_textureDialog->setName(materialIndex);
//    m_textureDialog->setMinSize(irr::core::dimension2du(400,700));
    m_textureDialog->setRelativePosition(position);
    m_textureDialog->grab();

}


void FishEditor::changeTextureToSelected()
{

    video::ITexture* texture = m_textureDialog->getSelectedTexture();

    SNamedMaterial* namedMat = m_fish->getNamedMaterials().at(m_selectedMaterialIndex);
    video::SMaterial *mat = namedMat->material;

    // Update button and material
    if(mat && texture)
    {
        IGUIButton* button = m_textureButtons.at(m_selectedMaterialIndex);
        button->setImage(texture);
        button->setToolTipText( core::stringw(texture->getName()).c_str() );
        button->setPressedImage(texture);

        mat->setTexture(0,texture);

    }
}


void FishEditor::saveFish(std::string filePath)
{


    // Copy node, set its position and rotation to default
    // and save it
    IAnimatedMeshSceneNode* copy = (IAnimatedMeshSceneNode* )
            m_fish->getAnimatedMeshSceneNodePtr()->clone();

    ISceneManager* smgr = m_device->getSceneManager();
    smgr->getFileSystem()->changeWorkingDirectoryTo(m_fishSimPath.c_str());

    copy->setPosition(irr::core::vector3df(0.0,10.0,0.0));
    copy->setRotation(irr::core::vector3df(0.0,0.0,0.0));
    copy->setDebugDataVisible(EDS_OFF);

    io::IWriteFile* file = smgr->getFileSystem()->createAndWriteFile(filePath.c_str());

    if (file)
    {
        io::IXMLWriter* writer = smgr->getFileSystem()->createXMLWriter(file);

        if(writer)
        {
            // m_fishSimPath is the path which is used for relative file names.
            // all filenames in the resulting save file will be relative to
            // m_fishSimPath. Before loading, its neccessary to change the
            // Working directory of irrlicht to m_fishSimPath. Then all files will
            // be found corretly.
            smgr->saveScene(writer,m_fishSimPath.c_str(),0,  copy);
            writer->drop();
        }
        file->drop();
    }
    copy->remove();
}


void FishEditor::createEditToolbox(){

    const int toolboxWitdh = TOOLBOX_WITDH;
    const int toolboxHeight = TOOLBOX_HEIGHT;

    const core::vector2di toolboxStartPos = core::vector2di(200,200);

    rect<s32> toolboxRect(0,0, toolboxWitdh, toolboxHeight);
    toolboxRect.UpperLeftCorner +=  toolboxStartPos;
    toolboxRect.LowerRightCorner += toolboxStartPos;

    m_winToolbox = m_guiEnv->addWindow(toolboxRect, false, L"Edit Toolbox", 0, GUI_ID_WIN_FISH_SETTINGS);
    m_winToolbox->grab();

    // create tab control
    IGUITabControl* tab = m_guiEnv->addTabControl(
                core::rect<s32>(2, 20,toolboxWitdh -2,toolboxHeight -7), m_winToolbox, true, true);

    ///
    ///  config tab
    ///
    IGUITab* tabConfig = tab->addTab(L"Config");

    m_configTab = new FishSimToolboxTab(m_guiEnv,tabConfig, toolboxWitdh, toolboxHeight);

    m_configTab->appendStaticText(L"Name:");
    m_edtBoxName = m_configTab->appendEditBox(L"Name");
    m_configTab->appendSeparator();

    m_configTab->appendStaticText(L"Position");
    m_edtBoxPosX = m_configTab->appendEditBoxWithDescription(L"X:", L"1.0");
    m_edtBoxPosY = m_configTab->appendEditBoxWithDescription(L"Y:", L"1.0");
    m_edtBoxPosZ = m_configTab->appendEditBoxWithDescription(L"Z:", L"1.0");
    m_configTab->appendSeparator();

    m_configTab->appendStaticText(L"Scale");
    m_edtBoxScaleX = m_configTab->appendEditBoxWithDescription(L"X:", L"1.0");
    m_edtBoxScaleY = m_configTab->appendEditBoxWithDescription(L"Y:", L"1.0");
    m_edtBoxScaleZ = m_configTab->appendEditBoxWithDescription(L"Z:", L"1.0");
    m_configTab->appendSeparator();

    m_checkBoxShowMesh = m_configTab->appendCheckBox(true, L"Show mesh", GUI_ID_CBOX_SHOW_MESH);

    m_configTab->appendButton(L"Apply", GUI_ID_BTN_SET_SCALE, L"Apply changes");

//    m_configTab->appendSeparator();

    /* For testing: A scrollbar that can change the length of the pelvic spines */
    if(m_fish->getAnimatedMeshSceneNodePtr()->getID() == GASTEROSTEUS_ACULEATUS_M){
        m_configTab->appendStaticText(L"Pelvic spine length");
        m_configTab->appendHorizontalScrollbar(GUI_ID_PELVIC_SPINE_SCROLLBAR);
    }

    m_configTab->appendSeparator();
    m_configTab->appendStaticText(L"Save fish to disk");
    m_configTab->appendButton(L"Save", GUI_ID_BTN_SAVE_FISH);


    updateConfigTab();


    ///
    /// texture tab
    ///
    ///

    IGUITab* tabTexture = tab->addTab(L"Textures");
    m_textureTab = new FishSimToolboxTab(m_guiEnv, tabTexture, toolboxWitdh, toolboxHeight);

    std::vector<SNamedMaterial*> editableMaterials = m_fish->getNamedMaterials();

    for(int i = 0; i < editableMaterials.size(); i++){

        SNamedMaterial* namedMat = editableMaterials.at(i);
        ITexture* diffuseTexture = namedMat->material->getTexture(0);

        if(namedMat->hasDiffuseTexture)
        {
            // Matieral should have a texture, but it was not found.
            // Then assing the "not found" texture
            if(!diffuseTexture)
            {
                namedMat->material->setTexture(0, m_notFoundTexture);
                diffuseTexture = m_notFoundTexture;
            }
            IGUIButton* btn = m_textureTab->appendTextureButton(L"", diffuseTexture,
                                                                   namedMat->name.c_str(),
                                                                   GUI_ID_BTN_CHANGE_TEXTURE);
            // To identify it later in callback
            btn->setName(core::stringc(i));

            m_textureButtons.push_back(btn);

        }
//        else
//        {
            /// Maybe put color picker here
//        }

    }


//    m_textureTab->appendHorizontalScrollbar(GUI_ID_SHAPE);

}


void FishEditor::shapeMe(int pos)
{
    IBoneSceneNode* spineLeft = m_fish->getAnimatedMeshSceneNodePtr()->getJointNode("pelvic_spine_left");
    IBoneSceneNode* spineRight = m_fish->getAnimatedMeshSceneNodePtr()->getJointNode("pelvic_spine_right");

    if(!spineLeft || ! spineRight)
        return;

    float scale = pos / 100.0 + 1.0;

    spineLeft->setScale(irr::core::vector3df(1.0,scale,1.0));
    spineRight->setScale(irr::core::vector3df(1.0,scale,1.0));

//    IBoneSceneNode* bb = m_fish->getAnimatedMeshSceneNodePtr()->getJointNode("backbone_2");

//    bb->setScale(irr::core::vector3df(scale,1.0,1.0));

}


} // end namespace fishsim
