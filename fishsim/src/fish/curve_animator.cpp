#include "fish/curve_animator.h"

using namespace cv;

#define CURVE_LENGTH 200
//#define SIN_ALPHA 1.1

namespace fishsim
{
CurveAnimator::CurveAnimator(){


    m_previousSpeed = 0;
    m_sinusPhase = 0;
    m_frameCounter = 0;
    m_amplitude = 0.5;
    m_speedMultiplier = 0.7;
    m_doDebugDrawing = false;

//    if(m_doDebugDrawing)
//    {
//       // Create a window for display
//        namedWindow( "color", WINDOW_AUTOSIZE );
//    }

}

CurveAnimator::~CurveAnimator(){
    m_bones.clear();
}

bool CurveAnimator::appendBone(irr::scene::IBoneSceneNode *bone, SBoneExtension::Axis axis, float boneLength){


    if(bone == 0 || boneLength == 0)
        return false;

    SBoneExtension b;

    b.length = boneLength;
    b.axis = axis;
    b.irrBonePtr = bone;
    b.rotation = 0.0;
    b.defaultRotation = bone->getRotation();

    m_bones.push_back(b);

    return true;
}



void CurveAnimator::animateBones(float forewardAmount){


    forewardAmount *= m_speedMultiplier;
    // Reset to 0
    if (fabs(forewardAmount) == 0.0)
    {
        resetBones();
    }

    // Do curve animation
    else{
        m_frameCounter++;

        // Build motion curve
        std::vector<Point2f> curve;
        for (int bone = 0; bone < CURVE_LENGTH; bone++)
        {
            Point2f p = calculatCurvePoint(bone, m_frameCounter, forewardAmount);
            curve.push_back(p);
        }

        // Calculate bone angles
        getBonesAngles(curve);

    }

    // Set rotation of bones
    applyAngles();
    return;
}


void CurveAnimator::resetBones(){

    m_sinusPhase = 0;
    m_frameCounter = 0;

    for(int i = 0; i < m_bones.size(); i++){
        m_bones.at(i).rotation = m_bones.at(i).rotation * 0.9;
    }
}


/// TODO: use relative angles instead of absolute ones.
/// This will remove the need of the defaultRotation
void CurveAnimator::applyAngles(){

    // Apply angles
    for(int i = 0; i < m_bones.size(); i++){

        SBoneExtension b = m_bones.at(i);
        b.rotation = b.rotation * 180 / M_PI;
        irr::core::vector3df boneRot;

        switch (b.axis)
        {

        case SBoneExtension::x :

            boneRot = b.defaultRotation + irr::core::vector3df(b.rotation, 0.0, 0.0 );
            break;

        case SBoneExtension::y :

            boneRot = b.defaultRotation + irr::core::vector3df(0.0 , b.rotation, 0.0 );
            break;

        case SBoneExtension::z :

            boneRot = b.defaultRotation + irr::core::vector3df(0.0 , 0.0, b.rotation );
             break;

        default:
            break;
        }

        b.irrBonePtr->setRotation(boneRot);
    }

}

void CurveAnimator::normalizeBones(){

    float fullLen = 0;

    for (std::vector<SBoneExtension>::iterator b = m_bones.begin(); b != m_bones.end(); b++)
    {
        fullLen += b->length;
    }

    for (std::vector<SBoneExtension>::iterator b = m_bones.begin(); b != m_bones.end(); b++)
    {
        b->length /= fullLen;
    }

}

/// Distance between two points in 2d
float CurveAnimator::dist_point2point(Point2f a, Point2f b){

    double dx = a.x - b.x;
    double dy = a.y - b.y;

    double res = std::sqrt(dx * dx +  dy * dy);
    return res;
}

/// Returns length of curve
float CurveAnimator::getCurveLength(std::vector<Point2f> curve){

    double res = 0;

    if (!curve.size())
        return res;

    Point2f prev = *curve.begin();

    for (std::vector<Point2f>::iterator it = curve.begin(); it != curve.end(); it++)
    {
        double l = dist_point2point(prev, *it);
        res += l;
        prev = *it;
    }

    return res;
}

/// The given curve will be split into segments. Each segment of the curve belong to a bone
std::vector<std::vector<Point2f> > CurveAnimator::getBonesPoints(std::vector<Point2f> &curve){

    std::vector<std::vector<Point2f> > res;

    double fullCurveLength = getCurveLength(curve);


    Point prevExPoint(-1, -1);
    std::vector<Point2f>::iterator prevEnd = curve.begin();
    int bIndex = 0;

    double totalResLen = 0;

    for (std::vector<SBoneExtension>::iterator it = m_bones.begin(); it != m_bones.end(); it++, bIndex++)
    {
        SBoneExtension b = *it;
        double boneLen = b.length * fullCurveLength;
        int pointsCount = 2;
        float prevExLen = 0;

        if (prevExPoint.x >= 0)
            prevExLen = dist_point2point(prevExPoint, *prevEnd);

        float curveLen = prevExLen + getCurveLength(std::vector<Point2f>(prevEnd, prevEnd + pointsCount));
        float cdiff = boneLen - curveLen;

        while (prevEnd + pointsCount != curve.end())
        {
            int newPointsCount = pointsCount + 1;
            float nextPdist = dist_point2point(*(prevEnd + pointsCount-1), *(prevEnd + newPointsCount-1));

            cdiff = boneLen - (curveLen + nextPdist);
            if (cdiff < 0)
            {
                break;
            }
            curveLen += nextPdist;
            pointsCount = newPointsCount;
        }

        Point2f prev = *(prevEnd + pointsCount-1);
        Point2f next = *(prevEnd + pointsCount);

        Point2f direction = next - prev;
        float dirD = dist_point2point(direction);
        direction.x /= dirD;
        direction.y /= dirD;

        float addDist = boneLen - curveLen;
        Point2f extra = direction * addDist + Point2f(prev.x, prev.y);

        std::vector<Point2f> curCurve = std::vector<Point2f>(prevEnd, prevEnd + pointsCount);
        curCurve.push_back(extra);

        /// insert prevExPoint into curCurve head
        if (prevExPoint.x >= 0)
            curCurve.insert(curCurve.begin(), prevExPoint);
        prevExPoint = extra;

        prevEnd += pointsCount;

        float c_len = getCurveLength(curCurve);
        totalResLen += c_len;
        res.push_back(curCurve);


    }
    return res;
}

/// Assumtion: given a curve segment, returns the slope of a line with y = b + slope * x
/// This unction tries to fit a line as good as possible into the curve
Mat CurveAnimator::solveLinearEquation(std::vector<Point2f> curve, int polyPower)
{

    if (!curve.size())
        return Mat::zeros(1, polyPower, CV_32FC1);

    Mat leftHandSide = Mat::zeros(curve.size() , polyPower + 1, CV_32FC1);
    Mat rightHandSide = Mat::zeros(curve.size(), 1, CV_32FC1);

    for (uint i = 0; i < curve.size(); i++)
    {
        Point2f p = curve[i];

        for (int j = 0; j <= polyPower; j++)
        {
            leftHandSide.at<float>(i, j) = pow(p.x, j);
        }

        rightHandSide.at<float>(i) = p.y;
    }
    Mat solution;
    solve(leftHandSide, rightHandSide, solution, DECOMP_SVD/*DECOMP_LU*/ /*DECOMP_QR*/);

    return solution;

}


/// Given a curve, returns the angle the bones should have to match the curve as good as possible
void CurveAnimator::getBonesAngles(std::vector<Point2f> &curve){


    Point2f shift;
    std::vector<Scalar>::iterator color;
    Mat plot;

//    if(m_doDebugDrawing){

//        std::vector<Scalar> colorsSet;
//        colorsSet.push_back(Scalar(255,0,0));
//        colorsSet.push_back(Scalar(0,255,0));
//        colorsSet.push_back(Scalar(0,0,255));
//        colorsSet.push_back(Scalar(255,255,0));
//        colorsSet.push_back(Scalar(255,0,255));
//        colorsSet.push_back(Scalar(255,0,0));
//        colorsSet.push_back(Scalar(0,255,255));
//        colorsSet.push_back(Scalar(255,0,255));
//        shift = Point2f(100, 100);
//        color = colorsSet.begin();
//        plot = Mat::zeros(500, 500, CV_8UC3);
//    }


    // Split curve into multiple curves, one for each bone
    std::vector<std::vector<Point2f> > fullBonesPoints = getBonesPoints(curve);

    for (int bIndex = 0; bIndex < m_bones.size(); bIndex++)
    {

        std::vector<Point2f> currentCurve = fullBonesPoints[bIndex];

//        if(m_doDebugDrawing){

//            color++;
//            for (std::vector<Point2f>::iterator it = currentCurve.begin(); it != currentCurve.end(); it++){

//                circle(plot, *it + shift , 1, *color, -1);
//            }
//        }

        Mat solution;
        try
        {
            solution = solveLinearEquation(currentCurve, 1);
        } catch (Exception e)
        {
            std::cout << "exception: " << e.err << std::endl;
        }

        /// k       (y = b + kx)   is in solution.at<float>(1)
        /// {we have y = solution.at<float>(0) * x^0 + solution.at<float>(1) * x^1 }

        m_bones.at(bIndex).rotation =atan(solution.at<float>(1));
    }

//    if(m_doDebugDrawing)
//    {
//        imshow("color", plot);
//        cv::waitKey(0);
//    }

}

// t is time. Gets increased by 1 every frame
Point2f CurveAnimator::calculatCurvePoint(float bone, float t, float s)
{
    float y = 0;


    if (m_previousSpeed != s)
    {

        float x1 = m_previousSpeed * t + m_sinusPhase;
        float v1 = sin(x1);

        float x2 = x1 + 0.1;
        float v2 = sin(x2);

        if (v2 < v1)
            m_sinusPhase = M_PI_2 + (M_PI_2 - asin(v1)) - s * t;
        else
            m_sinusPhase = x1 - s * t;

        m_previousSpeed = s;
    }

    float frequency = m_previousSpeed; // how fast bones will rotate
    static float amp = m_amplitude; // how strong bone rotation will be at maximum

/* START USING 1 POLYNOMINAL */

    const int polyPowII = 7;
    float dataII[polyPowII] = {-2.000000092, 3.333547778e-1, -2.666167009e-2, 6.123869786e-4, -6.065582711e-6, 2.704552702e-8, -4.534275453e-11};

    // calculate y = f(bone,t)
    for (int i = 0; i < polyPowII; i++)
    {
        y += pow(bone, i) * dataII[i];
    }

/* END USING 1 POLYNOMINAL */

    // At this point in code, we calculated the motion curve.
    // We have a y value for each x value.
    // Now apply sinus, to get a curve that is swinging back and forth
    y = y * (sin(frequency * t + m_sinusPhase) * amp);

    return Point2f(bone, y);
}


} // end namespace fishsim
