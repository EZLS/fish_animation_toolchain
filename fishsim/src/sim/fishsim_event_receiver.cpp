#include "sim/fishsim_event_receiver.h"

#include <string>

#include "sim/fishsim_globals.h"
#include "irrExt/CGUIFileSelector.h"

#include "utils/fishsim_logger.h"

using namespace irr;
using namespace gui;

namespace fishsim
{

FishSimEventReceiver::FishSimEventReceiver(irr::IrrlichtDevice *device,
                                           FishSimSceneManager *fishsimSmgr,
                                           FishSimGUI* fishsimGUI) :
    m_device(device), m_fishSimSmgr(fishsimSmgr), m_gui(fishsimGUI)
{

    m_device->grab();
    m_lastTimeLeftMousePressed = 0;
}



FishSimEventReceiver::~FishSimEventReceiver(){

    if(m_device)
        m_device->drop();
}


bool FishSimEventReceiver::OnEvent(const SEvent& event)
{

    /// Key events
    if(event.EventType == EET_KEY_INPUT_EVENT)
    {
        return onKeyEvents(event);
    }


    /// GUI events
    if (event.EventType == EET_GUI_EVENT)
    {
        return onGUIEvents(event);
    }

    /// Mouse events
    if(event.EventType == EET_MOUSE_INPUT_EVENT
            && !m_gui->hasToolboxOpen())
    {
        return onMouseEvent(event);
    }

    if(event.EventType == EET_LOG_TEXT_EVENT)
    {

        logInfo(event.LogEvent.Text);
        //std::string t = event.LogEvent.Text;
        // std::wstring wt(t.begin(), t.end());

        //const wchar_t* tt = wt.c_str();
        //m_context.infoListBox->addItem(tt);

        return true;
    }

    return false;
}


bool FishSimEventReceiver::onKeyEvents(const SEvent& event){


    IGUIEnvironment* env = m_device->getGUIEnvironment();

    // Some key events are only accepted when no window has focus
    // Root element has focus, if no other element has focus.
    bool rootHasFocus = env->getFocus() == 0;

    if(event.KeyInput.Key == KEY_ESCAPE
            && event.KeyInput.PressedDown)
    {

        // Tell GUI to close all windows. If a window
        // was closed, it will return true. If no window
        // was there, it will return false. In this case
        // we stop the simulation.

        if(m_gui->closeAllWindows())
        {
            return true;
        }

//        else
//        {
//            m_device->closeDevice();
//            return true;
//        }
    }

    if(event.KeyInput.Key == KEY_F1
            && event.KeyInput.PressedDown)
    {
        m_gui->toggleVisiblility();
        m_fishSimSmgr->enableMayaCamera(m_gui->isVisible());
        return true;
    }

    if(event.KeyInput.Key == KEY_KEY_F
            && event.KeyInput.PressedDown
            && rootHasFocus)
    {

        m_fishSimSmgr->focusSelected();

        // return false, otherwise key has no effect on text input
        return false;
    }

    if(event.KeyInput.Key == KEY_DELETE
            && event.KeyInput.PressedDown
            && rootHasFocus
            && !m_gui->hasToolboxOpen())
    {
        if(m_fishSimSmgr->deleteSelectedNode()){
            m_fishSimSmgr->updateTriangleWorld();
            m_fishSimSmgr->saveTempScene();
        }

        // return false, otherwise key has no effect on text input
        return false;
    }

    return false;
}



bool FishSimEventReceiver::onMouseEvent(const SEvent& event){

    int mouseX = event.MouseInput.X;
    int mouseY = event.MouseInput.Y;


    // Check if mouse is on a GUIElement. Don't select 3D object if mouseclick
    //was performed on a GUI Element.
    IGUIEnvironment* env = m_device->getGUIEnvironment();
    IGUIElement *root = env->getRootGUIElement();

    // GetElementFromPoint returns root GUIElement if no other element
    // is at this point.
    bool mouseNotOnGUIElement =
            (root == root->getElementFromPoint(irr::core::vector2di(mouseX, mouseY)));


    /// Select with CTRL + LEFT MOUSE
    if( event.MouseInput.isLeftPressed() &&
        m_gui->isVisible() &&
        mouseNotOnGUIElement &&
        event.MouseInput.Control )
    {
        m_fishSimSmgr->selectNodeAtScreenCoordinates(mouseX, mouseY);
        return true;
    }

    /// Select with double LEFT MOUSE
    if( event.MouseInput.Event == EMIE_LMOUSE_PRESSED_DOWN /*event.MouseInput.isLeftPressed()*/ &&
        m_gui->isVisible() &&
        mouseNotOnGUIElement/* &&*/
        /*event.MouseInput.Control*/ )
    {

        // Simple double click check
        u32 now = m_device->getTimer()->getTime();
        u32 dtMouseClicks =  now - m_lastTimeLeftMousePressed;

        if(dtMouseClicks < 400)
        {

            m_fishSimSmgr->selectNodeAtScreenCoordinates(mouseX, mouseY);

            return true;
        }
            m_lastTimeLeftMousePressed = now;

    }

    return false;
}




bool FishSimEventReceiver::onGUIEvents(const SEvent& event){


    if(event.GUIEvent.EventType == EGET_ELEMENT_CLOSED)
    {
        return onCloseEvents(event);
    }

    if(event.GUIEvent.EventType == EGET_BUTTON_CLICKED)
    {
        return onButtonEvents(event);
    }


    if(event.GUIEvent.EventType == EGET_MENU_ITEM_SELECTED)
    {
        return onMenuEvents(event);
    }


    if(event.GUIEvent.EventType == EGET_FILE_SELECTED ||
            event.GUIEvent.EventType == EGET_DIRECTORY_SELECTED)
    {
        return onFileDialogEvents(event);
    }

    if(event.GUIEvent.EventType ==  EGET_LISTBOX_CHANGED)
    {
        if(event.GUIEvent.Caller->getID() == GUI_ID_LISTBOX_FISH_TEXTURES)
        {
            m_gui->getFishEditor()->changeTextureToSelected();

            return true;
        }

    }

    if(event.GUIEvent.EventType == EGET_CHECKBOX_CHANGED)
    {
        if(event.GUIEvent.Caller->getID() ==  GUI_ID_CBOX_SHOW_MESH)
        {
            m_gui->getFishEditor()->applyConfigTabChanges();
            return true;
        }
    }

    /* For testing */
    if(event.GUIEvent.EventType == EGET_SCROLL_BAR_CHANGED)
    {
        if( event.GUIEvent.Caller->getID() == GUI_ID_PELVIC_SPINE_SCROLLBAR)
        {
            const s32 pos = ((IGUIScrollBar*)event.GUIEvent.Caller)->getPos();
            m_gui->getFishEditor()->shapeMe(pos);
        }

    }


    return false;

}


bool FishSimEventReceiver::onMenuEvents(const SEvent& event)
{

    IGUIEnvironment* env = m_device->getGUIEnvironment();
    IGUIContextMenu* menu = (IGUIContextMenu*) event.GUIEvent.Caller;

    s32 id = menu->getItemCommandId(menu->getSelectedItem());

    switch(id)
    {

    //            // Show model selection window
    //            case GUI_ID_ADD_NEW_FISH:
    //            {
    //                m_gui->openModelSelectionWindow();
    //                return true;
    //            }


    // Show camera settings
    case GUI_ID_MENU_OPEN_CAMERA_SETTINGS:
    {
        m_gui->openCameraSettings();
        return true;
    }

    // Show light settings
    case GUI_ID_MENU_OPEN_LIGHT_SETTINGS:
    {
        m_gui->openLightSettings();
        return true;
    }

    // QUIT
    case GUI_ID_MENU_QUIT:
    {
        // Close device and run it one last time
        // to process the quit event.
        m_device->closeDevice();
        m_device->run();

        return true;
    }


    // Edit fish
    case GUI_ID_MENU_EDIT_FISH:
    {

        m_gui->openFishEditToolbox();
        return true;

    }

    // Add static object
    case GUI_ID_MENU_ADD_STATIC_OBJ:
    {

        CGUIFileSelector *selector = m_gui->addFileSelector(L"Please choose a file",
                                                     FILE_DIALOG_ADD_STATIC_OBJ,
                                                     EFST_OPEN_DIALOG);

        selector->addFileFilter(L"obj Files", L"obj",0);
        return true;
    }

    // Add fish
    case GUI_ID_MENU_LOAD_FISH:
    {
        CGUIFileSelector *selector = m_gui->addFileSelector(L"Please choose a file",
                                                     FILE_DIALOG_LOAD_FISH,
                                                     EFST_OPEN_DIALOG);

        selector->addFileFilter(L"fish files", L"fish",0);
        selector->addFileFilter(L"DirectX Files", L"x",0);
        return true;
    }

    // Delete the selected node
    case GUI_ID_MENU_DELETE_SELECTION:
    {

        if(m_fishSimSmgr->deleteSelectedNode()){
            m_fishSimSmgr->updateTriangleWorld();
            m_fishSimSmgr->saveTempScene();
        }

        return true;
    }

    // Save scene
    case GUI_ID_MENU_SAVE_SCENE:
    {

        CGUIFileSelector *selector = m_gui->addFileSelector(L"Please choose a file",
                                                     FILE_DIALOG_SAVE_SCENE,
                                                     EFST_SAVE_DIALOG);

        selector->addFileFilter(L"scene files", L"scene",0);
        return true;

    }

    // Load scene
    case GUI_ID_MENU_LOAD_SCENE:
    {

        CGUIFileSelector *selector = m_gui->addFileSelector(L"Please choose a scene",
                                                     FILE_DIALOG_LOAD_SCENE,
                                                     EFST_OPEN_DIALOG);

        selector->addFileFilter(L"scene files", L"scene",0);
        return true;

    }

    // Disable lights
    case GUI_ID_MENU_DISABLE_LIGHTS:
        m_fishSimSmgr->setShadowLevel(SHADOW_SETTING_NONE);
        return true;


    // Enable lights
    case GUI_ID_MENU_ENABLE_LIGHTS:
        m_fishSimSmgr->setShadowLevel(SHADOW_SETTING_SOFT);
        return true;

    // Open help window
    case GUI_ID_MENU_HELP:
        m_gui->openHelpWindow();
        return true;
    }

    return false;



}







bool FishSimEventReceiver::onButtonEvents(const SEvent& event){


    IGUIEnvironment* env = m_device->getGUIEnvironment();
    s32 id = event.GUIEvent.Caller->getID();

    switch(id)
    {

    // Delete node
    case GUI_ID_MENU_DELETE_NODE:
    {
        m_fishSimSmgr->deleteSelectedNode();
        return true;
    }

    case GUI_ID_BTN_SET_CAMERA_SETTINGS:
    {
        m_gui->getCameraEditor()->applyCameraSettings();
        return true;
    }

    case GUI_ID_BTN_SET_LIGHT_QUALITY:
    {
        m_gui->getLightEditor()->applyQualitySettings();
        return true;
    }

    case GUI_ID_BTN_SET_LIGHT_COLOR:
    {
        m_gui->getLightEditor()->applyColorSettings();
        return true;
    }

    case GUI_ID_BTN_SET_LIGHT_POSITION:
    {
        m_gui->getLightEditor()->applyPositionSettings();
        return true;
    }

    case GUI_ID_BTN_SET_STATIC_CAM:
    {
        m_fishSimSmgr->setStaticCamToCurrentView();
        return true;
    }

    case GUI_ID_BTN_SAVE_FISH:
    {

        CGUIFileSelector *selector = m_gui->addFileSelector(L"Please choose a file",
                                                     FILE_DIALOG_SAVE_FISH,
                                                     EFST_SAVE_DIALOG);

        selector->addFileFilter(L"fish files", L"fish",0);
        return true;
    }


    case GUI_ID_BTN_CHANGE_TEXTURE:
    {

        // Show loading screen. Need to draw the scene once
        m_gui->showLoadingInfo();
        m_device->getVideoDriver()->beginScene();
        m_fishSimSmgr->drawAll();
        m_gui->draw();
        m_device->getVideoDriver()->endScene();

        // Name is index of editable material
        m_gui->getFishEditor()->openTextureDialog(event.GUIEvent.Caller->getName());

        m_gui->hideLoadingInfo();
        return true;
    }

        // Edit fish
    case GUI_ID_MENU_EDIT_FISH:
    {
        m_gui->openFishEditToolbox();
        return true;
    }

    case GUI_ID_BTN_SET_SCALE:
    {
        m_gui->getFishEditor()->applyConfigTabChanges();
        m_fishSimSmgr->saveTempScene();
        return true;
    }


    case GUI_ID_MENU_LOAD_SCENE:
    {
        CGUIFileSelector *selector = m_gui->addFileSelector(L"Please choose a scene",
                                                     FILE_DIALOG_LOAD_SCENE,
                                                     EFST_OPEN_DIALOG);

        selector->addFileFilter(L"scene files", L"scene",0);
        return true;

    }

    case GUI_ID_MENU_SAVE_SCENE:
    {

        CGUIFileSelector *selector = m_gui->addFileSelector(L"Please choose a file",
                                                     FILE_DIALOG_SAVE_SCENE,
                                                     EFST_SAVE_DIALOG);

        selector->addFileFilter(L"scene files", L"scene",0);
        return true;
    }

    case GUI_ID_MENU_OPEN_LIGHT_SETTINGS:
    {
        m_gui->openLightSettings();
        return true;

    }


    case GUI_ID_MENU_OPEN_CAMERA_SETTINGS:
    {
        m_gui->openCameraSettings();
        return true;
    }

    }
    return false;

}



bool FishSimEventReceiver::onFileDialogEvents(const SEvent& event){

    s32 id = event.GUIEvent.Caller->getID();

    //        if(id == GUI_ID_CDIALOG_AMBIENT_COLOR){
    //            m_gui->getLightEditor()->setAmbientColor();
    //            return true;
    //        }

    //        if(id == GUI_ID_CDIALOG_LIGHT_COLOR){
    //            m_gui->getLightEditor()->setLightColor();
    //            return true;
    //        }



    IGUIFileOpenDialog* dialog =
            (IGUIFileOpenDialog*)event.GUIEvent.Caller;

    // Extract filepath
    std::string fp = core::stringc(dialog->getFileName()).c_str();

    //Extract directoy path
    std::string  dir = core::stringc(dialog->getDirectoryName()).c_str();

    switch(id)
    {

    case FILE_DIALOG_SAVE_FISH:
        m_gui->getFishEditor()->saveFish(fp);
        return true;


    case FILE_DIALOG_ADD_STATIC_OBJ:
    {
        if(m_fishSimSmgr->loadEnvModel(fp))
        {
            m_fishSimSmgr->updateTriangleWorld();
            m_fishSimSmgr->saveTempScene();
        }
        return true;
    }

    case FILE_DIALOG_LOAD_FISH:

        if(m_fishSimSmgr->loadFishModel(dialog->getFileName()))
            m_fishSimSmgr->saveTempScene();
        return true;

    case FILE_DIALOG_SAVE_SCENE:
        m_fishSimSmgr->saveScene(fp);
        return true;


    case FILE_DIALOG_LOAD_SCENE:
        if(m_fishSimSmgr->loadScene(fp, false))
            m_fishSimSmgr->saveTempScene();
        return true;

    default:
        return false;


    }
}




bool FishSimEventReceiver::onCloseEvents(const SEvent& event){

    s32 id = event.GUIEvent.Caller->getID();

    // Fish edit toolbox was closed
    if (id == GUI_ID_WIN_FISH_SETTINGS)
    {
        m_gui->onCloseFishEditToolbox();
        return false;
    }

    // Light settings where closed
    if( id == GUI_ID_WIN_LIGHT_SETTINGS)
    {
        m_gui->onCloseLightSettings();
        return false;
    }

    // Camera settings where closed
    if( id == GUI_ID_WIN_CAMERA_SETTINGS)
    {
        m_gui->onCloseCameraSettings();
        return false;
    }

    // Help window was closed
    if(id == GUI_ID_WIN_HELP)
    {
        m_gui->onCloseHelpWindow();
        return false;
    }


}






} // end namespace fishsim
