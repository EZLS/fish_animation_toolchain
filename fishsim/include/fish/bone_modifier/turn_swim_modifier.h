/*! Copyright (C) 2017  	Department of Electrical Engineering & Computer Science, Institute of
 *			Real-Time Learning Systems, University of Siegen
 *
 *			Research Group of Ecology and Behavioral Biology, Institute of Biology,
 *			University of Siegen
 *
 * Contact: virtual.fish.project@gmail.com
 *
 * License: GNU General Public License  See LICENSE.txt for the full license.
 *
 * This file is part of the "FishSim Animation Toolchain".
 * "FishSim Animation Toolchain" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "FishSim Animation Toolchain" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "FishSim Animation Toolchain".  If not, see <http: *www.gnu.org/licenses/>.
 *
 * The "FishSim Animation Toolchain" was developed within the scope of the interdisciplinary,
 * DFG (Deutsche Forschungsgemeinschaft)-funded project “virtual fish” (KU
 * 689/11-1 and Wi 1531/12-1) of the Institute of Real-Time Learning Systems
 * (EZLS) and the Research Group of Ecology and Behavioral Biology at the
 * University of Siegen.
 *
 * \file turn_swim_modifier.h
 * \brief Bone modifer that implements the bone rotations while doing a turn.
 * Used for sailfin mollies.
 *
 * Is only be used directly in the move function. Thus, it does not allow to edit foreward swim after
 * recording.
*/

#ifndef H_TURN_SWIM_MODIFIER_H
#define H_TURN_SWIM_MODIFIER_H


#include <irrlicht.h>
#include <string>
#include <sensor_msgs/Joy.h>

#include "fish/bone_modifier.h"



namespace fishsim
{
class TurnSwimModifier : public BoneModifier
{

public:

    /// Constructor
    TurnSwimModifier(irr::scene::IAnimatedMeshSceneNode* fishNode,
                      std::string name, float speed = 1.0, bool isActive = true);

    /// Destructor
    ~TurnSwimModifier();

    /// Init modifier
    bool init();    

    /// Update with joy message
    void update(const sensor_msgs::Joy& joystickState);

    /// Update with single message
    void update(float inputVal1, float inputVal2 = 0, float inputVal3 = 0);

    /// Modify based on existing snapshots
    void modify(FishSnapshot* startState,
                FishSnapshot* endState,
                const sensor_msgs::Joy& joystickState );


private:

    /// All bones that are modified when fish is doing a turn.
    /// Must be in order like on the mesh.
    std::vector<irr::scene::IBoneSceneNode*> m_turnBones;

    /// Body
    irr::scene::IBoneSceneNode *m_head;
    irr::scene::IBoneSceneNode *m_backbone1;
    irr::scene::IBoneSceneNode *m_backbone2;
    irr::scene::IBoneSceneNode *m_backbone3;
    irr::scene::IBoneSceneNode *m_backbone4;

    /// Tail fins that will be rotated by
    /// p-controller
    irr::scene::IBoneSceneNode *m_tailfin_middle1;
    irr::scene::IBoneSceneNode *m_tailfin_middle2;
    irr::scene::IBoneSceneNode *m_tailfin_middle3;

    /// Tail fins that will be rotated manually after
    /// p-controller has rotated the tailfin_middle
    /// bones
    irr::scene::IBoneSceneNode *m_tailfin_up1;
    irr::scene::IBoneSceneNode *m_tailfin_up2;
    irr::scene::IBoneSceneNode *m_tailfin_up3;

    irr::scene::IBoneSceneNode *m_tailfin_down1;
    irr::scene::IBoneSceneNode *m_tailfin_down2;
    irr::scene::IBoneSceneNode *m_tailfin_down3;

    /// Current turn angle which is feed into the
    /// p-controller of the turn function
    float m_currentTurnAngle;

    /// P controller function
    void turnAnimation(float turnAmount);

};


} // end namespace fishsim

#endif // H_TURN_SWIM_MODIFIER_H
