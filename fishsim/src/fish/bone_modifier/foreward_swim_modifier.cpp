#include "fish/bone_modifier/foreward_swim_modifier.h"

using namespace irr;
using namespace scene;

namespace fishsim
{

ForewardSwimModifier::ForewardSwimModifier(IAnimatedMeshSceneNode *fishNode,
                                           std::string name, float speed, bool isActive)
        : BoneModifier(fishNode, name, speed, isActive)
{

}

ForewardSwimModifier::~ForewardSwimModifier()
{
    if(m_backbone1)
        m_backbone1->drop();

    if(m_backbone2)
        m_backbone2->drop();

    if(m_backbone3)
        m_backbone3->drop();

    if(m_backbone4)
        m_backbone4->drop();

    if(m_tailfin_middle1)
        m_tailfin_middle1->drop();

    if(m_tailfin_middle2)
        m_tailfin_middle2->drop();

    if(m_tailfin_middle3)
        m_tailfin_middle3->drop();

    if(m_tailfin_up1)
        m_tailfin_up1->drop();

    if(m_tailfin_up2)
        m_tailfin_up2->drop();

    if(m_tailfin_up3)
        m_tailfin_up3->drop();

    if(m_tailfin_down1)
        m_tailfin_down1->drop();

    if(m_tailfin_down2)
        m_tailfin_down2->drop();

    if(m_tailfin_down3)
        m_tailfin_down3->drop();

    if(m_curveAnimator)
        delete m_curveAnimator;
}

bool ForewardSwimModifier::init(){

    // Get bones that are used for animation
    bool allBonesFound = true;

    allBonesFound = allBonesFound && getAndGrabBone(m_head, "head");
    allBonesFound = allBonesFound && getAndGrabBone(m_backbone1,"backbone_1");
    allBonesFound = allBonesFound && getAndGrabBone(m_backbone2,"backbone_2");
    allBonesFound = allBonesFound && getAndGrabBone(m_backbone3,"backbone_3");
    allBonesFound = allBonesFound && getAndGrabBone(m_backbone4,"backbone_4");

    allBonesFound = allBonesFound && getAndGrabBone(m_tailfin_middle1,"tail_fin_middle1");
    allBonesFound = allBonesFound && getAndGrabBone(m_tailfin_middle2,"tail_fin_middle2");
    allBonesFound = allBonesFound && getAndGrabBone(m_tailfin_middle3,"tail_fin_middle3");


    allBonesFound = allBonesFound && getAndGrabBone(m_tailfin_up1, "tail_fin_up1");
    allBonesFound = allBonesFound && getAndGrabBone(m_tailfin_up2, "tail_fin_up2");
    allBonesFound = allBonesFound && getAndGrabBone(m_tailfin_up3, "tail_fin_up3");

    allBonesFound = allBonesFound && getAndGrabBone(m_tailfin_down1, "tail_fin_down1");
    allBonesFound = allBonesFound && getAndGrabBone(m_tailfin_down2, "tail_fin_down2");
    allBonesFound = allBonesFound && getAndGrabBone(m_tailfin_down3, "tail_fin_down3");

    if(!allBonesFound)
    {
        std::cerr << "Foreward swim modifier failed on init." << std::endl;
        m_isActive = false;
        return false;
    }

    // Initialize curve animator. It's important to append bones in order
    // like they are on the mesh
    m_curveAnimator = new CurveAnimator;

    m_curveAnimator->appendBone(m_head, SBoneExtension::z, 110);
    m_curveAnimator->appendBone(m_backbone1, SBoneExtension::z, 90);
    m_curveAnimator->appendBone(m_backbone2, SBoneExtension::z, 100);
    m_curveAnimator->appendBone(m_backbone3, SBoneExtension::z, 85);
    m_curveAnimator->appendBone(m_backbone4, SBoneExtension::z, 75);
    m_curveAnimator->appendBone(m_tailfin_middle1, SBoneExtension::z, 45);
    m_curveAnimator->appendBone(m_tailfin_middle2, SBoneExtension::z, 45);
    m_curveAnimator->appendBone(m_tailfin_middle3, SBoneExtension::z, 45);

    m_curveAnimator->normalizeBones();

    return true;
}

void ForewardSwimModifier::update(float inputVal1, float inputVal2, float inputVal3){

    if(!m_isActive)
        return;

    m_curveAnimator->animateBones(inputVal1);

    // Update the up and down tail fin parts too. They are not included in the
    // curve animator since they are parallel to each other
    m_tailfin_up1->setRotation(m_tailfin_middle1->getRotation());
    m_tailfin_up2->setRotation(m_tailfin_middle2->getRotation());
    m_tailfin_up3->setRotation(m_tailfin_middle3->getRotation());

    m_tailfin_down1->setRotation(m_tailfin_middle1->getRotation());
    m_tailfin_down2->setRotation(m_tailfin_middle2->getRotation());
    m_tailfin_down3->setRotation(m_tailfin_middle3->getRotation());
}

void ForewardSwimModifier::update(const sensor_msgs::Joy& joystickState){

    // modifier does not show up in fishsteering. Its only used together
    // with the move function. -> Do nothing
    return;
}

void ForewardSwimModifier::modify(FishSnapshot* startState, FishSnapshot* endState,
            const sensor_msgs::Joy& joystickState ){

    // modifier does not show up in fishsteering. Its only used together
    // with the move function. -> Do nothing
    return;
}



} // end namespace fishsim
