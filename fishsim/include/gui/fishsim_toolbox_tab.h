/*! Copyright (C) 2017  	Department of Electrical Engineering & Computer Science, Institute of
 *			Real-Time Learning Systems, University of Siegen
 *
 *			Research Group of Ecology and Behavioral Biology, Institute of Biology,
 *			University of Siegen
 *
 * Contact: virtual.fish.project@gmail.com
 *
 * License: GNU General Public License  See LICENSE.txt for the full license.
 *
 * This file is part of the "FishSim Animation Toolchain".
 * "FishSim Animation Toolchain" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "FishSim Animation Toolchain" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "FishSim Animation Toolchain".  If not, see <http: *www.gnu.org/licenses/>.
 *
 * The "FishSim Animation Toolchain" was developed within the scope of the interdisciplinary,
 * DFG (Deutsche Forschungsgemeinschaft)-funded project “virtual fish” (KU
 * 689/11-1 and Wi 1531/12-1) of the Institute of Real-Time Learning Systems
 * (EZLS) and the Research Group of Ecology and Behavioral Biology at the
 * University of Siegen.
 *
 *  \file fishsim_toolbox_tab.h
 *  \brief Helper class that offer functions to layout a tab in a toolbox in fishsim.
 * The basic idea is that you only call the various append***() functions to add a new GUI element to the
 * toolbox and the FishSimToolboxTab handles the layout for you. This allows the toolboxes to look
 * identical while making code development easier and faster.
*
*/

#ifndef H_FISHSIM_TOOLBOX_TAB_H
#define H_FISHSIM_TOOLBOX_TAB_H

#include <irrlicht.h>
#include <vector>

#include "IGUIButton.h"
#include "IGUIEnvironment.h"
#include "IGUIElement.h"
#include "IGUIStaticText.h"
#include "IGUICheckBox.h"
#include "IGUIEditBox.h"


namespace fishsim
{
//TODO: Add a scrollbar, when height of the elements gets greater than toolbox height.
class FishSimToolboxTab
{
    public:

        /*!
          \brief Constructor
          \param guiEnvironment Pointer to GUI environment.
          \param tab ID of the new tab.
          \param toolboxHeight height of the toolbox where this tab will be added.
        */
        FishSimToolboxTab(irr::gui::IGUIEnvironment* guiEnvironment, irr::gui::IGUITab* tab,
                          irr::u32 toolboxWidth, irr::u32 toolboxHeight);

        /*!
          \brief Destructor
        */
        ~FishSimToolboxTab();


        /*!
            \brief Append a seperator to tab. This is just a height offset,
            to allow you to create spaces between your elements.
        */
        void appendSeparator();


        /*!
            \brief Append an edit box to the tab.
            \param edtBoxTex Text of the edit box.
            \param id ID of the edit box.
            \return pointer to the new edit box.
        */
        irr::gui::IGUIEditBox* appendEditBox(const wchar_t* edtBoxTex,
                                             irr::s32 id= -1);

        /*!
            \brief Append an edit box with a leading description text to the tab.
            \param description Leading description of edit box.
            \param edtBoxTex Text of the edit box.
            \param id ID of the edit box.
            \return pointer to the new edit box.
        */
        irr::gui::IGUIEditBox* appendEditBoxWithDescription(const wchar_t* description,
                                                            const wchar_t* edtBoxTex,
                                                            irr::s32 id= -1);

        /*!
            \brief Append a static text. Long texts that would
                go over the border of the tab will be split to
                go over multiple lines.
            \param text Static text
            \param id ID of the static text element.
            \return pointer to the new static text.
        */
        irr::gui::IGUIStaticText* appendStaticText(const wchar_t* text,
                                                  irr::s32 id= -1);


        /*!
            \brief Append a button to the tab.
            \param text Text the button should show
            \param id ID of the button.
            \param tooltiptext tooltip the button.
            \return pointer to the new button.
        */
        irr::gui::IGUIButton* appendButton(const wchar_t* text,
                                           irr::s32 id= -1,
                                           const wchar_t* tooltiptext = 0);


        /*!
            \brief Append a check box.
            \param checked Is checkbox checked by default or not.
            \param id ID of the check box.
            \param text Leading description text.
            \return pointer to the new check box.
        */
        irr::gui::IGUICheckBox* appendCheckBox(bool checked, const wchar_t* text=0,
                                               irr::s32 id= -1);


        /*!
            \brief Append a image button with a description text beside the button.

            Used in fish editor to display changeable fish textures in toolbox.

            \param buttonText Text the button should show
            \param buttonImage Image the button should display
            \param descriptionText Text of the description
            \param id ID of the button.
            \param tooltiptext tooltip the button.

            \return pointer to the new image button.
        */
        irr::gui::IGUIButton* appendTextureButton(const wchar_t* buttonText,
                                                  irr::video::ITexture *buttonImage,
                                                  const wchar_t* descriptionText,
                                                  irr::s32 id= -1);


        /*!
            \brief Append a horizontal scrollbar to the tab.

            \param id ID of the scrollbar.

            \return pointer to the new scrollbar.
        */
        irr::gui::IGUIScrollBar* appendHorizontalScrollbar(irr::s32 id = -1);

        /*!
            \brief Append a static text as a description.
            \param text Description text.
            \param id ID of the static text.
            \return Pointer to the new static text.
        */
        irr::gui::IGUIStaticText* appendDescriptionText(const wchar_t* text,
                                                  irr::s32 id= -1);

private:

        /// Irrlicht gui environment
        irr::gui::IGUIEnvironment* m_guiEnv;

        /// Tab all new elements are put onto
        irr::gui::IGUITab* m_tab;

        /// Width of the toolbox where tab is attatched to
        int m_toolboxWidth;

        /// Height of the toolbox where tab is attatched to
        int m_toolboxHeight;

        /// All elements on the tab.
        std::vector<irr::gui::IGUIElement*> m_elements;

        /// Total height of all elements.
        int m_totalHeight;

        /*!
          \brief Create a rectangle with given height and with of the toolbox.
          \param RectangleHeight the height of the rectangle.
          \return A irrlicht rectangle.
         */
        irr::core::rect<irr::s32> makeRectangle(int rectangleHeight);
};


} // end namespace fishsim

#endif // H_FISHSIM_TOOLBOX_TAB_H
