#include "fish/gasterosteus/gasterosteus.h"

#include "sim/fishsim_globals.h"

#include "ros/joy_indices.h"

using namespace irr;
using namespace scene;
using namespace core;

// Gets multiplied with foreward input
#define FOREWARD_MULTIPLIER 0.8

// Gets multiplied with yaw input
#define YAW_MULTIPLIER 6.0

// Gets multiplied with pitch input
#define PITCH_MULTIPLIER 2.0

namespace fishsim
{

Gasterosteus::Gasterosteus(IAnimatedMeshSceneNode *node) : Fish(node)
{

    m_dorsalFinModifier = 0;
    m_pelvicSpineModifier = 0;
    m_forewardSwimModifier = 0;
    m_turnSwimModifier = 0;
    m_upSwimModifier = 0;

    m_forewardMultiplier = FOREWARD_MULTIPLIER;
    m_yawMultiplier = YAW_MULTIPLIER;
    m_pitchMultiplier = PITCH_MULTIPLIER;
}

Gasterosteus::~Gasterosteus()
{
    unload();
}

bool Gasterosteus::unload()
{

    if(m_dorsalFinModifier)
    {
        delete m_dorsalFinModifier;
        m_dorsalFinModifier = 0;
    }

    if(m_pelvicSpineModifier)
    {
        delete m_pelvicSpineModifier;
        m_pelvicSpineModifier = 0;
    }

    if(m_forewardSwimModifier)
    {
        delete m_forewardSwimModifier;
        m_forewardSwimModifier = 0;
    }

//    if(m_turnSwimModifier)
//    {
//        delete m_turnSwimModifier;
//        m_turnSwimModifier = 0;
//    }

    if(m_upSwimModifier)
    {
        delete m_upSwimModifier;
        m_upSwimModifier = 0;
    }

    if(m_pectoralModifier)
    {
        delete m_pectoralModifier;
        m_pectoralModifier = 0;
    }

    m_boneModifiers.clear();

    Fish::unload();

    return true;
}

bool Gasterosteus::load()
{

    /// 1. Mesh scene node settings like material flags, debug stuff etc..
    Fish::load();

    /// 2. Bone renaming. Remove bone prefix.
    Fish::removeBonePrefix("head");

    /// 3. Init editable body parts of the fish.
    initEditableMaterials();

    /// 4. init bone modifier
    initBoneModifier();

    return true;
}


void Gasterosteus::initEditableMaterials(){

    // Body should not be transparent
    //m_irrNode->getMaterial(0).MaterialType = irr::video::EMT_SOLID;

    makeMaterialEditable(0, L"body", true);
    makeMaterialEditable(1, L"dorsal", true);
    makeMaterialEditable(2, L"caudal", true);
    makeMaterialEditable(3, L"pectoral", true);
    makeMaterialEditable(4, L"anal", true);
    makeMaterialEditable(5, L"spines", true);


}

void Gasterosteus::initBoneModifier(){

    /// Init modifiers that are only used in direct mode:
    /// They only get used in move function. Not visible in
    /// fishsteering.

    // Foreward swimming animation
    m_forewardSwimModifier = new ForewardSwimModifier(m_irrNode, "foreward swim animator");
    m_forewardSwimModifier->init();
    m_forewardSwimModifier->setAmplitude(0.3);

    // Idle animation
    m_idleModifier = new IdleModifier(m_irrNode, "idle animator", 0.2);
    m_idleModifier->init();

    // Turn animation
    m_turnSwimModifier = new TurnSwimModifier(m_irrNode, "turn animator",  0.3);
    m_turnSwimModifier->init();

    // Pelvic spine modifier
    m_pectoralModifier = new PectoralModifier(m_irrNode, "pectoral modifier", 0.2);
    m_pectoralModifier->init();

//    // up/down animation
//    m_upSwimModifier = new UpSwimModifier(m_irrNode, "up/down animator",  0.1);
//    m_upSwimModifier->init();


    /// Init bone modifiers that can be used in indirect mode too.
    /// They are called from move and are also able to modify fishsnapshots
    /// through their modify function.
    /// These modifier will show up in fishsteering.

    // Dorsal fin
    m_dorsalFinModifier = new DorsalFinModifier(m_irrNode, "Spikes/Dorsal fin (L1, L2)", 3.5);
    if(m_dorsalFinModifier->init())
    {
        // Use dorsal fin 1 independantly
        m_dorsalFinModifier->useConnectedMode(false);
        m_dorsalFinModifier->setDorsalFin2Inverted(true);
        addBoneModifier(m_dorsalFinModifier);
    }

    // Pelvic spine modifier
    m_pelvicSpineModifier = new PelvicSpineModifier(m_irrNode, "Pelvic spines (R1)", 3.5);
    if(m_pelvicSpineModifier->init())
        addBoneModifier(m_pelvicSpineModifier);





}

void Gasterosteus::move(const sensor_msgs::Joy& joyState){

    float forewardAmount = joyState.axes.at(PS3_AXIS_STICK_LEFT_UPWARDS) * m_forewardMultiplier;
    float yawAmount = joyState.axes.at(PS3_AXIS_STICK_RIGHT_LEFTWARDS) * m_yawMultiplier;
    float pitchAmount = joyState.axes.at(PS3_AXIS_STICK_RIGHT_UPWARDS) * m_pitchMultiplier;

    /* Non kernel bug
    float shiftForewardAmount =   joyState.axes.at(PS3_AXIS_BUTTON_CROSS_UP)
                                - joyState.axes.at(PS3_AXIS_BUTTON_CROSS_DOWN);
    float shiftSideAmount =   (1.0 - joyState.axes.at(PS3_AXIS_BUTTON_CROSS_LEFT))
                                - (1.0 - joyState.axes.at(PS3_AXIS_BUTTON_CROSS_RIGHT));
    */

    float shiftForewardAmount = joyState.buttons.at(PS3_BUTTON_CROSS_UP) - joyState.buttons.at(PS3_BUTTON_CROSS_DOWN);
    float shiftSideAmount = joyState.buttons.at(PS3_BUTTON_CROSS_LEFT) - joyState.buttons.at(PS3_BUTTON_CROSS_RIGHT);


    slide(shiftForewardAmount / 2.0, shiftSideAmount / 2.0);
    move(forewardAmount,0.0, 0.0 ,yawAmount,pitchAmount,0.0);

    m_dorsalFinModifier->update(joyState);
    m_pelvicSpineModifier->update(joyState);

}

void Gasterosteus::move(float forewardAmount, float sidewardAmount, float upwardAmount, float yawAmount, float pitchAmount, float rollAmount)
{

    forewardAmount = Fish::getForewardSmoothed(forewardAmount);
    Fish::move(forewardAmount, sidewardAmount,  upwardAmount,  yawAmount,  pitchAmount,  rollAmount);

    // Update foreward swim modifier. This modifier uses absolute rotation
    // values. It should be called first, since it resets all rotation values
    // from the last move call
    m_forewardSwimModifier->update(forewardAmount);

    // Do turn and up animation. These modifier limit the rotation values to
    // a maximum.
    m_turnSwimModifier->update(yawAmount);
//    m_upSwimModifier->update(pitchAmount);

    // Idle modifier kicks in, if foreward amount is small or near zero.
    // Will play idle animation
    m_idleModifier->update(forewardAmount);

    m_pectoralModifier->update(forewardAmount);
}


} // end namespace fishsim
