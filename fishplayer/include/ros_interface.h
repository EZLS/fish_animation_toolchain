/*! Copyright (C) 2017  	Department of Electrical Engineering & Computer Science, Institute of
 *			Real-Time Learning Systems, University of Siegen
 *
 *			Research Group of Ecology and Behavioral Biology, Institute of Biology,
 *			University of Siegen
 *
 * Contact: virtual.fish.project@gmail.com
 *
 * License: GNU General Public License  See LICENSE.txt for the full license.
 *
 * This file is part of the "FishSim Animation Toolchain".
 * "FishSim Animation Toolchain" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "FishSim Animation Toolchain" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "FishSim Animation Toolchain".  If not, see <http: *www.gnu.org/licenses/>.
 *
 * The "FishSim Animation Toolchain" was developed within the scope of the interdisciplinary,
 * DFG (Deutsche Forschungsgemeinschaft)-funded project “virtual fish” (KU
 * 689/11-1 and Wi 1531/12-1) of the Institute of Real-Time Learning Systems
 * (EZLS) and the Research Group of Ecology and Behavioral Biology at the
 * University of Siegen.
*/
#ifndef H_ROS_INTERFACE_H
#define H_ROS_INTERFACE_H

#include "fish/fishsnapshot.h"

#include "ros/ros.h"

// FishSim messages
#include "fishsim/FishSnapshotStamped.h"

// FishSim services
#include "fishsim/loadScene.h"

// Automover services
#ifdef USE_AUTOMOVER
#include <fish_automover/enableAutoMover.h>
#endif

namespace fishplayer
{
class RosInterface
{

public:

    RosInterface(std::string ROSNamespace);

    ~RosInterface();

    void publishAll(std::vector<fishsim::FishSnapshot*> snapshots);

    bool callLoadFishSimScene(const std::string& scenePath, bool hideModels);
    bool loadFishScene(const std::string& scenePath);


#ifdef USE_AUTOMOVER
    bool callEnableAutoMoverSrv(bool autoMove,
                                bool doMimic,
                                bool doDorsalFinMove,
                                bool doGonopodiumMove);

    bool callLoadAutomoverScene(const std::string& scenePath);
#endif

private:

    ros::NodeHandle m_nodeHandle;
    std::string m_namespace;

    ros::Publisher m_snapshotPublisher;

    ros::AsyncSpinner* m_spinner;

    ros::ServiceClient m_loadSceneFishSimClient;

#ifdef USE_AUTOMOVER
    ros::ServiceClient m_loadSceneAutomoverClient;
    ros::ServiceClient m_enableAutoMoverClient;
#endif

    bool callLoadSceneSrv(const std::string& scenePath,
                          ros::ServiceClient server,
                          bool hideFishModelsOnStart);

};
}

#endif //H_ROS_INTERFACE_H
