#include "fish/gasterosteus/pelvic_spine_modifier.h"

#include "ros/joy_indices.h"

using namespace irr;
using namespace scene;

// Maximum rotation values for pelvic spines
#define PELVIC_FINS_IN 0.0f
#define PELVIC_FINS_OUT 65.0f

namespace fishsim
{


PelvicSpineModifier::PelvicSpineModifier(IAnimatedMeshSceneNode* fishNode,
                           std::string name,
                           float speed,
                           bool isActive)
    : BoneModifier(fishNode, name, speed, isActive)
{

}

PelvicSpineModifier::~PelvicSpineModifier(){

    if(m_pelvicSpineLeft)
        m_pelvicSpineLeft->drop();

    if(m_pelvicSpineRight)
        m_pelvicSpineRight->drop();
}

bool PelvicSpineModifier::init(){

    if(!m_fishNode)
        return false;

    bool allOk = true;
    allOk = allOk && getAndGrabBone(m_pelvicSpineLeft,"pelvic_spine_left");
    allOk = allOk && getAndGrabBone(m_pelvicSpineRight,"pelvic_spine_right");
    if(allOk)
    {
//        m_pelvicSpineLeft->setScale(irr::core::vector3df(1,2,1));
        return true;
    }
    else
    {
        std::cerr << "Was not able to init pelvic spine modifier." << std::endl;
        return false;
    }
}

/// Extract value of rear right 1 button and pass it to update function
void PelvicSpineModifier::update(const sensor_msgs::Joy& joystickState){

    if(!m_isActive)
        return;

    float val = joystickState.buttons.at(PS3_AXIS_BUTTON_REAR_RIGHT_1);
    update(val);

}

/// If button is pressed, extend the pelvic spines
void PelvicSpineModifier::update(float inputVal1, float inputVal2, float inputVal3){

    if(!m_isActive)
        return;

    if(inputVal1 > 0)
        changePelvicSpines(1);
    else
        changePelvicSpines(-1);

}

void PelvicSpineModifier::changePelvicSpines(int dir){

    core::vector3df pelvicSpineLeftRot = m_pelvicSpineLeft->getRotation();
    core::vector3df pelvicSpineRightRot = m_pelvicSpineRight->getRotation();

    float currRot = pelvicSpineRightRot.Z;
    currRot += m_speed * dir;

    // Check bounds
    if(currRot > PELVIC_FINS_IN && currRot < PELVIC_FINS_OUT)
    {
        core::vector3df vecToAdd = core::vector3df(0.0, 0.0, m_speed * dir);
        pelvicSpineLeftRot -= vecToAdd;
        pelvicSpineRightRot += vecToAdd;


        // Update irrlicht node
        m_pelvicSpineLeft->setRotation(pelvicSpineLeftRot);
        m_pelvicSpineRight->setRotation(pelvicSpineRightRot);
    }



}

void PelvicSpineModifier::modify(FishSnapshot* startState,
                               FishSnapshot* endState,
                               const sensor_msgs::Joy& joystickState ){


    float val = joystickState.buttons.at(PS3_AXIS_BUTTON_REAR_LEFT_1);
    int dir = -1;
    if(val > 0) dir = 1;


    core::vector3df pelvicSpineLeftRot = startState->getSingleBoneRot(m_pelvicSpineLeft->getName());
    core::vector3df pelvicSpineRightRot = startState->getSingleBoneRot(m_pelvicSpineRight->getName());

    float currRot = pelvicSpineRightRot.Z;
    currRot += m_speed * dir;

    // Check bounds
    if(currRot > PELVIC_FINS_IN && currRot < PELVIC_FINS_OUT)
    {
        core::vector3df vecToAdd = core::vector3df(0.0, 0.0, m_speed * dir);
        pelvicSpineLeftRot -= vecToAdd;
        pelvicSpineRightRot += vecToAdd;
    }

    endState->setSingleBoneRot(m_pelvicSpineLeft->getName(), pelvicSpineLeftRot);
    endState->setSingleBoneRot(m_pelvicSpineRight->getName(), pelvicSpineRightRot);
}


} // end namespace fishsim

