#ifndef H_MODEL_SELECTION_WINDOW_H__
#define H_MODEL_SELECTION_WINDOW_H__

#include <string>

#include <irrlicht.h>
#include "IGUIButton.h"
#include "IGUIEnvironment.h"
#include "IGUIElement.h"
#include "IGUIWindow.h"

#include "sim/fishsim_globals.h"
#include "sim/fishsim_helper_routines.h"

#include "irrExt/CGUIModalScreen.h"

#include "gui/fishsim_toolbox_tab.h"

#define MODEL_SELECTION_WINDOW_HEIGHT 700
#define MODEL_SELECTION_WINDOW_WIDTH 400




class ModelSelectionWindow
{

public:


    /// Constructor
    ModelSelectionWindow(irr::IrrlichtDevice* device, const std::string& fishSimPath);

    /// Destructor
    ~ModelSelectionWindow();

    /// Set up the default models
    void initDefaultModels();

private:

    /// Pointer to irrlicht device
    irr::IrrlichtDevice* m_device;

    /// Pointer to irrlicht gui environment
    irr::gui::IGUIEnvironment* m_guiEnv;

    /// The toolbox window
    irr::gui::IGUIWindow* m_modelSelectionWindow;

    /// Path where fishsim is located
    std::string m_fishSimPath;

    /// Poecilia latipinna tab
    FishSimToolboxTab* m_sailfinMolliesTab;

    /// Poecilia mexicana tab
    FishSimToolboxTab* m_mexicanaTab;

    /// Creates the add model window
    void createModelSelectionWindow();

};

#endif

