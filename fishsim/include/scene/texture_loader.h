/*! Copyright (C) 2017  	Department of Electrical Engineering & Computer Science, Institute of
 *			Real-Time Learning Systems, University of Siegen
 *
 *			Research Group of Ecology and Behavioral Biology, Institute of Biology,
 *			University of Siegen
 *
 * Contact: virtual.fish.project@gmail.com
 *
 * License: GNU General Public License  See LICENSE.txt for the full license.
 *
 * This file is part of the "FishSim Animation Toolchain".
 * "FishSim Animation Toolchain" is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * "FishSim Animation Toolchain" is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with "FishSim Animation Toolchain".  If not, see <http: *www.gnu.org/licenses/>.
 *
 * The "FishSim Animation Toolchain" was developed within the scope of the interdisciplinary,
 * DFG (Deutsche Forschungsgemeinschaft)-funded project “virtual fish” (KU
 * 689/11-1 and Wi 1531/12-1) of the Institute of Real-Time Learning Systems
 * (EZLS) and the Research Group of Ecology and Behavioral Biology at the
 * University of Siegen.
 *
 *  \file texture_loader.h
 *  \brief Helper class to load several textures from a given folder with a given prefix.
 *
*/

#ifndef H_TEXTURE_LOADER_H
#define H_TEXTURE_LOADER_H

#include <irrlicht.h>

#include <string>
#include <vector>



namespace fishsim
{
class TextureLoader
{

public:

    /*!
     * \brief Custom constructor.
     * \param driver pointer to irrlicht video driver.
     * \param filesystem pointer to irrlicht filesystem.
     * \param textureDir filepath where TextureLoader is searching for textures.
     */
    TextureLoader(irr::video::IVideoDriver *driver,
                  irr::io::IFileSystem *filesystem);


    /*!
     * \brief Destructor.
     */
    ~TextureLoader();


    /*!
     * \brief Get all textures with given prefix in a vector.
     * \param prefix the prefix the textures have to match.
     * \return  a vector with pointers to all textures that have been loaded.
     */
    std::vector<irr::video::ITexture*> getTextures(const std::string &prefix);

private:

    ///Pointer to scene manager.
    irr::video::IVideoDriver* m_driver;

    /// Pointer to irrlicht filesystem.
    irr::io::IFileSystem* m_filesystem;

    /// All textures that have been loaded.
    std::vector<irr::video::ITexture*> m_textures;

    /// Path where fishsim textures are stored.
    std::string m_textureDir;

    /// Is loading in progress.
    bool m_isLoadRunning;

    /// List of all files in the models folder.
    irr::io::IFileList *m_files;

    /*!
     * \brief Clear everything.
     */
    void clear();


    /*!
     * \brief Start the loading process. Can take a while.
     */
    void loadTextures();
};


} // end namespace fishsim
#endif // H_TEXTURE_LOADER_H
