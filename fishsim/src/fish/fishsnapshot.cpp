#include "fish/fishsnapshot.h"

namespace fishsim
{

FishSnapshot::FishSnapshot(){

}


FishSnapshot::~FishSnapshot(){

}

void FishSnapshot::setTime(ros::Time t){

    m_time = t;
}

void FishSnapshot::setName(const std::string& name){
    m_modelName = name;
}


void FishSnapshot::setPos(const irr::core::vector3df &pos){

    m_pos = pos;

}

void FishSnapshot::setRot(const irr::core::vector3df &rot){

    m_rot = rot;
}

void FishSnapshot::setScale(const irr::core::vector3df &scale){

    m_scale = scale;
}

void FishSnapshot::setSingleBoneRot(const std::string &boneName, const irr::core::vector3df &boneRot){

    m_boneRot[boneName] = boneRot;
}

const std::map<std::string, irr::core::vector3df> FishSnapshot::getBoneRotationMap() const{

    return m_boneRot;
}


const irr::core::vector3df &FishSnapshot::getSingleBoneRot(const std::string& boneName) const{

    try
    {
        return m_boneRot.at(boneName);
    }
    catch (const std::out_of_range& oor)
    {
        return irr::core::vector3df(0.0,0.0,0.0);
    }
}

ros::Time FishSnapshot::getTime(){

    return m_time;
}

const std::string& FishSnapshot::getName() const{

    return m_modelName;
}



 const irr::core::vector3df& FishSnapshot::getPos() const{

    return m_pos;
}
const irr::core::vector3df& FishSnapshot::getRot() const{

    return m_rot;
}

const irr::core::vector3df& FishSnapshot::getScale() const{

    return m_scale;
}

fishsim::FishSnapshotStamped FishSnapshot::toRosSnapshot() const{

    fishsim::FishSnapshotStamped snapshot;

    snapshot.modelName = m_modelName;

    snapshot.position.x = m_pos.X;
    snapshot.position.y = m_pos.Y;
    snapshot.position.z = m_pos.Z;

    snapshot.rotation.x = m_rot.X;
    snapshot.rotation.y = m_rot.Y;
    snapshot.rotation.z = m_rot.Z;

    for (auto const& irrBone : m_boneRot)
    {

        fishsim::BoneState rosBone;

        rosBone.boneName = irrBone.first;
        rosBone.boneRotation.x = irrBone.second.X;
        rosBone.boneRotation.y = irrBone.second.Y;
        rosBone.boneRotation.z = irrBone.second.Z;

        snapshot.bones.push_back(rosBone);

    }

    snapshot.timestamp = m_time;
    return snapshot;

}

void FishSnapshot::fromRosSnapshot(const fishsim::FishSnapshotStampedConstPtr &rosSnapshot){

    m_time = rosSnapshot->timestamp;
    m_modelName = rosSnapshot->modelName;
    m_pos = irr::core::vector3df(rosSnapshot->position.x,
                                 rosSnapshot->position.y,
                                 rosSnapshot->position.z);

    m_rot = irr::core::vector3df(rosSnapshot->rotation.x,
                                 rosSnapshot->rotation.y,
                                 rosSnapshot->rotation.z);

    for(auto bone: rosSnapshot->bones){
        m_boneRot[bone.boneName] = irr::core::vector3df(bone.boneRotation.x,
                                                        bone.boneRotation.y,
                                                        bone.boneRotation.z);
    }
}

const bool FishSnapshot::hasBoneRotationValue(const std::string boneName) const
{
    auto it = m_boneRot.find(boneName);

    if(it != m_boneRot.end())
    {
        return true;
    }

    return false;
}


} // end namespace fishsim
