
#include "include/auto_clip.h"


#include <QWidget>
#include <QCheckBox>
#include <QHBoxLayout>
#include <QVBoxLayout>
#include <QTimeEdit>
#include <QLabel>
#include "ros/ros.h"

#include "include/utils.h"

namespace fishplayer
{
AutoClip::AutoClip(QWidget *parent, float playTime) : Clip(parent, E_CLIP_TYPE::AUTO)
{
    m_playTime = playTime;
    m_mimicEnabled = false;
    m_dorsalFinEnabled = false;
    m_gonopodiumEnabled = false;

    if(m_playTime < 0.0)
        m_playTime = 0.0;

    verify();
    initUI();
}

AutoClip::AutoClip(QWidget *parent, const YAML::Node& node) : Clip(parent, E_CLIP_TYPE::AUTO)
{

    load(node);

    verify();
    initUI();
}

AutoClip::~AutoClip()
{

}

void AutoClip::initUI()
{
    QHBoxLayout *mainLayout = new QHBoxLayout;

    // Type
    QLabel* labelType = new QLabel("AUTO");
    mainLayout->addWidget(labelType);
    labelType->setStyleSheet("font-weight: bold;");
    labelType->setMaximumWidth(LABEL_TYPE_WIDTH);

    // Options
    QHBoxLayout *options = new QHBoxLayout;
    options->setSpacing(0);

    QCheckBox* checkBoxMimic = new QCheckBox("Mimic");
    checkBoxMimic->setChecked(m_mimicEnabled);
    options->addWidget(checkBoxMimic);

    QCheckBox* checkBoxGono = new QCheckBox("Gonopodium");
    checkBoxGono->setChecked(m_gonopodiumEnabled);
    options->addWidget(checkBoxGono);

    QCheckBox* checkBoxDorsal = new QCheckBox("DorsalFin");
    checkBoxDorsal->setChecked(m_dorsalFinEnabled);
    options->addWidget(checkBoxDorsal);
    mainLayout->addLayout(options);

    // Time
    QLineEdit *timeEdit = new QLineEdit( generateTimeString(m_playTime));
    timeEdit->setInputMask("00:00");
    timeEdit->setAlignment(Qt::AlignHCenter);
    timeEdit->setMaximumWidth(LABEL_TIME_WIDTH);
    mainLayout->addWidget(timeEdit);

    setLayout(mainLayout);

    connect(checkBoxMimic, SIGNAL(toggled(bool)), this, SLOT(mimicChanged(bool)));
    connect(checkBoxDorsal, SIGNAL(toggled(bool)), this, SLOT(dorsalFinChanged(bool)));
    connect(checkBoxGono, SIGNAL(toggled(bool)), this, SLOT(gonopodiumChanged(bool)));
    connect(timeEdit, SIGNAL(textChanged(QString)), this, SLOT(playtimeChanged(QString)));
}

//void AutoClip::paintEvent(QPaintEvent * event)
//{

//    QRect rect = event->rect();

////    QPainter painter(this);
////    painter.setRenderHint(QPainter::Antialiasing);
////    painter.setPen(Qt::black);
//////    painter.drawText(rect, Qt::AlignCenter, "Data");
////    painter.drawRect(rect);

////    QPainter p(this);
////    p.setRenderHint(QPainter::Antialiasing);
////    QPainterPath path;
////    path.addRoundedRect(rect, 10, 5);
////    QPen pen(Qt::black, 4);
////    p.setPen(pen);
//////    p.fillPath(path, Qt::red);
////    p.drawPath(path);

//}

QSize AutoClip::sizeHint()
{
    return QSize(430, 300);
}

void AutoClip::mimicChanged(bool enabled)
{
    m_mimicEnabled = enabled;
}

void AutoClip::dorsalFinChanged(bool enabled)
{
    m_dorsalFinEnabled = enabled;
}

void AutoClip::gonopodiumChanged(bool enabled)
{
    m_gonopodiumEnabled = enabled;
}

void AutoClip::playtimeChanged(QString playtimeString){

    float playtime = getTimeFromTimeString(playtimeString);
    m_playTime = playtime;

    // Tell playlist UI to update
    emit(clipHasChanged());
}



void AutoClip::play(RosInterface* roshandler)
{

#ifdef USE_AUTOMOVER
    m_isPlaying = true;
    // Start automover
    roshandler->callEnableAutoMoverSrv(true,
                                       m_mimicEnabled,
                                       m_dorsalFinEnabled,
                                       m_gonopodiumEnabled);

    ros::Time startTime = ros::Time::now();
    ros::Duration playDur = ros::Duration(m_playTime);
    ros::Duration sleepDur = ros::Duration(0.5);

    while( (( ros::Time::now() - startTime) <= playDur) && m_isPlaying)
    {
        sleepDur.sleep();
    }

    // Stop automover
    roshandler->callEnableAutoMoverSrv(false, false ,false, false );
#endif
}

bool AutoClip::verify()
{
    // Always valid
    m_isValid = true;
    return m_isValid;
}

float AutoClip::getDuration()
{
    return m_playTime;
}

void AutoClip::save(YAML::Emitter& emitter)
{
    emitter << YAML::BeginMap;

    emitter << YAML::Key << "type";
    emitter << YAML::Value << m_type;
    emitter << YAML::Key << "playTime";
    emitter << YAML::Value << m_playTime;
    emitter << YAML::Key << "mimic";
    emitter << YAML::Value << m_mimicEnabled;
    emitter << YAML::Key << "dorsalFin";
    emitter << YAML::Value << m_dorsalFinEnabled;
    emitter << YAML::Key << "gonopodium";
    emitter << YAML::Value << m_gonopodiumEnabled;

    emitter << YAML::EndMap;
}

bool AutoClip::load(const YAML::Node& node)
{

    if(! node.IsMap())
        return false;

    // Make sure all data is there
    YAML::Node playTime = node["playTime"];
    if(!playTime)
        return false;

    YAML::Node mimic = node["mimic"];
    if(!mimic)
        return false;

    YAML::Node gonopodium = node["gonopodium"];
    if(!gonopodium)
        return false;

    YAML::Node dorsalFin = node["dorsalFin"];
    if(!dorsalFin)
        return false;

    // Fill object
    m_playTime = playTime.as<float>();
    m_mimicEnabled = mimic.as<bool>();
    m_dorsalFinEnabled = dorsalFin.as<bool>();
    m_gonopodiumEnabled = gonopodium.as<bool>();

    if(m_playTime < 0.0)
        m_playTime = 0.0;

    return true;
}

}
