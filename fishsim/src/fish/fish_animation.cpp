#include "fish/fish_animation.h"

#include <rosbag/query.h>
#include <rosbag/view.h>
#include <rosbag/time_translator.h>
#include <rosbag/player.h>

#include <boost/filesystem.hpp>
#include <boost/foreach.hpp>


namespace fishsim
{

FishAnimation::FishAnimation(){

}

FishAnimation::~FishAnimation(){

    clear();
}

FishSnapshot* FishAnimation::getKeyframe(int index){

    if( index >= 0 && index < m_keyframes.size())
        return m_keyframes.at(index);
    else
        return 0;
}

bool FishAnimation::addKeyframe(FishSnapshot* snapshot){

    if(!snapshot)
    {
        return false;
    }

    // First keyframes is always added
    if(m_keyframes.size() == 0)
    {
        m_keyframes.push_back(snapshot);
        return true;
    }

    // Make sure time always increase.
    FishSnapshot* last = m_keyframes.back();

    if(last->getTime() >= snapshot->getTime())
    {       
        return false;
    }
    else
    {
        m_keyframes.push_back(snapshot);
        return true;
    }

}

bool FishAnimation::overrideKeyframe(int index, FishSnapshot* snapshot)
{
    if(index < 0 || index > m_keyframes.size() -1)
        return false;

    FishSnapshot* toDelete = m_keyframes.at(index);

    if(!toDelete)
        return false;

    snapshot->setTime(toDelete->getTime());

    delete toDelete;

    m_keyframes[index] = snapshot;
    return true;
}

ros::Duration FishAnimation::getDurationBetweenKeyframes(int indexFrom, int indexTo){

    if(m_keyframes.size() < 2)
        return ros::Duration(0.0);

    if(indexFrom < 0 || indexTo > m_keyframes.size() || indexFrom > indexTo)
        return ros::Duration(0.0);

    ros::Time start = m_keyframes.at(indexFrom)->getTime();
    ros::Time end = m_keyframes.at(indexTo)->getTime();

    return end - start;

}

ros::Duration FishAnimation::getAnimationDuration(){

    return getDurationBetweenKeyframes(0, m_keyframes.size() -1);
}



void FishAnimation::clear(){

    // Delete keyframes and clear vector
    for (auto it = m_keyframes.begin(); it != m_keyframes.end(); ++it){
        delete *it;
    }
    m_keyframes.clear();

}

bool FishAnimation::loadFromBag(const std::string &filename){

    try
    {
        std::cout << "load bag" << std::endl;
        rosbag::Bag bag;
        bag.open(filename, rosbag::bagmode::Read);

        std::vector<std::string> topics;
        topics.push_back(std::string("/fishsim/fishSnapshot"));
        rosbag::View view(bag, rosbag::TopicQuery(topics));

        clear();

        BOOST_FOREACH(rosbag::MessageInstance const m, view)
        {
            fishsim::FishSnapshotStampedConstPtr i =
                    m.instantiate<fishsim::FishSnapshotStamped>();

            FishSnapshot* keyframe = new FishSnapshot();
            keyframe->fromRosSnapshot(i);
            addKeyframe(keyframe);

        }
        bag.close();

        return true;
    }
    catch(rosbag::BagException e)
    {
        std::cerr << e.what() <<std::endl;
        return false;
    }
}

bool FishAnimation::writeToBag(const std::string &filename){

    try
    {
        rosbag::Bag bag;

        std::cout << "Save bag: " << filename << std::endl;

        bag.open(filename, rosbag::bagmode::Write);

        for(int i = 0; i < m_keyframes.size(); i++)
        {
            FishSnapshot* snap = m_keyframes.at(i);
            bag.write("/fishsim/fishSnapshot", snap->getTime(), snap->toRosSnapshot());
        }

        bag.close();

        return true;
    }
    catch(rosbag::BagException e)
    {

        std::cerr<< "Error: " << e.what() << std::endl;
        return false;
    }
}

bool FishAnimation::writeToBag(rosbag::Bag* bag)
{
    if(!bag)
        return false;

    try{


        for(int i = 0; i < m_keyframes.size(); i++)
        {
            FishSnapshot* snap = m_keyframes.at(i);
            bag->write("/fishsim/fishSnapshot", snap->getTime(), snap->toRosSnapshot());
        }
        return true;
    }
    catch(rosbag::BagException e)
    {

        std::cerr<< "Error: " << e.what() << std::endl;
        return false;
    }
}

} // end namespace fishsim
