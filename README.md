![FishSim](doc/media/fishsim_anim_toolchain_logo.png "Fish Animation Toolchain")
# FishSim Animation Toolchain
Computer-animated 3D fish stimuli for research in animal behavior. FishSim is a 3D fish stimuli simulator, based on the robot operation system ROS and the Irrlicht game engine. The toolchain provides a selection of programs for creation, animation as well as presentation of 3D virtual fish stimuli in a user-friendly way. It consists of the following components:

(1) **FishSim**: game engine based central operating tool to visualize 3D fish stimuli and their movements.

(2) **FishCreator**: tool for easy creation of variable 3D fish stimuli on the basis of a virtual species model which can be varied in length, height and thickness and equipped with different fin and body textures.

(3) **FishSteering**: tool for sequential animation of any number of prior created 3D fish stimuli using a video game controller. 

(4) **FishPlayer**: tool for easy and standardized presentation of previously created animation sequences on one or two screens.

All software needed to operate or work with FishSim is freely available. Please check the wiki for more informations on what FishSim is possible to do.

# Installation 

Please read the wiki for [installation instructions](https://bitbucket.org/EZLS/fish_animation_toolchain/wiki/Installation).

# Software requirements

- Computer/laptop with the free LINUX operating system Ubuntu (www.ubuntu.com). It is possible to run Ubuntu alongside Windows.
- If you want to include your own fish models you need the free 3D creation program Blender (www.blender.org) and a graphics editing program, like the free GIMP (www.gimp.org) or Adobe Photoshop (www.adobe.com), to create fin and body textures

# Manual
 You can find the current version of the manual [here](https://bitbucket.org/EZLS/fish_animation_toolchain/downloads/Manual_V7.pdf).

![FishSim_preview](doc/media/preview_1.png "FishSim Preview")
